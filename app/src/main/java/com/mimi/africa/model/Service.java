package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Service implements Parcelable {

      private int id;

      @Nullable
      private String name;

      @Nullable
      private String description;

      @Nullable
      private String condition;

      @Nullable
      @SerializedName("price")
      private String price;

      @Nullable
      @SerializedName("max_price")
      private String salePrice;

      @Nullable
      @SerializedName("min_price")
      private String offerPrice;

      @Nullable
      private Image image;

      @Nullable
      private List<Image> images;

      private double rating;

      @Nullable
      private Shop shop;

      @SerializedName("created_at")
      private CreatedAt createdAt;

      @SerializedName("feedbacks")
      private List<Feedback> feedbacks;

      @Ignore
      @SerializedName("feedbacks_sum")
      private int sumFeedbacks;

      private Owner owner;

      @Ignore
      @SerializedName("wishlists")
      private List<Wishlist> wishlists;


      public Service() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(@Nullable String name) {
            this.name = name;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(@Nullable String description) {
            this.description = description;
      }

      @Nullable
      public String getCondition() {
            return condition;
      }

      public void setCondition(@Nullable String condition) {
            this.condition = condition;
      }

      @Nullable
      public String getPrice() {
            return price;
      }

      public void setPrice(@Nullable String price) {
            this.price = price;
      }

      @Nullable
      public String getSalePrice() {
            return salePrice;
      }

      public void setSalePrice(@Nullable String salePrice) {
            this.salePrice = salePrice;
      }

      @Nullable
      public String getOfferPrice() {
            return offerPrice;
      }

      public void setOfferPrice(@Nullable String offerPrice) {
            this.offerPrice = offerPrice;
      }

      @Nullable
      public Image getImage() {
            return image;
      }

      public void setImage(@Nullable Image image) {
            this.image = image;
      }

      @Nullable
      public List<Image> getImages() {
            return images;
      }

      public void setImages(@Nullable List<Image> images) {
            this.images = images;
      }

      public double getRating() {
            return rating;
      }

      public void setRating(double rating) {
            this.rating = rating;
      }

      @Nullable
      public Shop getShop() {
            return shop;
      }

      public void setShop(@Nullable Shop shop) {
            this.shop = shop;
      }

      public CreatedAt getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
      }

      public List<Feedback> getFeedbacks() {
            return feedbacks;
      }

      public void setFeedbacks(List<Feedback> feedbacks) {
            this.feedbacks = feedbacks;
      }

      public int getSumFeedbacks() {
            return sumFeedbacks;
      }

      public void setSumFeedbacks(int sumFeedbacks) {
            this.sumFeedbacks = sumFeedbacks;
      }

      public Owner getOwner() {
            return owner;
      }

      public void setOwner(Owner owner) {
            this.owner = owner;
      }

      public List<Wishlist> getWishlists() {
            return wishlists;
      }

      public void setWishlists(List<Wishlist> wishlists) {
            this.wishlists = wishlists;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeString(this.description);
            dest.writeString(this.condition);
            dest.writeString(this.price);
            dest.writeString(this.salePrice);
            dest.writeString(this.offerPrice);
            dest.writeParcelable(this.image, flags);
            dest.writeTypedList(this.images);
            dest.writeDouble(this.rating);
            dest.writeParcelable(this.shop, flags);
            dest.writeParcelable(this.createdAt, flags);
            dest.writeTypedList(this.feedbacks);
            dest.writeInt(this.sumFeedbacks);
            dest.writeParcelable(this.owner, flags);
            dest.writeTypedList(this.wishlists);
      }

      protected Service(Parcel in) {
            this.id = in.readInt();
            this.name = in.readString();
            this.description = in.readString();
            this.condition = in.readString();
            this.price = in.readString();
            this.salePrice = in.readString();
            this.offerPrice = in.readString();
            this.image = in.readParcelable(Image.class.getClassLoader());
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.rating = in.readDouble();
            this.shop = in.readParcelable(Shop.class.getClassLoader());
            this.createdAt = in.readParcelable(CreatedAt.class.getClassLoader());
            this.feedbacks = in.createTypedArrayList(Feedback.CREATOR);
            this.sumFeedbacks = in.readInt();
            this.owner = in.readParcelable(Owner.class.getClassLoader());
            this.wishlists = in.createTypedArrayList(Wishlist.CREATOR);
      }

      public static final Creator<Service> CREATOR = new Creator<Service>() {
            @Override
            public Service createFromParcel(Parcel source) {
                  return new Service(source);
            }

            @Override
            public Service[] newArray(int size) {
                  return new Service[size];
            }
      };
}