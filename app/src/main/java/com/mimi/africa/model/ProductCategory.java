package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = ProductCategory.TABLE_NAME)
public class ProductCategory implements Parcelable {

      static final String TABLE_NAME = "product_categories";

      private static final String COLUMN_ID   = "id";
      public static final String COLUMN_PRIMARY_ID   = "primary_id";

      public static final String COLUMN_NAME   = "name";

      public static final String COLUMN_SLUG   = "slug";

      public static final String COLUMN_DESCRIPTION  = "description";

      @NonNull
      @PrimaryKey(autoGenerate = true)
      @ColumnInfo(index = true, name = COLUMN_PRIMARY_ID)
      private long primaryId;

      @ColumnInfo(index = true, name = COLUMN_ID)
      private int id;

      @Nullable
      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @Nullable
      @ColumnInfo( name = COLUMN_SLUG)
      private String slug;

      @Nullable
      @ColumnInfo( name = COLUMN_DESCRIPTION)
      private String description;

      private int featured;

      @Nullable
      @SerializedName("banner_image")
      private String bannerImage;

      @Nullable
      @androidx.room.Ignore
      private Image  image;

      @SerializedName("category_sub_group_id")
      private int categorySubGroupId;

      @androidx.room.Ignore
      @Nullable
      private List<Product> products;

      @SerializedName("listings")
      @androidx.room.Ignore
      @Nullable
      private List<Inventory> listings;

      @androidx.room.Ignore
      @SerializedName("sub_group")
      private CategorySubGroups subGroup;

      public ProductCategory() {
      }

      public ProductCategory(int id, String name, String icon) {
            this.id = id;
            this.name = name;
            this.bannerImage = icon;
      }


      public long getPrimaryId() {
            return primaryId;
      }

      public void setPrimaryId(long primaryId) {
            this.primaryId = primaryId;
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(@Nullable String name) {
            this.name = name;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(@Nullable String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(@Nullable String description) {
            this.description = description;
      }

      public int getFeatured() {
            return featured;
      }

      public void setFeatured(int featured) {
            this.featured = featured;
      }

      @Nullable
      public String getBannerImage() {
            return bannerImage;
      }

      public void setBannerImage(@Nullable String bannerImage) {
            this.bannerImage = bannerImage;
      }

      @Nullable
      public Image getImage() {
            return image;
      }

      public void setImage(@Nullable Image image) {
            this.image = image;
      }

      public int getCategorySubGroupId() {
            return categorySubGroupId;
      }

      public void setCategorySubGroupId(int categorySubGroupId) {
            this.categorySubGroupId = categorySubGroupId;
      }

      @Nullable
      public List<Product> getProducts() {
            return products;
      }

      public void setProducts(@Nullable List<Product> products) {
            this.products = products;
      }

      @Nullable
      public List<Inventory> getListings() {
            return listings;
      }

      public void setListings(@Nullable List<Inventory> listings) {
            this.listings = listings;
      }

      public CategorySubGroups getSubGroup() {
            return subGroup;
      }

      public void setSubGroup(CategorySubGroups subGroup) {
            this.subGroup = subGroup;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.primaryId);
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeString(this.slug);
            dest.writeString(this.description);
            dest.writeInt(this.featured);
            dest.writeString(this.bannerImage);
            dest.writeParcelable(this.image, flags);
            dest.writeInt(this.categorySubGroupId);
            dest.writeTypedList(this.products);
            dest.writeTypedList(this.listings);
            dest.writeParcelable(this.subGroup, flags);
      }

      protected ProductCategory(Parcel in) {
            this.primaryId = in.readLong();
            this.id = in.readInt();
            this.name = in.readString();
            this.slug = in.readString();
            this.description = in.readString();
            this.featured = in.readInt();
            this.bannerImage = in.readString();
            this.image = in.readParcelable(Image.class.getClassLoader());
            this.categorySubGroupId = in.readInt();
            this.products = in.createTypedArrayList(Product.CREATOR);
            this.listings = in.createTypedArrayList(Inventory.CREATOR);
            this.subGroup = in.readParcelable(CategorySubGroups.class.getClassLoader());
      }

      public static final Creator<ProductCategory> CREATOR = new Creator<ProductCategory>() {
            @Override
            public ProductCategory createFromParcel(Parcel source) {
                  return new ProductCategory(source);
            }

            @Override
            public ProductCategory[] newArray(int size) {
                  return new ProductCategory[size];
            }
      };
}
