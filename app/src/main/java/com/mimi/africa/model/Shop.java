package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = Shop.TABLE_NAME)

public class Shop implements Parcelable {

      public static final String TABLE_NAME = "shops";

      public static final String COLUMN_ID   = "id";

      public static final String COLUMN_PRIMARY_ID   = "_id";

      public static final String COLUMN_NAME   = "name";

      public static final String COLUMN_EMAIL   = "email";

      public static final String COLUMN_DESCRIPTION  = "description";

      public static final String COLUMN_BANNER_IMAGE  = "banner_image";

      public static final String COLUMN_CURRENT_BILLING_PLAN  = "current_billing_plan"
              ;
      public static final String COLUMN_TRIAL_ENDS_AT  = "trial_ends_at";

      public static final String COLUMN_ACTIVE= "active";

      public static final String COLUMN_ADDRESS= "address";

      public static final String COLUMN_LAT= "lat";

      public static final String COLUMN_LNG= "lng";

      private static final String COLUMN_SLUG = "slug";
      private static final String COLUMN_RATING = "rating";


      @NonNull
      @PrimaryKey(autoGenerate = true)
      @ColumnInfo(index = true, name = COLUMN_PRIMARY_ID)
      private long primaryId;

      @ColumnInfo( name = COLUMN_ID)
      private int id;

      private int ownerId;

      @Nullable
      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @Nullable
      @ColumnInfo(name = COLUMN_SLUG)
      private String slug;

      @Nullable
      @ColumnInfo( name = COLUMN_EMAIL)
      private String email;

      @Nullable
      @ColumnInfo(name = COLUMN_DESCRIPTION)
      private String description;

      @Nullable
      @SerializedName("banner_image")
      @ColumnInfo( name = COLUMN_BANNER_IMAGE)
      private String bannerImage;

      @Nullable
      @SerializedName("rating")
      @ColumnInfo( name = COLUMN_RATING)
      private double rating;

      @SerializedName("feedbacks_sum")
      private int sumFeedbacks;

      @SerializedName("shop_type")
      private String shopType;

      @Nullable
     @androidx.room.Ignore
      @SerializedName("image")
      private Image image;

      @Nullable
      @ColumnInfo(name = COLUMN_CURRENT_BILLING_PLAN)
      private String currentBillingPlan;

      @Nullable
      @ColumnInfo(name = COLUMN_TRIAL_ENDS_AT)
      private String trialEndsAt;

      @ColumnInfo(name = COLUMN_ACTIVE)
      private boolean active;

      @ColumnInfo(name = COLUMN_ADDRESS)
      private String address;

      @ColumnInfo(name = COLUMN_LAT)
      private String lat;

      @ColumnInfo(name = COLUMN_LNG)
      private String lng;

      @Nullable
      @ androidx.room.Ignore
      private ShopConfig config;

      @Nullable
      @ androidx.room.Ignore
      private List<Feedback> feedbacks;

      @Nullable
      @ androidx.room.Ignore
      @SerializedName("addresses")
    private List<Address> addresses;

      @Nullable
      @ androidx.room.Ignore
      @SerializedName("images")
      private List<Image> images;

      @Nullable
      @ androidx.room.Ignore
      @SerializedName("inventories")
      private List<Inventory> inventories;

      @Nullable
      @ androidx.room.Ignore
      @SerializedName("owner")
      private Owner owner;

      @Nullable
      @SerializedName("external_url")
      private String externalUrl;

      public Shop() {
      }

      public long getPrimaryId() {
            return primaryId;
      }

      public void setPrimaryId(long primaryId) {
            this.primaryId = primaryId;
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public int getOwnerId() {
            return ownerId;
      }

      public void setOwnerId(int ownerId) {
            this.ownerId = ownerId;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(@Nullable String name) {
            this.name = name;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(@Nullable String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getEmail() {
            return email;
      }

      public void setEmail(@Nullable String email) {
            this.email = email;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(@Nullable String description) {
            this.description = description;
      }

      @Nullable
      public String getBannerImage() {
            return bannerImage;
      }

      public void setBannerImage(@Nullable String bannerImage) {
            this.bannerImage = bannerImage;
      }

      public double getRating() {
            return rating;
      }

      public void setRating(double rating) {
            this.rating = rating;
      }

      public int getSumFeedbacks() {
            return sumFeedbacks;
      }

      public void setSumFeedbacks(int sumFeedbacks) {
            this.sumFeedbacks = sumFeedbacks;
      }

      public String getShopType() {
            return shopType;
      }

      public void setShopType(String shopType) {
            this.shopType = shopType;
      }

      @Nullable
      public Image getImage() {
            return image;
      }

      public void setImage(@Nullable Image image) {
            this.image = image;
      }

      @Nullable
      public String getCurrentBillingPlan() {
            return currentBillingPlan;
      }

      public void setCurrentBillingPlan(@Nullable String currentBillingPlan) {
            this.currentBillingPlan = currentBillingPlan;
      }

      @Nullable
      public String getTrialEndsAt() {
            return trialEndsAt;
      }

      public void setTrialEndsAt(@Nullable String trialEndsAt) {
            this.trialEndsAt = trialEndsAt;
      }

      public boolean isActive() {
            return active;
      }

      public void setActive(boolean active) {
            this.active = active;
      }

      public String getAddress() {
            return address;
      }

      public void setAddress(String address) {
            this.address = address;
      }

      public String getLat() {
            return lat;
      }

      public void setLat(String lat) {
            this.lat = lat;
      }

      public String getLng() {
            return lng;
      }

      public void setLng(String lng) {
            this.lng = lng;
      }

      @Nullable
      public ShopConfig getConfig() {
            return config;
      }

      public void setConfig(@Nullable ShopConfig config) {
            this.config = config;
      }

      @Nullable
      public List<Feedback> getFeedbacks() {
            return feedbacks;
      }

      public void setFeedbacks(@Nullable List<Feedback> feedbacks) {
            this.feedbacks = feedbacks;
      }

      @Nullable
      public List<Address> getAddresses() {
            return addresses;
      }

      public void setAddresses(@Nullable List<Address> addresses) {
            this.addresses = addresses;
      }

      @Nullable
      public List<Image> getImages() {
            return images;
      }

      public void setImages(@Nullable List<Image> images) {
            this.images = images;
      }

      @Nullable
      public List<Inventory> getInventories() {
            return inventories;
      }

      public void setInventories(@Nullable List<Inventory> inventories) {
            this.inventories = inventories;
      }

      @Nullable
      public Owner getOwner() {
            return owner;
      }

      public void setOwner(@Nullable Owner owner) {
            this.owner = owner;
      }

      @Nullable
      public String getExternalUrl() {
            return externalUrl;
      }

      public void setExternalUrl(@Nullable String externalUrl) {
            this.externalUrl = externalUrl;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.primaryId);
            dest.writeInt(this.id);
            dest.writeInt(this.ownerId);
            dest.writeString(this.name);
            dest.writeString(this.slug);
            dest.writeString(this.email);
            dest.writeString(this.description);
            dest.writeString(this.bannerImage);
            dest.writeDouble(this.rating);
            dest.writeInt(this.sumFeedbacks);
            dest.writeString(this.shopType);
            dest.writeParcelable(this.image, flags);
            dest.writeString(this.currentBillingPlan);
            dest.writeString(this.trialEndsAt);
            dest.writeByte(this.active ? (byte) 1 : (byte) 0);
            dest.writeString(this.address);
            dest.writeString(this.lat);
            dest.writeString(this.lng);
            dest.writeParcelable(this.config, flags);
            dest.writeTypedList(this.feedbacks);
            dest.writeTypedList(this.addresses);
            dest.writeTypedList(this.images);
            dest.writeTypedList(this.inventories);
            dest.writeParcelable(this.owner, flags);
            dest.writeString(this.externalUrl);
      }

      protected Shop(Parcel in) {
            this.primaryId = in.readLong();
            this.id = in.readInt();
            this.ownerId = in.readInt();
            this.name = in.readString();
            this.slug = in.readString();
            this.email = in.readString();
            this.description = in.readString();
            this.bannerImage = in.readString();
            this.rating = in.readDouble();
            this.sumFeedbacks = in.readInt();
            this.shopType = in.readString();
            this.image = in.readParcelable(Image.class.getClassLoader());
            this.currentBillingPlan = in.readString();
            this.trialEndsAt = in.readString();
            this.active = in.readByte() != 0;
            this.address = in.readString();
            this.lat = in.readString();
            this.lng = in.readString();
            this.config = in.readParcelable(ShopConfig.class.getClassLoader());
            this.feedbacks = in.createTypedArrayList(Feedback.CREATOR);
            this.addresses = in.createTypedArrayList(Address.CREATOR);
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.inventories = in.createTypedArrayList(Inventory.CREATOR);
            this.owner = in.readParcelable(Owner.class.getClassLoader());
            this.externalUrl = in.readString();
      }

      public static final Creator<Shop> CREATOR = new Creator<Shop>() {
            @Override
            public Shop createFromParcel(Parcel source) {
                  return new Shop(source);
            }

            @Override
            public Shop[] newArray(int size) {
                  return new Shop[size];
            }
      };
}