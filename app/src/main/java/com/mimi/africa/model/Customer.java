package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Customer implements Parcelable {

      private int id;

      @Nullable
      private String name;

      @Nullable
      @SerializedName("nice_name")
      private String niceName;

      @Nullable
      @SerializedName("email")
      private String email;

      public Customer() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(@Nullable String name) {
            this.name = name;
      }

      @Nullable
      public String getNiceName() {
            return niceName;
      }

      public void setNiceName(@Nullable String niceName) {
            this.niceName = niceName;
      }

      @Nullable
      public String getEmail() {
            return email;
      }

      public void setEmail(@Nullable String email) {
            this.email = email;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeString(this.niceName);
            dest.writeString(this.email);
      }

      protected Customer(Parcel in) {
            this.id = in.readInt();
            this.name = in.readString();
            this.niceName = in.readString();
            this.email = in.readString();
      }

      public static final Parcelable.Creator<Customer> CREATOR = new Parcelable.Creator<Customer>() {
            @Override
            public Customer createFromParcel(Parcel source) {
                  return new Customer(source);
            }

            @Override
            public Customer[] newArray(int size) {
                  return new Customer[size];
            }
      };
}
