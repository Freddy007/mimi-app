package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderItem implements Parcelable {

      @SerializedName("cart_id")
      private int cartId;

      @SerializedName("unit_price")
      private String unitPrice;

      @SerializedName("quantity")
      private String quantity;

      @SerializedName("item_description")
      private String itemDescription;

      @SerializedName("created_at")
      private String createdAt;

      public int getCartId() {
            return cartId;
      }

      public void setCartId(int cartId) {
            this.cartId = cartId;
      }

      public String getUnitPrice() {
            return unitPrice;
      }

      public void setUnitPrice(String unitPrice) {
            this.unitPrice = unitPrice;
      }

      public String getQuantity() {
            return quantity;
      }

      public void setQuantity(String quantity) {
            this.quantity = quantity;
      }

      public String getItemDescription() {
            return itemDescription;
      }

      public void setItemDescription(String itemDescription) {
            this.itemDescription = itemDescription;
      }

      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.cartId);
            dest.writeString(this.unitPrice);
            dest.writeString(this.quantity);
            dest.writeString(this.itemDescription);
            dest.writeString(this.createdAt);
      }

      public OrderItem() {
      }

      protected OrderItem(Parcel in) {
            this.cartId = in.readInt();
            this.unitPrice = in.readString();
            this.quantity = in.readString();
            this.itemDescription = in.readString();
            this.createdAt = in.readString();
      }

      public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
            @Override
            public OrderItem createFromParcel(Parcel source) {
                  return new OrderItem(source);
            }

            @Override
            public OrderItem[] newArray(int size) {
                  return new OrderItem[size];
            }
      };
}
