package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Wishlist implements Parcelable {

      private int id;

      @SerializedName("customer_id")
      private String customerId;

      @SerializedName("inventory_id")
      private String inventoryId;

      @SerializedName("product_id")
      private String productId;


      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getCustomerId() {
            return customerId;
      }

      public void setCustomerId(String customerId) {
            this.customerId = customerId;
      }

      public String getInventoryId() {
            return inventoryId;
      }

      public void setInventoryId(String inventoryId) {
            this.inventoryId = inventoryId;
      }

      public String getProductId() {
            return productId;
      }

      public void setProductId(String productId) {
            this.productId = productId;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.customerId);
            dest.writeString(this.inventoryId);
            dest.writeString(this.productId);
      }

      public Wishlist() {
      }

      protected Wishlist(Parcel in) {
            this.id = in.readInt();
            this.customerId = in.readString();
            this.inventoryId = in.readString();
            this.productId = in.readString();
      }

      public static final Creator<Wishlist> CREATOR = new Creator<Wishlist>() {
            @Override
            public Wishlist createFromParcel(Parcel source) {
                  return new Wishlist(source);
            }

            @Override
            public Wishlist[] newArray(int size) {
                  return new Wishlist[size];
            }
      };
}
