package com.mimi.africa.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = BasketCount.TABLE_NAME )
public class BasketCount {
      static final String TABLE_NAME = "basket_counts";

      @NonNull
      @PrimaryKey(autoGenerate = true)
      @ColumnInfo(index = true, name = "id")
      private long primaryId;

      private int count;

      public long getPrimaryId() {
            return primaryId;
      }

      public void setPrimaryId(long primaryId) {
            this.primaryId = primaryId;
      }

      public int getCount() {
            return count;
      }

      public void setCount(int count) {
            this.count = count;
      }
}
