package com.mimi.africa.model;

public class SliderImage {

      private int id;

      private String name;

      private String path;

      private String extension;

      private int order;

      private int featured;


      public SliderImage(int id, String name, String path, String extension, int order, int featured) {
            this.id = id;
            this.name = name;
            this.path = path;
            this.extension = extension;
            this.order = order;
            this.featured = featured;
      }

      public SliderImage() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getPath() {
            return path;
      }

      public void setPath(String path) {
            this.path = path;
      }

      public String getExtension() {
            return extension;
      }

      public void setExtension(String extension) {
            this.extension = extension;
      }

      public int getOrder() {
            return order;
      }

      public void setOrder(int order) {
            this.order = order;
      }

      public int getFeatured() {
            return featured;
      }

      public void setFeatured(int featured) {
            this.featured = featured;
      }
}
