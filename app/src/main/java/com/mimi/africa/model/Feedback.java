package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Feedback implements Parcelable {
      private int id;

      @SerializedName("customer_id")
      private int customerId;

      private double rating;

      @Nullable
      private String comment;

      @SerializedName("feedbackable_id")
      private int feedbackableId;

      @Nullable
      @SerializedName("feedbackable_type")
      private String feedbackableType;

      @SerializedName("approved")
      private int approved;

      @Nullable
      @SerializedName("created_at")
      private CreatedAt createdAt;

      @Nullable
      private Customer customer;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public int getCustomerId() {
            return customerId;
      }

      public void setCustomerId(int customerId) {
            this.customerId = customerId;
      }

      public double getRating() {
            return rating;
      }

      public void setRating(double rating) {
            this.rating = rating;
      }

      @Nullable
      public String getComment() {
            return comment;
      }

      public void setComment(@Nullable String comment) {
            this.comment = comment;
      }

      public int getFeedbackableId() {
            return feedbackableId;
      }

      public void setFeedbackableId(int feedbackableId) {
            this.feedbackableId = feedbackableId;
      }

      @Nullable
      public String getFeedbackableType() {
            return feedbackableType;
      }

      public void setFeedbackableType(@Nullable String feedbackableType) {
            this.feedbackableType = feedbackableType;
      }

      public int getApproved() {
            return approved;
      }

      public void setApproved(int approved) {
            this.approved = approved;
      }

      @Nullable
      public CreatedAt getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(@Nullable CreatedAt createdAt) {
            this.createdAt = createdAt;
      }

      @Nullable
      public Customer getCustomer() {
            return customer;
      }

      public void setCustomer(@Nullable Customer customer) {
            this.customer = customer;
      }

      public static Creator<Feedback> getCREATOR() {
            return CREATOR;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.customerId);
            dest.writeDouble(this.rating);
            dest.writeString(this.comment);
            dest.writeInt(this.feedbackableId);
            dest.writeString(this.feedbackableType);
            dest.writeInt(this.approved);
            dest.writeParcelable(this.createdAt, flags);
            dest.writeParcelable(this.customer, flags);
      }

      public Feedback() {
      }

      protected Feedback(Parcel in) {
            this.id = in.readInt();
            this.customerId = in.readInt();
            this.rating = in.readDouble();
            this.comment = in.readString();
            this.feedbackableId = in.readInt();
            this.feedbackableType = in.readString();
            this.approved = in.readInt();
            this.createdAt = in.readParcelable(CreatedAt.class.getClassLoader());
            this.customer = in.readParcelable(Customer.class.getClassLoader());
      }

      public static final Creator<Feedback> CREATOR = new Creator<Feedback>() {
            @Override
            public Feedback createFromParcel(Parcel source) {
                  return new Feedback(source);
            }

            @Override
            public Feedback[] newArray(int size) {
                  return new Feedback[size];
            }
      };
}
