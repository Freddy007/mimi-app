package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.room.Ignore;

import java.util.List;

public class Product implements Parcelable {

      private int id;

      @Nullable
      private String slug;

      @Nullable
      @Ignore
      private List<Inventory> inventories;

      @Nullable
      @Ignore
      private Shop shop;

      @Nullable
      @Ignore
      private List<Image> images;

      @Nullable
      @Ignore
      private List<ProductCategory> categories;

      public Product() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(@Nullable String slug) {
            this.slug = slug;
      }

      @Nullable
      public List<Inventory> getInventories() {
            return inventories;
      }

      public void setInventories(@Nullable List<Inventory> inventories) {
            this.inventories = inventories;
      }

      @Nullable
      public Shop getShop() {
            return shop;
      }

      public void setShop(@Nullable Shop shop) {
            this.shop = shop;
      }

      @Nullable
      public List<Image> getImages() {
            return images;
      }

      public void setImages(@Nullable List<Image> images) {
            this.images = images;
      }

      @Nullable
      public List<ProductCategory> getCategories() {
            return categories;
      }

      public void setCategories(@Nullable List<ProductCategory> categories) {
            this.categories = categories;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.slug);
            dest.writeTypedList(this.inventories);
            dest.writeParcelable(this.shop, flags);
            dest.writeTypedList(this.images);
            dest.writeTypedList(this.categories);
      }

      protected Product(Parcel in) {
            this.id = in.readInt();
            this.slug = in.readString();
            this.inventories = in.createTypedArrayList(Inventory.CREATOR);
            this.shop = in.readParcelable(Shop.class.getClassLoader());
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.categories = in.createTypedArrayList(ProductCategory.CREATOR);
      }

      public static final Creator<Product> CREATOR = new Creator<Product>() {
            @Override
            public Product createFromParcel(Parcel source) {
                  return new Product(source);
            }

            @Override
            public Product[] newArray(int size) {
                  return new Product[size];
            }
      };
}