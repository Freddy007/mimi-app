package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class OtherVariant implements Parcelable {

      @SerializedName("image_variant")
      private String imageVariant;

      @SerializedName("size_variant")
      private String sizeVariant;

      @SerializedName("color_variant")
      private String colorVariant;


      public String getImageVariant() {
            return imageVariant;
      }

      public void setImageVariant(String imageVariant) {
            this.imageVariant = imageVariant;
      }

      public String getSizeVariant() {
            return sizeVariant;
      }

      public void setSizeVariant(String sizeVariant) {
            this.sizeVariant = sizeVariant;
      }

      public String getColorVariant() {
            return colorVariant;
      }

      public void setColorVariant(String colorVariant) {
            this.colorVariant = colorVariant;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.imageVariant);
            dest.writeString(this.sizeVariant);
            dest.writeString(this.colorVariant);
      }

      public OtherVariant() {
      }

      protected OtherVariant(Parcel in) {
            this.imageVariant = in.readString();
            this.sizeVariant = in.readString();
            this.colorVariant = in.readString();
      }

      public static final Parcelable.Creator<OtherVariant> CREATOR = new Parcelable.Creator<OtherVariant>() {
            @Override
            public OtherVariant createFromParcel(Parcel source) {
                  return new OtherVariant(source);
            }

            @Override
            public OtherVariant[] newArray(int size) {
                  return new OtherVariant[size];
            }
      };
}
