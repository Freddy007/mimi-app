package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderBatch implements Parcelable {
      private int id;

      @Nullable
      @SerializedName("order_number")
      private String orderNumber;

      @SerializedName("customer_id")
      private int customerId;

      @SerializedName("delivery_charge")
      private String deliveryCharge;

      @SerializedName("total_with_delivery_charge")
      private String totalDeliveryCharge;

      @SerializedName("order_items")
      private List<OrderItem> orderItems;

      @SerializedName("created_at")
      private String createdAt;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getOrderNumber() {
            return orderNumber;
      }

      public void setOrderNumber(@Nullable String orderNumber) {
            this.orderNumber = orderNumber;
      }

      public int getCustomerId() {
            return customerId;
      }

      public void setCustomerId(int customerId) {
            this.customerId = customerId;
      }

      public String getDeliveryCharge() {
            return deliveryCharge;
      }

      public void setDeliveryCharge(String deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
      }

      public String getTotalDeliveryCharge() {
            return totalDeliveryCharge;
      }

      public void setTotalDeliveryCharge(String totalDeliveryCharge) {
            this.totalDeliveryCharge = totalDeliveryCharge;
      }

      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }

      public List<OrderItem> getOrderItems() {
            return orderItems;
      }

      public void setOrderItems(List<OrderItem> orderItems) {
            this.orderItems = orderItems;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.orderNumber);
            dest.writeInt(this.customerId);
            dest.writeString(this.deliveryCharge);
            dest.writeString(this.totalDeliveryCharge);
            dest.writeString(this.createdAt);
            dest.writeTypedList(this.orderItems);
      }

      public OrderBatch() {
      }

      protected OrderBatch(Parcel in) {
            this.id = in.readInt();
            this.orderNumber = in.readString();
            this.customerId = in.readInt();
            this.deliveryCharge = in.readString();
            this.totalDeliveryCharge = in.readString();
            this.createdAt = in.readString();
            this.orderItems = in.createTypedArrayList(OrderItem.CREATOR);
      }

      public static final Creator<OrderBatch> CREATOR = new Creator<OrderBatch>() {
            @Override
            public OrderBatch createFromParcel(Parcel source) {
                  return new OrderBatch(source);
            }

            @Override
            public OrderBatch[] newArray(int size) {
                  return new OrderBatch[size];
            }
      };
}
