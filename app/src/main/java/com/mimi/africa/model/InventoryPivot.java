package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InventoryPivot implements Parcelable {

      @SerializedName("order_id")
      private int orderId;

      @SerializedName("cart_id")
      private int cartId;

      @SerializedName("inventory_id")
      private int inventoryId;

      @Nullable
      @SerializedName("item_description")
      private String ItemDescription;

      private int quantity;

      @Nullable
      @SerializedName("unit_price")
      private String unitPrice;

      @Nullable
      @SerializedName("created_at")
      private String createdAt;

      @Nullable
      @SerializedName("other")
      private String otherVariants;

      public int getOrderId() {
            return orderId;
      }

      public void setOrderId(int orderId) {
            this.orderId = orderId;
      }

      public int getCartId() {
            return cartId;
      }

      public void setCartId(int cartId) {
            this.cartId = cartId;
      }

      public int getInventoryId() {
            return inventoryId;
      }

      public void setInventoryId(int inventoryId) {
            this.inventoryId = inventoryId;
      }

      @Nullable
      public String getItemDescription() {
            return ItemDescription;
      }

      public void setItemDescription(@Nullable String itemDescription) {
            ItemDescription = itemDescription;
      }

      public int getQuantity() {
            return quantity;
      }

      public void setQuantity(int quantity) {
            this.quantity = quantity;
      }

      @Nullable
      public String getUnitPrice() {
            return unitPrice;
      }

      public void setUnitPrice(@Nullable String unitPrice) {
            this.unitPrice = unitPrice;
      }

      @Nullable
      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(@Nullable String createdAt) {
            this.createdAt = createdAt;
      }

      @Nullable
      public String getOtherVariants() {
            return otherVariants;
      }

      public void setOtherVariants(@Nullable String otherVariants) {
            this.otherVariants = otherVariants;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.orderId);
            dest.writeInt(this.cartId);
            dest.writeInt(this.inventoryId);
            dest.writeString(this.ItemDescription);
            dest.writeInt(this.quantity);
            dest.writeString(this.unitPrice);
            dest.writeString(this.createdAt);
            dest.writeString(this.otherVariants);
      }

      public InventoryPivot() {
      }

      protected InventoryPivot(Parcel in) {
            this.orderId = in.readInt();
            this.cartId = in.readInt();
            this.inventoryId = in.readInt();
            this.ItemDescription = in.readString();
            this.quantity = in.readInt();
            this.unitPrice = in.readString();
            this.createdAt = in.readString();
            this.otherVariants = in.readString();
      }

      public static final Creator<InventoryPivot> CREATOR = new Creator<InventoryPivot>() {
            @Override
            public InventoryPivot createFromParcel(Parcel source) {
                  return new InventoryPivot(source);
            }

            @Override
            public InventoryPivot[] newArray(int size) {
                  return new InventoryPivot[size];
            }
      };
}
