package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class CategorySubGroups implements Parcelable {

      private int id;

      @Nullable
      @SerializedName("category_group_id")
      private String categoryGroupId;

      @Nullable
      private String name;

      @Nullable
      private String slug;

      @Nullable
      private String description;

      private CategoryGroup group;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getCategoryGroupId() {
            return categoryGroupId;
      }

      public void setCategoryGroupId(@Nullable String categoryGroupId) {
            this.categoryGroupId = categoryGroupId;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(@Nullable String name) {
            this.name = name;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(@Nullable String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(@Nullable String description) {
            this.description = description;
      }

      public CategoryGroup getGroup() {
            return group;
      }

      public void setGroup(CategoryGroup group) {
            this.group = group;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.categoryGroupId);
            dest.writeString(this.name);
            dest.writeString(this.slug);
            dest.writeString(this.description);
            dest.writeParcelable(this.group, flags);
      }

      public CategorySubGroups() {
      }

      protected CategorySubGroups(Parcel in) {
            this.id = in.readInt();
            this.categoryGroupId = in.readString();
            this.name = in.readString();
            this.slug = in.readString();
            this.description = in.readString();
            this.group = in.readParcelable(CategoryGroup.class.getClassLoader());
      }

      public static final Creator<CategorySubGroups> CREATOR = new Creator<CategorySubGroups>() {
            @Override
            public CategorySubGroups createFromParcel(Parcel source) {
                  return new CategorySubGroups(source);
            }

            @Override
            public CategorySubGroups[] newArray(int size) {
                  return new CategorySubGroups[size];
            }
      };
}
