package com.mimi.africa.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName =  Notification.TABLE_NAME)
public class Notification {

      public static final String TABLE_NAME = "notifications";

      public static final String COLUMN_ID = "id";
      public static final String COLUMN_NAME= "name";
      public static final String COLUMN_MESSAGE = "message";
      public static final String COLUMN_OWNER = "owner";
      public static final String COLUMN_READ = "read";
      public static final String COLUMN_HAS_IMAGE = "has_image";
      public static final String COLUMN_IMAGE = "image";
      public static final String COLUMN_HAS_LINK = "has_link";
      public static final String COLUMN_LINK = "link";
      public static final String COLUMN_DATE_CREATED = "date_created";
      public static final String NOTIFICATION_POSTS ="posts" ;
      public static final String NOTIFICATION_CHATS = "chats";
      private static final String COLUMN_SENDER = "sender";
      private static final String COLUMN_SENDER_ID = "sender_id";
      private static final String COLUMN_ARCHIVED = "archived";


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_ID)
      private long id;

      @SerializedName("name")
      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @ColumnInfo( name = COLUMN_MESSAGE)
      private String message;

      @ColumnInfo( name = COLUMN_OWNER)
      private String owner;

      @ColumnInfo( name = COLUMN_SENDER)
      private String sender;

      @ColumnInfo( name = COLUMN_SENDER_ID)
      private String senderId;

      @ColumnInfo( name = COLUMN_IMAGE)
      private String image;

      @ColumnInfo( name = COLUMN_HAS_IMAGE)
      private boolean hasImage;

      @ColumnInfo( name = COLUMN_LINK)
      private String link;

      @ColumnInfo( name = COLUMN_HAS_LINK)
      private boolean hasLink;

      @ColumnInfo( name = COLUMN_READ)
      private boolean read;

      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private long dateCreated;

      @ColumnInfo( name = COLUMN_ARCHIVED)
      private boolean archived;

      public long getId() {
            return id;
      }

      public void setId(long id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public String getOwner() {
            return owner;
      }

      public void setOwner(String owner) {
            this.owner = owner;
      }

      public String getSender() {
            return sender;
      }

      public void setSender(String sender) {
            this.sender = sender;
      }

      public String getSenderId() {
            return senderId;
      }

      public void setSenderId(String senderId) {
            this.senderId = senderId;
      }

      public boolean isRead() {
            return read;
      }

      public void setRead(boolean read) {
            this.read = read;
      }

      public String getImage() {
            return image;
      }

      public void setImage(String image) {
            this.image = image;
      }

      public boolean isHasImage() {
            return hasImage;
      }

      public void setHasImage(boolean hasImage) {
            this.hasImage = hasImage;
      }

      public String getLink() {
            return link;
      }

      public void setLink(String link) {
            this.link = link;
      }

      public boolean isHasLink() {
            return hasLink;
      }

      public void setHasLink(boolean hasLink) {
            this.hasLink = hasLink;
      }

      public long getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(long dateCreated) {
            this.dateCreated = dateCreated;
      }

      public boolean isArchived() {
            return archived;
      }

      public void setArchived(boolean archived) {
            this.archived = archived;
      }
}
