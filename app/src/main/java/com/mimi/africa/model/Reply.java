package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Reply implements Parcelable {

      private int id;

      private String reply;

      @SerializedName("customer_id")
      private int customerId;

      private int read;

      @SerializedName("attachments")
      private List<Image> images;

      @SerializedName("created_at")
      private String createdAt;

      public Reply() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getReply() {
            return reply;
      }

      public void setReply(String reply) {
            this.reply = reply;
      }

      public int getCustomerId() {
            return customerId;
      }

      public void setCustomerId(int customerId) {
            this.customerId = customerId;
      }

      public int getRead() {
            return read;
      }

      public void setRead(int read) {
            this.read = read;
      }

      public List<Image> getImages() {
            return images;
      }

      public void setImages(List<Image> images) {
            this.images = images;
      }

      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.reply);
            dest.writeInt(this.customerId);
            dest.writeInt(this.read);
            dest.writeTypedList(this.images);
            dest.writeString(this.createdAt);
      }

      protected Reply(Parcel in) {
            this.id = in.readInt();
            this.reply = in.readString();
            this.customerId = in.readInt();
            this.read = in.readInt();
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.createdAt = in.readString();
      }

      public static final Creator<Reply> CREATOR = new Creator<Reply>() {
            @Override
            public Reply createFromParcel(Parcel source) {
                  return new Reply(source);
            }

            @Override
            public Reply[] newArray(int size) {
                  return new Reply[size];
            }
      };
}
