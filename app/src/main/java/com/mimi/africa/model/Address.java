package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Address implements Parcelable {

      private int id;

      @SerializedName("address_type")
      private String addressType;

      private String city;

      private String state;

      private String phone;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getAddressType() {
            return addressType;
      }

      public void setAddressType(String addressType) {
            this.addressType = addressType;
      }

      public String getCity() {
            return city;
      }

      public void setCity(String city) {
            this.city = city;
      }

      public String getState() {
            return state;
      }

      public void setState(String state) {
            this.state = state;
      }

      public String getPhone() {
            return phone;
      }

      public void setPhone(String phone) {
            this.phone = phone;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.addressType);
            dest.writeString(this.city);
            dest.writeString(this.state);
            dest.writeString(this.phone);
      }

      public Address() {
      }

      protected Address(Parcel in) {
            this.id = in.readInt();
            this.addressType = in.readString();
            this.city = in.readString();
            this.state = in.readString();
            this.phone = in.readString();
      }

      public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
            @Override
            public Address createFromParcel(Parcel source) {
                  return new Address(source);
            }

            @Override
            public Address[] newArray(int size) {
                  return new Address[size];
            }
      };
}
