package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cart implements Parcelable {
      private int id;

      @Nullable
      @SerializedName("order_number")
      private String orderNumber;

      @SerializedName("shop_id")
      private int shopId;

      @SerializedName("customer_id")
      private int customerId;

      @Nullable
      private String total;

      @Nullable
      private String discount;

      @Nullable
      private String shipping;

      @Nullable
      @SerializedName("grand_total")
      private String grandTotal;

      @Nullable
      @SerializedName("billing_address")
      private String billingAddress;

      @Nullable
      @SerializedName("shipping_address")
      private String shippingAddress;

      @Nullable
      @SerializedName("delivery_date")
      private String deliveryDate;

      @SerializedName("payment_status")
      private int paymentStatus;

      @SerializedName("payment_method_id")
      private int paymentMethodId;

      @SerializedName("order_status_id")
      private int orderStatusId;

      @Nullable
      private Shop shop;

      @Nullable
      private List<Inventory> items;
      
      @Nullable
      @SerializedName("status")
      private Status status;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getOrderNumber() {
            return orderNumber;
      }

      public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
      }

      public int getShopId() {
            return shopId;
      }

      public void setShopId(int shopId) {
            this.shopId = shopId;
      }

      public int getCustomerId() {
            return customerId;
      }

      public void setCustomerId(int customerId) {
            this.customerId = customerId;
      }

      @Nullable
      public String getTotal() {
            return total;
      }

      public void setTotal(String total) {
            this.total = total;
      }

      @Nullable
      public String getDiscount() {
            return discount;
      }

      public void setDiscount(String discount) {
            this.discount = discount;
      }

      @Nullable
      public String getShipping() {
            return shipping;
      }

      public void setShipping(String shipping) {
            this.shipping = shipping;
      }

      @Nullable
      public String getGrandTotal() {
            return grandTotal;
      }

      public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
      }

      @Nullable
      public String getBillingAddress() {
            return billingAddress;
      }

      public void setBillingAddress(String billingAddress) {
            this.billingAddress = billingAddress;
      }

      @Nullable
      public String getShippingAddress() {
            return shippingAddress;
      }

      public void setShippingAddress(String shippingAddress) {
            this.shippingAddress = shippingAddress;
      }

      @Nullable
      public String getDeliveryDate() {
            return deliveryDate;
      }

      public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
      }

      public int getPaymentStatus() {
            return paymentStatus;
      }

      public void setPaymentStatus(int paymentStatus) {
            this.paymentStatus = paymentStatus;
      }

      public int getPaymentMethodId() {
            return paymentMethodId;
      }

      public void setPaymentMethodId(int paymentMethodId) {
            this.paymentMethodId = paymentMethodId;
      }

      public int getOrderStatusId() {
            return orderStatusId;
      }

      public void setOrderStatusId(int orderStatusId) {
            this.orderStatusId = orderStatusId;
      }

      @Nullable
      public Shop getShop() {
            return shop;
      }

      public void setShop(Shop shop) {
            this.shop = shop;
      }

      @Nullable
      public List<Inventory> getItems() {
            return items;
      }

      public void setItems(List<Inventory> items) {
            this.items = items;
      }

      @Nullable
      public Status getStatus() {
            return status;
      }

      public void setStatus(Status status) {
            this.status = status;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.orderNumber);
            dest.writeInt(this.shopId);
            dest.writeInt(this.customerId);
            dest.writeString(this.total);
            dest.writeString(this.discount);
            dest.writeString(this.shipping);
            dest.writeString(this.grandTotal);
            dest.writeString(this.billingAddress);
            dest.writeString(this.shippingAddress);
            dest.writeString(this.deliveryDate);
            dest.writeInt(this.paymentStatus);
            dest.writeInt(this.paymentMethodId);
            dest.writeInt(this.orderStatusId);
            dest.writeParcelable(this.shop, flags);
            dest.writeTypedList(this.items);
            dest.writeParcelable(this.status, flags);
      }

      public Cart() {
      }

      protected Cart(@NonNull Parcel in) {
            this.id = in.readInt();
            this.orderNumber = in.readString();
            this.shopId = in.readInt();
            this.customerId = in.readInt();
            this.total = in.readString();
            this.discount = in.readString();
            this.shipping = in.readString();
            this.grandTotal = in.readString();
            this.billingAddress = in.readString();
            this.shippingAddress = in.readString();
            this.deliveryDate = in.readString();
            this.paymentStatus = in.readInt();
            this.paymentMethodId = in.readInt();
            this.orderStatusId = in.readInt();
            this.shop = in.readParcelable(Shop.class.getClassLoader());
            this.items = in.createTypedArrayList(Inventory.CREATOR);
            this.status = in.readParcelable(Status.class.getClassLoader());
      }

      public static final Creator<Cart> CREATOR = new Creator<Cart>() {
            @NonNull
            @Override
            public Cart createFromParcel(@NonNull Parcel source) {
                  return new Cart(source);
            }

            @NonNull
            @Override
            public Cart[] newArray(int size) {
                  return new Cart[size];
            }
      };
}
