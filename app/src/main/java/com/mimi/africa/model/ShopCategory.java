package com.mimi.africa.model;

import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;

public class ShopCategory {

    public int image;
    @Nullable
    public Drawable imageDrw;
    public String title;
    public String brief;
    public int image_bg;

    public ShopCategory() {
    }

}
