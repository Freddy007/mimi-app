package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName =  Chat.TABLE_NAME)
public class Chat implements Parcelable {

      static final String TABLE_NAME = "chats";

      private int id;

      private String subject;

      private String message;

      @SerializedName("shop_id")
      private int shopId;

      @SerializedName("created_at")
      private String createdAt;

      @SerializedName("attachments")
      private List<Image> images;

      private List<Reply> replies;

      public Chat() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getSubject() {
            return subject;
      }

      public void setSubject(String subject) {
            this.subject = subject;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public int getShopId() {
            return shopId;
      }

      public void setShopId(int shopId) {
            this.shopId = shopId;
      }

      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }

      public List<Image> getImages() {
            return images;
      }

      public void setImages(List<Image> images) {
            this.images = images;
      }

      public List<Reply> getReplies() {
            return replies;
      }

      public void setReplies(List<Reply> replies) {
            this.replies = replies;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.subject);
            dest.writeString(this.message);
            dest.writeInt(this.shopId);
            dest.writeString(this.createdAt);
            dest.writeTypedList(this.images);
            dest.writeTypedList(this.replies);
      }

      protected Chat(Parcel in) {
            this.id = in.readInt();
            this.subject = in.readString();
            this.message = in.readString();
            this.shopId = in.readInt();
            this.createdAt = in.readString();
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.replies = in.createTypedArrayList(Reply.CREATOR);
      }

      public static final Creator<Chat> CREATOR = new Creator<Chat>() {
            @Override
            public Chat createFromParcel(Parcel source) {
                  return new Chat(source);
            }

            @Override
            public Chat[] newArray(int size) {
                  return new Chat[size];
            }
      };
}
