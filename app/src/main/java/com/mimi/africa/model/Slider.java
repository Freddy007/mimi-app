package com.mimi.africa.model;

import com.google.gson.annotations.SerializedName;

public class Slider {

      private int id;

      private String title;

      @SerializedName("title_color")
      private String titleColor;

      @SerializedName("sub_title")
      private String subTitle;

      private SliderImage image;

      @SerializedName("sub_title_color")
      private String subTitleColor;

      private String link;

      private int order;

      public Slider(int id, String title, String titleColor, String subTitle, SliderImage image, String subTitleColor, String link, int order) {
            this.id = id;
            this.title = title;
            this.titleColor = titleColor;
            this.subTitle = subTitle;
            this.image = image;
            this.subTitleColor = subTitleColor;
            this.link = link;
            this.order = order;
      }

      public Slider() {
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getTitle() {
            return title;
      }

      public void setTitle(String title) {
            this.title = title;
      }

      public String getTitleColor() {
            return titleColor;
      }

      public void setTitleColor(String titleColor) {
            this.titleColor = titleColor;
      }

      public String getSubTitle() {
            return subTitle;
      }

      public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
      }

      public SliderImage getImage() {
            return image;
      }

      public void setImage(SliderImage image) {
            this.image = image;
      }

      public String getSubTitleColor() {
            return subTitleColor;
      }

      public void setSubTitleColor(String subTitleColor) {
            this.subTitleColor = subTitleColor;
      }

      public String getLink() {
            return link;
      }

      public void setLink(String link) {
            this.link = link;
      }

      public int getOrder() {
            return order;
      }

      public void setOrder(int order) {
            this.order = order;
      }
}
