package com.mimi.africa.model;

import com.google.gson.annotations.SerializedName;

public class UserInterest {

      private int id;

      private String interest;

      @SerializedName("interest_places")
      private String interestPlaces;

      @SerializedName("user_id")
      private int userId;

      @SerializedName("created_at")
      private String createdAt;

      @SerializedName("updated_at")
      private String updatedAt;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getInterest() {
            return interest;
      }

      public void setInterest(String interest) {
            this.interest = interest;
      }

      public String getInterestPlaces() {
            return interestPlaces;
      }

      public void setInterestPlaces(String interestPlaces) {
            this.interestPlaces = interestPlaces;
      }

      public int getUserId() {
            return userId;
      }

      public void setUserId(int userId) {
            this.userId = userId;
      }

      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }

      public String getUpdatedAt() {
            return updatedAt;
      }

      public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
      }
}
