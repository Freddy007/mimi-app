package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Delivery implements Parcelable {

      @SerializedName("charge")
      private int charge;

      public Delivery(int charge) {
            this.charge = charge;
      }

      public int getCharge() {
            return charge;
      }

      public void setCharge(int charge) {
            this.charge = charge;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.charge);
      }

      protected Delivery(Parcel in) {
            this.charge = in.readInt();
      }

      public static final Parcelable.Creator<Delivery> CREATOR = new Parcelable.Creator<Delivery>() {
            @Override
            public Delivery createFromParcel(Parcel source) {
                  return new Delivery(source);
            }

            @Override
            public Delivery[] newArray(int size) {
                  return new Delivery[size];
            }
      };
}
