package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Group implements Parcelable {

      private int id;

      @Nullable
      @SerializedName("category_group_id")
      private String categoryGroupId;

      @Nullable
      private String name;

      @Nullable
      private String slug;

      @Nullable
      private String description;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getCategoryGroupId() {
            return categoryGroupId;
      }

      public void setCategoryGroupId(String categoryGroupId) {
            this.categoryGroupId = categoryGroupId;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(String description) {
            this.description = description;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.categoryGroupId);
            dest.writeString(this.name);
            dest.writeString(this.slug);
            dest.writeString(this.description);
      }

      public Group() {
      }

      protected Group(@NonNull Parcel in) {
            this.id = in.readInt();
            this.categoryGroupId = in.readString();
            this.name = in.readString();
            this.slug = in.readString();
            this.description = in.readString();
      }

      public static final Creator<Group> CREATOR = new Creator<Group>() {
            @NonNull
            @Override
            public Group createFromParcel(@NonNull Parcel source) {
                  return new Group(source);
            }

            @NonNull
            @Override
            public Group[] newArray(int size) {
                  return new Group[size];
            }
      };
}
