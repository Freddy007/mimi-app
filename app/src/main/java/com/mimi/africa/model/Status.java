package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Status implements Parcelable {

      private int id;

      @Nullable
      private String name;

      @Nullable
      @SerializedName("label_color")
      private String labelColor;

      private boolean fulfilled;

      @Nullable
      @SerializedName("created_at")
      private String createdAt;


      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      @Nullable
      public String getLabelColor() {
            return labelColor;
      }

      public void setLabelColor(String labelColor) {
            this.labelColor = labelColor;
      }

      public boolean isFulfilled() {
            return fulfilled;
      }

      public void setFulfilled(boolean fulfilled) {
            this.fulfilled = fulfilled;
      }

      @Nullable
      public String getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeString(this.labelColor);
            dest.writeByte(this.fulfilled ? (byte) 1 : (byte) 0);
            dest.writeString(this.createdAt);
      }

      public Status() {
      }

      protected Status(@NonNull Parcel in) {
            this.id = in.readInt();
            this.name = in.readString();
            this.labelColor = in.readString();
            this.fulfilled = in.readByte() != 0;
            this.createdAt = in.readString();
      }

      public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
            @NonNull
            @Override
            public Status createFromParcel(@NonNull Parcel source) {
                  return new Status(source);
            }

            @NonNull
            @Override
            public Status[] newArray(int size) {
                  return new Status[size];
            }
      };
}
