package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CategoryGroup implements Parcelable {

      private int id;

      @Nullable
      private String name;

      @Nullable
      private String slug;

      @Nullable
      private String description;

      @Nullable
      private String icon;

      private int order;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(String description) {
            this.description = description;
      }

      @Nullable
      public String getIcon() {
            return icon;
      }

      public void setIcon(String icon) {
            this.icon = icon;
      }

      public int getOrder() {
            return order;
      }

      public void setOrder(int order) {
            this.order = order;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeString(this.slug);
            dest.writeString(this.description);
            dest.writeString(this.icon);
            dest.writeInt(this.order);
      }

      public CategoryGroup() {
      }

      protected CategoryGroup(@NonNull Parcel in) {
            this.id = in.readInt();
            this.name = in.readString();
            this.slug = in.readString();
            this.description = in.readString();
            this.icon = in.readString();
            this.order = in.readInt();
      }

      public static final Parcelable.Creator<CategoryGroup> CREATOR = new Parcelable.Creator<CategoryGroup>() {
            @NonNull
            @Override
            public CategoryGroup createFromParcel(@NonNull Parcel source) {
                  return new CategoryGroup(source);
            }

            @NonNull
            @Override
            public CategoryGroup[] newArray(int size) {
                  return new CategoryGroup[size];
            }
      };
}
