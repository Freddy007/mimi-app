package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = Inventory.TABLE_NAME)
public class Inventory implements Parcelable {

      static final String TABLE_NAME = "inventories";

      private static final String COLUMN_ID   = "id";

      public static final String COLUMN_PRIMARY_ID   = "primary_id";

      public static final String COLUMN_TITLE   = "title";

      public static final String COLUMN_SLUG   = "slug";

      public static final String COLUMN_DESCRIPTION  = "description";
      private static final String COLUMN_CONDITION = "condition" ;
      private static final String COLUMN_SALE_PRICE = "sale_price";
      private static final String COLUMN_OFFER_PRICE = "offer_price";
      private static final String COLUMN_OFFER_START = "offer_start";
      private static final String COLUMN_OFFER_END = "offer_end" ;

      @NonNull
      @PrimaryKey(autoGenerate = true)
      @ColumnInfo(index = true, name = COLUMN_PRIMARY_ID)
      private long primaryId;

      @ColumnInfo(index = true, name = COLUMN_ID)
      private int id;

      @Nullable
      @ColumnInfo(index = true, name = COLUMN_SLUG)
      private String slug;

      @Nullable
      @ColumnInfo(index = true, name = COLUMN_TITLE)
      private String title;

      @Nullable
      @ColumnInfo(index = true, name = COLUMN_DESCRIPTION)
      private String description;

      @Nullable
      @ColumnInfo(index = true, name = COLUMN_CONDITION)
      private String condition;

      @Nullable
      @SerializedName("sale_price")
      @ColumnInfo(index = true, name = COLUMN_SALE_PRICE)
      private String salePrice;

      @Nullable
      @SerializedName("offer_price")
      @ColumnInfo(index = true, name = COLUMN_OFFER_PRICE)
      private String offerPrice;

      @Nullable
      @SerializedName("offer_start")
      @ColumnInfo(index = true, name = COLUMN_OFFER_START)
      private String offerStart;

      @Nullable
      @SerializedName("offer_end")
      @ColumnInfo(index = true, name = COLUMN_OFFER_END)
      private String offerEnd;

      @SerializedName("free_shipping")
      @ColumnInfo(index = true, name = "free_shipping")
      private boolean freeShipping;

      @SerializedName("has_offer")
      @ColumnInfo(index = true, name = "has_offer")
      private boolean hasOffer;

      @SerializedName("hot_item")
      @ColumnInfo(index = true, name = "hot_item")
      private boolean hotItem;

      @SerializedName("min_order_quantity")
      @ColumnInfo( name = "min_order_quantity")
      private int minOrderQuantity;

      @Nullable
      @Ignore
      @ColumnInfo(index = true, name = "image")
      private Image image;

      @Nullable
      @Ignore
      @ColumnInfo(index = true, name = "images")
      private List<Image> images;

      @ColumnInfo(index = true, name = "rating")
      private double rating;

      @Ignore
      @SerializedName("feedbacks_sum")
      private int sumFeedbacks;

      @ColumnInfo(index = true, name = "soldOut")
      private boolean soldOut;

      @SerializedName("product_unit")
      private  String productUnit;

      @SerializedName("product_measurement")
      private  String productMeasurement;

      @SerializedName("shipping_cost")
      private  String shippingCost;

      @SerializedName("discount_percent")
      private  float discountPercent;

      @SerializedName("discount_value")
      private  float discountValue;

      @Nullable
      @Ignore
      @ColumnInfo(index = true, name = "product")
      private Product product;

      @Nullable
      @Ignore
      @ColumnInfo(index = true, name = "shop")
      private Shop shop;

      @Nullable
      @Ignore
      @ColumnInfo(index = true, name = "attributes")
      private List<Attribute> attributes;

      @Ignore
      @ColumnInfo(index = true, name = "feedbacks")
      private List<Feedback> feedbacks;

      @Ignore
      @ColumnInfo(index = true, name = "stock_quantity")
      @SerializedName("stock_quantity")
      private int stockQuantity;

      @Nullable
      @Ignore
      @SerializedName("created")
      @ColumnInfo(index = true, name = "created")
      private CreatedAt createdAt;

      @Nullable
      @SerializedName("created_at")
      @ColumnInfo(index = true, name = "created_at")
      private String created;

      @Nullable
      @Ignore
      private InventoryPivot pivot;

      @Nullable
      @SerializedName("wishlist")
      @Ignore
      private Wishlist wishlist;

      @Nullable
      @SerializedName("wishlists")
      @Ignore
      private List<Wishlist> wishlists;

      @SerializedName("wishlist_count")
      private int wishlistCount;

      public Inventory() {
      }

      public long getPrimaryId() {
            return primaryId;
      }

      public void setPrimaryId(long primaryId) {
            this.primaryId = primaryId;
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getSlug() {
            return slug;
      }

      public void setSlug(@Nullable String slug) {
            this.slug = slug;
      }

      @Nullable
      public String getTitle() {
            return title;
      }

      public void setTitle(@Nullable String title) {
            this.title = title;
      }

      @Nullable
      public String getDescription() {
            return description;
      }

      public void setDescription(@Nullable String description) {
            this.description = description;
      }

      @Nullable
      public String getCondition() {
            return condition;
      }

      public void setCondition(@Nullable String condition) {
            this.condition = condition;
      }

      @Nullable
      public String getSalePrice() {
            return salePrice;
      }

      public void setSalePrice(@Nullable String salePrice) {
            this.salePrice = salePrice;
      }

      @Nullable
      public String getOfferPrice() {
            return offerPrice;
      }

      public void setOfferPrice(@Nullable String offerPrice) {
            this.offerPrice = offerPrice;
      }

      @Nullable
      public String getOfferStart() {
            return offerStart;
      }

      public void setOfferStart(@Nullable String offerStart) {
            this.offerStart = offerStart;
      }

      @Nullable
      public String getOfferEnd() {
            return offerEnd;
      }

      public void setOfferEnd(@Nullable String offerEnd) {
            this.offerEnd = offerEnd;
      }

      public boolean isFreeShipping() {
            return freeShipping;
      }

      public void setFreeShipping(boolean freeShipping) {
            this.freeShipping = freeShipping;
      }

      public boolean isHasOffer() {
            return hasOffer;
      }

      public void setHasOffer(boolean hasOffer) {
            this.hasOffer = hasOffer;
      }

      public boolean isHotItem() {
            return hotItem;
      }

      public void setHotItem(boolean hotItem) {
            this.hotItem = hotItem;
      }

      public int getMinOrderQuantity() {
            return minOrderQuantity;
      }

      public void setMinOrderQuantity(int minOrderQuantity) {
            this.minOrderQuantity = minOrderQuantity;
      }

      @Nullable
      public Image getImage() {
            return image;
      }

      public void setImage(@Nullable Image image) {
            this.image = image;
      }

      @Nullable
      public List<Image> getImages() {
            return images;
      }

      public void setImages(@Nullable List<Image> images) {
            this.images = images;
      }

      public double getRating() {
            return rating;
      }

      public void setRating(double rating) {
            this.rating = rating;
      }

      public int getSumFeedbacks() {
            return sumFeedbacks;
      }

      public void setSumFeedbacks(int sumFeedbacks) {
            this.sumFeedbacks = sumFeedbacks;
      }

      public boolean isSoldOut() {
            return soldOut;
      }

      public void setSoldOut(boolean soldOut) {
            this.soldOut = soldOut;
      }

      public String getProductUnit() {
            return productUnit;
      }

      public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
      }

      public String getProductMeasurement() {
            return productMeasurement;
      }

      public void setProductMeasurement(String productMeasurement) {
            this.productMeasurement = productMeasurement;
      }

      public String getShippingCost() {
            return shippingCost;
      }

      public void setShippingCost(String shippingCost) {
            this.shippingCost = shippingCost;
      }

      public float getDiscountPercent() {
            return discountPercent;
      }

      public void setDiscountPercent(float discountPercent) {
            this.discountPercent = discountPercent;
      }

      public float getDiscountValue() {
            return discountValue;
      }

      public void setDiscountValue(float discountValue) {
            this.discountValue = discountValue;
      }

      @Nullable
      public Product getProduct() {
            return product;
      }

      public void setProduct(@Nullable Product product) {
            this.product = product;
      }

      @Nullable
      public Shop getShop() {
            return shop;
      }

      public void setShop(@Nullable Shop shop) {
            this.shop = shop;
      }

      @Nullable
      public List<Attribute> getAttributes() {
            return attributes;
      }

      public void setAttributes(@Nullable List<Attribute> attributes) {
            this.attributes = attributes;
      }

      public List<Feedback> getFeedbacks() {
            return feedbacks;
      }

      public void setFeedbacks(List<Feedback> feedbacks) {
            this.feedbacks = feedbacks;
      }

      public int getStockQuantity() {
            return stockQuantity;
      }

      public void setStockQuantity(int stockQuantity) {
            this.stockQuantity = stockQuantity;
      }

      @Nullable
      public CreatedAt getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(@Nullable CreatedAt createdAt) {
            this.createdAt = createdAt;
      }

      @Nullable
      public String getCreated() {
            return created;
      }

      public void setCreated(@Nullable String created) {
            this.created = created;
      }

      @Nullable
      public InventoryPivot getPivot() {
            return pivot;
      }

      public void setPivot(@Nullable InventoryPivot pivot) {
            this.pivot = pivot;
      }

      @Nullable
      public Wishlist getWishlist() {
            return wishlist;
      }

      public void setWishlist(@Nullable Wishlist wishlist) {
            this.wishlist = wishlist;
      }

      @Nullable
      public List<Wishlist> getWishlists() {
            return wishlists;
      }

      public void setWishlists(@Nullable List<Wishlist> wishlists) {
            this.wishlists = wishlists;
      }

      public int getWishlistCount() {
            return wishlistCount;
      }

      public void setWishlistCount(int wishlistCount) {
            this.wishlistCount = wishlistCount;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.primaryId);
            dest.writeInt(this.id);
            dest.writeString(this.slug);
            dest.writeString(this.title);
            dest.writeString(this.description);
            dest.writeString(this.condition);
            dest.writeString(this.salePrice);
            dest.writeString(this.offerPrice);
            dest.writeString(this.offerStart);
            dest.writeString(this.offerEnd);
            dest.writeByte(this.freeShipping ? (byte) 1 : (byte) 0);
            dest.writeByte(this.hasOffer ? (byte) 1 : (byte) 0);
            dest.writeByte(this.hotItem ? (byte) 1 : (byte) 0);
            dest.writeInt(this.minOrderQuantity);
            dest.writeParcelable(this.image, flags);
            dest.writeTypedList(this.images);
            dest.writeDouble(this.rating);
            dest.writeInt(this.sumFeedbacks);
            dest.writeByte(this.soldOut ? (byte) 1 : (byte) 0);
            dest.writeString(this.productUnit);
            dest.writeString(this.productMeasurement);
            dest.writeString(this.shippingCost);
            dest.writeFloat(this.discountPercent);
            dest.writeFloat(this.discountValue);
            dest.writeParcelable(this.product, flags);
            dest.writeParcelable(this.shop, flags);
            dest.writeTypedList(this.attributes);
            dest.writeTypedList(this.feedbacks);
            dest.writeInt(this.stockQuantity);
            dest.writeParcelable(this.createdAt, flags);
            dest.writeString(this.created);
            dest.writeParcelable(this.pivot, flags);
            dest.writeParcelable(this.wishlist, flags);
            dest.writeTypedList(this.wishlists);
            dest.writeInt(this.wishlistCount);
      }

      protected Inventory(Parcel in) {
            this.primaryId = in.readLong();
            this.id = in.readInt();
            this.slug = in.readString();
            this.title = in.readString();
            this.description = in.readString();
            this.condition = in.readString();
            this.salePrice = in.readString();
            this.offerPrice = in.readString();
            this.offerStart = in.readString();
            this.offerEnd = in.readString();
            this.freeShipping = in.readByte() != 0;
            this.hasOffer = in.readByte() != 0;
            this.hotItem = in.readByte() != 0;
            this.minOrderQuantity = in.readInt();
            this.image = in.readParcelable(Image.class.getClassLoader());
            this.images = in.createTypedArrayList(Image.CREATOR);
            this.rating = in.readDouble();
            this.sumFeedbacks = in.readInt();
            this.soldOut = in.readByte() != 0;
            this.productUnit = in.readString();
            this.productMeasurement = in.readString();
            this.shippingCost = in.readString();
            this.discountPercent = in.readFloat();
            this.discountValue = in.readFloat();
            this.product = in.readParcelable(Product.class.getClassLoader());
            this.shop = in.readParcelable(Shop.class.getClassLoader());
            this.attributes = in.createTypedArrayList(Attribute.CREATOR);
            this.feedbacks = in.createTypedArrayList(Feedback.CREATOR);
            this.stockQuantity = in.readInt();
            this.createdAt = in.readParcelable(CreatedAt.class.getClassLoader());
            this.created = in.readString();
            this.pivot = in.readParcelable(InventoryPivot.class.getClassLoader());
            this.wishlist = in.readParcelable(Wishlist.class.getClassLoader());
            this.wishlists = in.createTypedArrayList(Wishlist.CREATOR);
            this.wishlistCount = in.readInt();
      }

      public static final Creator<Inventory> CREATOR = new Creator<Inventory>() {
            @Override
            public Inventory createFromParcel(Parcel source) {
                  return new Inventory(source);
            }

            @Override
            public Inventory[] newArray(int size) {
                  return new Inventory[size];
            }
      };
}
