package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Image implements Parcelable {

    private int id;

    @Nullable
    private String name;

    @Nullable
    public String path;

    @Nullable
    private String extension;

    @Nullable
    private String size;

    private int order;

    private int featured;

    @Nullable
    @SerializedName("product_id")
    private String productId;

    @Nullable
    @SerializedName("created_at")
    private String createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getPath() {
        return path;
    }

    public void setPath(@Nullable String path) {
        this.path = path;
    }

    @Nullable
    public String getExtension() {
        return extension;
    }

    public void setExtension(@Nullable String extension) {
        this.extension = extension;
    }

    @Nullable
    public String getSize() {
        return size;
    }

    public void setSize(@Nullable String size) {
        this.size = size;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    @Nullable
    public String getProductId() {
        return productId;
    }

    public void setProductId(@Nullable String productId) {
        this.productId = productId;
    }

    @Nullable
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(@Nullable String createdAt) {
        this.createdAt = createdAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.path);
        dest.writeString(this.extension);
        dest.writeString(this.size);
        dest.writeInt(this.order);
        dest.writeInt(this.featured);
        dest.writeString(this.productId);
        dest.writeString(this.createdAt);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.path = in.readString();
        this.extension = in.readString();
        this.size = in.readString();
        this.order = in.readInt();
        this.featured = in.readInt();
        this.productId = in.readString();
        this.createdAt = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
