package com.mimi.africa.model;

import com.google.gson.annotations.SerializedName;

public class Rating {

      @SerializedName("average_rating")
      private String averageRating;

      @SerializedName("sum")
      private String sum;

      public Rating(String averageRating) {
            this.averageRating = averageRating;
      }

      public Rating() {
      }

      public String getAverageRating() {
            return averageRating;
      }

      public void setAverageRating(String averageRating) {
            this.averageRating = averageRating;
      }

      public String getSum() {
            return sum;
      }

      public void setSum(String sum) {
            this.sum = sum;
      }
}
