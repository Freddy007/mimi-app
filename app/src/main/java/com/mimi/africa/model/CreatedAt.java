package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class CreatedAt implements Parcelable {
      private int id;

      @Nullable
      private String date;

      @SerializedName("timezone_type")
      private int timezoneType;

      @Nullable
      private String timezone;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      @Nullable
      public String getDate() {
            return date;
      }

      public void setDate(String date) {
            this.date = date;
      }

      public int getTimezoneType() {
            return timezoneType;
      }

      public void setTimezoneType(int timezoneType) {
            this.timezoneType = timezoneType;
      }

      @Nullable
      public String getTimezone() {
            return timezone;
      }

      public void setTimezone(String timezone) {
            this.timezone = timezone;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.date);
            dest.writeInt(this.timezoneType);
            dest.writeString(this.timezone);
      }

      public CreatedAt() {
      }

      protected CreatedAt(@NonNull Parcel in) {
            this.id = in.readInt();
            this.date = in.readString();
            this.timezoneType = in.readInt();
            this.timezone = in.readString();
      }

      public static final Parcelable.Creator<CreatedAt> CREATOR = new Parcelable.Creator<CreatedAt>() {
            @NonNull
            @Override
            public CreatedAt createFromParcel(@NonNull Parcel source) {
                  return new CreatedAt(source);
            }

            @NonNull
            @Override
            public CreatedAt[] newArray(int size) {
                  return new CreatedAt[size];
            }
      };
}
