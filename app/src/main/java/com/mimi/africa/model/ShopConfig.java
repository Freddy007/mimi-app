package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ShopConfig implements Parcelable {

      @SerializedName("shop_id")
      private int shopId;

      @Nullable
      @SerializedName("support_phone")
      private String supportPhone;

      @Nullable
      @SerializedName("support_email")
      private String supportEmail;

      @Nullable
      @SerializedName("maintenance_mode")
      private String maintenanceMode;

      @Nullable
      @SerializedName("return_refund")
      private String returnRefund;

      public ShopConfig() {
      }

      public int getShopId() {
            return shopId;
      }

      public void setShopId(int shopId) {
            this.shopId = shopId;
      }

      @Nullable
      public String getSupportPhone() {
            return supportPhone;
      }

      public void setSupportPhone(@Nullable String supportPhone) {
            this.supportPhone = supportPhone;
      }

      @Nullable
      public String getSupportEmail() {
            return supportEmail;
      }

      public void setSupportEmail(@Nullable String supportEmail) {
            this.supportEmail = supportEmail;
      }

      @Nullable
      public String getMaintenanceMode() {
            return maintenanceMode;
      }

      public void setMaintenanceMode(@Nullable String maintenanceMode) {
            this.maintenanceMode = maintenanceMode;
      }

      @Nullable
      public String getReturnRefund() {
            return returnRefund;
      }

      public void setReturnRefund(@Nullable String returnRefund) {
            this.returnRefund = returnRefund;
      }


      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.shopId);
            dest.writeString(this.supportPhone);
            dest.writeString(this.supportEmail);
            dest.writeString(this.maintenanceMode);
            dest.writeString(this.returnRefund);
      }

      protected ShopConfig(Parcel in) {
            this.shopId = in.readInt();
            this.supportPhone = in.readString();
            this.supportEmail = in.readString();
            this.maintenanceMode = in.readString();
            this.returnRefund = in.readString();
      }

      public static final Creator<ShopConfig> CREATOR = new Creator<ShopConfig>() {
            @Override
            public ShopConfig createFromParcel(Parcel source) {
                  return new ShopConfig(source);
            }

            @Override
            public ShopConfig[] newArray(int size) {
                  return new ShopConfig[size];
            }
      };
}
