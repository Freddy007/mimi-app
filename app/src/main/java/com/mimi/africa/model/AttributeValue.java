package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AttributeValue implements Parcelable {

    private String id;

    @SerializedName("shop_id")
    private String shopId;

    @SerializedName("value")
    private String value;

    @SerializedName("color")
    private String color;

    @SerializedName("order")
    private int order;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.shopId);
        dest.writeString(this.value);
        dest.writeString(this.color);
        dest.writeInt(this.order);
    }

    public AttributeValue() {
    }

    protected AttributeValue(Parcel in) {
        this.id = in.readString();
        this.shopId = in.readString();
        this.value = in.readString();
        this.color = in.readString();
        this.order = in.readInt();
    }

    public static final Parcelable.Creator<AttributeValue> CREATOR = new Parcelable.Creator<AttributeValue>() {
        @Override
        public AttributeValue createFromParcel(Parcel source) {
            return new AttributeValue(source);
        }

        @Override
        public AttributeValue[] newArray(int size) {
            return new AttributeValue[size];
        }
    };
}
