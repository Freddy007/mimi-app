package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PaymentStatus  {

      private int id;

      @SerializedName("status")
      private String status;

      @SerializedName("message")
      private String message;

      private Data data;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getStatus() {
            return status;
      }

      public void setStatus(String status) {
            this.status = status;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public Data getData() {
            return data;
      }

      public void setData(Data data) {
            this.data = data;
      }

      public class Data {
            private  String link;

            public String getLink() {
                  return link;
            }

            public void setLink(String link) {
                  this.link = link;
            }
      }
}


