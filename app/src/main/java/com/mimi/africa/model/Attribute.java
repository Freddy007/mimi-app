package com.mimi.africa.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Attribute implements Parcelable {
      private int id;

      private int order;

      @Nullable
      private String name;

      @Nullable
      private String value;

      @Nullable
      private String color;

      @Nullable
      private String pattern_img;

      @SerializedName("attribute_values")
      private List<AttributeValue> attributeValues;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public int getOrder() {
            return order;
      }

      public void setOrder(int order) {
            this.order = order;
      }

      @Nullable
      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      @Nullable
      public String getValue() {
            return value;
      }

      public void setValue(String value) {
            this.value = value;
      }

      @Nullable
      public String getColor() {
            return color;
      }

      public void setColor(String color) {
            this.color = color;
      }

      @Nullable
      public String getPattern_img() {
            return pattern_img;
      }

      public void setPattern_img(String pattern_img) {
            this.pattern_img = pattern_img;
      }

      public List<AttributeValue> getAttributeValues() {
            return attributeValues;
      }

      public void setAttributeValues(List<AttributeValue> attributeValues) {
            this.attributeValues = attributeValues;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.order);
            dest.writeString(this.name);
            dest.writeString(this.value);
            dest.writeString(this.color);
            dest.writeString(this.pattern_img);
            dest.writeTypedList(this.attributeValues);
      }

      public Attribute() {
      }

      protected Attribute(Parcel in) {
            this.id = in.readInt();
            this.order = in.readInt();
            this.name = in.readString();
            this.value = in.readString();
            this.color = in.readString();
            this.pattern_img = in.readString();
            this.attributeValues = in.createTypedArrayList(AttributeValue.CREATOR);
      }

      public static final Creator<Attribute> CREATOR = new Creator<Attribute>() {
            @Override
            public Attribute createFromParcel(Parcel source) {
                  return new Attribute(source);
            }

            @Override
            public Attribute[] newArray(int size) {
                  return new Attribute[size];
            }
      };
}
