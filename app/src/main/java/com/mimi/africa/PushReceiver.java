package com.mimi.africa;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.Gson;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.di.AppInjector;
import com.mimi.africa.event.NotificationUpdated;
import com.mimi.africa.event.PaymentSuccess;
import com.mimi.africa.event.SendMessageEvent;
import com.mimi.africa.model.Message;
import com.mimi.africa.model.Notification;
import com.mimi.africa.model.Post;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.notifications.NotificationDetailActivity;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PushReceiver extends BroadcastReceiver {

    private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};

    AppExecutors appExecutors;

    @Override
    public void onReceive(Context context, Intent intent) {

        appExecutors = new AppExecutors();

        String notificationTitle = "MyApp";
        String notificationText = "Test notification";

        String type = intent.getStringExtra("type");
        String image = intent.getStringExtra("image");
        String link = intent.getStringExtra("link");
        boolean hasLink = intent.getBooleanExtra("has_link", false);
        boolean hasImage =  intent.getBooleanExtra("has_image", false);

        Gson gson = new Gson();
        Message message;
        message = gson.fromJson(intent.getStringExtra("message"), Message.class);

        Post post = new Post();
        post.setMessage(message);
        post.setImage(image);
        post.setHas_image(hasImage);
        post.setLink(link);
        post.setHas_link(hasLink);
        post.setType(type);

        int notificationlId = 1002;
        String channelName = "locationUpdatesChannel";
        String channelId = "locationUpdatesChannelId";

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean notificationPref = sharedPreferences.getBoolean(Constants.NOTIFICATION_PREF, false);

        if (post.getType().equals("post" )){
            notificationText = intent.getStringExtra("message");

            try {
                NotificationCompat.Style notificationStyle;

                if (post.isHas_image()){
                    Bitmap largeBitmapIcon = processImageUrlToBitmap(post.getImage());
                    notificationStyle = ( new NotificationCompat.BigPictureStyle().bigPicture(largeBitmapIcon));
                }else {
                    notificationStyle = (new NotificationCompat.BigTextStyle().bigText(post.getMessage().getContent()));
                }


                Notification appNotification = new Notification();
                appNotification.setName(post.getMessage().getTitle());
                appNotification.setMessage(post.getMessage().getContent());
                appNotification.setOwner(Notification.NOTIFICATION_POSTS);
                appNotification.setImage(post.getImage());
                appNotification.setHasImage(post.isHas_image());
                appNotification.setHasLink(post.isHas_link());
                appNotification.setLink(post.getLink());
                appNotification.setDateCreated(System.currentTimeMillis());

                appExecutors.getDiskIO().execute(() -> {
//                    MimiDb mimiDb = MimiDb.getInstance(context);
//                    long id = mimiDb.notificationDaoAccess().insert(appNotification);

                    Intent notificationIntent = new Intent(context, NotificationDetailActivity.class);
//                    notificationIntent.putExtra("id", id);
                    notificationIntent.putExtra("title", post.getMessage().getTitle());
                    notificationIntent.putExtra("content", post.getMessage().getContent());
                    notificationIntent.putExtra("has_image", post.isHas_image());
                    notificationIntent.putExtra("image", post.getImage());
                    notificationIntent.putExtra("link", post.getLink());
                    notificationIntent.putExtra("has_link", post.isHas_link());

                    if (notificationPref) {
                        showNotificationForPost(notificationlId, channelId, channelName, context, post.getMessage().getContent(),
                                post.getMessage().getTitle(), notificationStyle, notificationIntent);
                    }
                });

            } catch (Exception e) {
                Log.e("PushReceiverClass", "location Updates: " + e.getMessage());

            }
//        }


        }

        if (post.getType().equals("chat" )){
            notificationText = intent.getStringExtra("message");

            try {
                NotificationCompat.Style notificationStyle;

                String content = String.valueOf(Html.fromHtml(post.getMessage().getContent()));

                if (post.isHas_image()){
                    Bitmap largeBitmapIcon = processImageUrlToBitmap(post.getImage());
                    notificationStyle = ( new NotificationCompat.BigPictureStyle().bigPicture(largeBitmapIcon));
                }else {
                    notificationStyle = (new NotificationCompat.BigTextStyle().bigText(content));
                }

                Notification appNotification = new Notification();
                appNotification.setName(post.getMessage().getTitle());
                appNotification.setMessage(post.getMessage().getContent());
                appNotification.setOwner(Notification.NOTIFICATION_CHATS);
                appNotification.setSender(intent.getStringExtra("shop_name"));
                appNotification.setSenderId(String.valueOf(intent.getIntExtra("shop_id",0)));
                appNotification.setImage(post.getImage());
                appNotification.setHasImage(post.isHas_image());
                appNotification.setHasLink(post.isHas_link());
                appNotification.setLink(post.getLink());
                appNotification.setRead(false);
                appNotification.setArchived(false);
                appNotification.setDateCreated(System.currentTimeMillis());

//                appExecutors.getDiskIO().execute(() -> {
//                    MimiDb mimiDb = MimiDb.getInstance(context);

                    String shopId = String.valueOf(intent.getIntExtra("shop_id",0));

//                    if (mimiDb.notificationDaoAccess().fetchNotificationByShopId(shopId) != null ){
//
//                        Notification notification = mimiDb.notificationDaoAccess().fetchNotificationByShopId(shopId);
//                        notification.setDateCreated(System.currentTimeMillis());
//                        notification.setName(post.getMessage().getTitle());
//                        notification.setMessage(post.getMessage().getContent());
//                        notification.setArchived(false);
//                        notification.setRead(false);
//                        mimiDb.notificationDaoAccess().updateNotification(notification);
//
//                    }else {
//                        mimiDb.notificationDaoAccess().insert(appNotification);
//                    }

                    EventBus.getDefault().post(new NotificationUpdated( appNotification, post, shopId));

                    Intent notificationIntent = new Intent(context, MessageActivity.class);
                    notificationIntent.putExtra("customer_id",  intent.getStringExtra("customer_id"));
                    notificationIntent.putExtra(Constants.SHOP_ID,  intent.getIntExtra("shop_id",0));
                    notificationIntent.putExtra(Constants.SHOP_NAME,  intent.getStringExtra("shop_name"));

                    if (notificationPref) {
                        showNotificationForChat(notificationlId, channelId, channelName, context, content, post.getMessage().getTitle(), notificationStyle, notificationIntent);
                    }

                    EventBus.getDefault().post(new SendMessageEvent());

                    CustomSharedPrefs.saveLastChatTime(context, System.currentTimeMillis());

//                });


            } catch (Exception e) {
                Log.e("PushReceiverClass", "location Updates: " + e.getMessage());

            }
        }

        if (post.getType().equals("payment")){
            EventBus.getDefault().post(new PaymentSuccess());
            Log.i("PushReceiver", "onReceive: received event");
        }

    }

    public static void showNotificationForPost(int notificationId,String channelId,
                                               String channelName,Context context, String body, String title,  NotificationCompat.Style notificationStyle, Intent intent) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int importance = NotificationManager.IMPORTANCE_HIGH;

        Intent snoozeIntent = new Intent(context, MyBroadcastReceiver.class);
        snoozeIntent.setAction("READ MORE");
//        snoozeIntent.putExtra("title", title);
//        snoozeIntent.putExtra("content", body);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(context, 0, snoozeIntent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        androidx.core.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setVibrate(DEFAULT_VIBRATION_PATTERN)
                .setContentText(body)
                .setStyle(notificationStyle)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setNumber(1)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setAutoCancel(true);

        if (intent != null){
//            Intent mainHomeActivity = new Intent(context, MainHomeActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            mBuilder.addAction(R.drawable.ic_more, "READ MORE",
                    resultPendingIntent);

            mBuilder.setContentIntent(resultPendingIntent);
        }

        notificationManager.notify(notificationId, mBuilder.build());
    }

    public static void showNotificationForChat(int notificationId,String channelId,
                                               String channelName,Context context, String body, String title,  NotificationCompat.Style notificationStyle, Intent intent) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int importance = NotificationManager.IMPORTANCE_HIGH;

        Intent snoozeIntent = new Intent(context, MyBroadcastReceiver.class);
        snoozeIntent.setAction("READ MORE");
//        snoozeIntent.putExtra("title", title);
//        snoozeIntent.putExtra("content", body);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(context, 0, snoozeIntent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        androidx.core.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setVibrate(DEFAULT_VIBRATION_PATTERN)
                .setContentText(body)
                .setStyle(notificationStyle)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setNumber(1)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setAutoCancel(true);

        if (intent != null){
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            mBuilder.addAction(R.drawable.ic_more, "READ MORE",
                    resultPendingIntent);

            mBuilder.setContentIntent(resultPendingIntent);
        }

        notificationManager.notify(notificationId, mBuilder.build());
    }

    private static Bitmap processImageUrlToBitmap(String imageUrl){

        InputStream inputStream;

        try{
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            inputStream = connection.getInputStream();
            return BitmapFactory.decodeStream(inputStream);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}