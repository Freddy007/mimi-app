package com.mimi.africa.repository.common;

import androidx.lifecycle.LiveData;

import com.mimi.africa.api.APIService;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.common.Resource;
import com.mimi.africa.utils.AbsentLiveData;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.RateLimiter;
import com.mimi.africa.utils.Utils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public abstract class PSRepository {


    //region Variables

    protected final APIService psApiService;

    protected final AppExecutors appExecutors;
    protected final MimiDb db;
    @Inject
    protected Connectivity connectivity;
    protected RateLimiter<String> rateLimiter = new RateLimiter<>( Config.API_SERVICE_CACHE_LIMIT, TimeUnit.MINUTES);

    //endregion


    //region Constructor

    /**
     * Constructor of PSRepository
     * @param psApiService Panacea-Soft API Service Instance
     * @param appExecutors Executors Instance
     * @param db Panacea-Soft DB
     */
    protected PSRepository(APIService psApiService, AppExecutors appExecutors, MimiDb db) {
        Utils.psLog("Inside NewsRepository");
        this.psApiService = psApiService;
        this.appExecutors = appExecutors;
        this.db = db;
    }

    //endregion

    //region public Methods

    public LiveData<Resource<Boolean>> save(Object obj) {

        if(obj == null) {
            return AbsentLiveData.create();
        }

        SaveTask saveTask = new SaveTask(psApiService, db, obj);
        appExecutors.getDiskIO().execute(saveTask);
        return saveTask.getStatusLiveData();
    }


    public LiveData<Resource<Boolean>> delete(Object obj) {

        if(obj == null) {
            return AbsentLiveData.create();
        }

        DeleteTask deleteTask = new DeleteTask(psApiService, db, obj);
        appExecutors.getDiskIO().execute(deleteTask);
        return deleteTask.getStatusLiveData();
    }
    //endregion

}
