package com.mimi.africa.repository.clearpackage;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.mimi.africa.api.APIService;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.common.Resource;
import com.mimi.africa.repository.common.PSRepository;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

public class ClearPackageRepository extends PSRepository {

    @Inject
    ClearPackageRepository(APIService psApiService, AppExecutors appExecutors, MimiDb db) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside CategoryRepository");
    }

    public LiveData<Resource<Boolean>> clearAllTheData() {

        final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

        appExecutors.getNetworkIO().execute(() -> {

            try {
                db.beginTransaction();

//                db.shopDao().deleteAll();
//                db.productDao().deleteAll();
//                db.blogDao().deleteAll();
//                db.categoryDao().deleteAllCategory();
//                db.commentDao().deleteAll();
//                db.productAttributeDetailDao().deleteAll();
//                db.basketDao().deleteAllBasket();
//                db.categoryMapDao().deleteAll();
//                db.commentDetailDao().deleteAll();
//                db.deletedObjectDao().deleteAll();
//                db.imageDao().deleteTable();
//                db.notificationDao().deleteAllNotificationList();
//                db.productAttributeHeaderDao().deleteAll();
//                db.productCollectionDao().deleteAll();
//                db.productColorDao().deleteAll();
//                db.productMapDao().deleteAll();
//                db.productSpecsDao().deleteAll();
//                db.psAppInfoDao().deleteAll();
//                db.psAppVersionDao().deleteAll();
//                db.ratingDao().deleteAll();
//                db.shippingMethodDao().deleteAll();
//                db.subCategoryDao().deleteAllSubCategory();
//                db.transactionDao().deleteAllTransactionList();
//                db.transactionOrderDao().deleteAll();

//                db.setTransactionSuccessful();
            } catch (NullPointerException ne) {
                Utils.psErrorLog("Null Pointer Exception : ", ne);

                statusLiveData.postValue(Resource.error(ne.getMessage(), false));
            } catch (Exception e) {
                Utils.psErrorLog("Exception : ", e);

                statusLiveData.postValue(Resource.error(e.getMessage(), false));
            } finally {
                db.endTransaction();
            }

            statusLiveData.postValue(Resource.success(true));


        });

        return statusLiveData;
    }

}
