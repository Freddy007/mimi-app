package com.mimi.africa.repository.basket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.mimi.africa.api.APIService;
import com.mimi.africa.api.ApiResponse;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.common.NetworkBoundResource;
import com.mimi.africa.db.common.Resource;
import com.mimi.africa.db.dao.BasketCountDao;
import com.mimi.africa.db.dao.ShopDao;
import com.mimi.africa.db.dao.ShopDaoAccess;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.model.Shop;
import com.mimi.africa.repository.common.PSRepository;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Utils;

import java.util.List;

import javax.inject.Inject;

public class BasketCountRepository extends PSRepository {

private final BasketCountDao basketCountDao;


    @Inject
    BasketCountRepository(APIService psApiService, AppExecutors appExecutors, MimiDb db, BasketCountDao basketCountDao) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside BasketCountRepository");

        this.basketCountDao = basketCountDao;
    }

    public   LiveData<List<BasketCount>>getAllBasketCount() {
        return this.basketCountDao.getAllBasketCount();
    }

}
