package com.mimi.africa.repository.notification;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.mimi.africa.api.APIService;
import com.mimi.africa.api.ApiResponse;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.common.NetworkBoundResource;
import com.mimi.africa.db.common.Resource;
import com.mimi.africa.db.dao.NotificationDaoAccess;
import com.mimi.africa.db.dao.ShopDao;
import com.mimi.africa.db.dao.ShopDaoAccess;
import com.mimi.africa.model.Notification;
import com.mimi.africa.model.Shop;
import com.mimi.africa.repository.common.PSRepository;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Utils;

import java.util.List;

import javax.inject.Inject;

public class NotificationRepository extends PSRepository {

private final NotificationDaoAccess notificationDaoAccess;


    @Inject
    NotificationRepository(APIService psApiService, AppExecutors appExecutors, MimiDb db, NotificationDaoAccess notificationDaoAccess) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside ShopRepository");

        this.notificationDaoAccess = notificationDaoAccess;

    }

    public   LiveData<List<Notification>>getNotifications() {
        return this.notificationDaoAccess.fetchNotifications();
    }

    public   LiveData<List<Notification>>fetchArchivedChatNotifications() {
        return this.notificationDaoAccess.fetchArchivedChatNotifications();
    }

    public   LiveData<List<Notification>>fetchUnreadChatNotifications() {
        return this.notificationDaoAccess.fetchUnreadChatNotifications();
    }

    public   List<Notification>fetchMessages() {
        return this.notificationDaoAccess.fetchMessages();
    }

    public LiveData<Resource<Boolean>> clearAllTheData() {

        final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

        appExecutors.getNetworkIO().execute(() -> {

            try {
                db.beginTransaction();

//                db.setTransactionSuccessful();
            } catch (NullPointerException ne) {
                Utils.psErrorLog("Null Pointer Exception : ", ne);

                statusLiveData.postValue(Resource.error(ne.getMessage(), false));
            } catch (Exception e) {
                Utils.psErrorLog("Exception : ", e);

                statusLiveData.postValue(Resource.error(e.getMessage(), false));
            } finally {
                db.endTransaction();
            }

            statusLiveData.postValue(Resource.success(true));


        });

        return statusLiveData;
    }

}
