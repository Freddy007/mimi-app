package com.mimi.africa.repository.shop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.mimi.africa.api.APIService;
import com.mimi.africa.api.ApiResponse;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.common.NetworkBoundResource;
import com.mimi.africa.db.common.Resource;
import com.mimi.africa.db.dao.ShopDao;
import com.mimi.africa.db.dao.ShopDaoAccess;
import com.mimi.africa.model.Shop;
import com.mimi.africa.repository.common.PSRepository;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Utils;

import java.util.List;

import javax.inject.Inject;

public class NewShopRepository extends PSRepository {

private final ShopDaoAccess shopDao;


    @Inject
    NewShopRepository(APIService psApiService, AppExecutors appExecutors, MimiDb db, ShopDaoAccess shopDaoAccess) {
        super(psApiService, appExecutors, db);

        Utils.psLog("Inside ShopRepository");

        this.shopDao = shopDaoAccess;

    }

    public   LiveData<List<Shop>>getShops() {
        return this.shopDao.fetchAllShops();
    }


    public LiveData<Resource<Boolean>> clearAllTheData() {

        final MutableLiveData<Resource<Boolean>> statusLiveData = new MutableLiveData<>();

        appExecutors.getNetworkIO().execute(() -> {

            try {
                db.beginTransaction();

//                db.setTransactionSuccessful();
            } catch (NullPointerException ne) {
                Utils.psErrorLog("Null Pointer Exception : ", ne);

                statusLiveData.postValue(Resource.error(ne.getMessage(), false));
            } catch (Exception e) {
                Utils.psErrorLog("Exception : ", e);

                statusLiveData.postValue(Resource.error(e.getMessage(), false));
            } finally {
                db.endTransaction();
            }

            statusLiveData.postValue(Resource.success(true));


        });

        return statusLiveData;
    }

}
