package com.mimi.africa.event;

import com.mimi.africa.model.Shop;

public class ShopMoreClickedEvent {

      private Shop shop;

      public ShopMoreClickedEvent(Shop shop) {
            this.shop = shop;
      }

      public Shop getShop() {
            return shop;
      }

      public void setShop(Shop shop) {
            this.shop = shop;
      }
}
