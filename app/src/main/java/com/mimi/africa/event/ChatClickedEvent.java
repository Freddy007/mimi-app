package com.mimi.africa.event;

public class ChatClickedEvent {

      private String text;

      public ChatClickedEvent(String text) {
           this.text = text;
      }

      public String getText() {
            return text;
      }

      public void setText(String text) {
            this.text = text;
      }
}
