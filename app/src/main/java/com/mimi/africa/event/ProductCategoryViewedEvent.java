package com.mimi.africa.event;

import com.mimi.africa.model.ProductCategory;

public class ProductCategoryViewedEvent {

      private ProductCategory productCategory;

      public ProductCategoryViewedEvent(ProductCategory productCategory) {
          this.productCategory = productCategory;
      }

      public ProductCategory getProductCategory() {
            return productCategory;
      }

      public void setProductCategory(ProductCategory productCategory) {
            this.productCategory = productCategory;
      }
}
