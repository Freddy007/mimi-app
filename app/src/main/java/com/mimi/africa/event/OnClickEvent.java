package com.mimi.africa.event;

import android.view.View;

public class OnClickEvent {

      private View view;

      public OnClickEvent(View view) {
           this.view = view;
      }

      public View getView() {
            return view;
      }

      public void setView(View view) {
            this.view = view;
      }
}
