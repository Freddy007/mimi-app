package com.mimi.africa.event;

import android.view.MenuItem;
import android.view.View;

public class OnOptionItemSelected {

      private MenuItem menuItem;

      private String name;

      public OnOptionItemSelected(MenuItem item, String name){
           this.menuItem = item;
           this.name = name;
      }

      public MenuItem getMenuItem() {
            return menuItem;
      }

      public void setMenuItem(MenuItem menuItem) {
            this.menuItem = menuItem;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }
}
