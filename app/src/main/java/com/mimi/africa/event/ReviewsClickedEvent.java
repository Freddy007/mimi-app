package com.mimi.africa.event;

import com.mimi.africa.model.Feedback;

import java.util.List;

public class ReviewsClickedEvent {

      public ReviewsClickedEvent(List<Feedback> feedbackList) {
            this.feedbackList = feedbackList;
      }

      private List<Feedback> feedbackList;

      public List<Feedback> getFeedbackList() {
            return feedbackList;
      }

      public void setFeedbackList(List<Feedback> feedbackList) {
            this.feedbackList = feedbackList;
      }
}
