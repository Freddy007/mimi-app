package com.mimi.africa.event;

import com.viven.imagezoom.ImageZoomHelper;

public class ProductGalleryTouchEvent {

      private ImageZoomHelper imageZoomHelper;

      public ProductGalleryTouchEvent(ImageZoomHelper imageZoomHelper) {
            this.imageZoomHelper = imageZoomHelper;
      }

      public ImageZoomHelper getImageZoomHelper() {
            return imageZoomHelper;
      }

      public void setImageZoomHelper(ImageZoomHelper imageZoomHelper) {
            this.imageZoomHelper = imageZoomHelper;
      }
}
