package com.mimi.africa.event;


import com.mimi.africa.model.Notification;
import com.mimi.africa.model.Post;

public class NotificationUpdated {

      private Notification notification;
      private Post post;
      private String shopId;

      public NotificationUpdated(Notification notification, Post post, String shopId) {
          this.notification = notification;
          this.post = post;
          this.shopId = shopId;
      }

      public Notification getNotification() {
            return notification;
      }

      public void setNotification(Notification notification) {
            this.notification = notification;
      }

      public Post getPost() {
            return post;
      }

      public void setPost(Post post) {
            this.post = post;
      }

      public String getShopId() {
            return shopId;
      }

      public void setShopId(String shopId) {
            this.shopId = shopId;
      }
}
