package com.mimi.africa.event;

public class SearchEvent {

      private String term;

      public SearchEvent( String terrm) {
           this.term = terrm;
      }

      public String getTerm() {
            return term;
      }

      public void setTerm(String term) {
            this.term = term;
      }
}
