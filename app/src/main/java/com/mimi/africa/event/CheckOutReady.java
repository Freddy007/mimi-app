package com.mimi.africa.event;

public class CheckOutReady {

    private String paymentMode;

    public CheckOutReady(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
}
