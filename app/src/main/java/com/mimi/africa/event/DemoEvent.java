package com.mimi.africa.event;

public class DemoEvent {

      private int qty;
      private String slug;

      public DemoEvent(int qty, String slug) {
           this.qty = qty;
            this.slug = slug;
      }

      public int getQty() {
            return qty;
      }

      public void setQty(int qty) {
            this.qty = qty;
      }

      public String getSlug() {
            return slug;
      }

      public void setSlug(String slug) {
            this.slug = slug;
      }

}
