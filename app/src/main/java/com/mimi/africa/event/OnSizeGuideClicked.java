package com.mimi.africa.event;

import android.view.View;

public class OnSizeGuideClicked {

      private String type;

      public OnSizeGuideClicked(String size) {
           this.type = size;
      }

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }
}
