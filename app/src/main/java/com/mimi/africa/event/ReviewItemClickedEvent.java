package com.mimi.africa.event;

import com.mimi.africa.model.Feedback;

public class ReviewItemClickedEvent {

      private Feedback feedback;

      public ReviewItemClickedEvent(Feedback feedback) {
            this.feedback = feedback;
      }

      public Feedback getFeedback() {
            return feedback;
      }

      public void setFeedback(Feedback feedback) {
            this.feedback = feedback;
      }
}
