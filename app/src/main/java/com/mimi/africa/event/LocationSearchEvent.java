package com.mimi.africa.event;

public class LocationSearchEvent {

      private String term;

      public LocationSearchEvent(String term) {
           this.term = term;
      }

      public String getTerm() {
            return term;
      }

      public void setTerm(String term) {
            this.term = term;
      }
}
