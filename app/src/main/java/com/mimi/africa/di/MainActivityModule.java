package com.mimi.africa.di;

import androidx.annotation.NonNull;

import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.apploading.AppLoadingActivity;
import com.mimi.africa.ui.apploading.AppLoadingFragment;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.basket.fragment.BasketListFragment;
import com.mimi.africa.ui.category.SubGroupListFragment;
import com.mimi.africa.ui.category.SingleCategoryActivity;
import com.mimi.africa.ui.auth.LoginActivity;
import com.mimi.africa.ui.auth.RegisterUserActivity;
import com.mimi.africa.ui.cart.CartActivity;
import com.mimi.africa.ui.cart.CartFragment;
import com.mimi.africa.ui.category.CategoryListActivity;
import com.mimi.africa.ui.category.SingleCategoryFragment;
import com.mimi.africa.ui.category.SubGroupListActivity;
import com.mimi.africa.ui.category.services.ServiceCategoryListFragment;
import com.mimi.africa.ui.category.services.ServiceSubGroupListActivity;
import com.mimi.africa.ui.chat.ChatFragment;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.checkout.CheckoutActivity;
import com.mimi.africa.ui.checkout.CheckoutFragment;
import com.mimi.africa.ui.checkout.CheckoutFragment1;
import com.mimi.africa.ui.checkout.CheckoutFragment2;
import com.mimi.africa.ui.checkout.CheckoutFragment3;
import com.mimi.africa.ui.checkout.CheckoutStatusFragment;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.ui.favourite.FavouritesFragment;
import com.mimi.africa.ui.forgotPassword.UserForgotPasswordActivity;
import com.mimi.africa.ui.forgotPassword.UserForgotPasswordFragment;
import com.mimi.africa.ui.gallery.GalleryActivity;
import com.mimi.africa.ui.gallery.GalleryFragment;
import com.mimi.africa.ui.health.HealthCategoryActivity;
import com.mimi.africa.ui.help.HelpFragment;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.login.UserLoginActivity;
import com.mimi.africa.ui.login.UserLoginFragment;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.mainSearch.SearchHomeFragment;
import com.mimi.africa.ui.mainhome.MainHomeFragment;
import com.mimi.africa.ui.mainhome.ProductServicesFragment;
import com.mimi.africa.ui.mainhome.ProductsFragmentTab;
import com.mimi.africa.ui.mainhome.ServicesFragmentTab;
import com.mimi.africa.ui.message.ArchivedActivity;
import com.mimi.africa.ui.message.MessageFragment;
import com.mimi.africa.ui.message.UnreadActivity;
import com.mimi.africa.ui.notifications.NotificationsFragment;
import com.mimi.africa.ui.orderhistory.MyOrdersActivity;
import com.mimi.africa.ui.orderhistory.OrderHistoryFragment;
import com.mimi.africa.ui.payment.FlutterPayActivity;
import com.mimi.africa.ui.product.AllProductListFragment;
import com.mimi.africa.ui.product.CategoryProductsListFragment;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.product.ProductDetailsFragment;
import com.mimi.africa.ui.product.ProductListActivity;
import com.mimi.africa.ui.product.ProductListFragment;
import com.mimi.africa.ui.product.ProductSizeGuideFragment;
import com.mimi.africa.ui.profile.ProfileFragment;
import com.mimi.africa.ui.promo.PromoFragment;
import com.mimi.africa.ui.register.UserRegisterActivity;
import com.mimi.africa.ui.register.UserRegisterFragment;
import com.mimi.africa.ui.reviews.ReviewsFragment;
import com.mimi.africa.ui.search.ProductsFragmentSearchTabs;
import com.mimi.africa.ui.search.SearchStoreActivity;
import com.mimi.africa.ui.search.ServicesFragmentSearchTabs;
import com.mimi.africa.ui.search.StoresFragmentSearchTabs;
import com.mimi.africa.ui.service.ServiceDetailsActivity;
import com.mimi.africa.ui.service.ServiceDetailsFragment;
import com.mimi.africa.ui.service.ServiceListActivity;
import com.mimi.africa.ui.service.ServiceListFragment;
import com.mimi.africa.ui.services.ServicesActivity;
import com.mimi.africa.ui.services.ServicesFragment;
import com.mimi.africa.ui.settings.AppSettingsFragment;
import com.mimi.africa.ui.shop.ShopListActivity;
import com.mimi.africa.ui.shop.ShopListFragment;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.ui.shop.StoreFragment;
import com.mimi.africa.ui.shop.version2.HealthShopFragment;
import com.mimi.africa.ui.shop.version2.HealthShopHomeActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeFragment;
import com.mimi.africa.ui.wishlist.WishListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

      @NonNull
      @ContributesAndroidInjector(modules = MainModule.class )
      abstract MainHomeActivity contributeMainHomeActivity();

      @NonNull
      @ContributesAndroidInjector( )
      abstract UnreadActivity contributeUnreadActivity();

      @NonNull
      @ContributesAndroidInjector( )
      abstract MyOrdersActivity contributeMyOrdersActivity();

      @NonNull
      @ContributesAndroidInjector( )
      abstract ArchivedActivity contributeArchivedActivity();

      @NonNull
      @ContributesAndroidInjector(modules = MainSearchModule.class )
      abstract MainSearchActivity contributeMainSearchActivity();

      @NonNull
      @ContributesAndroidInjector(modules = SubGroupListModule.class )
      abstract SubGroupListActivity contributeSubGroupListActivity();

      @NonNull
      @ContributesAndroidInjector(modules = ServiceSubGroupListModule.class )
      abstract ServiceSubGroupListActivity contributeServiceSubGroupListActivity();

      @NonNull
      @ContributesAndroidInjector( )
      abstract LocationActivity contributeLocationActivity();

      @NonNull
      @ContributesAndroidInjector()
      abstract CartActivity contributeCartActivity();

      @NonNull
      @ContributesAndroidInjector(modules = ProductModule.class)
      abstract ProductDetailsActivity contributeProductDetailsActivity();

      @NonNull
      @ContributesAndroidInjector(modules = ServiceModule.class)
      abstract ServiceDetailsActivity contributeServiceDetailsActivity();

      @NonNull
      @ContributesAndroidInjector(modules = CheckoutModule.class)
      abstract CheckoutActivity contributeCheckoutActivity();

      @NonNull
      @ContributesAndroidInjector(modules = StoreModule.class)
      abstract StoreActivity contributeStoreActivity();

      @NonNull
      @ContributesAndroidInjector(modules = StoreHomeModule.class)
      abstract StoreHomeActivity contributeStoreHomeActivity();

      @NonNull
      @ContributesAndroidInjector(modules = HealthShopHomeModule.class)
      abstract HealthShopHomeActivity contributeHealthShopHomeActivity();

      @NonNull
      @ContributesAndroidInjector
      abstract LoginActivity contributeLoginActivity();

      @NonNull
      @ContributesAndroidInjector
      abstract RegisterUserActivity contributeRegisterUserActivity();

      @NonNull
      @ContributesAndroidInjector(modules = ProductListModule.class)
      abstract ProductListActivity contributeProductListActivity();

      @NonNull
      @ContributesAndroidInjector(modules = ServiceListModule.class)
      abstract ServiceListActivity contributeServiceListActivity();

      @NonNull
      @ContributesAndroidInjector()
      abstract CategoryListActivity contributeCategoryListActivity();

      @NonNull
      @ContributesAndroidInjector( modules = SingleCategoryModule.class)
      abstract SingleCategoryActivity contributeSingleCategoryActivity();

      @ContributesAndroidInjector(modules = SearchStoreModule.class)
      abstract SearchStoreActivity contributeSearchStoreActivity();

      @ContributesAndroidInjector()
      abstract HealthCategoryActivity contributeHealthCategoryActivity();

      @ContributesAndroidInjector( modules = ShopListModule.class)
      abstract ShopListActivity contributeShopListActivity();

      @ContributesAndroidInjector( modules = ServicesModule.class)
      abstract ServicesActivity contributeServicesActivity();

      @ContributesAndroidInjector( modules = GalleryModule.class)
      abstract GalleryActivity contributeGalleryActivity();

      @ContributesAndroidInjector( modules = ChatFragmentModule.class)
      abstract MessageActivity contributeMessageActivity();

      @ContributesAndroidInjector(modules = BasketListModule.class)
      abstract BasketListActivity contributeBasketListActivity();

      @ContributesAndroidInjector(modules = MainCheckoutModule.class)
      abstract MainCheckoutActivity contributeMainCheckoutActivity();

      @ContributesAndroidInjector(modules = UserLoginModule.class)
      abstract UserLoginActivity contributeUserLoginActivity();

      @ContributesAndroidInjector(modules = UserForgotPasswordModule.class)
      abstract UserForgotPasswordActivity contributeUserForgotPasswordActivity();

      @ContributesAndroidInjector(modules = UserRegisterModule.class)
      abstract UserRegisterActivity contributeUserRegisterActivity();

      @ContributesAndroidInjector(modules = AppLoadingFragmentModule.class)
      abstract AppLoadingActivity contributeAppLoadingActivity();

      @ContributesAndroidInjector
      abstract FlutterPayActivity contributeFlutterPayActivity();

}


@Module
abstract class MainModule {

      @NonNull
      @ContributesAndroidInjector
      abstract AppSettingsFragment contributeAppSettingsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract MainHomeFragment contributeMainHomeFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ProductServicesFragment contributeProductServicesFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ProductsFragmentTab contributeProductsFragmentTab();

      @NonNull
      @ContributesAndroidInjector
      abstract ServicesFragmentTab contributeServicesFragmentTab();

      @NonNull
      @ContributesAndroidInjector
      abstract HelpFragment contributeHelpFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract SearchHomeFragment contributeSearchHomeFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract OrderHistoryFragment contributeOrderHistoryFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract CartFragment contributeCartFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ProfileFragment contributeProfileFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract WishListFragment contributeWishListFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract NotificationsFragment contributeNotificationsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract PromoFragment contributeNotificationsPromoFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract FavouritesFragment contributeFavouritesFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ProductsFragmentSearchTabs contributeProductsFragmentSearchTabs();

      @NonNull
      @ContributesAndroidInjector
      abstract ServicesFragmentSearchTabs contributeServicesFragmentSearchTabs();

      @NonNull
      @ContributesAndroidInjector
      abstract StoresFragmentSearchTabs contributeStoresFragmentSearchTabs();

      @NonNull
      @ContributesAndroidInjector
      abstract ChatFragment contributeChatFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract MessageFragment contributeMessageFragment();

      @ContributesAndroidInjector
      abstract BasketListFragment basketFragment();

      @ContributesAndroidInjector
      abstract AllProductListFragment allProductListFragment();


      @ContributesAndroidInjector
      abstract CategoryProductsListFragment contributeCategoryProductsListFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract HealthShopFragment  contributeHealthShopFragment();

}

@Module
abstract class ProductModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ProductDetailsFragment contributeProductDetailsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract CartFragment contributeCartFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ReviewsFragment contributeReviewsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ProductSizeGuideFragment contributeProductSizeGuideFragment();
}

      @Module
      abstract class MainSearchModule {

            @NonNull
            @ContributesAndroidInjector
            abstract SearchHomeFragment contributeSearchHomeFragment();

      }


@Module
abstract class StoreModule {

      @NonNull
      @ContributesAndroidInjector
      abstract StoreFragment contributeStoreFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract StoreHomeFragment contributeStoreHomeFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ReviewsFragment contributeReviewsFragment();
}

@Module
abstract class StoreHomeModule{

      @NonNull
      @ContributesAndroidInjector
      abstract StoreHomeFragment contributeStoreHomeFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ReviewsFragment contributeReviewsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ChatFragment contributeChatFragment();

}
@Module
abstract class HealthShopHomeModule{

      @NonNull
      @ContributesAndroidInjector
      abstract HealthShopFragment contributeHealthShopFragment();

}

@Module
abstract class ServiceModule {
      @NonNull
      @ContributesAndroidInjector
      abstract ServiceDetailsFragment contributeServiceDetailsFragment();

      @NonNull
      @ContributesAndroidInjector
      abstract ReviewsFragment contributeReviewsFragment();
}

@Module
abstract class ProductListModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ProductListFragment contributeProductListFragment();

      @ContributesAndroidInjector
      abstract AllProductListFragment contributeAllProductListFragment();

      @ContributesAndroidInjector
      abstract CategoryProductsListFragment contributeCategoryProductsListFragment();
}

@Module
abstract class ServiceListModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ServiceListFragment contributeServiceListFragment();
}

@Module
abstract class ShopListModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ShopListFragment contributeShopListFragment();
}

@Module
abstract class CheckoutModule {

      @NonNull
      @ContributesAndroidInjector
      abstract CheckoutFragment contributeCheckoutFragment();
}

@Module
abstract class SearchStoreModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ProductsFragmentSearchTabs contributeProductsFragmentSearchTabs();

      @NonNull
      @ContributesAndroidInjector
      abstract ServicesFragmentSearchTabs contributeServicesFragmentSearchTabs();


      @NonNull
      @ContributesAndroidInjector
      abstract StoresFragmentSearchTabs contributeStoresFragmentSearchTabs();


}

@Module
abstract class SingleCategoryModule {

      @NonNull
      @ContributesAndroidInjector
      abstract SingleCategoryFragment contributeSingleCategoryFragment();
}

@Module
abstract class ServicesModule {

      @NonNull
      @ContributesAndroidInjector
      abstract ServicesFragment contributeServicesFragment();
}

@Module
abstract class GalleryModule {

      @NonNull
      @ContributesAndroidInjector
      abstract GalleryFragment contributeGalleryFragment();
}

@Module
abstract class BasketListModule {

      @NonNull
      @ContributesAndroidInjector
      abstract BasketListFragment contributeBasketListFragment();
}

@Module
abstract class MainCheckoutModule {

      @NonNull
      @ContributesAndroidInjector
      abstract CheckoutFragment1 contributeCheckoutFragment1();

      @NonNull
      @ContributesAndroidInjector
      abstract CheckoutFragment2 contributeCheckoutFragment2();

      @NonNull
      @ContributesAndroidInjector
      abstract CheckoutFragment3 contributeCheckoutFragment3();

      @NonNull
      @ContributesAndroidInjector
      abstract CheckoutStatusFragment contributeCheckoutStatusFragment();

}

@Module
abstract class UserLoginModule {
      @NonNull
      @ContributesAndroidInjector
      abstract UserLoginFragment contributeUserLoginFragment();


            @NonNull
            @ContributesAndroidInjector
            abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();

}
@Module
abstract class UserForgotPasswordModule {
      @NonNull
      @ContributesAndroidInjector
      abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();

}

@Module
abstract class AppLoadingFragmentModule {
      @NonNull
      @ContributesAndroidInjector
      abstract AppLoadingFragment contributeAppLoadingFragment();
}

@Module
abstract class ChatFragmentModule {
      @NonNull
      @ContributesAndroidInjector
      abstract ChatFragment contributeChatFragment();
}

@Module
abstract class UserRegisterModule {
      @NonNull
      @ContributesAndroidInjector
      abstract UserRegisterFragment contributeUserRegisterFragment();

}

@Module
abstract class SubGroupListModule {
      @NonNull
      @ContributesAndroidInjector
      abstract SubGroupListFragment contributeCategoryListFragment();

}


@Module
abstract class ServiceSubGroupListModule {
      @NonNull
      @ContributesAndroidInjector
      abstract ServiceCategoryListFragment contributeServiceCategoryListFragment();

}

