package com.mimi.africa.di;

import android.app.Application;

import androidx.annotation.NonNull;

import com.mimi.africa.MimiApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        MainActivityModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @NonNull
        @BindsInstance Builder application(Application application);
        @NonNull
        AppComponent build();
    }
    void inject(MimiApp mimiApp);

}
