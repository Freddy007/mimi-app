package com.mimi.africa.di;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mimi.africa.ui.basket.viewModel.BasketViewModel;
import com.mimi.africa.viewModels.BasketCountViewModel;
import com.mimi.africa.viewModels.ShippingMethodViewModel;
import com.mimi.africa.viewModels.ShopViewModel;
import com.mimi.africa.viewModels.apploading.AppLoadingViewModel;
import com.mimi.africa.viewModels.clearalldata.ClearAllDataViewModel;
import com.mimi.africa.viewModels.common.PSNewsViewModelFactory;
import com.mimi.africa.viewModels.notification.NotificationViewModel;
import com.mimi.africa.viewModels.product.ProductColorViewModel;
import com.mimi.africa.viewModels.user.UserViewModel;
import com.mimi.africa.viewObject.coupon.CouponDiscountViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
abstract class ViewModelModule {

      @Binds
      abstract ViewModelProvider.Factory bindViewModelFactory(PSNewsViewModelFactory factory);

      @Binds
      @IntoMap
      @ViewModelKey(BasketViewModel.class)
      abstract ViewModel bindBasketViewModel(BasketViewModel basketViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(UserViewModel.class)
      abstract ViewModel bindUserViewModel(UserViewModel userViewModel);


      @Binds
      @IntoMap
      @ViewModelKey(AppLoadingViewModel.class)
      abstract ViewModel bindPSAppInfoViewModel(AppLoadingViewModel psAppInfoViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(ClearAllDataViewModel.class)
      abstract ViewModel bindClearAllDataViewModel(ClearAllDataViewModel clearAllDataViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(ProductColorViewModel.class)
      abstract ViewModel bindProductColorViewModel(ProductColorViewModel productColorViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(ShippingMethodViewModel.class)
      abstract ViewModel bindShippingMethodViewModel(ShippingMethodViewModel shippingMethodViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(CouponDiscountViewModel.class)
      abstract ViewModel bindCouponDiscountViewModel(CouponDiscountViewModel couponDiscountViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(ShopViewModel.class)
      abstract ViewModel bindShopViewModel(ShopViewModel shopViewModel);

      @Binds
      @IntoMap
      @ViewModelKey(BasketCountViewModel.class)
      abstract ViewModel bindBasketCountViewModel(BasketCountViewModel basketCountViewModel);

        @Binds
      @IntoMap
      @ViewModelKey(NotificationViewModel.class)
      abstract ViewModel bindNotificationViewModel(NotificationViewModel notificationViewModel);

//      @Binds
//      @IntoMap
//      @ViewModelKey(TransactionListViewModel.class)
//      abstract ViewModel bindTransactionListViewModel(TransactionListViewModel transactionListViewModel);

}


