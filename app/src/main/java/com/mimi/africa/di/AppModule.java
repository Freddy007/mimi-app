package com.mimi.africa.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mimi.africa.api.APIService;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.db.dao.BasketCountDao;
import com.mimi.africa.db.dao.BasketDao;
import com.mimi.africa.db.dao.DeletedObjectDao;
import com.mimi.africa.db.dao.NotificationDaoAccess;
import com.mimi.africa.db.dao.PSAppInfoDao;
import com.mimi.africa.db.dao.PSAppVersionDao;
import com.mimi.africa.db.dao.ProductCategoryDaoAccess;
import com.mimi.africa.db.dao.ProductColorDao;
import com.mimi.africa.db.dao.ShippingMethodDao;
import com.mimi.africa.db.dao.ShopDaoAccess;
import com.mimi.africa.db.dao.UserDao;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.LiveDataCallAdapterFactory;


import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.sentry.core.Sentry;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {

    @NonNull
    @Singleton
    @Provides
    APIService provideApiService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.addInterceptor(interceptor);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

      return   new Retrofit.Builder()
                .baseUrl(Config.APP_API_URL)
                .client(okhttpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
               .addCallAdapterFactory(new LiveDataCallAdapterFactory())
               .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(APIService.class);
    }

    @NonNull
    @Singleton
    @Provides
    MimiDb provideDb(@NonNull Application app) {
        try{
        return Room.databaseBuilder(app, MimiDb.class, "mimi_db")
                //.addMigrations(MIGRATION_1_2)
                .fallbackToDestructiveMigration()
                .build();
        }catch (Exception exception){
            Sentry.captureException(exception);
        }
        return null;
    }

    @NonNull
    @Singleton
    @Provides
    Connectivity provideConnectivity(Application app) {
        return new Connectivity(app);
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences(@NonNull Application app) {
        return PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
    }

    @Singleton
    @Provides
    ShopDaoAccess provideShopDaoAccess(MimiDb db) {
        return db.shopDaoAccess();
    }

    @Singleton
    @Provides
    ProductCategoryDaoAccess provideProductCategoryDaoAccess(MimiDb db) {
        return db.productCategoryDaoAccess();
    }

    @Singleton
    @Provides
    BasketDao provideBasketDao(MimiDb db) {
        return db.basketDao();
    }

    @Singleton
    @Provides
    UserDao provideUserDao(MimiDb db) {
        return db.userDao();
    }

    @Singleton
    @Provides
    PSAppInfoDao providePSAppInfoDao(MimiDb db){return db.psAppInfoDao();}

    @Singleton
    @Provides
    PSAppVersionDao providePSAppVersionDao(MimiDb db){return db.psAppVersionDao();}

    @Singleton
    @Provides
    DeletedObjectDao provideDeletedObjectDao(MimiDb db){return db.deletedObjectDao();}

    @Singleton
    @Provides
    ProductColorDao provideProductColorDao(MimiDb db) {
        return db.productColorDao();
    }

    @Singleton
    @Provides
    ShippingMethodDao provideShippingMethodDao(MimiDb db){return db.shippingMethodDao();}

    @Singleton
    @Provides
    BasketCountDao provideBasketCountDao(MimiDb db){return db.basketCountDao();}

    @Singleton
    @Provides
    NotificationDaoAccess provideNotificationDaoAccess(MimiDb db){return db.notificationDaoAccess();}

//    @Singleton
//    @Provides
//    ShopDaoAccess provideShopDaoAccess(MimiDb db){return db.shopDaoAccess();}
}
