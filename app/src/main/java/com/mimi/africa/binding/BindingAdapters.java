package com.mimi.africa.binding;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

/**
 * Data Binding adapters specific to the app.
 */
public class BindingAdapters {
    @BindingAdapter("visibleGone")
    public static void showHide(@NonNull View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
