package com.mimi.africa.viewModels.notification;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.Notification;
import com.mimi.africa.repository.notification.NotificationRepository;
import com.mimi.africa.repository.shop.NewShopRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NotificationViewModel extends ViewModel {

    private LiveData<List<Notification>> notifications;
    private MutableLiveData<List<Notification>> messages;

    private LiveData<List<Notification>> archived;
    private LiveData<List<Notification>> unread;
    private MutableLiveData<List<Notification>> archivedList;
    private NotificationRepository mRepository;

    @Inject
    NotificationViewModel(NotificationRepository repository) {

        mRepository = repository;
       notifications = repository.getNotifications();
        archived = repository.fetchArchivedChatNotifications();
        unread =    repository.fetchUnreadChatNotifications();

        loadMessages(mRepository);

    }

    public LiveData<List<Notification>> getNotifications() {
        return notifications;
    }

    public LiveData<List<Notification>> getArchived() {
        return archived;
    }

    public LiveData<List<Notification>> getUnread() {
        return unread;
    }

    public void setNotifications(LiveData<List<Notification>> notifications) {
        this.notifications = notifications;
    }

    public LiveData<List<Notification>> getMessages() {
        if (messages == null) {
            messages = new MutableLiveData<>();
            loadMessages(mRepository);
        }
        return messages;
    }

    @SuppressLint("StaticFieldLeak")
    private void loadMessages(NotificationRepository repository) {

            new AsyncTask<Void,Void,List<Notification>>() {
                @Override
                protected List<Notification> doInBackground(Void... voids) {
                    return repository.fetchMessages();
                }

                @Override
                protected void onPostExecute(List<Notification> data) {
                    if (messages != null) {
                        messages.setValue(data);
                    }
                }
            }.execute();
        }

}
