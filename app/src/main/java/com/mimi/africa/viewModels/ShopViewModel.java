package com.mimi.africa.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.Shop;
import com.mimi.africa.repository.shop.NewShopRepository;
import com.mimi.africa.repository.shop.ShopRepository;
import com.mimi.africa.utils.AbsentLiveData;

import java.util.List;

import javax.inject.Inject;

public class ShopViewModel extends ViewModel {

//    MimiDb mimiDb;

    private LiveData<List<Shop>> shops;

    @Inject
    ShopViewModel(NewShopRepository repository) {

//        shops = Transformations.switchMap(shopObj, obj -> {

             shops = repository.getShops();


    }

//    public ShopViewModel(@NonNull Application application) {
//        super(application);
//
//        mimiDb = MimiDb.getInstance(application);
//
//        shops = mimiDb.shopDaoAccess().fetchAllShops();
//    }

    public LiveData<List<Shop>> getShops() {
        return shops;
    }

    public void setShops(LiveData<List<Shop>> shops) {
        this.shops = shops;
    }

}
