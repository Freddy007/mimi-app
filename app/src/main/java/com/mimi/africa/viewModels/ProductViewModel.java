package com.mimi.africa.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.ProductCategory;

import java.util.List;

import javax.inject.Inject;

public class ProductViewModel extends AndroidViewModel {

    @Inject
    MimiDb mimiDb;

    private LiveData<List<ProductCategory>> productCategories;

    public ProductViewModel(@NonNull Application application) {
        super(application);


//        MimiDb mimiDb = MimiDb.getInstance(this.getApplication());

        productCategories = mimiDb.productCategoryDaoAccess().fetchAll();
    }

    public LiveData<List<ProductCategory>> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(LiveData<List<ProductCategory>> productCategories) {
        this.productCategories = productCategories;
    }
}
