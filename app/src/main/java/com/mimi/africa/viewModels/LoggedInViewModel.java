//package com.mimi.africa.viewModels;
//
//import android.app.Application;
//
//import androidx.annotation.NonNull;
//import androidx.lifecycle.AndroidViewModel;
//import androidx.lifecycle.LiveData;
//import androidx.lifecycle.MutableLiveData;
//import androidx.lifecycle.Transformations;
//
//import com.mimi.africa.db.MimiDb;
//import com.mimi.africa.model.BasketCount;
//import com.mimi.africa.utils.AbsentLiveData;
//
//import java.util.List;
//
//public class LoggedInViewModel extends AndroidViewModel {
//
//      private LiveData<Boolean> loggedIn;
//      private MutableLiveData<LoggedInViewModel.TmpDataHolder> loggedInObj = new MutableLiveData<>();
//
//
//      public LoggedInViewModel(@NonNull Application application){
//            super(application);
//            MimiDb mimiDb = MimiDb.getInstance(application);
////            basketCounts = mimiDb.basketCountDao().getAllBasketCount();
//            loggedIn = Transformations.switchMap(loggedInObj,(TmpDataHolder obj)-> {
//                  if (obj == null) {
//                        return AbsentLiveData.create();
//                  }
////                return   mimiDb.basketCountDao().getAllBasketCount();
//            });
//      }
//
//
//      public LiveData<List<BasketCount>> getBasketCounts() {
//            return basketCounts;
//      }
//
//      public void setBasketCounts(LiveData<List<BasketCount>> basketCounts) {
//            this.basketCounts = basketCounts;
//      }
//
//      public void setBasketListObj() {
//            LoggedInViewModel.TmpDataHolder tmpDataHolder = new LoggedInViewModel.TmpDataHolder();
//            basketCountObj.setValue(tmpDataHolder);
//      }
//
//      //save basket
//      public void setSaveToBasketListObj(int count) {
//
//            LoggedInViewModel.TmpDataHolder tmpDataHolder = new LoggedInViewModel.TmpDataHolder();
//            tmpDataHolder.count = count;
//      }
//
//      //region Holder
//      class TmpDataHolder {
//            public int count = 0;
//            public boolean loggedIn = false;
//      }
//}
