package com.mimi.africa.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.repository.basket.BasketCountRepository;
import com.mimi.africa.repository.shop.NewShopRepository;
import com.mimi.africa.utils.AbsentLiveData;

import java.util.List;

import javax.inject.Inject;

public class BasketCountViewModel extends ViewModel {

      private LiveData<List<BasketCount>> basketCounts;
      private MutableLiveData<BasketCountViewModel.TmpDataHolder> basketCountObj = new MutableLiveData<>();

      @Inject
      BasketCountViewModel(BasketCountRepository repository) {
            basketCounts = Transformations.switchMap(basketCountObj,(TmpDataHolder obj)-> {
                  if (obj == null) {
                        return AbsentLiveData.create();
                  }
                  return   repository.getAllBasketCount();
            });
      }


      public LiveData<List<BasketCount>> getBasketCounts() {
            return basketCounts;
      }

      public void setBasketCounts(LiveData<List<BasketCount>> basketCounts) {
            this.basketCounts = basketCounts;
      }

      public void setBasketListObj() {
            BasketCountViewModel.TmpDataHolder tmpDataHolder = new BasketCountViewModel.TmpDataHolder();
            basketCountObj.setValue(tmpDataHolder);
      }

      //save basket
      public void setSaveToBasketListObj(int count) {

            BasketCountViewModel.TmpDataHolder tmpDataHolder = new BasketCountViewModel.TmpDataHolder();
            tmpDataHolder.count = count;
      }

      //region Holder
      class TmpDataHolder {
            public int count = 0;
      }
}
