package com.mimi.africa.listener;

import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class CustomTouchListener implements View.OnTouchListener {
      public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                  case MotionEvent.ACTION_DOWN:
                        ((TextView) view).setTextColor(0xFFFFFFFF); // white
                        break;
                  case MotionEvent.ACTION_CANCEL:
                  case MotionEvent.ACTION_UP:
                        ((TextView) view).setTextColor(Color.parseColor("#4a4a4a")); // lightblack
                        break;
            }
            return false;
      }
}