package com.mimi.africa;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.multidex.MultiDexApplication;

import com.mimi.africa.di.AppInjector;
import com.mimi.africa.utils.CustomSharedPrefs;

import java.net.URL;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import io.sentry.android.core.SentryAndroid;
import io.sentry.core.SentryLevel;
import me.pushy.sdk.Pushy;

public class MimiApp extends MultiDexApplication implements HasAndroidInjector {

      private static final String TAG = MimiApp.class.getSimpleName();

      @Inject
      DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

      @Override
      protected void attachBaseContext(Context base) {
            super.attachBaseContext(base);
            AppInjector.init(this);
      }

      @Override
      public void onCreate() {
            super.onCreate();

            if (!Pushy.isRegistered(getApplicationContext())) {
                  new RegisterForPushNotificationsAsync().execute();
            }

            SentryAndroid.init(this, options -> {
                  options.setBeforeSend((event, hint) -> {
                        if (SentryLevel.DEBUG.equals(event.getLevel()))
                              return null;
                        else
                              return event;
                  });
            });

      }

      @Override
      public AndroidInjector<Object> androidInjector() {
            return dispatchingAndroidInjector;
      }

      private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
            protected Exception doInBackground(Void... params) {
                  try {
                        // Assign a unique token to this device
                        String deviceToken = Pushy.register(getApplicationContext());

                        // Log it for debugging purposes
                        Log.d(TAG, "Pushy device token: " + deviceToken);

                        CustomSharedPrefs.savePushyToken(getApplicationContext(), deviceToken);
                        // Send the token to your backend server via an HTTP GET request
                        new URL("https://mimi.africa/api/set-token?token=" + deviceToken).openConnection();
                  }
                  catch (Exception exc) {
                        // Return exc to onPostExecute
                        return exc;
                  }

                  // Success
                  return null;
            }

            @Override
            protected void onPostExecute(Exception exc) {
                  // Failed?
                  if (exc != null) {
                        // Show error as toast message
                        Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                        return;
                  }
            }
      }
}
