package com.mimi.africa.utils;

import androidx.annotation.NonNull;

public class Config {

      public static final String APP_API_URL = "https://www.mimi.africa/";
      public static final String API_KEY = "10101010" ;
      public static String ATTRIBUTE_SEPARATOR = "#"; // don't change
      public static final int API_SERVICE_CACHE_LIMIT = 5; // Minutes Cache

      /**
       * AppVersion
       * For your app, you need to change according based on your app version
       */
      @NonNull
      public static String APP_VERSION = "1.1";

      /**
       * APP Setting
       * Set false, your app is production
       * It will turn off the logging Process
       */
      public static boolean IS_DEVELOPMENT = true;

      public static final String LANGUAGE_CODE = "en";
      public static final String DEFAULT_LANGUAGE_COUNTRY_CODE = "";
      public static final String DEFAULT_LANGUAGE = LANGUAGE_CODE;

      public static final String DECIMAL_PLACES_FORMAT = ",###";
      /**
       * Show currency at font or back
       * true => $ 2,555.00
       * false => 2,555,00 $
       */
      public static final boolean SYMBOL_SHOW_FRONT = true;


      /**
       * Region playstore
       */
      @NonNull
      public static String PLAYSTORE_MARKET_URL_FIX = "market://details?id=";
      @NonNull
      public static String PLAYSTORE_HTTP_URL_FIX = "http://play.google.com/store/apps/details?id=";

      /**
       * Loading Limit Count Setting
       */

      public static final int TRANSACTION_COUNT = 10;

      public static final int TRANSACTION_ORDER_COUNT = 10;

      public static int RATING_COUNT = 30;

      public static int LOAD_FROM_DB = 10; //not increase

      public static int PRODUCT_COUNT = 30;

      public static int LIST_CATEGORY_COUNT = 30;

      public static int LIST_NEW_FEED_COUNT = 30;

      public static int NOTI_LIST_COUNT = 30;

      public static int COMMENT_COUNT = 30;

      public static int COLLECTION_PRODUCT_LIST_LIMIT = 30;

      /**
       * Facebook login Config
       */
      public static boolean ENABLE_FACEBOOK_LOGIN = false;

      /**
       * Google login Config
       */
      public static boolean ENABLE_GOOGLE_LOGIN = false;

      /**
       * Phone login Config
       */
      public static boolean ENABLE_PHONE_LOGIN = false;

      /**
       * GDPR Configs
       */
      public static String CONSENTSTATUS_PERSONALIZED = "PERSONALIZED";
      public static String CONSENTSTATUS_NON_PERSONALIZED = "NON_PERSONALIZED";
      public static String CONSENTSTATUS_UNKNOWN = "UNKNOWN";
      public static String CONSENTSTATUS_CURRENT_STATUS = "UNKNOWN";
      public static String CONSENTSTATUS_IS_READY_KEY = "CONSENTSTATUS_IS_READY";
}
