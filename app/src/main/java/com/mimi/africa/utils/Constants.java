package com.mimi.africa.utils;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class Constants {


    public static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationaddress";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    public static final String WIDGET_RESULT = "wigdet_lists";
      public static final String SERVICE_OBECT = "service_object";
      public static final String SHOP_OBECT = "shop_object" ;
      public static final String INVENTORY_OBJECT = "inventory_object";
      public static final String CATEGORIES = "Categories";
      public static final String PRODUCT_CATEGORIES = "Product Categories";
      public static final String HEALTH = "Health";
      public static final String SERVICES = "Services";
      public static final String SERVICE_CATEGORIES = "Service Categories";
      public static final String EMPTY = "";
      public static final String HEALTH_FACILITY = "health-facility";
      public static final String PHARMACY= "pharmacy";
      public static final String SPECIALIST_CLINIC= "specialist-clinic";
      public static final String INVENTORY_TYPE= "inventory";
      public static final String SERVICE_TYPE= "service";
      public static final String SHOP_TYPE = "shop";
      public static final String SHOP_NAME = "name";
      public static final String NOTIFICATION_PREF = "notification";
      public static final String POINTS = "points";
      public static final String PLACE_ID = "place_id";
      public static final String PRODUCT_ID = "product_id";

//      public static final String TEMP_URL = "/mimis/public";

      public static final String DEFAULT_PAYMENT_METHOD = "6";
      public static final String SHOP_ID = "shop_id";
      public static final String DEFAULT_CURRENCY = "GHS";
      public static final int DEFAULT_RADIUS_DISTANCE = 10;

      public static final String PAYMENT_CASH_ON_DELIVERY = "COD";
      public static final  String PAYMENT_PAYPAL = "PAYMENT_PAYPAL";
      public static final String PAYMENT_STRIPE = "PAYMENT_STRIPE";
      public static final String PAYMENT_BANK = "PAYMENT_BANK";
      public static final String PAYMENT_MOMO= "PAYMENT_MOMO";

      public static final String PLATFORM = "android"; // Please don't change!
      public static final String CATEGORY_ID = "category_id" ;
      public static final String CATEGORY_NAME = "category_name";
      public static final String FILTERING_TYPE = "filtering_type";
      public static final String FILTERING_TYPE_NAME = "filtering_type_name";
      public static final String TRENDING = "new_arrivals";
      public static final String HAS_OFFERS = "has_offers";
      public static final int REQUEST_CODE__BASKET_FRAGMENT = 1002;
      public static final String ORIGIN = "origin";
      public static final String SELL_PRODUCTS = "sell-products" ;
    public static final String STORES = "Stores";
    public static final String SHOP_IMAGE = "shop_image";
    public static final String SUB_GROUP_NAME = "sub_group_name" ;

    public static int REQUEST_CODE__PHONE_CALL_PERMISSION = 2030;

    @NonNull
      public static String BASE_URL = "https://www.mimi.africa/";

    @NonNull
    public static String PAYMENTS_BASE_URL = "https://api.flutterwave.com/";

    @NonNull
    public static String AUTHENTICATION_URL = "https://www.mimi.africa/";

    @NonNull
    public static String IMAGES_BASE_URL = "https://www.mimi.africa/image/";
    public static final String DETAILS_CONTENT    = "DETAILS_CONTENT";
    public static final String GOOGLE_MAP_PACKAGE = "com.google.android.apps.maps";
    public static final String DEFAULT_LOCATION   = "";
    public static final String VENUE_PHOTOS       = "photos";
    public static final String VENUE_GROUP        = "venue";
    public static final String VENUE_SORT        = "popular";
    public static final int    LIMIT_10           =  10;
    public static final int    LIMIT_100           =  100;

    public static final String SHARED_TRANSITION_NAME = "profile";
    public static final String SHARED_PREFERENCES = "Preferences";
    public static final String SECTION = "section";
    public static final String SEARCH_TERM = "search_term";
    public static final String SUB_GROUP_ID= "sub_group_id";
    public static final String RANDOM= "random";

//    public static final String SPECIALIST_CLINIC= "specialist-clinic";
    @Nullable
    private static Retrofit retrofit = null;

    //region User
    @NonNull
    String FACEBOOK_ID = "FACEBOOK_ID";
    @NonNull
    String PHONE_ID = "PHONE_ID";
    @NonNull
    String GOOGLE_ID = "GOOGLE_ID";
    @NonNull
    String USER_ID = "USER_ID";
    @NonNull
    String OTHER_USER_ID = "OTHER_USER_ID";
    @NonNull
    String OTHER_USER_NAME = "OTHER_USER_NAME";
    @NonNull
    String USER_NAME = "USER_NAME";
    @NonNull
    String USER_PHONE = "USER_PHONE";
    @NonNull
    String USER_EMAIL = "USER_EMAIL";
    @NonNull
    String USER_NO_USER = "nologinuser"; // Don't Change
    @NonNull
    String USER_NO_DEVICE_TOKEN = "nodevicetoken"; // Don't Change
    @NonNull
    String USER_PASSWORD = "password";
    @NonNull
    String RECEIVE_USER_ID = "RECEIVE_USER_ID";
    @NonNull
    String RECEIVE_USER_NAME = "RECEIVE_USER_NAME";
    @NonNull
    String RECEIVE_USER_IMG_URL = "RECEIVE_USER_IMG_URL";
    @NonNull
    String USER_EMAIL_TO_VERIFY = "USER_EMAIL_TO_VERIFY";
    @NonNull
    String USER_PASSWORD_TO_VERIFY = "USER_PASSWORD_TO_VERIFY";
    @NonNull
    String USER_NAME_TO_VERIFY = "USER_NAME_TO_VERIFY";
    @NonNull
    String USER_ID_TO_VERIFY = "USER_ID_TO_VERIFY";

      public static boolean PRE_LOAD_FULL_IMAGE = true;
      public static int ZERO =  0;



      @Nullable
    public static Retrofit getRetrofit(@NonNull String url, String token) {

            if (retrofit != null )retrofit = null;
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(provideOkHttpClient(token))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit;
    }


    @NonNull
    private static OkHttpClient provideOkHttpClient(@Nullable String token) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.addInterceptor(interceptor);

        if (token != null){
            okhttpClientBuilder.addInterceptor(tokenHeaderIntercept(token));
        }

//        okhttpClientBuilder.addInterceptor(new TokenRenewInterceptor(getSession()));
//        okhttpClientBuilder.addInterceptor(new Authori(getApiInterface(), getSession()));
        return okhttpClientBuilder.build();
    }

    @NonNull
    private static Interceptor tokenHeaderIntercept(String token){
       Interceptor interceptor = chain -> {
           Request newRequest = chain.request().newBuilder()
                   .addHeader("Authorization", "Bearer  " + token)
                   .build();
           return chain.proceed(newRequest);
       };
       return interceptor;
    }


    public interface SharedPrefCredential {
        String SHARED_PREF_CURRENCY = "sharedPreferencesLoggedInUser" ;
    }

    public interface Preferences {
        String USER_REGISTRATION = "registered";
    }

    public interface DefaultValue {
        int MINIMUM_LENGTH_PASS = 6;
        int MAXIMUM_LENGTH = 20;
    }
}
