package com.mimi.africa.utils;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Address;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.databinding.ProductsFragmentSearchTabsBinding;
import com.mimi.africa.model.Shop;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.auth.LoginActivity;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.forgotPassword.UserForgotPasswordActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.squareup.picasso.Picasso;


import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.disposables.Disposable;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class Utils {

    private static final String TAG = Utils.class.getSimpleName();
    private static Typeface fromAsset;
    private static SpannableString spannableString;
    private static Fonts currentTypeface;
    public static String currentPhotoPath;

    public static void addToolbarScrollFlag(@NonNull Toolbar toolbar) {
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
    }

    public static void removeToolbarScrollFlag(@NonNull Toolbar toolbar) {
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
    }

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    @NonNull
    public static String numberFormat(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return numberFormat(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + numberFormat(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static float roundDouble(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @NonNull
    public static String priceFormat(Float amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(round(amount, 2));
    }

    @NonNull
    public static String getDateString(Long timeStamp, @NonNull String pattern) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);
            Date netDate = (new Date(timeStamp)); //* 1000L));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "error";
        }
    }

    @NonNull
    public static String getDateString(@NonNull Date netDate, @NonNull String pattern) {

        try {

            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

            return sdf.format(netDate);

        } catch (Exception ex) {
            return "error";
        }
    }

    @Nullable
    public static Date getDate(@NonNull String timeStamp) {

        return getDateCurrentTimeZone(Long.parseLong(timeStamp));

    }

    @Nullable
    public static Date getDateCurrentTimeZone(long timestamp) {
        try {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(timestamp);
            return cal.getTime();


        } catch (Exception e) {
        }
        return null;
    }

    public static void updateUserLoginData(@NonNull SharedPreferences pref, @NonNull User user){
        addUserLoginData(pref,user,user.userPassword);
        deleteUserVerifyData(pref);
    }

    public static void registerUserLoginData(@NonNull SharedPreferences pref, @NonNull User user, String password){
//        addUserLoginData(pref,user,password);
//        addUserVerifyData(pref,user,password);
    }

    public static void addUserLoginData(@NonNull SharedPreferences pref, @NonNull User user, String password){
        pref.edit().putString(Constant.FACEBOOK_ID, user.facebookId).apply();
//        pref.edit().putString(Constant.PHONE_ID, user.phoneId).apply();
        pref.edit().putString(Constant.GOOGLE_ID, user.googleId).apply();
        pref.edit().putString(Constant.USER_PHONE, user.userPhone).apply();
        pref.edit().putString(Constant.USER_ID, user.userId).apply();
        pref.edit().putString(Constant.USER_NAME, user.userName).apply();
        pref.edit().putString(Constant.FULL_NAME, user.billingLastName).apply();
        pref.edit().putString(Constant.USER_EMAIL, user.userEmail).apply();
        pref.edit().putString(Constant.ADDRESS, user.billingAddress1).apply();
        pref.edit().putString(Constant.CITY, user.billingCity).apply();
        pref.edit().putString(Constant.USER_PASSWORD, password).apply();

    }

    private static void deleteUserVerifyData(@NonNull SharedPreferences pref){
        pref.edit().putString(Constant.USER_EMAIL_TO_VERIFY, Constant.EMPTY_STRING).apply();
        pref.edit().putString(Constant.USER_PASSWORD_TO_VERIFY, Constant.EMPTY_STRING).apply();
        pref.edit().putString(Constant.USER_NAME_TO_VERIFY, Constant.EMPTY_STRING).apply();
        pref.edit().putString(Constant.USER_ID_TO_VERIFY, Constant.EMPTY_STRING).apply();
    }

    private static void addUserVerifyData(@NonNull SharedPreferences pref, @NonNull User user, String password){
        pref.edit().putString(Constant.USER_EMAIL_TO_VERIFY, user.userEmail).apply();
        pref.edit().putString(Constant.USER_PASSWORD_TO_VERIFY, password).apply();
        pref.edit().putString(Constant.USER_NAME_TO_VERIFY, user.userName).apply();
        pref.edit().putString(Constant.USER_ID_TO_VERIFY, user.userId).apply();
    }


    public interface NavigateOnUserVerificationActivityCallback {
        void onSuccess();
    }


    @NonNull
    public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date date = new Date();
        return dateFormat.format(date);
    }

    @NonNull
    public static String generateCurrentTime() {

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();

        return ts;
    }

    public static boolean toggleUpDownWithAnimation(@NonNull View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(150).rotation(180);
            return true;
        } else {
            view.animate().setDuration(150).rotation(0);
            return false;
        }
    }

    @NonNull
    public static AppLanguage appLanguage = new AppLanguage();


    public static int getDrawableInt(@NonNull Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    public static void setImageToImageView(Context context, @Nullable ImageView imageView, int drawable) {

        if (imageView != null && drawable != 0) {

            Glide.with(context).load(drawable).thumbnail(Glide.with(context).load(drawable)).into(imageView);

        } else {

            if (imageView != null) {
                imageView.setImageResource(R.drawable.placeholder_image);
            }

        }
    }

    @Nullable
    public static View getCurrentView(@NonNull ViewPager viewPager) {
        final int currentItem = viewPager.getCurrentItem();
        for (int i = 0; i < viewPager.getChildCount(); i++) {
            final View child = viewPager.getChildAt(i);
            final ViewPager.LayoutParams layoutParams = (ViewPager.LayoutParams) child.getLayoutParams();

            int position = 0;
            try {
                Field f = layoutParams.getClass().getDeclaredField("position");
                f.setAccessible(true);
                position = f.getInt(layoutParams); //IllegalAccessException
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (!layoutParams.isDecor && currentItem == position) {
                return child;
            }
        }
        return null;
    }

    @NonNull
    public static String checkUserId(@NonNull String loginUserId) {
        if (loginUserId.trim().equals("")) {
            loginUserId = Constant.USER_NO_USER;
        }
        return loginUserId;
    }

    public static int getToolbarHeight(@NonNull Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    @SuppressLint("RestrictedApi")
    public static void removeShiftMode(@NonNull BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }

    public static void setMargins(@NonNull View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static int pxToDp(@NonNull Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int dpToPx(@NonNull Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static Point getScreenSize(@NonNull Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getSize(size);
        } catch (NoSuchMethodError e) {
            // For lower than api 11
            size.x = display.getWidth();
            size.y = display.getHeight();
        }
        return size;
    }

    public static boolean isAndroid_5_0() {
        String version = Build.VERSION.RELEASE;
        if (version != null && !version.equals("")) {
            String[] versionDetail = version.split("\\.");
            Log.d("TEAMPS", "0 : " + versionDetail[0] + " 1 : " + versionDetail[1]);
            if (versionDetail[0].equals("5")) {
                  return versionDetail[1].equals("0") || versionDetail[1].equals("00");
            }
        }

        return false;
    }

    public static void psLog(@NonNull String log) {
        if (Config.IS_DEVELOPMENT) {
            Log.d("TEAMPS", log);
        }

    }

    public static boolean isGooglePlayServicesOK(Activity activity) {

        final int GPS_ERRORDIALOG_REQUEST = 9001;

        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable, activity, GPS_ERRORDIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(activity, "Can't connect to Google Play services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public static int getScreenHeight(@NonNull Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }

    public static int getScreenWidth(@NonNull Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static boolean isEmailFormatValid(@NonNull String email) {
        boolean isValid = false;

        String expression = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void saveBitmapImage(@NonNull Context context, @NonNull Bitmap b, String picName) {
        FileOutputStream fos;
        try {
            fos = context.openFileOutput(picName, Context.MODE_APPEND);
            b.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("TEAMPS", "file not found");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("TEAMPS", "io exception");
            e.printStackTrace();
        }

    }

    @Nullable
    public static Bitmap loadBitmapImage(@NonNull Context context, String picName) {
        Bitmap b = null;
        FileInputStream fis;
        try {
            fis = context.openFileInput(picName);
            b = BitmapFactory.decodeStream(fis);
            fis.close();

        } catch (FileNotFoundException e) {
            Log.d("TEAMPS", "file not found");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("TEAMPS", "io exception");
            e.printStackTrace();
        }
        return b;
    }

    public static Typeface getTypeFace(@NonNull Context context, Fonts fonts) {

        if (currentTypeface == fonts) {
            if (fromAsset == null) {
                if (fonts == Fonts.NOTO_SANS) {
                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSans-Regular.ttf");
                } else if (fonts == Fonts.ROBOTO) {
//                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
//                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Regular.ttf");
                } else if (fonts == Fonts.ROBOTO_MEDIUM) {
                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Bold.ttf");
                } else if (fonts == Fonts.ROBOTO_LIGHT) {
                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Regular.ttf");
                } else if (fonts == Fonts.MM_FONT) {
                    fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/mymm.ttf");
                }

            }
        } else {
            if (fonts == Fonts.NOTO_SANS) {
                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSans-Regular.ttf");
            } else if (fonts == Fonts.ROBOTO) {
//                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
//                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Regular.ttf");
            } else if (fonts == Fonts.ROBOTO_MEDIUM) {
                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Bold.ttf");
            } else if (fonts == Fonts.ROBOTO_LIGHT) {
                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Regular.ttf");
            } else if (fonts == Fonts.MM_FONT) {
                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/mymm.ttf");
            } else {
                fromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inventory-Sans-Regular.ttf");
            }

            //fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Italic.ttf");
            currentTypeface = fonts;
        }
        return fromAsset;
    }
//
    public static SpannableString getSpannableString(@NonNull Context context, String str, Fonts font) {
        spannableString = new SpannableString(str);
        spannableString.setSpan(new MimiTypefaceSpan("", Utils.getTypeFace(context, font)), 0, spannableString.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public enum Fonts {
        ROBOTO,
        NOTO_SANS,
        ROBOTO_LIGHT,
        ROBOTO_MEDIUM,
        MM_FONT
    }

    public enum LoadingDirection {
        top,
        bottom,
        none
    }

//    public static Bitmap getUnRotatedImage(String imagePath, Bitmap rotatedBitmap) {
//        int rotate = 0;
//        try {
//            File imageFile = new File(imagePath);
//            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotate = 270;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotate = 180;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotate = 90;
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//
//        Matrix matrix = new Matrix();
//        matrix.postRotate(rotate);
//        return Bitmap.createBitmap(rotatedBitmap, 0, 0, rotatedBitmap.getWidth(), rotatedBitmap.getHeight(), matrix,
//                true);
//    }

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[4].getLineNumber();
    }

    @NonNull
    public static String getClassName(@NonNull Object obj) {
        return "" + ((Object) obj).getClass();
    }

    public static void psErrorLog(@NonNull String log, @NonNull Object obj) {
        try {
            Log.d("TEAMPS", log);
            Log.d("TEAMPS", "Line : " + getLineNumber());
            Log.d("TEAMPS", "Class : " + getClassName(obj));
        } catch (Exception ee) {
            Log.d("TEAMPS", "Error in psErrorLog");
        }
    }

    public static void psErrorLog(@NonNull String log, @NonNull Exception e) {
        try {
            StackTraceElement l = e.getStackTrace()[0];
            Log.d("TEAMPS", log);
            Log.d("TEAMPS", "Line : " + l.getLineNumber());
            Log.d("TEAMPS", "Method : " + l.getMethodName());
            Log.d("TEAMPS", "Class : " + l.getClassName());
        } catch (Exception ee) {
            Log.d("TEAMPS", "Error in psErrorLogE");
        }

    }


    public static void unbindDrawables(@NonNull View view) {
        try {
            if (view.getBackground() != null) {
                view.getBackground().setCallback(null);
            }
            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    unbindDrawables(((ViewGroup) view).getChildAt(i));
                }

                if (!(view instanceof AdapterView)) {
                    ((ViewGroup) view).removeAllViews();
                }
            }
        } catch (Exception e) {
            Utils.psErrorLog("Error in Unbind", e);
        }
    }

    public static boolean isStoragePermissionGranted(@NonNull Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Utils.psLog("Permission is granted");
                return true;
            } else {
                Utils.psLog("Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Utils.psLog("Permission is granted");
            return true;
        }
    }

    // Sleep Me
    public static void sleepMe(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Utils.psErrorLog("InterruptedException", e);
        } catch (Exception e) {
            Utils.psErrorLog("Exception", e);
        }
    }


    public static void hideKeyboard(@NonNull Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);

            if (imm != null) {
                if (activity.getCurrentFocus() != null) {
                    imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                }
            }
        } catch (Exception e) {
            Utils.psErrorLog("Error in hide keyboard.", e);
        }
    }

    @NonNull
    public static String generateKeyForChatHeadId(@NonNull String senderId, @NonNull String receiverId) {


        if (senderId.compareTo(receiverId) < 0) {

            return senderId + "_" + receiverId;

        } else if (senderId.compareTo(receiverId) > 0) {

            return receiverId + "_" + senderId;

        } else {
            //Need to apply proper solution later

            return senderId + "_" + receiverId;
        }
    }

    //For Price DecimalFormat
    @NonNull
    public static String format(double value) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(Config.DECIMAL_PLACES_FORMAT);
        return df.format(value);

    }


    @NonNull
    public static File createImageFile(@NonNull Context context) throws IOException {


        // Create an locationImage file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    //Ad
//    public static void initInterstitialAd(Context context, String adKey) {
//        //load ad
//        AdRequest adRequest = new AdRequest.Builder().build();
//
//        InterstitialAd interstitial;
//        // Prepare the Interstitial Ad
//        interstitial = new InterstitialAd(context);
//
//        // Insert the Ad Unit ID
//        interstitial.setAdUnitId(adKey);
//
//        interstitial.loadAd(adRequest);
//
//        // Prepare an Interstitial Ad Listener
//        interstitial.setAdListener(new AdListener() {
//            public void onAdLoaded() {
//                // Call displayInterstitial() function
//                displayInterstitial(interstitial);
//            }
//        });
//    }

//    public static void displayInterstitial(InterstitialAd interstitial) {
//        // If Ads are loaded, show Interstitial else show nothing.
//        if (interstitial.isLoaded()) {
//            interstitial.show();
//        }
//    }


    public static boolean toggleUporDown(@NonNull View v) {
        if (v.getRotation() == 0) {
            v.animate().setDuration(150).rotation(180);
            return true;
        } else {
            v.animate().setDuration(150).rotation(0);
            return false;
        }
    }


    public static void setDatesToShared(String start_date, String end_date, @NonNull SharedPreferences pref) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constant.CITY_START_DATE, start_date);
        editor.putString(Constant.CITY_END_DATE, end_date);
        editor.apply();

    }

//    public static void callPhone(@NonNull Fragment fragment, String phoneNo) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            // Older Version No ne
//            //ed to request Permission
//            String dial = "tel:" + phoneNo;
//            fragment.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
//        } else {
//            // Need to request Permission
//            if (fragment.getActivity() != null) {
//                if (ContextCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    fragment.requestPermissions(new String[]{
//                            Manifest.permission.CALL_PHONE
//                    }, Constant.REQUEST_CODE__PHONE_CALL_PERMISSION);
//                } else {
//                    String dial = "tel:" + phoneNo;
//                    fragment.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
//                }
//            }
//        }
//    }

//    public static void phoneCallPermissionResult(int requestCode,
//                                                 int[] grantResults, @NonNull Fragment fragment, String phoneNo) {
//        if (requestCode == Constant.REQUEST_CODE__PHONE_CALL_PERMISSION) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                callPhone(fragment, phoneNo);
//            } else {
//                Utils.psLog("Permission not Granted");
//            }
//        }
//    }

    public static void callPhone(@NonNull Activity activity, String phoneNo) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // Older Version No need to request Permission
            String dial = "tel:" + phoneNo;
            activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        } else {
            // Need to request Permission
            if (activity != null) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    activity.requestPermissions(new String[]{
                            Manifest.permission.CALL_PHONE
                    }, Constant.REQUEST_CODE__PHONE_CALL_PERMISSION);
                } else {
                    String dial = "tel:" + phoneNo;
                    activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                }
            }
        }
    }

    public static void phoneCallPermissionResult(int requestCode,
                                                 int[] grantResults, @NonNull Activity activity, String phoneNo) {
        if (requestCode == Constant.REQUEST_CODE__PHONE_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhone(activity, phoneNo);
            } else {
                Utils.psLog("Permission not Granted");
            }
        }
    }

    public static long convertDateTimeToMilliseconds(@NonNull String dateString) {

//            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        Date date = null;

        long dateConverted = 0;

        try {
            date = sdf.parse(dateString.trim());
            dateConverted =  (date.getTime());
            Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateConverted;
    }

    public static long convertDateOnlyToMilliseconds(@NonNull String dateString) {

//            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;

        long dateConverted = 0;

        try {
            date = sdf.parse(dateString.trim());
            dateConverted =  (date.getTime());
            Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateConverted;
    }

    @NonNull
    public static String TwoDecimalPlace(double number){
        DecimalFormat df2 = new DecimalFormat("#.##");
        return df2.format(number);

    }

    public static void LoadImage(Context ctx,  ImageView imageview, String url){

//        Picasso.with(ctx)
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(imageview);
    }

    public static void ShowSnackBar(@NonNull Activity activity, @NonNull String content, int length ){
        Snackbar.make(activity.findViewById(android.R.id.content), content, length).show();

    }

    public static void RemoveView(@NonNull View view ){
        view.setVisibility(View.GONE);
    }

    public static void ShowView(@NonNull View view ){
        view.setVisibility(View.VISIBLE);
    }

    public static boolean isUserLoggedIn(Context context) {
        return CustomSharedPrefs.getUserStatus(context);
    }

    public static void  showProgressDialog(Context context,  Dialog progressDialog, int layout) {

        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        view
//        progressDialog.setContentView(R.layout.dialog_progress);

        progressDialog.setContentView(layout);
        progressDialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(progressDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        progressDialog.show();
        progressDialog.getWindow().setAttributes(lp);
    }

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.layout_badge, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }
    
    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }

    public static void getDirectionsOnGoogleMap(Activity activity ,String lat, String lng) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat+ "," + lng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(mapIntent);
        }
    }

    public static void hideFirstFab(final View v) {
        v.setVisibility(View.GONE);
        v.setTranslationY(v.getHeight());
        v.setAlpha(0f);
    }

    public static boolean twistFab(final View v, boolean rotate) {
        v.animate().setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .rotation(rotate ? 165f : 0f);
        return rotate;
    }

    public static void showFab(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0f);
        v.setTranslationY(v.getHeight());
        v.animate()
                .setDuration(300)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }

    public static void hideFab(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(1f);
        v.setTranslationY(0);
        v.animate()
                .setDuration(300)
                .translationY(v.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                }).alpha(0f)
                .start();
    }

    public static void callPhone(Fragment fragment, String phoneNo) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // Older Version No need to request Permission
            String dial = "tel:" + phoneNo;
            fragment.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        } else {
            // Need to request Permission
            if (fragment.getActivity() != null) {
                if (ContextCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    fragment.requestPermissions(new String[]{
                            Manifest.permission.CALL_PHONE
                    }, Constants.REQUEST_CODE__PHONE_CALL_PERMISSION);
                } else {
                    String dial = "tel:" + phoneNo;
                    fragment.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                }
            }
        }
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        Log.d("TEAMPS", "collapse: " + (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void addBottomDots(@NonNull LinearLayout layout_dots, int size, int current, Context context) {
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(context);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(context, R.color.overlay_dark_10), PorterDuff.Mode.SRC_ATOP);
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current].setColorFilter(ContextCompat.getColor(context, R.color.colorPrimaryLight), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public static void disposeAPIService(Disposable disposable) {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public static void navigateToLogin(Context context) {
       context. startActivity(new Intent(context, LoginActivity.class));
    }

    public static void navigateToChat(Context context, Shop shop) {
        Intent intent = new Intent(context, MessageActivity.class);
        intent.putExtra(Constants.SHOP_NAME, shop.getName());
        intent.putExtra(Constants.SHOP_ID, shop.getId());
        context.startActivity(intent);
    }

    public static void navigateToSearch(Context context){
        context.startActivity(new Intent(context, MainSearchActivity.class));
    }

    @NotNull
    public static List<Address> getAddresses(Context context) {
        return  LocationUtilService.getYourAddress(context);
    }

    public static void navigateAfterForgotPassword(Activity activity, NavigationController navigationController){
        if (activity instanceof MainHomeActivity) {

            Intent intent = new Intent(activity, UserForgotPasswordActivity.class);
            activity.startActivity(intent);

        } else {
            Intent intent = new Intent(activity, UserForgotPasswordActivity.class);
            activity.startActivity(intent);

            try {
                if (activity != null) {
                    activity.finish();
                }
            } catch (Exception e) {
                Utils.psErrorLog("Error in closing activity.", e);
            }
        }

    }

    public static void navigateAfterUserLogin(Activity activity, NavigationController navigationController, String origin){
        if (activity instanceof MainHomeActivity) {
            navigationController.navigateToUserProfile((MainHomeActivity) activity);

        } else {
            try {

                if (activity != null) {
                    Log.i(TAG, "navigateAfterUserLogin:  1 " + activity.getLocalClassName() );
                    if (origin.equals(StoreHomeActivity.class.getSimpleName())){
                        activity.startActivity(new Intent(activity, MainHomeActivity.class));
                    }else if (origin.equals(BasketListActivity.class.getSimpleName())){
                        activity.startActivity(new Intent(activity, MainCheckoutActivity.class));
                    }else {
                        activity.startActivity(new Intent(activity, MainHomeActivity.class));
                    }
                }
            } catch (Exception e) {
                Utils.psErrorLog("Error in closing parent activity.", e);
            }
        }
    }

    public static void navigateOnUserVerificationActivity(String userIdToVerify,String loginUserId,String origin,
                                                          PSDialogMsg psDialogMsg,Activity activity,
                                                          NavigationController navigationController,
                                                          NavigateOnUserVerificationActivityCallback callback
    ) {
//        if (userIdToVerify.isEmpty()) {
            if (loginUserId.equals("")) {

                psDialogMsg.showInfoDialog(activity.getString(R.string.error_message__login_first), activity.getString(R.string.app__ok));
                psDialogMsg.show();

                psDialogMsg.okButton.setOnClickListener(v1 -> {
                    psDialogMsg.cancel();
                    navigationController.navigateToUserLoginActivity(activity, origin);
                });

            } else {
                callback.onSuccess();
            }
//        }else {
//
////            navigationController.navigateToVerifyEmailActivity(activity);
//
//            Utils.ShowSnackBar(activity, "Hello Login", Snackbar.LENGTH_LONG);

//        }
    }

//    public static void navigateAfterUserRegister(Activity activity, NavigationController navigationController){
//        if (activity instanceof MainActivity) {
//            ((MainActivity) activity).setToolbarText(((MainActivity) activity).binding.toolbar, activity.getString(R.string.verify_email));
//
//            navigationController.navigateToVerifyEmail((MainActivity) activity);
//
//        } else {
//            navigationController.navigateToVerifyEmailActivity(activity);
//            try {
//                if (activity != null) {
//                    activity.finish();
//                }
//            } catch (Exception e) {
//                Utils.psErrorLog("Error in closing parent activity.", e);
//            }
//        }
//    }
}
