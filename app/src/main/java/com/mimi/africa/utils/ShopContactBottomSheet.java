package com.mimi.africa.utils;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mimi.africa.R;
import com.mimi.africa.model.Shop;


public class ShopContactBottomSheet extends BottomSheetDialogFragment {

    private BottomSheetBehavior mBehavior;
    private AppBarLayout app_bar_layout;
    private LinearLayout lyt_profile;

    MapView mMapView;
    private GoogleMap googleMap;

    private Shop shop;

    private GoogleMap mMap;

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        final View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_shop_info, null);

        dialog.setContentView(view);

        if (shop != null ) {
            ((TextView) view.findViewById(R.id.name)).setText(shop.getName());
            ((TextView) view.findViewById(R.id.name_toolbar)).setText(shop.getName());
        }

        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);

        app_bar_layout = view.findViewById(R.id.app_bar_layout);
        lyt_profile = view.findViewById(R.id.lyt_profile);

        ImageView imageView = view.findViewById(R.id.image);

        if (shop.getImages() != null && shop.getImages().size() >0 ){
            String url = Constants.IMAGES_BASE_URL+ shop.getImages().get(0).getPath();
            Utils.LoadImage(getContext(), imageView, url);
        }else {
            Utils.LoadImage(getContext(), imageView, shop.getBannerImage());
        }



        ((TextView) view.findViewById(R.id.addressTextView))
                .setText(shop.getAddress());

        ((TextView) view.findViewById(R.id.locationDistance))
                .setText(String.format("%s km away", String.format("%.2f", getLocationDistance(shop))));

        if (shop.getAddresses() != null  && shop.getAddresses().size() > 0 ){

            view.findViewById(R.id.phoneLayout).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.phoneNumberTextView)).setText(shop.getAddresses().get(0).getPhone());

            view.findViewById(R.id.call_phone_button).setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + shop.getAddresses().get(0).getPhone()));
                startActivity(intent);
            });

        }else {
            view.findViewById(R.id.phoneLayout).setVisibility(View.GONE);
        }

        ((TextView) view.findViewById(R.id.emailTextView)).setText(shop.getEmail());

        if (shop.getConfig() != null) {
            ((TextView) view.findViewById(R.id.secondaryPhoneNumber)).setText(shop.getConfig().getSupportPhone());


        }
        view.findViewById(R.id.lyt_spacer).setMinimumHeight(Tools.getScreenHeight() / 2);

        hideView(app_bar_layout);

        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    showView(app_bar_layout, getActionBarSize());
                    hideView(lyt_profile);
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    hideView(app_bar_layout);
                    showView(lyt_profile, getActionBarSize());
                }

                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        view.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mMapView = dialog.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (shop.getLat() != null) {

            mMapView.getMapAsync(mMap -> {
                googleMap = mMap;

                // For showing a move to my location button
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng location = new LatLng(Double.parseDouble(shop.getLat()), Double.parseDouble(shop.getLng()));
                googleMap.addMarker(new MarkerOptions().position(location).title(shop.getName()).snippet(shop.getAddress()));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            });
        }

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private void hideView(View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = 0;
        view.setLayoutParams(params);
    }

    private void showView(View view, int size) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = size;
        view.setLayoutParams(params);
    }

    private int getActionBarSize() {
        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int size = (int) styledAttributes.getDimension(0, 0);
        return size;
    }

    private double getLocationDistance(Shop p) {

        double distanceInKm = 0.0;

        if (p.getLat() != null) {
            String latitude = p.getLat();
            String longitude = p.getLng();

            if (LocationPreferences.isLocationLatLonAvailable(getContext())) {

                double latitude_dbl = Double.parseDouble(latitude);
                double longitude_dbl = Double.parseDouble(longitude);

                android.location.Location endLocation = new android.location.Location("New Location");
                endLocation.setLatitude(latitude_dbl);
                endLocation.setLongitude(longitude_dbl);


                distanceInKm = LocationUtilService.getDistanceInKm(getContext(), endLocation);
            }

        }
        return distanceInKm;

    }

      @Override
      public void onResume() {
            super.onResume();
            mMapView.onResume();
      }

      @Override
      public void onPause() {
            super.onPause();
            mMapView.onPause();
      }

      @Override
      public void onDestroy() {
            super.onDestroy();
            mMapView.onDestroy();
      }

      @Override
      public void onLowMemory() {
            super.onLowMemory();
            mMapView.onLowMemory();
      }

}
