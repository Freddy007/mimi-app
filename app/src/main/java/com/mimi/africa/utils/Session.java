package com.mimi.africa.utils;

import androidx.annotation.Nullable;

public interface Session {

    void setLogin(boolean status);

    boolean isLoggedIn();

    void saveToken(String token);

    @Nullable
    String getToken();

    void saveUserId(String userId);

    @Nullable
    String getUserId();

    void saveName(String name);

    @Nullable
    String getName();

    void saveEmail(String email);

    @Nullable
    String getEmail();

    void savePassword(String password);

    @Nullable
    String getPassword();

    void invalidate();

    void saveOnBoardingStatus(boolean status);

}
