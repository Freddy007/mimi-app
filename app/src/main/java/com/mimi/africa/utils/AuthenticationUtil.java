package com.mimi.africa.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mimi.africa.api.AuthenticationAPIInterface;
import com.mimi.africa.model.Authorization;
import com.mimi.africa.model.UserInterest;
import com.mimi.africa.ui.activities.MainHomeActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class AuthenticationUtil {

    public static boolean aBoolean;
    public static boolean aBoolean1;
    public static boolean aBoolean2;
    public static boolean aBoolean3;
    private LinearLayout signupForAccount;
    private AuthenticationAPIInterface authenticationAPIInterface;
    public static Session session;

    private static final String TAG = AuthenticationUtil.class.getSimpleName();

    public static void checkFieldValidity(@NonNull final EditText editTextEmail,
                                          @NonNull final EditText editTextPassword, @Nullable final EditText editTextName, Context context) {

        if (editTextName != null){
            editTextName.setOnFocusChangeListener((v, hasFocus) -> {
                if (!hasFocus) {
                    if (editTextName.getText().toString().equals("")) {
                        Toast.makeText(context, "fill all input fields!!", Toast.LENGTH_SHORT).show();
                        aBoolean = false;
                    } else {
                        aBoolean = true;
                    }
                } else {
                    aBoolean = !editTextName.getText().toString().equals("");
                }
            });

        }

        editTextEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (editTextEmail.getText().toString().equals("")
                        || !emailValidity(editTextEmail.getText().toString())) {
                    Toast.makeText(context, "invalid email!", Toast.LENGTH_SHORT).show();
                    aBoolean = false;
                } else {
                    aBoolean = true;
                }
            } else {
                aBoolean = !editTextEmail.getText().toString().equals("")
                        && emailValidity(editTextEmail.getText().toString());
            }
        });

        editTextPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!aBoolean3) {
                if (!hasFocus) {
                    if (editTextPassword.getText().toString().equals("") || !passwordValidity(editTextPassword.getText().toString())) {
                        Toast.makeText(context, "password not long enough!", Toast.LENGTH_SHORT).show();

                        aBoolean1 = false;
                    } else {
                        aBoolean1 = true;
                    }
                } else {
                    aBoolean1 = !editTextPassword.getText().toString().equals("")
                            && passwordValidity(editTextPassword.getText().toString());
                }
            }
        });

        editTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(@NonNull Editable s) {
                if (s.toString().contains(" ")) {
                    aBoolean2 = false;
                    aBoolean3 = true;

                } else {
                    aBoolean2 = true;
                    aBoolean3 = false;
                }

                if (passwordValidity(editTextPassword.getText().toString())) {
                    aBoolean1 = true;
                }
            }
        });

        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (emailValidity(editTextEmail.getText().toString())) {
                    aBoolean = true;
                }
            }
        });
    }

    /**
     * checking email validation
     */
    public static boolean emailValidity(@NonNull String text) {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches();
    }

    /**
     * checking password validation
     */
    public static boolean passwordValidity(@NonNull String text) {
        return text.length() >= Constants.DefaultValue.MINIMUM_LENGTH_PASS
                && text.length() <= Constants.DefaultValue.MAXIMUM_LENGTH;
    }

    public static boolean isEmpty(@NonNull String text) {
        return text.equals("");
    }

    public static void processLogin(@NonNull Response<List<Authorization>> response, String email, String password, @NonNull Session session, @NonNull Activity activity) {

        CustomSharedPrefs.setNotFirstTimeStatus(activity, false);
        session.invalidate();
        session.saveUserId(response.body().get(0).getUserId());
        session.saveName(response.body().get(0).getName());
        session.saveEmail(email);
        session.savePassword(password);
        session.saveToken(response.body().get(0).getToken());
        session.setLogin(true);
//        session.saveOnBoardingStatus( response.body().get(0).isBoardingStatus());

        List<UserInterest> interests = response.body().get(0).getInterests();

        List<String> stringList = new ArrayList<>();
//
//        try{
//
//            if (interests != null){
//                if (interests.size() != 0  ){
//
//                    for (int i = 0; i < interests.size(); i++ ){
//                        stringList.add(interests.get(i).getInterest());
//                    }
//
//                    RecommendationPreferences.saveUserInterestCategories(activity, stringList );
//                }
//            }
//
//        }catch(NullPointerException e){
//            Log.e(TAG, "processLogin:  "  + e.getMessage() );
//        }


        startMainActivity(activity);
    }

    public static void startMainActivity(@NonNull Activity activity){
        Intent mainIntent = new Intent(activity, MainHomeActivity.class);
        activity.startActivity(mainIntent);
    }

    public static Session getSession(Context context){
        if (session == null) {
            session = new Session() {

                @Override
                public void setLogin(boolean status) {
                    CustomSharedPrefs.setUserLoginStatus(context, status);
                }

                @Override
                public boolean isLoggedIn() {
                    return CustomSharedPrefs.getUserStatus(context);
                }

                @Override
                public void saveToken(String token) {
                    CustomSharedPrefs.saveToken(context, token);
                }

                @Nullable
                @Override
                public String getName() {
                    return CustomSharedPrefs.getUserName(context);
                }

                @Nullable
                @Override
                public String getToken() {
                    return CustomSharedPrefs.getUserToken(context);
                }

                @Override
                public void saveUserId(String userId) {
                    CustomSharedPrefs.saveUserId(context, userId);
                }

                @Nullable
                @Override
                public String getUserId() {
                    return CustomSharedPrefs.getUserID(context);
                }

                @Override
                public void saveName(String name) {
                     CustomSharedPrefs.saveName(context, name);
                }

                @Override
                public void saveEmail(String email) {
                    CustomSharedPrefs.saveEmail(context, email);
                }

                @Nullable
                @Override
                public String getEmail() {
                    return CustomSharedPrefs.getUserEmail(context);
                }

                @Override
                public void savePassword(String password) {
                    CustomSharedPrefs.savePassword(context, password);
                }

                @Nullable
                @Override
                public String getPassword() {
                    return CustomSharedPrefs.getUserPassword(context);
                }

                @Override
                public void invalidate() {
                    CustomSharedPrefs.removeUserPreferences(context);
                }

                  @Override
                  public void saveOnBoardingStatus(boolean status) {
                        CustomSharedPrefs.setOnBoarding(context, status);
                  }
            };
        }
        return session;
    }

    public static void hideKeyboard(@NonNull Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}


