package com.mimi.africa.utils;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mimi.africa.R;

public class Tools {

      public static void setSystemBarColor(@NonNull Activity act) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  Window window = act.getWindow();
                  window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                  window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                  window.setStatusBarColor(act.getResources().getColor(R.color.colorPrimaryDark));
            }
      }

      public static void displayImageOriginal(Context ctx, @NonNull ImageView img, @DrawableRes int drawable) {
            try {
                  Glide.with(ctx).load(drawable)
//                          .crossFade()
                          .diskCacheStrategy(DiskCacheStrategy.NONE)
                          .into(img);
            } catch (Exception e) {
            }
      }

//      public static void displayImageRound(final Context ctx, final ImageView img, @DrawableRes int drawable) {
//            try {
//                  Glide.with(ctx).load(drawable).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
//                        @Override
//                        protected void setResource(Bitmap resource) {
//                              RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(ctx.getResources(), resource);
//                              circularBitmapDrawable.setCircular(true);
//                              img.setImageDrawable(circularBitmapDrawable);
//                        }
//                  });
//            } catch (Exception e) {
//            }
//      }

      public static int dpToPx(@NonNull Context c, int dp) {
            Resources r = c.getResources();
            return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
      }

      public static void setSystemBarLight(@NonNull Activity act) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                  View view = act.findViewById(android.R.id.content);
                  int flags = view.getSystemUiVisibility();
                  flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                  view.setSystemUiVisibility(flags);
            }
      }


      public static void nestedScrollTo(@NonNull final NestedScrollView nested, @NonNull final View targetView) {
            nested.post(new Runnable() {
                  @Override
                  public void run() {
                        nested.scrollTo(500, targetView.getBottom());
                  }
            });
      }

      public static String getEmailFromName(String name) {
            if (name != null && !name.equals("")) {
                  String email = name.replaceAll(" ", ".").toLowerCase().concat("@mail.com");
                  return email;
            }
            return name;
      }

      public static int getScreenHeight() {
            return Resources.getSystem().getDisplayMetrics().heightPixels;
      }

      public static void copyToClipboard(Context context, String data) {
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("clipboard", data);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(context, "Text copied to clipboard", Toast.LENGTH_SHORT).show();
      }


}
