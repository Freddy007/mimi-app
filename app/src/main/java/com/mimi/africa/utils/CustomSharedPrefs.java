package com.mimi.africa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.mimi.africa.model.User;

public class CustomSharedPrefs {
    private static final String PREF_LOGGED_IN_USER = "loggedInUser";
    private static final String PREF_USER_TOKEN = "userToken";
    private static final String PREF_PUSHY_TOKEN = "pushyToken";
    private static final String PREF_USER_EMAIL = "userEmail";
    private static final String PREF_USER_PHONE = "userPhone";
    private static final String PREF_USER_PASSWORD = "userPassword";
    private static final String PREF_LOG_IN_STATUS = "userLoginStatus";
    private static final String PREF_FIRST_TIME_LOG_IN = "firstTimeLogin";
    private static final String PREF_ONBOARDING_SET = "onBoardingSet";
    private static final String PREF_USER_NAME = "userName";
    private static final String PREF_USER_ID = "userId";
    private static final String PREF_USER_ADDRESS = "address";

    private static final String PREF_USER_CART_IDS= "cart_ids";
    private static final String PREF_USER_CART_STATUS = "cart_status";
    private static final String PREF_RELOAD_PAGE = "reload_page";

    private static final String PREF_CART_QTY = "cart_qty";
    private static final String PREF_LAST_CHAT_TIME = "last_chat";

    public static boolean getUserStatus(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean spContainsUser = false;

//        if (sharedPreferences.contains(PREF_LOGGED_IN_USER)){
        if (sharedPreferences.contains(PREF_LOG_IN_STATUS)){
            spContainsUser = true;
        }

        return spContainsUser;
    }

    public static void setUserLoginStatus(Context context, boolean status){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_LOG_IN_STATUS, status);
        editor.apply();
    }

    @Nullable
    public static void setNotFirstTimeStatus(Context context, boolean status){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_FIRST_TIME_LOG_IN, status);
        editor.apply();
    }

    @Nullable
    public static boolean getFirstTimeStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_FIRST_TIME_LOG_IN, false);
    }

    @Nullable
    public static void setLoggedInUser(Context context, User user){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(PREF_LOGGED_IN_USER, json);
        editor.apply();
    }

    @Nullable
    public static String getPrefLoggedInUser(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_LOGGED_IN_USER, "");
    }

    public static void saveToken(Context context, String token){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_TOKEN, token);
        editor.apply();
    }

    @Nullable
    public static String getUserToken(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_TOKEN, "");
    }

    @Nullable
    public static String getPushyToken(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_PUSHY_TOKEN, "");
    }

    public static void savePushyToken(Context context, String token){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_PUSHY_TOKEN, token);
        editor.apply();
    }

    @Nullable
    public static long getLastChatTime(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(PREF_LAST_CHAT_TIME, 0);
    }

    @Nullable
    public static void saveLastChatTime(Context context, long time){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PREF_LAST_CHAT_TIME, time);
        editor.apply();
    }

    @Nullable
    public static void saveName(Context context, String name){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_NAME, name);
        editor.apply();
    }

    @Nullable
    public static String getUserName(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_NAME, "");
    }

    public static void saveUserId(Context context, String userId){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_ID, userId);
        editor.apply();
    }

    @Nullable
    public static String getUserID(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_ID, "");
    }

    public static boolean isUserNameAvailable(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean spContainsUser = false;
        if (sharedPreferences.contains(PREF_USER_NAME)){
            spContainsUser = true;
        }

        return spContainsUser;
    }

    @Nullable
    public static void saveEmail(Context context, String email){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_EMAIL, email);
        editor.apply();
    }

    @Nullable
    public static String getUserEmail(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_EMAIL, "");
    }

    @Nullable
    public static void savePhone(Context context, String phone){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_PHONE, phone);
        editor.apply();
    }

    @Nullable
    public static String getUserPhone(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_PHONE, "");
    }


    public static void saveUserAddress(Context context, String address){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_ADDRESS, address);
        editor.apply();
    }

    @Nullable
    public static String getUserAddress(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_ADDRESS, "");
    }

    @Nullable
    public static void savePassword(Context context, String password){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_PASSWORD, password);
        editor.apply();
    }

    @Nullable
    public static String getUserPassword(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_PASSWORD, "");
    }

    public static void removeUserPreferences(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove(PREF_LOG_IN_STATUS);
        editor.remove(PREF_LOGGED_IN_USER);
        editor.remove(PREF_USER_TOKEN);
        editor.remove(PREF_USER_NAME);
        editor.remove(PREF_USER_EMAIL);
        editor.remove(PREF_USER_PASSWORD);
        editor.remove (PREF_ONBOARDING_SET);
        editor.apply();
    }

    public static void setOnBoarding(Context context, boolean status){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_ONBOARDING_SET, status);
        editor.apply();
    }

    public static boolean getOnBoardingStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_ONBOARDING_SET, false);
    }

    public static void saveCartIds(Context context, String cart_ids){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_CART_IDS, cart_ids);
        editor.apply();
    }

    @Nullable
    public static String getUserCartIds(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_CART_IDS, "");
    }

    public static void removeCartIds(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_USER_CART_IDS);
        editor.apply();
    }


    public static void setCartStatus(Context context, boolean status){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_USER_CART_STATUS, status);
        editor.apply();
    }

    public static boolean getCartStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_USER_CART_STATUS, false);
    }

    public static void removeCartStatus(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_USER_CART_STATUS);
        editor.apply();
    }

    public static void setReloadPageStatus(Context context, boolean status){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_RELOAD_PAGE, status);
        editor.apply();
    }

    public static boolean getReloadPageStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_RELOAD_PAGE, false);
    }

    public static void removeLoadPageStatus(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_RELOAD_PAGE);
        editor.apply();
    }

    public static void setCartQty(Context context, int cart_qty){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PREF_CART_QTY, cart_qty);
        editor.apply();
    }

    public static int getCartQty(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt(PREF_CART_QTY, 0);
    }

    public static void removeCartQty(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_CART_QTY);
        editor.apply();
    }

}
