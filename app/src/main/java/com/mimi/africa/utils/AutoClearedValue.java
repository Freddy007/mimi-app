package com.mimi.africa.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * A value holder that automatically clears the reference if the Fragment's view is destroyed.
 * @param <T>
 */
public class AutoClearedValue<T> {

    @Nullable
    private T value;

    public AutoClearedValue(@NonNull Fragment fragment, T value) {
        FragmentManager fragmentManager = fragment.getFragmentManager();
        if(fragmentManager != null) {
            fragmentManager.registerFragmentLifecycleCallbacks(
                    new FragmentManager.FragmentLifecycleCallbacks() {
                        @Override
                        public void onFragmentViewDestroyed(FragmentManager fm, Fragment f) {
                            if (f == fragment) {
                                AutoClearedValue.this.value = null;

                                fragmentManager.unregisterFragmentLifecycleCallbacks(this);

                            }
                        }
                    }, false);
        }
        this.value = value;
    }

    @Nullable
    public T get() {
        return value;
    }

}
