package com.mimi.africa.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemVerticalListBinding;
import com.mimi.africa.event.LoadMoreProducts;
import com.mimi.africa.event.LoadPreviousProducts;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Notification;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ProductVerticalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Inventory> items = new ArrayList<>();
    private ItemVerticalListBinding itemVerticalListBinding;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public ProductVerticalListAdapter(Context context, List<Inventory> items) {
        this.items = items;
        ctx = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        itemVerticalListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_vertical_list, parent, false);
        final View itemView = itemVerticalListBinding.getRoot();
        vh = new OriginalViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            final Inventory p = items.get(position);
            view.title.setText(p.getTitle());

//            view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(p.getSalePrice()))));

            if ( p.getOfferPrice() != null){
                view.originalPrice.setVisibility(View.VISIBLE);
                view.originalPrice.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(p.getOfferPrice()))));
                view.price.setPaintFlags(view.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(p.getSalePrice()))));

            }else {
                view.originalPrice.setVisibility(View.GONE);
                view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(p.getSalePrice()))));
            }

            if (p.getShop() != null) {
                view.shopNameTextView.setText(p.getShop().getName());
            }

            if (p.getStockQuantity() == 0 || p.getStockQuantity() < 0) {
                view.isSoldTextView.setVisibility(View.VISIBLE);
            }

            String dateCreatedString;
            if (p.getCreatedAt() != null && p.getCreatedAt().getDate() != null) {
                dateCreatedString = p.getCreatedAt().getDate();
            }else {
                dateCreatedString = p.getCreated();
            }

            if (dateCreatedString != null) {
                long milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);

                String timeAgo = TimeAgo.getTimeAgo(milliseconds);

                view.addedDateStrTextView.setText(timeAgo);
            }

            view.ratingBar.setRating((float) p.getRating());

            view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));

            if (p.getImage() != null && p.getImage().getPath() != null ){
                if (p.getImage().getPath() != null) {
                    if (p.getImage().getPath().startsWith("http")){

                        Picasso.get().load(p.getImage().getPath()).into(view.productImageView);
//                        Picasso.with(ctx).load(p.getImage().getPath()).into(view.productImageView);
                    }else {
                        String imageUrl = Constants.IMAGES_BASE_URL +  p.getImage().getPath();
                        Picasso.get().load(imageUrl).into(view.productImageView);
                    }
                }
            }else {
                view.productImageView.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
            }

//            if (p.getImages() != null && p.getImages().size() > 0) {
//                String url = Constants.IMAGES_BASE_URL + p.getImages().get(0).getPath();
//                Utils.LoadImage(ctx, view.productImageView, url );
//            }else{
//                view.productImageView.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
//            }

            view.addressTextView.setText(p.getCondition());

            if (p.getShop() != null ) {
                if (p.getShop().getOwner() != null && p.getShop().getOwner().getImage() != null) {
                    String url = Constants.IMAGES_BASE_URL + p.getShop().getOwner().getImage().getPath();
                    Utils.LoadImage(ctx, view.profileCircleImageView, url);
                }
            }

            view.productImageView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.productImageView, items.get(position), position);
                }
            });

            view.addressTextView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.addressTextView, items.get(position), position);
                }
            });

            view.price.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.price, items.get(position), position);
                }
            });

            view.shopNameTextView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.shopNameTextView, items.get(position), position);
                }
            });

            view.productImageView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.productImageView, items.get(position), position);
                }
            });

            view.productImageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemLongClick(view.productImageView, items.get(position), position);
                    }
                    return true;
                }
            });
        }

        if ((position == items.size() -1)){
            EventBus.getDefault().post(new LoadMoreProducts());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Inventory obj, int pos);

        void onItemLongClick(View view, Inventory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Inventory obj, MenuItem item);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public CardView imageCardView;
        public TextView shopNameTextView;
        public ImageView image;
        public ImageView productImageView;
        public TextView title;
        public TextView price;
        public TextView originalPrice;
        public TextView addressTextView;
        public TextView isSoldTextView;
        public TextView addedDateStrTextView;
        public ImageView profileCircleImageView;
        public ImageButton more;
        public View lyt_parent;
        public AppCompatRatingBar ratingBar;
        public TextView feedbackSum;

        public OriginalViewHolder(@NonNull View v) {
            super(v);

            cardView = itemVerticalListBinding.cardView;
            shopNameTextView = itemVerticalListBinding.nameTextView;
            imageCardView = itemVerticalListBinding.cardView12;
            image = itemVerticalListBinding.imageView2;
            productImageView = itemVerticalListBinding.itemImageView;
            title = itemVerticalListBinding.titleTextView;
            price = itemVerticalListBinding.priceTextView;
            addressTextView = itemVerticalListBinding.addressTextView;
            isSoldTextView = itemVerticalListBinding.isSoldTextView;
            addedDateStrTextView = itemVerticalListBinding.addedDateStrTextView;
            profileCircleImageView = itemVerticalListBinding.profileCircleImageView;
            originalPrice                             = itemVerticalListBinding.originalPriceTextView;
            ratingBar                                     = itemVerticalListBinding.rating;
            feedbackSum                            = itemVerticalListBinding.feedbackSum;
        }
    }

}