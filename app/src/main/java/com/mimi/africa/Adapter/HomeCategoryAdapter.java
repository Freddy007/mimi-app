package com.mimi.africa.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.CategoryItemHorizontalBinding;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class HomeCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private CategoryItemHorizontalBinding categoryItemHorizontalBinding;
    private List<ProductCategory> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public HomeCategoryAdapter(Context context, List<ProductCategory> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout constraintLayout;
        public CardView imageCardView;
        public TextView nameTextView;
        public ImageView image;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            constraintLayout = categoryItemHorizontalBinding.constraintLayout;
            nameTextView = categoryItemHorizontalBinding.categoryText;
//            imageCardView = categoryItemHorizontalBinding.cardView12;
            image = categoryItemHorizontalBinding.itemCategoryImageView;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        categoryItemHorizontalBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.category_item_horizontal, parent, false);
        final View itemView = categoryItemHorizontalBinding.getRoot();
        vh = new OriginalViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final ProductCategory p = items.get(position);
            view.nameTextView.setText(p.getName());

            switch (p.getName()){

                case Constants.CATEGORIES:
                    view.image.setImageResource((R.drawable.ic_devices_other_black_24dp));
                    break;

                case Constants.HEALTH:
                    view.image.setImageResource((R.drawable.ic_local_hospital_black_24dp));
                    break;

                case Constants.SERVICES:
                    view.image.setImageResource((R.drawable.ic_local_laundry_service_black_24dp));
                    break;

                case Constants.STORES:
                    view.image.setImageResource((R.drawable.ic_store_black_24dp));
                    break;

            }

            view.image.setOnClickListener((View v) -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.image, items.get(position), position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, ProductCategory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, ProductCategory obj, MenuItem item);
    }

}