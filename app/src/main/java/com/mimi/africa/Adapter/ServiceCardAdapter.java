package com.mimi.africa.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.databinding.ItemHorizontalBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Service;
import com.mimi.africa.model.Wishlist;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ServiceCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ServiceCardAdapter.class.getSimpleName();
    private ItemHorizontalBinding itemHorizontalBinding;
    private List<Service> items = new ArrayList<>();
    private String mCustomerId;

    private Context ctx;
    private SharedPreferences sharedPreferences;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public ServiceCardAdapter(Context context, List<Service> items) {
        this.items = items;
        ctx = context;

        if (context != null ) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
            mCustomerId = sharedPreferences.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        }
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView imageCardView;
        public ImageView serviceImageView;
        public TextView shopNameTextView;
        public ImageView locationImage;
        public TextView title;
        public TextView price;
        public TextView addressTextView;
        public AppCompatRatingBar ratingBar;
        public TextView feedBackSum;
        public ImageButton more;
        public TextView dateTextView;
        public AppCompatImageView profileCircleImageView;
        public AppCompatImageView favoriteImageView;
        public View lyt_parent;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            serviceImageView = itemHorizontalBinding.serviceImageView;
            shopNameTextView = itemHorizontalBinding.nameTextView;
            imageCardView = itemHorizontalBinding.cardView12;
            locationImage = itemHorizontalBinding.imageView2;
            title = itemHorizontalBinding.titleTextView;
            price = itemHorizontalBinding.priceTextView;
            addressTextView = itemHorizontalBinding.addressTextView;
            dateTextView = itemHorizontalBinding.addedDateStrTextView;
            profileCircleImageView = itemHorizontalBinding.profileCircleImageView;
            ratingBar                               = itemHorizontalBinding.rating;
            feedBackSum  = itemHorizontalBinding.feedbackSum;
            favoriteImageView                 = itemHorizontalBinding.favoriteImageView;

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        itemHorizontalBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_horizontal, parent, false);
        final View itemView = itemHorizontalBinding.getRoot();
        vh = new OriginalViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final Service service = items.get(position);
            view.title.setText(service.getName());
            try{
                view.price.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(service.getPrice()))));
            }catch (Exception e){
                Log.e(TAG, "onBindViewHolder: " );
            }

            if (service.getShop() != null) {
                view.shopNameTextView.setText(service.getShop().getName());
            }

            if (service.getImages().size() > 0) {
                String imageUrl = Constants.IMAGES_BASE_URL + service.getImages().get(0).getPath();
                Log.i("ServiceCardAdapter ", "onNext onBindViewHolder:  " + imageUrl);
                Picasso.get().load(imageUrl).into(view.serviceImageView);
//                Picasso.with(ctx).load(imageUrl).into(view.serviceImageView);
            }

            String dateCreatedString = null;

            if (service.getCreatedAt() != null) {
                dateCreatedString = service.getCreatedAt().getDate();
            }

            long milliseconds = 0;
            if (dateCreatedString != null) {
                milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);

                String timeAgo = TimeAgo.getTimeAgo(milliseconds);
                view.dateTextView.setText(timeAgo);
            }

            if (service.getShop().getOwner() != null && service.getShop().getOwner().getImage() != null) {
                String url = Constants.IMAGES_BASE_URL + service.getShop().getOwner().getImage().getPath();

//                Picasso.with(ctx).load(url).transform(new CircleTransform()).into(view.profileCircleImageView);
                Picasso.get().load(url).transform(new CircleTransform()).into(view.profileCircleImageView);

                Log.i(TAG, "onBindViewHolder:  Service Owner " + url);
            }

            view.ratingBar.setRating((float) service.getRating());

            view.feedBackSum.setText(String.format(" ( %d ) ", service.getSumFeedbacks()));

            view.imageCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.imageCardView, items.get(position), position);
                 }
                }
            });

            view.addressTextView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.addressTextView, items.get(position), position);
                }
            });

            view.locationImage.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.locationImage, items.get(position), position);
                }
            });


            view.price.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.price, items.get(position), position);
                }
            });

            view.shopNameTextView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.shopNameTextView, items.get(position), position);
                }
            });

            boolean hasFavourite = isHasFavourite(service);

            if (isHasFavourite(service)){
                view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
            }

            view.favoriteImageView.setOnClickListener(v -> {
                if (hasFavourite) {
                    view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
                    Utils.ShowSnackBar((Activity) ctx, "Already added to favourites!", Snackbar.LENGTH_SHORT);
                }else  {
                    view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_off));
                    mOnItemClickListener.onItemClick(view.favoriteImageView, items.get(position), position);
                }

                if (mOnItemClickListener != null) {
                    if (hasFavourite) {
                        view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
                        Utils.ShowSnackBar((Activity) ctx, "Already added to favourites!", Snackbar.LENGTH_SHORT);
                    }else  {
                        view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_off));
                        mOnItemClickListener.onItemClick(view.favoriteImageView, items.get(position), position);
//                        view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));
                    }
                }
            });

//            view.price.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mOnItemClickListener != null) {
//                        mOnItemClickListener.onItemClick(view.price, items.get(position), position);
//                    }
//                }
//            });

//            Picasso.with(ctx).load(Uri.parse("https://via.placeholder.com/300x300.png?text=noImage"))
//                      .into(view.locationImage);

//            view.lyt_parent.setOnClickListener(view12 -> {
//                if (mOnItemClickListener != null) {
//                    mOnItemClickListener.onItemClick(view12, items.get(position), position);
//                }
//            });

//            view.more.setOnClickListener(view1 -> {
//                if (onMoreButtonClickListener == null) return;
//                onMoreButtonClick(view1, p);
//            });

        }
    }

    private boolean isHasFavourite(Service service) {
        boolean hasFavourite = false;

        if (service.getWishlists() != null) {
            for (Wishlist wishlist : service.getWishlists()){
                if (wishlist.getCustomerId().equals(mCustomerId)
                        && wishlist.getProductId().equals(String.valueOf(service.getId()))) {
                    hasFavourite = true;
                    break;
                }
            }
        }
        return hasFavourite;
    }

    private void onMoreButtonClick(final View view, final Inventory p) {
        PopupMenu popupMenu = new PopupMenu(ctx, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onMoreButtonClickListener.onItemClick(view, p, item);
                return true;
            }
        });
        popupMenu.inflate(R.menu.menu_product_more);
        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        return items.size();
//        return 3;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Service obj, int pos);

    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Inventory obj, MenuItem item);
    }

}