package com.mimi.africa.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.event.ShopMoreClickedEvent;
import com.mimi.africa.model.Shop;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.LocationUtilService;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ShopCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Shop> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public ShopCardAdapter(Context context, List<Shop> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView price;
        public AppCompatRatingBar ratingBar;
        public TextView feedBackSum;
        public TextView storeLocation;
        public TextView storeDistance;
        public ImageButton more;
        public View lyt_parent;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            image = v.findViewById(R.id.image);
            title = v.findViewById(R.id.title);
            ratingBar = v.findViewById(R.id.rating_bar);
            feedBackSum = v.findViewById(R.id.feedback_sum);
            storeLocation = v.findViewById(R.id.store_location);
            storeDistance = v.findViewById(R.id.store_distance);
            more                  = v.findViewById(R.id.more);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_card, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final Shop p = items.get(position);

            view.title.setText(p.getName());

            if (p.getAddress() != null){
                view.storeLocation.setText(p.getAddress());
            }else {
                view.storeLocation.setText("");
            }

            if (p.getLat() != null){
                String latitude = p.getLat();
                String longitude = p.getLng();

                if (LocationPreferences.isLocationLatLonAvailable(ctx)){

                    double latitude_dbl = Double.parseDouble(latitude);
                    double longitude_dbl = Double.parseDouble(longitude);

                    android.location.Location endLocation = new android.location.Location("New Location");
                    endLocation.setLatitude(latitude_dbl);
                    endLocation.setLongitude(longitude_dbl);

                    double locationServiceLastLocation = LocationUtilService.getDistanceInKm(ctx, endLocation);

                view.storeDistance.
                        setText(String.format("%s km away", String.format("%.2f", locationServiceLastLocation)));
                }
                }else{
                view.storeDistance.setText("N/A");

            }

                String url = null;

                if (p.getImages() != null && p.getImages().size() >0 ){
                    url = Constants.IMAGES_BASE_URL+ p.getImages().get(0).getPath();
                    Utils.LoadImage(ctx, view.image, url);
                }else {
                    Utils.LoadImage(ctx, view.image, p.getBannerImage());
                }

                view.ratingBar.setRating((float) p.getRating());

            view.feedBackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));

            view.image.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, items.get(position), position);
                }
            });

            view.title.setOnClickListener(v -> {
                                    if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, items.get(position), position);
                }
            });


//            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mOnItemClickListener != null) {
//                        mOnItemClickListener.onItemClick(view, items.get(position), position);
//                    }
//                }
//            });

            view.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new ShopMoreClickedEvent(p));
                }
            });
        }
    }

//    private void onMoreButtonClick(final View view, final ShopProduct p) {
//        PopupMenu popupMenu = new PopupMenu(ctx, view);
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                onMoreButtonClickListener.onItemClick(view, p, item);
//                return true;
//            }
//        });
//        popupMenu.inflate(R.menu.menu_product_more);
//        popupMenu.show();
//    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Shop obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Shop obj, MenuItem item);
    }

}