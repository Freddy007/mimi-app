package com.mimi.africa.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.Order;
import com.mimi.africa.model.ShopProduct;

import java.util.ArrayList;
import java.util.List;

public class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Order> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public OrderHistoryAdapter(Context context, List<Order> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView cardContainer;
        public ImageView image;
        public TextView title;
        public TextView dateCreatedTextView;
        public ImageButton more;
        public View lyt_parent;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
//            locationImage = v.findViewById(R.id.locationImage);

            cardContainer = v.findViewById(R.id.card_container);
            title = v.findViewById(R.id.historyNameTextView);
            dateCreatedTextView = v.findViewById(R.id.dateTextView);
//            price = v.findViewById(R.id.price);
//            more = v.findViewById(R.id.more);
//            lyt_parent = v.findViewById(R.id.lyt_parent);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final Order p = items.get(position);

            String statusText = p.getStatus().getName();

            view.title.setText(String.format("Order %s", p.getOrderNumber() + " ( " + statusText + "  )"));

            switch (statusText){
                case "Confirmed":
                    view.title.setBackgroundColor(ctx.getResources().getColor(R.color.amber_50));
                    break;

                case "Fulfilled":
                    view.title.setBackgroundColor(ctx.getResources().getColor(R.color.fulfilled_color));
                    break;

                    default:
                        view.title.setBackgroundColor(ctx.getResources().getColor(R.color.delivered_returned_color));

            }

            if (p.getInventories() != null) {
                if (p.getInventories().get(0).getPivot() != null) {
                    view.dateCreatedTextView.setText(p.getInventories().get(0).getPivot().getCreatedAt());
                }
            }

//            view.title.setText(p.title);
//            view.price.setText(p.price);
//            Tools.displayImageOriginal(ctx, view.locationImage, p.locationImage);

            view.cardContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                    }
                }
            });

//            view.more.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (onMoreButtonClickListener == null) return;
//                    onMoreButtonClick(view, p);
//                }
//            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
//        return 3;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Order obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, ShopProduct obj, MenuItem item);
    }

}