//package com.mimi.mimi.Adapter;
//
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.databinding.DataBindingUtil;
//
//import com.mimi.mimi.R;
//import com.mimi.mimi.databinding.ItemHorizontalWithStoreBinding;
//import com.mimi.mimi.model.Inventory;
//import com.mimi.mimi.ui.common.DataBoundListAdapter;
//import com.mimi.mimi.ui.common.DataBoundViewHolder;
//import com.mimi.mimi.utils.Config;
//import com.mimi.mimi.utils.Utils;
//
//import java.util.Objects;
//
//
//public class ItemHorizontalListAdapter extends DataBoundListAdapter<Inventory, ItemHorizontalWithStoreBinding> {
//
//    private final androidx.databinding.DataBindingComponent dataBindingComponent;
//    private final ItemHorizontalListAdapter.NewsClickCallback callback;
//    private DataBoundListAdapter.DiffUtilDispatchedInterface diffUtilDispatchedInterface;
//
//    public ItemHorizontalListAdapter(androidx.databinding.DataBindingComponent dataBindingComponent,
//                                     ItemHorizontalListAdapter.NewsClickCallback callback,
//                                     DiffUtilDispatchedInterface diffUtilDispatchedInterface) {
//        this.dataBindingComponent = dataBindingComponent;
//        this.callback = callback;
//        this.diffUtilDispatchedInterface = diffUtilDispatchedInterface;
//    }
//
//    @Override
//    protected ItemHorizontalWithStoreBinding createBinding(@NonNull ViewGroup parent) {
//        ItemHorizontalWithStoreBinding binding = DataBindingUtil
//                .inflate(LayoutInflater.from(parent.getContext()),
//                        R.layout.item_horizontal_with_store, parent, false,
//                        dataBindingComponent);
//        binding.getRoot().setOnClickListener(v -> {
//            Inventory item = binding.getItem();
//            if (item != null && callback != null) {
//                callback.onClick(item);
//            }
//        });
//        return binding;
//
//
//    }
//
//
//    @Override
//    public void bindView(DataBoundViewHolder<ItemHorizontalWithStoreBinding> holder, int position) {
//        super.bindView(holder, position);
//    }
//
//    @Override
//    protected void dispatched() {
//        if (diffUtilDispatchedInterface != null) {
//            diffUtilDispatchedInterface.onDispatched();
//        }
//    }
//
//    @Override
//    protected void bind(@NonNull ItemHorizontalWithStoreBinding binding, @NonNull Inventory item) {
//
//        binding.setItem(item);
//
//        binding.conditionTextView.setText(binding.getRoot().getResources().getString(R.string.item_condition__type, item.getCondition()));
////        String currencySymbol = item.itemCurrency.currencySymbol;
//        String currencySymbol = "GHS";
//        String price;
//        try {
//            price = Utils.format(Double.parseDouble(item.getSalePrice()));
//        } catch (Exception e) {
//            price = item.getSalePrice();
//        }
//
//        String currencyPrice;
//        if (Config.SYMBOL_SHOW_FRONT) {
//            currencyPrice = currencySymbol + " " + price;
//        } else {
//            currencyPrice = price + " " + currencySymbol;
//        }
//        binding.priceTextView.setText(currencyPrice);
//
//        if (item.isSoldOut()) {
//            binding.isSoldTextView.setVisibility(View.VISIBLE);
//        } else {
//            binding.isSoldTextView.setVisibility(View.GONE);
//        }
//
////        if (item.priceUnit.equals("")){
////            binding.lakhTextView.setVisibility(View.GONE);
////        } else{
////            binding.lakhTextView.setVisibility(View.VISIBLE);
////            binding.lakhTextView.setText(item.priceUnit);
////        }
//    }
//
//    @Override
//    protected boolean areItemsTheSame(@NonNull Inventory oldItem, @NonNull Inventory newItem) {
//        return Objects.equals(oldItem.getId(), newItem.getId());
////                && oldItem.title.equals(newItem.title)
////                && oldItem.isFavourited.equals(newItem.isFavourited)
////                && oldItem.favouriteCount.equals(newItem.favouriteCount)
////                && oldItem.itemCondition.name.equals(newItem.itemCondition.name)
////                && oldItem.isSoldOut.equals(newItem.isSoldOut);
//    }
//
//    @Override
//    protected boolean areContentsTheSame(@NonNull Inventory oldItem, @NonNull Inventory newItem) {
//        return Objects.equals(oldItem.getId(), newItem.getId());
////                && oldItem.getTitle().equals(newItem.getTitle())
////                && oldItem.isFavourited.equals(newItem.isFavourited)
////                && oldItem.favouriteCount.equals(newItem.favouriteCount)
////                && oldItem.getCondition().equals(newItem.getCondition())
////                && oldItem.isSoldOut().equals(newItem.isSoldOut());
//    }
//
//    public interface NewsClickCallback {
//        void onClick(Inventory item);
//    }
//
//
//}
//
//
