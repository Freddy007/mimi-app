package com.mimi.africa.Adapter;

        import android.content.Context;
        import android.util.Log;
        import android.util.SparseBooleanArray;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.RatingBar;
        import android.widget.TextView;

        import androidx.recyclerview.widget.RecyclerView;


        import com.mimi.africa.R;
        import com.mimi.africa.model.Feedback;
        import com.mimi.africa.utils.TimeAgo;
        import com.mimi.africa.utils.Utils;

        import java.util.ArrayList;
        import java.util.List;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    private static final String TAG = FeedbackAdapter.class.getSimpleName();
    private Context ctx;
    private List<Feedback> items;
    private FeedbackAdapter.OnClickListener onClickListener = null;

    private SparseBooleanArray selected_items;
    private int current_selected_idx = -1;

    public void setOnClickListener(FeedbackAdapter.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnItemClickListener(Feedback feedback, FeedbackAdapter.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, review, time, date;
        public RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            review = view.findViewById(R.id.review);
            time    = view.findViewById(R.id.time);
            ratingBar = view.findViewById(R.id.ratingBar);

        }
    }

    public FeedbackAdapter(Context mContext, List<Feedback> items) {
        this.ctx = mContext;
        this.items = items;
        selected_items = new SparseBooleanArray();
    }

    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false);
        return new FeedbackAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FeedbackAdapter.ViewHolder holder, final int position) {
        final Feedback feedback = items.get(position);

        if (feedback.getCustomer() != null) {
            holder.name.setText(feedback.getCustomer().getName() + " ( Verified Purchase )");
        }

        holder.review.setText(feedback.getComment());

        holder.ratingBar.setRating((float) feedback.getRating());

        String dateCreatedString = null;
        if (feedback.getCreatedAt() != null) {
            dateCreatedString = feedback.getCreatedAt().getDate();
        }
        Log.i(TAG, "onBindViewHolder: feedback date:  " + dateCreatedString + "  " + feedback.getCreatedAt());

        long milliseconds = 0;
        if (dateCreatedString != null) {
            milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);
            Log.i(TAG, "onBindViewHolder: feedback date:  " + milliseconds);

        }

        String timeAgo = TimeAgo.getTimeAgo(milliseconds);

        holder.time.setText(timeAgo);

//        if (feedback.getCustomer() != null) {
//            holder.image_letter.setText(feedback.getCustomer().getName().substring(0, 1));
//        }

//        holder.lyt_parent.setActivated(selected_items.get(position, false));

//        holder.lyt_parent.setOnClickListener(v -> {
//            if (onClickListener == null) return;
//            onClickListener.onItemClick(v, feedback, position);
//        });

//        holder.lyt_parent.setOnLongClickListener(v -> {
//            if (onClickListener == null) return false;
//            onClickListener.onItemLongClick(v, feedback, position);
//            return true;
//        });

//        toggleCheckedIcon(holder, position);
//        displayImage(holder, feedback);

    }

    private void displayImage(FeedbackAdapter.ViewHolder holder, Feedback feedback) {
//        if (feedback.locationImage != null) {
//            Tools.displayImageRound(ctx, holder.locationImage, inbox.locationImage);
//            holder.locationImage.setColorFilter(null);
//            holder.image_letter.setVisibility(View.GONE);
//        } else {
//            holder.locationImage.setImageResource(R.drawable.shape_circle);
//            holder.locationImage.setColorFilter(ctx.getResources().getColor(R.color.blue_800));
//            holder.image_letter.setVisibility(View.VISIBLE);
//        }
    }

    private void toggleCheckedIcon(FeedbackAdapter.ViewHolder holder, int position) {
        if (selected_items.get(position, false)) {
//            holder.lyt_image.setVisibility(View.GONE);
//            holder.lyt_checked.setVisibility(View.VISIBLE);
            if (current_selected_idx == position) resetCurrentIndex();
        } else {
//            holder.lyt_checked.setVisibility(View.GONE);
//            holder.lyt_image.setVisibility(View.VISIBLE);
            if (current_selected_idx == position) resetCurrentIndex();
        }
    }

    public Feedback getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void toggleSelection(int pos) {
        current_selected_idx = pos;
        if (selected_items.get(pos, false)) {
            selected_items.delete(pos);
        } else {
            selected_items.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selected_items.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selected_items.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selected_items.size());
        for (int i = 0; i < selected_items.size(); i++) {
            items.add(selected_items.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        items.remove(position);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        current_selected_idx = -1;
    }

    public interface OnClickListener {
        void onItemClick(View view, Feedback obj, int pos);

        void onItemLongClick(View view, Feedback obj, int pos);
    }
}