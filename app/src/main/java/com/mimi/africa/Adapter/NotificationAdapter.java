package com.mimi.africa.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.notifications.NotificationDetailActivity;
import com.mimi.africa.utils.ItemAnimation;
import com.mimi.africa.utils.TimeAgo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<Notification> items = new ArrayList<>();
    private List<Notification> itemsFiltered;

    public NotificationAdapter(List<Notification> items) {
        this.items = items;
    }

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private int animation_type = 0;

    public interface OnItemClickListener {
        void onItemClick(View view, Notification obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public NotificationAdapter(Context context, List<Notification> items, int animation_type) {
        this.items = items;
        ctx = context;
        this.animation_type = animation_type;
        this.itemsFiltered = items;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView description;
        public TextView descriptionBold;
        public TextView dateTextView;
        public View lyt_parent;
        public CardView cardView;

        public OriginalViewHolder(View v) {
            super(v);
//            locationImage = v.findViewById(R.id.locationImage);
//            name = v.findViewById(R.id.name);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            description = v.findViewById(R.id.description_normal);
            descriptionBold = v.findViewById(R.id.description_bold);
            dateTextView = v.findViewById(R.id.date);
            cardView  = v.findViewById(R.id.card_view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            Notification p = items.get(position);
//            view.name.setText(position + " | " + p.getName());
//            view.name.setText(p.getName());


            if (p.isRead()){
                view.description.setVisibility(View.VISIBLE);
                view.description.setText((p.getMessage()));
                view.description.setTextColor(ctx.getResources().getColor(R.color.grey_60));
                view.descriptionBold.setVisibility(View.GONE);
            }else {
                view.descriptionBold.setVisibility(View.VISIBLE);
                view.descriptionBold.setText((p.getMessage()));
                view.description.setVisibility(View.GONE);
            }

            long date = p.getDateCreated();

            String initialDateString = new SimpleDateFormat("dd/MM/yyyy").format(new Date(p.getDateCreated()));

            String timeAgo = TimeAgo.getTimeAgo(date);

            view.dateTextView.setText(String.valueOf(timeAgo));

            ((OriginalViewHolder) holder).cardView.setOnClickListener(v -> {
                Intent notificationIntent = new Intent(ctx, NotificationDetailActivity.class);
                notificationIntent.putExtra("id", p.getId());
                notificationIntent.putExtra("title", p.getName());
                notificationIntent.putExtra("content", p.getMessage());
                notificationIntent.putExtra("has_image", p.isHasImage());
                notificationIntent.putExtra("image", p.getImage());
                notificationIntent.putExtra("link", p.getLink());
                notificationIntent.putExtra("has_link", p.isHasLink());

                ctx.startActivity(notificationIntent);
            });

//            setAnimation(view.itemView, position);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private int lastPosition = -1;
    private boolean on_attach = true;

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, animation_type);
            lastPosition = position;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemsFiltered = items;
                } else {
                    List<Notification> filteredList = new ArrayList<>();
                    for (Notification row : items) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    itemsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsFiltered = (ArrayList<Notification>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}