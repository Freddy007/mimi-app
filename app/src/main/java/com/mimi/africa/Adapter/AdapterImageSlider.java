package com.mimi.africa.Adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mimi.africa.R;
import com.mimi.africa.model.Image;
import com.mimi.africa.utils.Constants;

import java.util.List;

public  class AdapterImageSlider extends PagerAdapter {

        private Activity act;
        private List<Image> items;

        public AdapterImageSlider.OnItemClickListener onItemClickListener;

        private interface OnItemClickListener {
            void onItemClick(View view, Image obj);
        }

        public void setOnItemClickListener(AdapterImageSlider.OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        // constructor
        public AdapterImageSlider(Activity activity, List<Image> items) {
            this.act = activity;
            this.items = items;
        }

        @Override
        public int getCount() {
            return this.items.size();
        }

        public Image getItem(int pos) {
            return items.get(pos);
        }

        public void setItems(List<Image> items) {
            this.items = items;
            notifyDataSetChanged();
        }

      public void setImageItems(List<Image> items) {
            this.items = items;
            notifyDataSetChanged();
      }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            final Image image = items.get(position);
            LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.item_slider_image, container, false);

            ImageView imageView = v.findViewById(R.id.image);
            MaterialRippleLayout lyt_parent = v.findViewById(R.id.lyt_parent);

            String imageUrl = Constants.IMAGES_BASE_URL + image.getPath();

              Glide.with(act).load(Uri.parse(String.format("%s?p=medium", imageUrl)))
//                      .crossFade()
                      .diskCacheStrategy(DiskCacheStrategy.ALL)
                      .into(imageView);

              lyt_parent.setOnClickListener(v1 -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v1, image);
                }
            });

            container.addView(v);

            return v;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }