package com.mimi.africa.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.databinding.ItemHorizontalBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Product;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull
    private String TAG = ProductCardAdapter.class.getSimpleName();

    private ItemHorizontalBinding itemHorizontalBinding;
    private List<Product> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;
    private APIService mAPIService;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public ProductCardAdapter(Context context, List<Product> items) {
        this.items = items;
        ctx = context;
       mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);

    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout itemConstraintLayout;
        public CardView imageCardView;
        public TextView shopNameTextView;
        public ImageView image;
        public ImageView productImageView;
        public TextView title;
        public TextView price;
        public TextView originalPrice;
        public TextView addressTextView;
        public TextView isSoldTextView;
        public TextView addedDateStrTextView;
        public ImageButton more;
        public View lyt_parent;
        public AppCompatImageView profileCircleImageView;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            shopNameTextView = itemHorizontalBinding.nameTextView;
            imageCardView = itemHorizontalBinding.cardView12;
            image = itemHorizontalBinding.imageView2;
            productImageView = itemHorizontalBinding.serviceImageView;
            title = itemHorizontalBinding.titleTextView;
            price = itemHorizontalBinding.priceTextView;
            addressTextView = itemHorizontalBinding.addressTextView;
            isSoldTextView = itemHorizontalBinding.isSoldTextView;
            addedDateStrTextView = itemHorizontalBinding.addedDateStrTextView;
            itemConstraintLayout = itemHorizontalBinding.itemConstraintLayout;
            profileCircleImageView = itemHorizontalBinding.profileCircleImageView;
            originalPrice                             = itemHorizontalBinding.originalPriceTextView;

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        itemHorizontalBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_horizontal, parent, false);
        final View itemView = itemHorizontalBinding.getRoot();
        vh = new OriginalViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            Product product = items.get(position);

            if (product.getInventories() != null && product.getInventories().size() > 0) {

                view.itemConstraintLayout.setVisibility(View.VISIBLE);

                Inventory inventory = product.getInventories().get(0);

                view.title.setText(inventory.getTitle());

                if ( inventory.getOfferPrice() != null){

                    view.originalPrice.setVisibility(View.VISIBLE);
                    view.originalPrice.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getOfferPrice()))));

                    view.price.setPaintFlags(view.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));

                }else {
                    view.originalPrice.setVisibility(View.GONE);
                    view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));
                }

                if (inventory.getShop() != null) {
                    view.shopNameTextView.setText(inventory.getShop().getName());

                    if (inventory.getShop().getOwner() != null && inventory.getShop().getOwner().getImage() != null) {
                        String url = Constants.IMAGES_BASE_URL + inventory.getShop().getOwner().getImage().getPath();

                        Picasso.get().load(url).transform(new CircleTransform()).into(view.profileCircleImageView);
//                        Picasso.with(ctx).load(url).transform(new CircleTransform()).into(view.profileCircleImageView);

                    }
                }

                if (inventory.getImages() != null && inventory.getImages().size() > 0) {
                    String url = Constants.IMAGES_BASE_URL + inventory.getImages().get(0).getPath();
                    Utils.LoadImage(ctx, view.productImageView, url);
                }

                if (inventory.getStockQuantity() > 0) {
                    view.isSoldTextView.setVisibility(View.GONE);
                } else {
                    view.isSoldTextView.setVisibility(View.VISIBLE);
                }

                view.addressTextView.setText(inventory.getCondition());

                String dateCreatedString = inventory.getCreated();

                long milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);

                String timeAgo = TimeAgo.getTimeAgo(milliseconds);

                view.addedDateStrTextView.setText(timeAgo);

                view.imageCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onItemClick(view.imageCardView, items.get(position).getInventories().get(0), position);
                        }
                    }
                });

                view.addressTextView.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view.addressTextView, items.get(position).getInventories().get(0), position);
                    }
                });

                view.price.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view.price, items.get(position).getInventories().get(0), position);
                    }
                });

                view.shopNameTextView.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view.shopNameTextView, items.get(position).getInventories().get(0), position);
                    }
                });

            }else {

//                view.itemConstraintLayout.setVisibility(View.GONE);
            }
        }
    }

    private void onMoreButtonClick(final View view, final Inventory p) {
        PopupMenu popupMenu = new PopupMenu(ctx, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onMoreButtonClickListener.onItemClick(view, p, item);
                return true;
            }
        });
        popupMenu.inflate(R.menu.menu_product_more);
        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        return items.size();
//        return 3;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Inventory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Inventory obj, MenuItem item);
    }

}