package com.mimi.africa.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.databinding.ItemHorizontalBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Wishlist;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ShopProductGridCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull
    private String TAG = ShopProductGridCardAdapter.class.getSimpleName();

    private ItemHorizontalBinding itemHorizontalBinding;
    private List<Inventory> items = new ArrayList<>();
    private String mCustomerId;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;
    private APIService mAPIService;
    private SharedPreferences sharedPreferences;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public ShopProductGridCardAdapter(Context context, List<Inventory> items) {
        this.items = items;
        ctx = context;
        if (context != null ) {
              sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
              mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
              mCustomerId = sharedPreferences.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        }

    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView imageCardView;
        public TextView shopNameTextView;
        public ImageView locationImage;
        public ImageView productImageView;
        public TextView title;
        public TextView price;
        public TextView originalPrice;
        public TextView addressTextView;
        public TextView isSoldTextView;
        public TextView addedDateStrTextView;
        public AppCompatImageView profileCircleImageView;
        public AppCompatImageView favoriteImageView;
        public TextView favCountTextView;
        public AppCompatRatingBar ratingBar;
        public TextView feedbackSum;
        public ImageButton more;
        public View lyt_parent;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            shopNameTextView = itemHorizontalBinding.nameTextView;
            imageCardView = itemHorizontalBinding.cardView12;
            locationImage = itemHorizontalBinding.imageView2;
            productImageView = itemHorizontalBinding.serviceImageView;
            title = itemHorizontalBinding.titleTextView;
            price = itemHorizontalBinding.priceTextView;
            addressTextView = itemHorizontalBinding.addressTextView;
            isSoldTextView = itemHorizontalBinding.isSoldTextView;
            addedDateStrTextView = itemHorizontalBinding.addedDateStrTextView;
            profileCircleImageView = itemHorizontalBinding.profileCircleImageView;
            originalPrice                             = itemHorizontalBinding.originalPriceTextView;
            favoriteImageView                 = itemHorizontalBinding.favoriteImageView;
            ratingBar                                     = itemHorizontalBinding.rating;
            feedbackSum                            = itemHorizontalBinding.feedbackSum;
            favCountTextView                 = itemHorizontalBinding.favCountTextView;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        itemHorizontalBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_horizontal, parent, false);
        final View itemView = itemHorizontalBinding.getRoot();
        vh = new OriginalViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final Inventory p = items.get(position);
            view.title.setText(p.getTitle());

            if ( p.getOfferPrice() != null){

                view.originalPrice.setVisibility(View.VISIBLE);
                view.originalPrice.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(p.getOfferPrice()))));
                view.originalPrice.setTextSize(11);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    view.originalPrice.setTextColor(ctx.getColor(R.color.blue_900));
                }
                view.price.setPaintFlags(view.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                view.price.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(p.getSalePrice()))));

            }else {
                view.originalPrice.setVisibility(View.GONE);
                view.price.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(p.getSalePrice()))));
                view.price.setTextSize(11);

                view.price.setPaintFlags(view.originalPrice.getPaintFlags());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    view.price.setTextColor(ctx.getColor(R.color.blue_900));
                }
            }

            view.ratingBar.setRating((float) p.getRating());

            view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));

            if (p.getShop() != null) {
                view.shopNameTextView.setText(p.getShop().getName());
            }

            if (mCustomerId != null && !mCustomerId.isEmpty()) {
                if (p.getWishlist() != null && (p.getWishlist().getCustomerId().equals(mCustomerId))) {
                    view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
                }
            }

            if (p.getStockQuantity() == 0) {
                view.isSoldTextView.setVisibility(View.VISIBLE);
            }

              String dateCreatedString = p.getCreated() ;

            long milliseconds = 0;
            if (dateCreatedString != null) {
                milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);

                String timeAgo = TimeAgo.getTimeAgo(milliseconds);
                view.addedDateStrTextView.setText(timeAgo);
            }

            if (p.getImages() != null && p.getImages().size() > 0) {
                String url = Constants.IMAGES_BASE_URL + p.getImages().get(0).getPath();
                Utils.LoadImage(ctx, view.productImageView, url );
            }else{
                view.productImageView.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
            }

          if(  p.getStockQuantity() > 0){
              view.isSoldTextView.setVisibility(View.GONE);
          }else {
              view.isSoldTextView.setVisibility(View.VISIBLE);
          }

          view.addressTextView.setText(p.getCondition());

          if (p.getShop() != null ) {
              if (p.getShop().getOwner() != null && p.getShop().getOwner().getImage() != null) {
                  String url = Constants.IMAGES_BASE_URL + p.getShop().getOwner().getImage().getPath();
                  Picasso.get().load(url).transform(new CircleTransform()).into(view.profileCircleImageView);
//                  Picasso.with(ctx).load(url).transform(new CircleTransform()).into(view.profileCircleImageView);
              }else {
                  view.profileCircleImageView.setImageDrawable(ctx.getDrawable(R.drawable.circle_default_image));
              }
          }

            view.imageCardView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view.imageCardView, items.get(position), position);
             }
            });

            if (isHasFavourite(p)) {
                view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
                view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));
            }else  {
                view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_off));
                view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));
            }

            view.favoriteImageView.setOnClickListener(v -> {

              if (mOnItemClickListener != null) {
                  if (isHasFavourite(p)) {
                              view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_on));
                              view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));
                             Utils.ShowSnackBar((Activity) ctx, "Already added to favourites!", Snackbar.LENGTH_SHORT);
                          }else  {
                        view.favoriteImageView.setImageDrawable(ctx.getDrawable(R.drawable.heart_off));
                        mOnItemClickListener.onItemClick(view.favoriteImageView, items.get(position), position);
                        view.feedbackSum.setText(String.format(" ( %d ) ", p.getSumFeedbacks()));
                  }
              }
          });

          view.favCountTextView.setText(String.format(" %d", p.getWishlistCount()));

            view.addressTextView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.addressTextView, items.get(position), position);
                }
            });


            view.locationImage.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.locationImage, items.get(position), position);
                }
            });

            view.price.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.price, items.get(position), position);
                }
            });

            view.shopNameTextView.setOnClickListener(v -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view.shopNameTextView, items.get(position), position);
                    }
            });
        }
    }

    private boolean isHasFavourite(Inventory p) {
        boolean hasFavourite = false;

        if (p.getWishlists() != null && p.getWishlists().size() > 0) {
            for (Wishlist wishlist : p.getWishlists()) {
                if (wishlist.getCustomerId().equals(mCustomerId) && wishlist.getInventoryId().equals(String.valueOf(p.getId()))) {
                    Log.i(TAG, "isHasFavourite:  " + p.getTitle());
                    Log.i(TAG, "isHasFavourite:  " + wishlist.getProductId());
                    hasFavourite = true;
                    break;
                }
            }
        }
        Log.i(TAG, "isHasFavourite:  " + hasFavourite);
        return hasFavourite;
    }

    private void onMoreButtonClick(final View view, final Inventory p) {
        PopupMenu popupMenu = new PopupMenu(ctx, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onMoreButtonClickListener.onItemClick(view, p, item);
                return true;
            }
        });
        popupMenu.inflate(R.menu.menu_product_more);
        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        return items.size();
//        return 3;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Inventory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Inventory obj, MenuItem item);
    }

    public void addToWishList(APIService mAPIService, Inventory inventory){
        String customer_id = CustomSharedPrefs.getUserID(ctx);
        String product_id = String.valueOf(inventory.getProduct() != null ? inventory.getProduct().getId() : null);
        mAPIService.addToWishList(customer_id, String.valueOf(inventory.getId()), product_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {}

                    @Override
                    public void onError(Throwable e) {
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                        if (ctx != null)
                            Utils.ShowSnackBar((Activity)ctx, "Successfully added to wishlist!", Snackbar.LENGTH_LONG);
                    }
                });
    }


}