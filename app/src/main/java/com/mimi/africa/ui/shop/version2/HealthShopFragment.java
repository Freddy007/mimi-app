package com.mimi.africa.ui.shop.version2;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentShopProfileBinding;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShopViewModel;

import javax.inject.Inject;


public class HealthShopFragment extends BaseFragment {

    //region Variables

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private ShopViewModel shopViewModel;

    private static final int REQUEST_CALL = 1;
    private MenuItem basketMenuItem;

    private Shop mShop;

    @Inject
    protected NavigationController navigationController;

    @VisibleForTesting
    private AutoClearedValue<FragmentShopProfileBinding> binding;
    //endregion

    public  HealthShopFragment(){}

    public static HealthShopFragment newInstance(Shop shop){
        HealthShopFragment healthShopFragment = new HealthShopFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.SHOP_OBECT, shop);
        healthShopFragment.setArguments(bundle);

        return healthShopFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            mShop = getArguments().getParcelable(Constants.SHOP_OBECT);
//            binding.get().setShop(mShop);
        }

    }

    //region Override Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentShopProfileBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_profile, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);
        setHasOptionsMenu(true);

        binding.get().titleTextView.setText(mShop.getName());
        binding.get().descTextView.setText(Html.fromHtml(mShop.getDescription()));
        binding.get().emailTextView.setText(mShop.getEmail());
        binding.get().address1.setText(mShop.getAddress());
        binding.get().WebsiteTextView.setText(mShop.getExternalUrl());



        if (mShop.getAddresses() != null && mShop.getAddresses().size() > 0) {
//                binding.get().setAddress(mShop.getAddresses().get(0));
            binding.get().phoneTextView.setText(mShop.getAddresses().get(0).getPhone());
            binding.get().phone1TextView.setText(mShop.getAddresses().get(0).getPhone());
//            binding.get().phone2TextView.setText(mShop.getAddresses().get(0));
        }

        return binding.get().getRoot();
    }

    private void setBasketMenuItemVisible(Boolean isVisible) {
        if (basketMenuItem != null) {
            basketMenuItem.setVisible(isVisible);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.basket_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
//        basketMenuItem = menu.findItem(R.id.action_basket);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

//        if (item.getItemId() == R.id.action_basket) {
//            navigationController.navigateToBasketList(getActivity());
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initUIAndActions() {

//        binding.get().aboutImageView.setOnClickListener(view ->
//                navigationController.navigateToGalleryActivity(getActivity(), Constant.IMAGE_TYPE_ABOUT, shopViewModel.selectedShopId));

        //For Phone 1
        binding.get().phoneTextView.setOnClickListener(view1 -> {

            String number = binding.get().phoneTextView.getText().toString();
            if (!(number.trim().isEmpty() || number.trim().equals("-"))) {
//                Utils.callPhone(ShopProfileFragment.this, number);
            }
        });

        //For phone 2
        binding.get().phone1TextView.setOnClickListener(view -> {

            String number1 = binding.get().phone1TextView.getText().toString();
            if (!(number1.trim().isEmpty() || number1.trim().equals("-"))) {
                Utils.callPhone(HealthShopFragment.this, number1);
            }

        });


        //For phone 3
        binding.get().phone2TextView.setOnClickListener(view -> {

            String number2 = binding.get().phone2TextView.getText().toString();

            if (!(number2.trim().isEmpty() || number2.trim().equals("-"))) {
                Utils.callPhone(HealthShopFragment.this, number2);
            }
        });


        //For phone 4
        binding.get().phone3TextView.setOnClickListener(view -> {

            String number3 = binding.get().phone3TextView.getText().toString();

            if (!(number3.trim().isEmpty() || number3.trim().equals("-"))) {
                Utils.callPhone(HealthShopFragment.this, number3);

            }
        });

        //For email
        binding.get().emailTextView.setOnClickListener(view -> {
            try {
                final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType(Constant.EMAIL_TYPE);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{String.valueOf(binding.get().emailTextView.getText())});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.hello));
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
            } catch (Exception e) {
                Utils.psErrorLog("doEmail", e);
            }
        });

        //For website
        binding.get().WebsiteTextView.setOnClickListener(view -> {

            if (binding.get().WebsiteTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().WebsiteTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().WebsiteTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }

        });

        //For facebook
        binding.get().facebookTextView.setOnClickListener(view -> {

            if (binding.get().facebookTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().facebookTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().facebookTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }
        });

        //For google plus
        binding.get().gplusTextView.setOnClickListener(view -> {

            if (binding.get().gplusTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().gplusTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().gplusTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }
        });

        //For twitter
        binding.get().twitterTextView.setOnClickListener(view -> {

            if (binding.get().twitterTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().twitterTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().twitterTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }

        });

        //For instagram
        binding.get().instaTextView.setOnClickListener(view -> {

            if (binding.get().instaTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().instaTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().instaTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }
        });

        //For youtube
        binding.get().youtubeTextView.setOnClickListener(view -> {

            if (binding.get().youtubeTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().youtubeTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().youtubeTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }

        });

        //For pinterest
        binding.get().pinterestTextView.setOnClickListener(view -> {

            if (binding.get().pinterestTextView.getText().toString().startsWith(Constant.HTTP) || binding.get().pinterestTextView.getText().toString().startsWith(Constant.HTTPS)) {
                String url = binding.get().pinterestTextView.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.invalid_url), Toast.LENGTH_SHORT).show();
            }
        });

        binding.get().shopPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number1 = binding.get().phone1TextView.getText().toString();
                if (!(number1.trim().isEmpty() || number1.trim().equals("-"))) {
                    Utils.callPhone(HealthShopFragment.this, number1);
                }
            }
        });
    }

    @Override
    protected void initViewModels() {
    }

    @Override
    protected void initAdapters() {
    }

    //  private  void replaceAboutUsData()
    @Override
    protected void initData() {

    }

    private void basketData() {
    }

}