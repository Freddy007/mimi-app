package com.mimi.africa.ui.shop.version2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.HomeCategoryAdapter;
import com.mimi.africa.Adapter.ImageSliderAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.databinding.ActivityStoreBinding;
import com.mimi.africa.databinding.ActivityStoreHomeBinding;
import com.mimi.africa.event.ReviewItemClickedEvent;
import com.mimi.africa.event.ShopMoreClickedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.reviews.ReviewsFragment;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import io.sentry.core.Sentry;

public class StoreHomeActivity extends BaseActivity {

    private static final String TAG = StoreHomeActivity.class.getSimpleName();

    public ActivityStoreHomeBinding activityStoreBinding;


    @Nullable
    private  Shop shop;
    private String mProductId;

      @Nullable
      private Runnable runnable = null;
      @NonNull
      private Handler handler = new Handler();
      private ShopProductGridCardAdapter mProductsAdapter;
      @Nullable
      private ImageSliderAdapter adapterImageSlider;
      @Nullable
      private HomeCategoryAdapter homeCategoryAdapter;
      private List<Feedback> mFeedBacks;
      private BottomSheetBehavior mBehavior;
      private BottomSheetDialog mBottomSheetDialog;
      private View bottom_sheet;

      private PSDialogMsg psDialogMsg;

    public ProgressDialog progressDialog;

    @Inject
    public APIService mAPIService;

      @Inject
      protected SharedPreferences pref;

    private Toolbar toolbar;

      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         activityStoreBinding = DataBindingUtil.setContentView(this, R.layout.activity_store_home);

        initToolbar();
        initComponent(activityStoreBinding);

    }

    private void initToolbar() {
    }

    private void initComponent(ActivityStoreHomeBinding activityStoreBinding) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

          psDialogMsg = new PSDialogMsg(this, false);

          bottom_sheet = findViewById(R.id.bottom_sheet);
          mBehavior = BottomSheetBehavior.from(bottom_sheet);
          mBottomSheetDialog = new BottomSheetDialog(this);

          if (getIntent() != null ) {
                shop = getIntent().getParcelableExtra(Constants.SHOP_OBECT);
                Log.i(TAG, "initComponent:  StoreHome " + shop.getId() );
                if (shop != null ) {
                      String title = shop.getName();
                      if (title != null) {
                            initToolbar(activityStoreBinding.toolbar, title);
                      } else {
                            initToolbar(activityStoreBinding.toolbar, "SHOP");
                      }
                }

                if (shop != null && shop.getFeedbacks() != null) {
                      mFeedBacks = shop.getFeedbacks();
                }

                if (getIntent().getStringExtra(Constants.PRODUCT_ID) != null){
                      mProductId = getIntent().getStringExtra(Constants.PRODUCT_ID);
                }

                activityStoreBinding.visitButton.setOnClickListener(v ->{
                      if (shop.getLat() != null || shop.getLng() != null) {
                            Utils.getDirectionsOnGoogleMap(StoreHomeActivity.this, shop.getLat(), shop.getLng());
                      }else {
                            Utils.ShowSnackBar(StoreHomeActivity.this, "No location available for this shop", Snackbar.LENGTH_LONG);
                      }
                });

                activityStoreBinding.chat.setOnClickListener(v -> {
                      try {

                            String loginUserId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
                            String userIdToVerify = pref.getString(Constant.USER_ID_TO_VERIFY, Constant.EMPTY_STRING);

                            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId,TAG, psDialogMsg,
                                    this, navigationController, new Utils.NavigateOnUserVerificationActivityCallback() {
                                          @Override
                                          public void onSuccess() {
                                                Intent intent = new Intent(StoreHomeActivity.this, MessageActivity.class);
                                                intent.putExtra(Constants.SHOP_NAME, shop.getName());
                                                intent.putExtra(Constants.SHOP_ID, shop.getId());
                                              if (shop.getImage() != null) {
                                                  Log.i(TAG, "onSuccess:  Image " + shop.getImage().path);
                                                  intent.putExtra(Constants.SHOP_IMAGE, shop.getImage().getPath());
                                              }
                                              startActivity(intent);
                                          }
                                    });
                      } catch (Exception e) {
                            Sentry.captureException(e);
                      }
                });

                activityStoreBinding.reviewsButton.setOnClickListener(v -> {
                      if (mFeedBacks != null && mFeedBacks.size() >0 ){
                            ReviewsFragment reviewsFragment = ReviewsFragment.newInstance(mFeedBacks);
                            showDialog(reviewsFragment);
                      }else {
                            CharSequence charSequence;
                            Snackbar snackbar = Snackbar.make(v, "This shop has no reviews!", Snackbar.LENGTH_LONG);
                            snackbar.setAction("ADD A REVIEW", new MyUndoListener()).show();
                      }
                });

                StoreHomeFragment storeHomeFragment = StoreHomeFragment.newInstance(shop);
                setupFragment(storeHomeFragment);
          }
    }

      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_search_without_location, menu);
            return true;
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()){
                  case android.R.id.home:
                        finish();
                        break;
                  case R.id.action_search:
                        startActivity(new Intent(StoreHomeActivity.this, MainSearchActivity.class));
                        break;

                  case R.id.action_more:
                              EventBus.getDefault().post(new ShopMoreClickedEvent(shop));
                              break;
            }

            if (item.getItemId() == android.R.id.home) {
                  finish();
            }else if (item.getItemId() == R.id.action_cart){

            } else {
//                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }

      public class MyUndoListener implements View.OnClickListener {

            @Override
            public void onClick(View v) {
                 EventBus.getDefault().post(new ReviewItemClickedEvent(new Feedback()));
            }
      }

}