package com.mimi.africa.ui.category;


import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.mimi.africa.Adapter.ShopCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentSingleCategoryBinding;
import com.mimi.africa.event.LocationSearchEvent;
import com.mimi.africa.event.ProductCategoryViewedEvent;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.LocationUtilService;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShopViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class SingleCategoryFragmentCopy extends BaseFragment {

      private static final String TAG = SingleCategoryFragmentCopy.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentSingleCategoryBinding> binding;

      @Inject
      APIService mAPIService;
      private ShopViewModel shopsViewModel;
      private TextView locationTextView;
      private String mCategorySlug;
      private Activity mActivity;
      private Context mContext;
      private Handler mHandler;

      public SingleCategoryFragmentCopy() {}

      public static SingleCategoryFragmentCopy newInstance(String slug){
            SingleCategoryFragmentCopy singleCategoryFragment = new SingleCategoryFragmentCopy();
            Bundle bundle = new Bundle();
            bundle.putString("category_slug", slug);
            singleCategoryFragment.setArguments(bundle);

            return singleCategoryFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            mHandler = new Handler();

            if (getArguments() != null){
                  mCategorySlug = getArguments().getString("category_slug");

                  ProductCategory productCategory = new ProductCategory();
                  productCategory.setName(mCategorySlug);
                  EventBus.getDefault().post(new ProductCategoryViewedEvent(productCategory));
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentSingleCategoryBinding fragmentSingleCategoryBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_single_category, container, false, dataBindingComponent);

            binding = new AutoClearedValue<>(this, fragmentSingleCategoryBinding);

            shopsViewModel = ViewModelProviders.of(this).get(ShopViewModel.class);

            mActivity = getActivity();
            mContext = getContext();

            return fragmentSingleCategoryBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            if (getActivity() != null)
            binding.get().storesAvailableRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
            binding.get().storesNearRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
            binding.get(). previouslyVisitedRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
            binding.get().suggestedProductsRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));

      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {
//            getAvailableShops();
            getShopsWithCategory();
//            getAShopsNearYou();
//            getPreviouslyVisitedShops();
      }

      private void getShopsByAddress(String address) {

            mAPIService.getShopsByAddress(address)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

                                try {
//                                      Utils.ShowView(binding.get().shopsProgressBar);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {
                                ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());

                                try {
                                      binding.get(). storesAvailableRecyclerView.setAdapter(shopCardAdapter);
//                                      binding.get().storesNearRecyclerView.setAdapter(shopCardAdapter);

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }

                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                      navigateToStore(obj);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG, e, "onError:  " + e.getMessage());

//                                reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
//                                disposeAPIService(disposable);

                                try {

//                                      Utils.RemoveView(binding.get().shopsProgressBar);
//                                      Utils.ShowView(binding.get().mallsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      private void getAShopsNearYou() {

            final boolean[] empty = {false};
                  mAPIService.getShops()
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<ShopsResponse>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) { disposable = d; }

                                @Override
                                public void onNext(@NonNull ShopsResponse shopsResponse) {

                                      List<Shop> closestShops = new ArrayList<>();

                                      appExecutors.getDiskIO().execute(() -> {
                                            try {
                                                  for (Shop shop : shopsResponse.getShop()) {

                                                        if (shop.getLat() != null || shop.getLng() != null) {
                                                              String lat = shop.getLat();
                                                              String lng = shop.getLng();
                                                              if (LocationPreferences.isLocationLatLonAvailable(mContext)) {

                                                                    android.location.Location endLocation = new android.location.Location("New End Location");
                                                                    endLocation.setLatitude( Double.parseDouble(lat));
                                                                    endLocation.setLongitude(Double.parseDouble(lng));

                                                                    double shopLocationDistanceToYou = LocationUtilService.getDistanceInKm(mContext, endLocation);

                                                                    if (shopLocationDistanceToYou <= Constants.DEFAULT_RADIUS_DISTANCE) {
                                                                          closestShops.add(shop);
                                                                    }
                                                              }
                                                        }
                                                  }
                                            } catch (Exception e) {
                                                  Sentry.captureException(e);
                                                  e.printStackTrace();
                                            }
                                      });

                                      try {

                                            mHandler.postDelayed(() -> {
                                                  try {
                                                        ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), closestShops);
                                                        if (closestShops.size() > 0){
                                                              binding.get().storesNearRecyclerView.setAdapter(shopCardAdapter);

                                                              shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                                    navigateToStore(obj);
                                                              });

                                                        }else {
                                                              empty[0] = true;
                                                        }
                                                  } catch (Exception e) {
                                                        e.printStackTrace();
                                                        Sentry.captureException(e);
                                                  }
                                            },3000);

                                      } catch (Exception e) {
                                            logError(TAG,e, "onNext:  ");
                                      }
                                }

                                @Override
                                public void onError(Throwable e) {
                                      logError(TAG, e, "onError:  " + e.getMessage());
                                }

                                @Override
                                public void onComplete() {
                                      try {

                                            if (empty[0]) {
                                                  Utils.ShowView(binding.get().noStoresInYourArea);
                                                  Utils.RemoveView(binding.get().storesNearRecyclerView);
                                            } else {
                                                  Utils.RemoveView(binding.get().noStoresInYourArea);
                                                  Utils.ShowView(binding.get().storesNearRecyclerView);
                                            }
                                      }catch (Exception e){
                                            logError(TAG, e, "onComplete");
                                      }
                                }
                          });

            getPreviouslyVisitedShops();
      }

      private void getPreviouslyVisitedShops() {

                  if (getActivity() != null)
                        shopsViewModel.getShops().observe(getActivity(), shopList -> {
                              if (shopList != null) {
                                    if (shopList.size() > 0) {

                                          try {

                                                if (binding.get().noHistory != null)
                                                      binding.get().noHistory.setVisibility(View.GONE);

                                                ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopList);

                                                if (binding.get().previouslyVisitedRecyclerView != null)

                                                      binding.get().previouslyVisitedRecyclerView.setAdapter(shopCardAdapter);

                                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                      navigateToStore(obj);
                                                });
                                          }catch (Exception e){
                                                logError(TAG, e,"getPreviouslyVisitedShops");
                                    }
                                    } else {
                                          if (binding.get().noHistory != null)
                                                binding.get().noHistory.setVisibility(View.VISIBLE);
                                    }
                              }
                        });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onSearchQuery(LocationSearchEvent event){
            getShopsByAddress(event.getTerm());
            Toast.makeText(getContext(), event.getTerm(), Toast.LENGTH_SHORT).show();
      }

      @NotNull
      private Address getAddress(Context context) {
            List<Address> addresses = LocationUtilService.getYourAddress(context);
            return addresses.get(0);
      }

      private void getAvailableShops(){

            final boolean[] empty = {false};

            mAPIService.getAllCategoryProducts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<ProductCategory>>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull List<ProductCategory> productCategories) {

                                List<Shop> shopList = new ArrayList<>();

                                try {

                                      appExecutors.getDiskIO().execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                  for (ProductCategory productCategory : productCategories) {

                                                        if (productCategory.getSlug().equals(mCategorySlug))

                                                              if (productCategory.getProducts() != null && productCategory.getProducts().size() > 0) {

                                                                    if (!shopList.contains(productCategory.getProducts().get(0).getShop())) {
                                                                          shopList.add(productCategory.getProducts().get(0).getShop());
                                                                    }
                                                              }
                                                  }
                                            }
                                      });


                                      mHandler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                  if (shopList.size() > 0) {

                                                        ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopList);
                                                        binding.get().storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                                        shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                              navigateToStore(obj);
                                                        });
                                                  }else {
                                                        empty[0] = true;
                                                  }
                                            }
                                      },3000);


                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }
                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getShop Listing  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                                if (empty[0]){
                                      Utils.ShowView(binding.get().noStoresAvailable);
                                      Utils.RemoveView(binding.get().storesAvailableRecyclerView);
                                }else {
                                      Utils.RemoveView(binding.get().noStoresAvailable);
                                      Utils.ShowView(binding.get().storesAvailableRecyclerView);
                                }
                          }
                    });
      }

      private void getShopsWithCategory(){

            final boolean[] empty = {false};

            mAPIService.getShopsByCategory(mCategorySlug)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {

                                try {

                                      if (shopsResponse.getShop().size() > 0) {

                                            ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());
                                            binding.get().storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                            shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                  navigateToStore(obj);
                                            });
                                      }else {
                                            empty[0] = true;
                                      }

                                }catch (Exception e){
                                      logError(TAG, e, "onNext" );
                                }
                          }
                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG, e, "onError: getShop Listing  ");
                          }

                          @Override
                          public void onComplete() {
                                try {

                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noStoresAvailable);
                                            Utils.RemoveView(binding.get().storesAvailableRecyclerView);
                                      }else {
                                            Utils.RemoveView(binding.get().noStoresAvailable);
                                            Utils.ShowView(binding.get().storesAvailableRecyclerView);
                                      }

                                }catch (Exception e){
                                      logError(TAG, e, "onComplete:  ");
                                }
                                getAShopsNearYou();
                          }
                    });
      }

}
