package com.mimi.africa.ui.orderhistory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.OrderBatch;
import com.mimi.africa.model.ShopProduct;

import java.util.ArrayList;
import java.util.List;

public class OrderBatchHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<OrderBatch> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public OrderBatchHistoryAdapter(Context context, List<OrderBatch> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView cardContainer;
        public ImageView image;
        public TextView title;
        public TextView totalAmountValueTextView;
        public TextView viewDetailTitleTextView;
        public TextView dateCreatedTextView;
        public ImageButton more;
        public View lyt_parent;

        public OriginalViewHolder(@NonNull View v) {
            super(v);

            cardContainer = v.findViewById(R.id.card_container);
            title = v.findViewById(R.id.transaction_no_value_text_view);
            totalAmountValueTextView = v.findViewById(R.id.totalAmountValueTextView);
            viewDetailTitleTextView = v.findViewById(R.id.viewDetailTitleTextView);

//            dateCreatedTextView = v.findViewById(R.id.dateTextView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card, parent, false);
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_list, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final OrderBatch p = items.get(position);
            view.title.setText(p.getOrderNumber());
            String subtotal = String.format("%.0f", Double.valueOf(p.getTotalDeliveryCharge()));
            String deliveryCharge = String.format("%.0f", Double.valueOf(p.getDeliveryCharge()));
            int grandTotal = Integer.parseInt(subtotal) + Integer.parseInt( deliveryCharge);

            view.totalAmountValueTextView.setText(String.format(" GHS %d", grandTotal));
//            view.dateCreatedTextView.setText(p.getCreatedAt());

            view.viewDetailTitleTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, OrderBatch obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, ShopProduct obj, MenuItem item);
    }

}