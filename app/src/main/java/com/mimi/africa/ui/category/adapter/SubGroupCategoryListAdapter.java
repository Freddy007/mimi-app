package com.mimi.africa.ui.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.SubGroup;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.List;

import info.androidhive.fontawesome.FontDrawable;
import io.sentry.core.Sentry;

public class SubGroupCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SubGroup> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private int lastPosition = -1;


    public interface OnItemClickListener {
        void onItemClick(View view, SubGroup obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SubGroupCategoryListAdapter(Context context, List<SubGroup> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public ImageView categoryIconImageView;
        public TextView title;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
            image = v.findViewById(R.id.imageView);
            categoryIconImageView = v.findViewById(R.id.categoryIconImageView);
            title = v.findViewById(R.id.newsTitleTextView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_adapter, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            SubGroup subGroup = items.get(position);

                view.title.setText(subGroup.getName());

                if (subGroup.getImage() != null){
                    String url = Constants.IMAGES_BASE_URL + subGroup.getImage().getPath();
                    Picasso.get().load(url).into(view.image);
//                    Picasso.with(ctx).load(url).into(view.image);
                }else{
                    view.image.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
                }

                if (subGroup.getGroup().getIcon() != null) {
                    String font = subGroup.getGroup().getIcon().replace("-", "_");

                    try {
                        Field resourceField = R.string.class.getDeclaredField(font + "_solid");
                        int resourceId = resourceField.getInt(resourceField);

                        FontDrawable drawable = new FontDrawable(ctx, resourceId, true, false);
                        drawable.setTextColor(ContextCompat.getColor(ctx, android.R.color.white));

                        if (subGroup.getCategoryGroupId() != null && !(subGroup.getCategoryGroupId().equals("10"))) {
                            view.categoryIconImageView.setImageDrawable(drawable);
                        }
                    } catch (Exception e) {
                        Sentry.captureException(e);
                        e.printStackTrace();
                    }
                }

                setAnimation(holder.itemView, position);

                view.image.setOnClickListener(view1 -> {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view1, items.get(position), position);
                    }
                });
//            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else {
            lastPosition = position;
        }
    }
}