package com.mimi.africa.ui.health;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.Adapter.AdapterGridShopCategory;
import com.mimi.africa.Adapter.AdapterGridShopProductCard;
import com.mimi.africa.Adapter.ShopCardAdapter;
import com.mimi.africa.BuildConfig;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.shop.version2.HealthShopHomeActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HealthCategoryActivity extends BaseActivity {
      private static final String TAG = HealthCategoryActivity.class.getSimpleName();

      @Nullable
      private ActionBar actionBar;
      private Toolbar toolbar;
      private RecyclerView recyclerView;
      private AdapterGridShopCategory mAdapter;

      private RecyclerView healthCenterRecyclerview;
      private RecyclerView pharmaciesRecyclerView;
      private RecyclerView specialistClinicsRecyclerView;
      private Button inviteFriendsButton;
      private AdapterGridShopProductCard mProductsAdapter;
      private APIService mAPIService;


      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_health_category);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }

            initToolbar();
            initComponent();

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        hideKeyboard();
                  }
            },1000);

      }


      private void initToolbar() {
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

        actionBar.setTitle("Health Category");
      }

      private void initComponent() {
            inviteFriendsButton = findViewById(R.id.invite_friends_button);
            healthCenterRecyclerview = findViewById(R.id.health_center_recyclerview);
            pharmaciesRecyclerView = findViewById(R.id.pharmacy_recyclerView);
            specialistClinicsRecyclerView = findViewById(R.id.specialist_clinics_recyclerView);

            healthCenterRecyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
            pharmaciesRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
            specialistClinicsRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

            getShops(Constants.HEALTH_FACILITY, healthCenterRecyclerview);
            getShops(Constants.PHARMACY, pharmaciesRecyclerView);
            getShops(Constants.SPECIALIST_CLINIC, specialistClinicsRecyclerView);

            inviteFriendsButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        shareContent();
                  }
            });
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
//            finish();
                  startActivity(new Intent(getApplicationContext(), MainHomeActivity.class));
            } else {
                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }

      private void getShops(String type , RecyclerView recyclerView){
            final boolean[] empty = {false};

            mAPIService.getShopsByType(type)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {

                                if (shopsResponse.getShop().size() > 0) {

                                      ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getApplicationContext(), shopsResponse.getShop());

                                      try {
                                            recyclerView.setAdapter(shopCardAdapter);

                                      } catch (Exception e) {
                                            Log.e(TAG, "onNext:  " + e.getMessage());

                                      }

                                      shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
//                                            navigateToStore(obj);
                                            Intent intent = new Intent(HealthCategoryActivity.this, HealthShopHomeActivity.class);
                                            intent.putExtra(Constants.SHOP_OBECT, obj);
                                            startActivity(intent);
                                      });
                                }else {
                                      empty[0] = true;
                                }
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.e(TAG, "onError:  " +e.getMessage());
                          }

                          @Override
                          public void onComplete() {

                                TextView no_health_centers_textview = findViewById(R.id.no_health_centers_textview);
                                TextView no_pharmacies_centers_textview = findViewById(R.id.no_pharmacies_centers_textview);
                                TextView no_specialist_clinics_textview= findViewById(R.id.no_specialist_clinics_textview);

                                if (empty[0] ){
                                      switch (type) {
                                            case Constants.HEALTH_FACILITY:
                                                  Utils.ShowView(no_health_centers_textview);
                                                  break;

                                            case Constants.PHARMACY:
                                                  Utils.ShowView(no_pharmacies_centers_textview);
                                                  break;

                                            case Constants.SPECIALIST_CLINIC:
                                                  Utils.ShowView(no_specialist_clinics_textview);
                                                  break;
                                      }

                                }
                          }
                    });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(DemoEvent event) {
      }

      protected void navigateToStore(Shop obj){
//            Intent intent = new Intent(this, StoreActivity.class);
            Intent intent = new Intent(this, StoreHomeActivity.class);
            intent.putExtra(Constants.SHOP_OBECT, obj);
            startActivity(intent);
      }

      private void hideKeyboard() {
            View view = this.getCurrentFocus();
            if (view != null) {
                  InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                  if (imm != null) {
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                  }
            }
      }

      private void shareContent(){
            Uri imageUri = Uri.parse("android.resource://" + getPackageName()
                    + "/drawable/" + "ic_launcher");

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                     getString(R.string.share_message) + ": https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
      }


}
