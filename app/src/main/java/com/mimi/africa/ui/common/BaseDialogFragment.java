package com.mimi.africa.ui.common;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.mimi.africa.R;
import com.mimi.africa.di.Injectable;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.sentry.core.Sentry;

public abstract class BaseDialogFragment extends DialogFragment implements Injectable {

      private boolean isFadeIn = false;

//      @Inject
//      protected ViewModelProvider.Factory viewModelFactory;

      @Inject
      protected Connectivity connectivity;

      @Inject
      protected SharedPreferences pref;

      protected String loginUserId;

      protected String shippingId;

      protected String shippingTax;

      protected String overAllTax;

      protected String shippingTaxLabel;

      protected String overAllTaxLabel;

      protected String versionNo;

      protected Boolean force_update = false;

      protected String force_update_msg;

      protected String force_update_title;

      protected String cod, paypal, stripe, messenger, whatsappNo;

      protected String consent_status;

      protected String userName,fullName, phoneNumber,email, address,city;

      protected String userEmailToVerify, userPasswordToVerify , userNameToVerify, userIdToVerify;

      protected String shopId, shopPhoneNumber, shopStandardShippingEnable, shopNoShippingEnable, shopZoneShippingEnable;


      @Override
      public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            loadLoginUserId();
            initUIAndActions();
            initViewModels();
            initAdapters();
            initData();
      }

      protected abstract void initUIAndActions();

      protected abstract void initViewModels();

      protected abstract void initAdapters();

      protected abstract void initData();

      protected void fadeIn(@NonNull View view) {

            if (!isFadeIn) {
                  view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
                  isFadeIn = true; // Fade in will do only one time.
            }
      }

      protected void loadLoginUserId() {
            try {

                  if (getActivity() != null && getActivity().getBaseContext() != null) {
                        loginUserId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
                        userName = pref.getString(Constant.USER_NAME, Constant.EMPTY_STRING);
                        email = pref.getString(Constant.USER_EMAIL, Constant.EMPTY_STRING);
                        fullName = pref.getString(Constant.FULL_NAME, Constant.EMPTY_STRING);
                        phoneNumber = pref.getString(Constant.USER_PHONE, Constant.EMPTY_STRING);
                        address = pref.getString(Constant.ADDRESS, Constant.EMPTY_STRING);
                        city = pref.getString(Constant.CITY, Constant.EMPTY_STRING);

                        shippingId = pref.getString(Constant.SHIPPING_ID, "");
                        shippingTax = pref.getString(Constant.PAYMENT_SHIPPING_TAX, "");
                        versionNo = pref.getString(Constant.APPINFO_PREF_VERSION_NO, "");
                        force_update = pref.getBoolean(Constant.APPINFO_PREF_FORCE_UPDATE, false);
                        force_update_msg = pref.getString(Constant.APPINFO_FORCE_UPDATE_MSG, "");
                        force_update_title = pref.getString(Constant.APPINFO_FORCE_UPDATE_TITLE, "");
//                        overAllTax = pref.getString(Constant.PAYMENT_OVER_ALL_TAX, Constant.EMPTY_STRING);
                        overAllTax = "0";
                        overAllTaxLabel = pref.getString(Constant.PAYMENT_OVER_ALL_TAX_LABEL, Constant.EMPTY_STRING);
                        shippingTaxLabel = pref.getString(Constant.PAYMENT_SHIPPING_TAX_LABEL, Constant.EMPTY_STRING);
//                        cod = pref.getString(Constants.PAYMENT_CASH_ON_DELIVERY, Constant.ZERO);
                        cod = "1";
                        paypal = pref.getString(Constants.PAYMENT_PAYPAL, Constant.ZERO);
//                        stripe = pref.getString(Constants.PAYMENT_STRIPE, Constant.ZERO);
                        stripe = "1";
                        messenger = pref.getString(Constant.MESSENGER, Constant.ZERO);
                        whatsappNo = pref.getString(Constant.WHATSAPP, Constant.ZERO);
                        consent_status = pref.getString(Config.CONSENTSTATUS_CURRENT_STATUS, Config.CONSENTSTATUS_CURRENT_STATUS);
                        userEmailToVerify = pref.getString(Constant.USER_EMAIL_TO_VERIFY, Constant.EMPTY_STRING);
                        userPasswordToVerify = pref.getString(Constant.USER_PASSWORD_TO_VERIFY, Constant.EMPTY_STRING);
                        userNameToVerify = pref.getString(Constant.USER_NAME_TO_VERIFY, Constant.EMPTY_STRING);
                        userIdToVerify = pref.getString(Constant.USER_ID_TO_VERIFY, Constant.EMPTY_STRING);
                        shopId = pref.getString(Constants.SHOP_ID, Constant.EMPTY_STRING);
                        shopPhoneNumber = pref.getString(Constant.SHOP_PHONE_NUMBER, Constant.EMPTY_STRING);
                        shopNoShippingEnable = pref.getString(Constant.SHOP_NO_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                        shopZoneShippingEnable = pref.getString(Constant.SHOP_ZONE_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                        shopStandardShippingEnable = pref.getString(Constant.SHOP_STANDARD_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                  }

            } catch (NullPointerException ne) {
                  Utils.psErrorLog("Null Pointer Exception.", ne);
                  Sentry.captureException(ne);
            } catch (Exception e) {
                  Utils.psErrorLog("Error in getting notification flag data.", e);
                  Sentry.captureException(e);
            }
      }

}
