package com.mimi.africa.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityUnreadBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.viewModels.notification.NotificationViewModel;

import java.util.List;

import javax.inject.Inject;

public class ArchivedActivity extends BaseActivity {

    protected ActivityUnreadBinding activityUnreadBinding;

    private NotificationViewModel notificationViewModel;
    private RecyclerView recyclerView;
    private View view;
    private LinearLayout noNotificationsLinearLayout;
    private LinearLayout progressLinearLayout;
    private MessageAdapter mAdapter;
    private Notification mNotification;
    private String mShopId;
    private long mId;
    private List<Notification> items;

    @Inject
    MimiDb mimiDb;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityUnreadBinding = DataBindingUtil.setContentView(this, R.layout.activity_unread);

        recyclerView = activityUnreadBinding.messagesRecyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        noNotificationsLinearLayout =activityUnreadBinding.noMessagesLinearLayout;
        progressLinearLayout = activityUnreadBinding.lytProgress;
//        animation_type = ItemAnimation.BOTTOM_UP;

        initViewModels();

        getArchived();

        initToolbar("Archived Messages");
    }

    private void initToolbar(String title) {
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    private void initViewModels(){
            notificationViewModel =  ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel.class);
    }

    private void processView(List<Notification> appNotifications) {
        if (appNotifications != null && appNotifications.size() != 0) {
            setAdapter(appNotifications);
            noNotificationsLinearLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        } else {
            noNotificationsLinearLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void setAdapter(List<Notification> notificationList) {
        //set data and list adapter
        try{
            mAdapter = new MessageAdapter(this, notificationList);
            recyclerView.setAdapter(mAdapter);

            mAdapter.setOnClickListener(new MessageAdapter.OnClickListener() {
                @Override
                public void onItemClick(View view, Notification obj, int pos) {
                    mNotification = obj;
                    appExecutors.getDiskIO().execute(() -> {
                        obj.setRead(true);
                        mimiDb.notificationDaoAccess().updateNotification(obj);
                    });

                    mShopId = obj.getSenderId();
                    mId            = obj.getId();
                    Intent intent = new Intent(ArchivedActivity.this, MessageActivity.class);
                    intent.putExtra(Constants.SHOP_ID, Integer.valueOf(obj.getSenderId()));
                    intent.putExtra(Constants.SHOP_NAME, obj.getSender());
                    startActivity(intent);
                }

                @Override
                public void onItemLongClick(View view, Notification obj, int pos) {

//                    if (mBottomSheetDialog != null ) {
//                        bottomMessageOptionsLayoutBinding.get().titleTextView.setText(String.format("Conversation with %s", obj.getSender()));
//
//                        if (obj.isRead()){
//                            bottomMessageOptionsLayoutBinding.get().readStatus.setText("MARK AS UNREAD");
//                        }else {
//                            bottomMessageOptionsLayoutBinding.get().readStatus.setText("MARK AS READ");
//                        }
//
//                        if (obj.isArchived()){
//                            bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("UNARCHIVE MESSAGE");
//                        }else {
//                            bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("ARCHIVE MESSAGE");
//                        }
//
//                        mBottomSheetDialog.get().show();
//
//                        bottomMessageOptionsLayoutBinding.get().archiveMessage.setOnClickListener(v -> {
//                            showProgressIndicator();
//                            archiveMessage(obj);
//
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    hideProgressBar();
//                                    refresh();
//                                }
//                            },3000);
//
//
//                            mBottomSheetDialog.get().dismiss();
//                        });
//
//                        bottomMessageOptionsLayoutBinding.get().readStatus.setOnClickListener(v -> {
//                            setReadStatus(obj);
//                            showProgressIndicator();
//
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    hideProgressBar();
//                                    refresh();
//                                }
//                            },3000);
//
//                            mBottomSheetDialog.get().dismiss();
//                        });
//
//                    }
                }
            });

        }catch (Exception e){
            if (mAdapter != null ){
//                        mAdapter = new AppNotificationAdapter(getContext(), items, animation_type);
//                        recyclerView.setAdapter(mAdapter);
            }
        }
    }

    private void getArchived() {

            notificationViewModel.getArchived().observe(this, appNotifications -> {
                items = appNotifications;
                processView(appNotifications);
            });

    }



}
