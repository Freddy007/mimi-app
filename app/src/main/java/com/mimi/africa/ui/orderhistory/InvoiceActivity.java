package com.mimi.africa.ui.orderhistory;

import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.OrderBatch;
import com.mimi.africa.ui.orderhistory.adapter.InvoiceItemAdapter;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.utils.ViewAnimation;


public class InvoiceActivity extends AppCompatActivity {

    private ImageButton bt_toggle_items, bt_toggle_address, bt_toggle_description;
    private View lyt_expand_items, lyt_expand_address, lyt_expand_description;
    private NestedScrollView nested_scroll_view;
    private TextView mOrderAmountTextView;
    private TextView mOrderTotalTextView;
    private TextView tvInvoiceCodeTextView;
    private TextView mInvoiceDateTextView;
    private RecyclerView mItemsRecyclerView;
    private TextView deliveryChargeTextView;

    private OrderBatch mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expansion_panel_invoice);

        if (getIntent() != null && getIntent().getParcelableExtra("order") != null){
            mOrder = getIntent().getParcelableExtra("order");
        }

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Tools.setSystemBarColor(this, R.color.teal_700);
    }

    private void initComponent() {

        // nested scrollview
        nested_scroll_view = (NestedScrollView) findViewById(R.id.nested_scroll_view);
        mOrderAmountTextView = findViewById(R.id.order_amount);
        mOrderTotalTextView = findViewById(R.id.totalOrderAmount);
        tvInvoiceCodeTextView = findViewById(R.id.tv_invoice_code);
        mInvoiceDateTextView = findViewById(R.id.invoice_date);
        mItemsRecyclerView     = findViewById(R.id.itemsRecyclerview);
        deliveryChargeTextView = findViewById(R.id.delivery_charge);

        if (mOrder != null){
            float amount = Utils.roundDouble(Double.valueOf(mOrder.getTotalDeliveryCharge()), 2);
            float deliveryCharge = Utils.roundDouble(Double.valueOf(mOrder.getDeliveryCharge()), 2);

            mOrderAmountTextView.setText(String.format("GHS %s ", String.valueOf(amount + deliveryCharge)));
            mOrderTotalTextView.setText(String.format(Constants.DEFAULT_CURRENCY +"  %s ", String.valueOf(amount + deliveryCharge)));

            tvInvoiceCodeTextView.setText(String.format( "%s", mOrder.getOrderNumber()));

            mInvoiceDateTextView.setText(mOrder.getCreatedAt());

            deliveryChargeTextView.setText(String.format("%s %s", Constants.DEFAULT_CURRENCY, mOrder.getDeliveryCharge()));

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            InvoiceItemAdapter invoiceItemAdapter = new InvoiceItemAdapter(this, mOrder.getOrderItems());
            mItemsRecyclerView.setHasFixedSize(false);
            mItemsRecyclerView.setLayoutManager(layoutManager);
            mItemsRecyclerView.setAdapter(invoiceItemAdapter);

        }


        // section items
        bt_toggle_items = (ImageButton) findViewById(R.id.bt_toggle_items);
        lyt_expand_items = (View) findViewById(R.id.lyt_expand_items);
        bt_toggle_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, lyt_expand_items);
            }
        });

        // section address
        bt_toggle_address = (ImageButton) findViewById(R.id.bt_toggle_address);
        lyt_expand_address = (View) findViewById(R.id.lyt_expand_address);
        bt_toggle_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, lyt_expand_address);
            }
        });

        // copy to clipboard
        final TextView tv_invoice_code = (TextView) findViewById(R.id.tv_invoice_code);
        ImageButton bt_copy_code = (ImageButton) findViewById(R.id.bt_copy_code);
        bt_copy_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.copyToClipboard(getApplicationContext(), tv_invoice_code.getText().toString());
            }
        });
    }


    private void toggleSection(View bt, final View lyt) {
        boolean show = toggleArrow(bt);
        if (show) {
            ViewAnimation.expand(lyt, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(nested_scroll_view, lyt);
                }
            });
        } else {
            ViewAnimation.collapse(lyt);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
