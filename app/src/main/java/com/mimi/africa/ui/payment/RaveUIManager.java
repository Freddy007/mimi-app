package com.mimi.africa.ui.payment;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayInitializer;
import com.flutterwave.raveandroid.data.PaymentTypesCurrencyChecker;
import com.flutterwave.raveandroid.rave_java_commons.Meta;
import com.flutterwave.raveandroid.rave_java_commons.SubAccount;
import com.flutterwave.raveandroid.rave_presentation.RavePayManager;
import com.flutterwave.raveandroid.rave_presentation.data.Utils;
import com.mimi.africa.R;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_ACCOUNT;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_ACH;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_BANK_TRANSFER;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_BARTER;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_CARD;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_FRANCO_MOBILE_MONEY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_GH_MOBILE_MONEY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_MPESA;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_RW_MOBILE_MONEY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_SA_BANK_ACCOUNT;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_UG_MOBILE_MONEY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_UK;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_USSD;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.PAYMENT_TYPE_ZM_MOBILE_MONEY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.RAVEPAY;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.RAVE_PARAMS;
import static com.flutterwave.raveandroid.rave_java_commons.RaveConstants.RAVE_REQUEST_CODE;

public class RaveUIManager extends RavePayManager {
    private String TAG = RaveUIManager.class.getSimpleName();
    private Activity activity;
    private Fragment supportFragment;
    private android.app.Fragment fragment;
    private int theme = R.style.DefaultTheme;
    private boolean allowSaveCard = true;
    private boolean usePhoneAndEmailSuppliedToSaveCards = true;
    protected boolean showStagingLabel = true;
    private Boolean allowEditPhone = true;

    private ArrayList<Integer> orderedPaymentTypesList = new ArrayList<>();

    public RaveUIManager(Activity activity) {
        super();
        this.activity = activity;
    }

    public RaveUIManager(Fragment fragment) {
        super();
        this.supportFragment = fragment;
    }

    public RaveUIManager(android.app.Fragment fragment) {
        super();
        this.fragment = fragment;
    }


    public RaveUIManager setMeta(List<Meta> meta) {
        this.meta = Utils.stringifyMeta(meta);
        return this;
    }

    public RaveUIManager setSubAccounts(List<SubAccount> subAccounts) {
        this.subAccounts = Utils.stringifySubaccounts(subAccounts);
        return this;
    }

    public RaveUIManager setEmail(String email) {
        this.email = email;
        return this;
    }

    public RaveUIManager setAmount(double amount) {
        if (amount != 0) {
            this.amount = amount;
        }
        return this;
    }

    public RaveUIManager setPublicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    public RaveUIManager setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
        return this;
    }

    public RaveUIManager setTxRef(String txRef) {
        this.txRef = txRef;
        return this;
    }

    public RaveUIManager setNarration(String narration) {
        this.narration = narration;
        return this;
    }

    public RaveUIManager setCurrency(String currency) {
        this.currency = currency;
        switch (currency) {
            case "KES":
                country = "KE";
                break;
            case "GHS":
                country = "GH";
                break;
            case "ZAR":
                country = "ZA";
                break;
            case "TZS":
                country = "TZ";
                break;
            default:
                country = "NG";
                break;
        }
        return this;
    }

    @Deprecated
    public RaveUIManager setCountry(String country) {
        this.country = country;
        return this;
    }

    public RaveUIManager setfName(String fName) {
        this.fName = fName;
        return this;
    }

    public RaveUIManager setlName(String lName) {
        this.lName = lName;
        return this;
    }

    public RaveUIManager setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public RaveUIManager setPhoneNumber(String phoneNumber, Boolean isEditable) {
        this.phoneNumber = phoneNumber;
        this.allowEditPhone = isEditable;
        return this;
    }

    public RaveUIManager setPaymentPlan(String payment_plan) {
        this.payment_plan = payment_plan;
        return this;
    }

    public RaveUIManager onStagingEnv(boolean isStaging) {
        this.staging = isStaging;
        return this;
    }

    public RaveUIManager isPreAuth(boolean isPreAuth) {
        this.isPreAuth = isPreAuth;
        return this;
    }

    public RaveUIManager shouldDisplayFee(boolean displayFee) {
        this.displayFee = displayFee;
        return this;
    }

    public RaveUIManager showStagingLabel(boolean showStagingLabel) {
        this.showStagingLabel = showStagingLabel;
        return this;
    }

    public RaveUIManager withTheme(int theme) {
        this.theme = theme;
        return this;
    }

    public RaveUIManager allowSaveCardFeature(boolean allowSaveCard) {
        this.allowSaveCard = allowSaveCard;
        return this;
    }

    public RaveUIManager allowSaveCardFeature(boolean allowSaveCard, boolean usePhoneAndEmailSuppliedToSaveCards) {
        this.allowSaveCard = allowSaveCard;
        this.usePhoneAndEmailSuppliedToSaveCards = usePhoneAndEmailSuppliedToSaveCards;
        return this;
    }

    public RaveUIManager acceptAchPayments(boolean withAch) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_ACH) && withAch)
            orderedPaymentTypesList.add(PAYMENT_TYPE_ACH);
        return this;
    }

    public RaveUIManager acceptCardPayments(boolean withCard) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_CARD) && withCard)
            orderedPaymentTypesList.add(PAYMENT_TYPE_CARD);
        return this;
    }

    public RaveUIManager acceptMpesaPayments(boolean withMpesa) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_MPESA) && withMpesa)
            orderedPaymentTypesList.add(PAYMENT_TYPE_MPESA);
        return this;
    }

    public RaveUIManager acceptAccountPayments(boolean withAccount) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_ACCOUNT) && withAccount)
            orderedPaymentTypesList.add(PAYMENT_TYPE_ACCOUNT);
        return this;
    }

    public RaveUIManager acceptGHMobileMoneyPayments(boolean withGHMobileMoney) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_GH_MOBILE_MONEY) && withGHMobileMoney)
            orderedPaymentTypesList.add(PAYMENT_TYPE_GH_MOBILE_MONEY);
        return this;
    }

    public RaveUIManager acceptUgMobileMoneyPayments(boolean withUgMobileMoney) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_UG_MOBILE_MONEY) && withUgMobileMoney)
            orderedPaymentTypesList.add(PAYMENT_TYPE_UG_MOBILE_MONEY);
        return this;
    }

    public RaveUIManager acceptRwfMobileMoneyPayments(boolean withRwfMobileMoney) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_RW_MOBILE_MONEY) && withRwfMobileMoney)
            orderedPaymentTypesList.add(PAYMENT_TYPE_RW_MOBILE_MONEY);
        return this;
    }

    public RaveUIManager acceptZmMobileMoneyPayments(boolean withZmMobileMoney) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_ZM_MOBILE_MONEY) && withZmMobileMoney)
            orderedPaymentTypesList.add(PAYMENT_TYPE_ZM_MOBILE_MONEY);
        return this;
    }

    public RaveUIManager acceptUkPayments(boolean withUk) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_UK) && withUk)
            orderedPaymentTypesList.add(PAYMENT_TYPE_UK);
        return this;
    }

    public RaveUIManager acceptSaBankPayments(boolean withSaBankAccount) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_SA_BANK_ACCOUNT) && withSaBankAccount)
            orderedPaymentTypesList.add(PAYMENT_TYPE_SA_BANK_ACCOUNT);
        return this;
    }

    public RaveUIManager acceptFrancMobileMoneyPayments(boolean withFrancMobileMoney) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_FRANCO_MOBILE_MONEY) && withFrancMobileMoney)
            orderedPaymentTypesList.add(PAYMENT_TYPE_FRANCO_MOBILE_MONEY);
        return this;
    }

    public RaveUIManager acceptBankTransferPayments(boolean withBankTransfer) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_BANK_TRANSFER) && withBankTransfer)
            orderedPaymentTypesList.add(PAYMENT_TYPE_BANK_TRANSFER);
        return this;
    }


    public RaveUIManager acceptBankTransferPayments(boolean withBankTransfer, boolean isPermanent) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_BANK_TRANSFER) && withBankTransfer)
            orderedPaymentTypesList.add(PAYMENT_TYPE_BANK_TRANSFER);
        this.isPermanent = isPermanent;
        return this;
    }

    public RaveUIManager acceptBankTransferPayments(boolean withBankTransfer, int duration, int frequency) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_BANK_TRANSFER) && withBankTransfer)
            orderedPaymentTypesList.add(PAYMENT_TYPE_BANK_TRANSFER);
        this.duration = duration;
        this.frequency = frequency;
        return this;
    }

    public RaveUIManager acceptUssdPayments(boolean withUssd) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_USSD) && withUssd)
            orderedPaymentTypesList.add(PAYMENT_TYPE_USSD);
        return this;
    }

    public RaveUIManager acceptBarterPayments(boolean withBarter) {
        if (!orderedPaymentTypesList.contains(PAYMENT_TYPE_BARTER) && withBarter)
            orderedPaymentTypesList.add(PAYMENT_TYPE_BARTER);
        return this;
    }

    public RaveUIManager initialize() {
        filterPaymentTypes();

        if (orderedPaymentTypesList.size() == 0) {
            if (activity != null) {
                Toast.makeText(activity, "No valid payment types for the selected currency.", Toast.LENGTH_LONG).show();
            } else if (supportFragment != null && supportFragment.getContext() != null) {
                Toast.makeText(supportFragment.getContext(), "No valid payment types for the selected currency.", Toast.LENGTH_LONG).show();
            } else if (fragment != null && fragment.getActivity() != null) {
                Toast.makeText(fragment.getActivity(), "No valid payment types for the selected currency.", Toast.LENGTH_LONG).show();
            } else {
                Log.d(RAVEPAY, "No valid payment types for the selected currency.");
            }
            return this;
        }

        if (activity != null) {
            Log.i(TAG, "initialize: " + createRavePayInitializer().toString());
            Intent intent = new Intent(activity, RavePayActivity.class);
            intent.putExtra(RAVE_PARAMS, Parcels.wrap(createRavePayInitializer()));
            activity.startActivityForResult(intent, RAVE_REQUEST_CODE);
        } else if (supportFragment != null && supportFragment.getContext() != null) {
            Intent intent = new Intent(supportFragment.getContext(), RavePayActivity.class);
            intent.putExtra(RAVE_PARAMS, Parcels.wrap(createRavePayInitializer()));
            supportFragment.startActivityForResult(intent, RAVE_REQUEST_CODE);
        } else if (fragment != null && fragment.getActivity() != null) {
            Intent intent = new Intent(fragment.getActivity(), RavePayActivity.class);
            intent.putExtra(RAVE_PARAMS, Parcels.wrap(createRavePayInitializer()));
            fragment.startActivityForResult(intent, RAVE_REQUEST_CODE);
        } else {
            Log.d(RAVEPAY, "Context is required!");
        }
        return this;
    }

    private void filterPaymentTypes() {
        orderedPaymentTypesList =
                new PaymentTypesCurrencyChecker().applyCurrencyChecks(
                        orderedPaymentTypesList,
                        currency
                );
    }

    private RavePayInitializer createRavePayInitializer() {
        Log.i(TAG, "sendPaymentDetails: " +  getEmail() + " "+
                getAmount()+" "+
                getPublicKey() +" "+
                getEncryptionKey()+" "+
                getTxRef()+" "+
                getNarration()+" "+
                getCurrency()+" "+
                getCountry()+" "+
                getfName()+" "+
                getlName()+" "+
                theme+" "+
                getPhoneNumber()+" "+
                allowEditPhone+" "+
                allowSaveCard+" "+
                usePhoneAndEmailSuppliedToSaveCards+" "+
                isPermanent()+" "+
                getDuration()+" "+
                getFrequency()+" "+
                isStaging()+" "+
                getMeta() +" "+
                getSubAccounts() +" "+
                getPayment_plan() +" "+
                isPreAuth() +" "+
                showStagingLabel +" "+
                isDisplayFee()+" "+
                orderedPaymentTypesList.get(0).toString());
        return new RavePayInitializer(
                getEmail(),
                getAmount(),
                getPublicKey(),
                getEncryptionKey(),
                getTxRef(),
                getNarration(),
                getCurrency(),
                getCountry(),
                getfName(),
                getlName(),
                theme,
                getPhoneNumber(),
                allowEditPhone,
                allowSaveCard,
                usePhoneAndEmailSuppliedToSaveCards,
                isPermanent(),
                getDuration(),
                getFrequency(),
                isStaging(),
                getMeta(),
                getSubAccounts(),
                getPayment_plan(),
                isPreAuth(),
                showStagingLabel,
                isDisplayFee(),
                orderedPaymentTypesList);
    }

    public ArrayList<Integer> getOrderedPaymentTypesList() {
        return orderedPaymentTypesList;
    }
}
