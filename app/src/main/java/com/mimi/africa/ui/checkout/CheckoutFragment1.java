package com.mimi.africa.ui.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.CheckoutFragment1Binding;
import com.mimi.africa.model.City;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.viewModels.user.UserViewModel;

import javax.inject.Inject;


public class CheckoutFragment1 extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private UserViewModel userViewModel;
    private PSDialogMsg psDialogMsg;
    public String countryId = Constant.NO_DATA;
    public String cityId = Constant.NO_DATA;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @VisibleForTesting
    private AutoClearedValue<CheckoutFragment1Binding> binding;
//    private AutoClearedValue<ProgressDialog> prgDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CheckoutFragment1Binding dataBinding = DataBindingUtil.inflate(inflater, R.layout.checkout_fragment_1, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();

    }

    @Override
    public void onDispatched() {

    }

    @Override
    protected void initUIAndActions() {

        psDialogMsg = new PSDialogMsg(this.getActivity(), true);

        if (getActivity() instanceof MainCheckoutActivity) {
            ((MainCheckoutActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
            ((MainCheckoutActivity) getActivity()).progressDialog.setCancelable(false);
        }

//        if (userViewModel != null) {
//            if (userViewModel.user != null) {

//                pref.edit().putString(Constant.USER_NAME,   binding.get().userNameTextView.getText().toString()).apply();
//                pref.edit().putString(Constant.FULL_NAME,   binding.get().nameTextView.getText().toString()).apply();
//                pref.edit().putString(Constant.USER_EMAIL, binding.get().emailTextView.getText().toString()).apply();
//                pref.edit().putString(Constant.ADDRESS, binding.get().addressTextView.getText().toString()).apply();
//                pref.edit().putString(Constant.CITY, binding.get().cityTextView.getText().toString()).apply();

//                binding.get().userNameTextView.setText(userName);
//                binding.get().nameTextView.setText(fullName);
//                binding.get().emailTextView.setText(email);
//                binding.get().phoneNumberTextView.setText(phoneNumber);
//                binding.get().addressTextView.setText(CustomSharedPrefs.getUserAddress(getContext()));
//                binding.get().cityTextView.setText(city);

                if ( userViewModel != null && userViewModel.user.billingFirstName != null) {
                    binding.get().firstNameEditText.setText(this.userViewModel.user.shippingFirstName);
                } else {
                    binding.get().firstNameEditText.setText("");
                }

                if ( userViewModel != null && userViewModel.user.shippingEmail != null) {
                    binding.get().emailEditText.setText(userViewModel.user.shippingEmail);
                }else{
//                    binding.get().emailEditText.setText(email);
                }

                if (userViewModel != null && userViewModel.user.shippingPhone != null) {
                    binding.get().phoneEditText.setText(this.userViewModel.user.shippingPhone);
                }else {
//                    binding.get().phoneEditText.setText(phoneNumber);
                }

                if (userViewModel != null && userViewModel.user.shippingAddress1 != null) {
//                    binding.get().address1EditText.setText(this.userViewModel.user.shippingAddress1);
                    binding.get().address1EditText.setText(this.userViewModel.user.shippingAddress1);
                }else {

                }

                if (userViewModel != null && userViewModel.user.shippingCity != null) {
                    binding.get().cityTextView.setText(this.userViewModel.user.shippingCity);
                }else{
//                    binding.get().cityTextView.setText(city);
                }

            }


    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {}

    @Override
    protected void initData() {

        if (userViewModel != null) {
            userViewModel.setLoginUser();
            userViewModel.getLoginUser().observe(this, listResource -> {
                if (listResource != null && listResource.size() > 0) {
                    CheckoutFragment1.this.fadeIn(binding.get().getRoot());
                    userViewModel.user = listResource.get(0).user;
                    if (getActivity() != null) {
                        ((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).setCurrentUser(userViewModel.user);
                        CheckoutFragment1.this.initUIAndActions();
                    }
                }
            });

            userViewModel.getUpdateUserData().observe(this, listResource -> {

                if (listResource != null) {

                    switch (listResource.status) {
                        case SUCCESS:
                            if (listResource.data != null) {
                                userViewModel.setLoadingState(false);
                                if (CheckoutFragment1.this.getActivity() != null && CheckoutFragment1.this.getActivity() instanceof MainCheckoutActivity) {
                                    ((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).progressDialog.hide();
                                }

                                userViewModel.setLoginUser();
                                if (CheckoutFragment1.this.getActivity() != null) {
                                    ((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).navigateFragment(((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).binding, 2);
                                }
                            }
                            break;

                        case ERROR:

                            if (CheckoutFragment1.this.getActivity() != null) {
                                ((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).number = 1;
                            }

                            psDialogMsg.showErrorDialog(listResource.message, CheckoutFragment1.this.getString(R.string.app__ok));
                            psDialogMsg.show();
                            psDialogMsg.okButton.setOnClickListener(v -> psDialogMsg.cancel());

                            if (CheckoutFragment1.this.getActivity() != null && CheckoutFragment1.this.getActivity() instanceof MainCheckoutActivity) {
                                ((MainCheckoutActivity) CheckoutFragment1.this.getActivity()).progressDialog.hide();
                            }

                            userViewModel.setLoadingState(false);
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    boolean checkToUpdateProfile() {
        if (userViewModel != null ) {
            return binding.get().firstNameEditText.getText().toString().equals(userViewModel.user.shippingFirstName) &&
                    binding.get().lastNameEditText.getText().toString().equals(userViewModel.user.shippingLastName) &&
                    binding.get().address1EditText.getText().toString().equals(userViewModel.user.shippingAddress1) &&
                    binding.get().emailEditText.getText().toString().equals(userViewModel.user.shippingEmail) &&
                    binding.get().phoneEditText.getText().toString().equals(userViewModel.user.shippingPhone);
        }

        return false;

    }

    public boolean checkShippingAddressEditTextIsEmpty() {
        return binding.get().address1EditText.getText().toString().isEmpty();
    }

    public boolean checkBillingAddressEditTextIsEmpty() {
        return false;
    }

    public boolean checkUserEmailEditTextIsEmpty(){
        return binding.get().emailEditText.getText().toString().isEmpty();
    }

    public void updateUserProfile() {

        pref.edit().putString(Constant.USER_NAME,   userViewModel.user.userName).apply();
        pref.edit().putString(Constant.FULL_NAME,    binding.get().firstNameEditText.getText().toString()).apply();
        pref.edit().putString(Constant.USER_EMAIL,     binding.get().emailEditText.getText().toString()).apply();
        pref.edit().putString(Constant.ADDRESS,    binding.get().address1EditText.getText().toString()).apply();
        pref.edit().putString(Constant.CITY, binding.get().cityTextView.getText().toString()).apply();

        User user = new User(
                loginUserId,
                "",
                "",
                "",
                "",
                userViewModel.user.userName,
                binding.get().emailEditText.getText().toString(),
                binding.get().phoneEditText.getText().toString(),
                userViewModel.user.userPassword,
                userViewModel.user.userAboutMe,
                userViewModel.user.userCoverPhoto,
                userViewModel.user.userProfilePhoto,
                userViewModel.user.roleId,
                userViewModel.user.status,
                userViewModel.user.isBanned,
                userViewModel.user.addedDate,
              binding.get().firstNameEditText.getText().toString(),
              binding.get().firstNameEditText.getText().toString(),
             "",
                binding.get().address1EditText.getText().toString(),
               "",
              "",
                "",
                binding.get().cityTextView.getText().toString(),
              "",
                binding.get().emailEditText.getText().toString(),
                binding.get().phoneEditText.getText().toString(),
                binding.get().firstNameEditText.getText().toString(),
                binding.get().lastNameEditText.getText().toString(),
                "",
                binding.get().address1EditText.getText().toString(),
                "",
                "",
               "",
                binding.get().cityTextView.getText().toString(),
               "",
                binding.get().emailEditText.getText().toString(),
                binding.get().phoneEditText.getText().toString(),
                userViewModel.user.deviceToken,
                userViewModel.user.code,
                userViewModel.user.verifyTypes,
                userViewModel.user.addedDateStr,
                new City(userViewModel.cityId,
                        "",
                        userViewModel.countryId, null, null,
                        null, null, null, null, null)
        );

        // TODO
        if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
            ((MainCheckoutActivity) getActivity()).progressDialog.show();
        }

        userViewModel.setUpdateUserObj(user);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.REQUEST_CODE__SEARCH_FRAGMENT && resultCode == Constant.RESULT_CODE__SEARCH_WITH_COUNTRY) {
            this.countryId = data.getStringExtra(Constant.COUNTRY_ID);
            userViewModel.countryId = this.countryId;
            this.cityId = "";
            userViewModel.cityId = this.cityId;
            if ((getActivity()) != null) {
                ((MainCheckoutActivity) getActivity()).user.city.id = userViewModel.cityId;
            }

        } else if (requestCode == Constant.REQUEST_CODE__SEARCH_FRAGMENT && resultCode == Constant.RESULT_CODE__SEARCH_WITH_CITY) {
            this.cityId = data.getStringExtra(Constant.CITY_ID);
            userViewModel.cityId = this.cityId;
            if ((getActivity()) != null) {
                ((MainCheckoutActivity) getActivity()).user.city.id = userViewModel.cityId;
            }
        }
    }
}
