package com.mimi.africa.ui.message;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mimi.africa.R;
import com.mimi.africa.model.Notification;
import com.mimi.africa.utils.TimeAgo;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context ctx;
    private List<Notification> items;
    private OnClickListener onClickListener = null;

    private SparseBooleanArray selected_items;
    private int current_selected_idx = -1;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnItemClickListener(Notification notification, OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView from, email, message, date, image_letter;
                public ImageView image;
        public RelativeLayout lyt_checked, lyt_image;
        public View lyt_parent;

        public ViewHolder(View view) {
            super(view);
            from = view.findViewById(R.id.from);
            email = view.findViewById(R.id.email);
            message = view.findViewById(R.id.message);
            date = view.findViewById(R.id.date);
            image_letter = view.findViewById(R.id.image_letter);
            image = view.findViewById(R.id.image);
            lyt_checked = view.findViewById(R.id.lyt_checked);
            lyt_image = view.findViewById(R.id.lyt_image);
            lyt_parent = view.findViewById(R.id.lyt_parent);

        }
    }

    public MessageAdapter(Context mContext, List<Notification> items) {
        this.ctx = mContext;
        this.items = items;
        selected_items = new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inbox, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Notification notification = items.get(position);

        if (notification.getSender() != null) {

            if (notification.isArchived()){
                holder.from.setPaintFlags( holder.from.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.email.setPaintFlags( holder.from.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.message.setPaintFlags( holder.message.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            if (notification.isRead()){
                holder.from.setText(ctx.getResources().getString(R.string.new_message));
                holder.from.setTypeface(holder.from.getTypeface(), Typeface.ITALIC);
                holder.email.setTypeface(holder.email.getTypeface(), Typeface.ITALIC);
            }else {
                    holder.from.setText(ctx.getResources().getString(R.string.new_message));
                    holder.from.setTypeface(holder.from.getTypeface(), Typeface.NORMAL);
                    holder.email.setTypeface(holder.email.getTypeface(), Typeface.NORMAL);
            }
        }

        if (notification.getName() != null) {
            if (notification.isRead()){

                holder.email.setText(notification.getSender());
                holder.email.setTypeface(holder.from.getTypeface(), Typeface.NORMAL);

            }else {
                holder.email.setText(notification.getSender());
                holder.email.setTypeface(holder.from.getTypeface(), Typeface.BOLD);
            }

        }
        holder.message.setText(Html.fromHtml(notification.getMessage()));

        String timeAgo = TimeAgo.getTimeAgo(notification.getDateCreated());

        holder.date.setText(timeAgo);

        if (notification.getSender() != null) {
            holder.image_letter.setText(notification.getSender().substring(0, 1));
        }

        holder.lyt_parent.setActivated(selected_items.get(position, false));

        holder.lyt_parent.setOnClickListener(v -> {
            if (onClickListener == null) return;
            onClickListener.onItemClick(v, notification, position);
        });

        holder.lyt_parent.setOnLongClickListener(v -> {
            if (onClickListener == null) return false;
            onClickListener.onItemLongClick(v, notification, position);
            return true;
        });

        toggleCheckedIcon(holder, position);
        displayImage(holder, notification);

    }

    private void displayImage(ViewHolder holder, Notification notification) {
        holder.image.setImageResource(R.drawable.shape_circle);
        holder.image.setColorFilter(ctx.getResources().getColor(R.color.blue_800));
        holder.image_letter.setVisibility(View.VISIBLE);
    }

    private void toggleCheckedIcon(ViewHolder holder, int position) {
        if (selected_items.get(position, false)) {
            holder.lyt_image.setVisibility(View.GONE);
            holder.lyt_checked.setVisibility(View.VISIBLE);
            if (current_selected_idx == position) resetCurrentIndex();
        } else {
            holder.lyt_checked.setVisibility(View.GONE);
            holder.lyt_image.setVisibility(View.VISIBLE);
            if (current_selected_idx == position) resetCurrentIndex();
        }
    }

    public Notification getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void toggleSelection(int pos) {
        current_selected_idx = pos;
        if (selected_items.get(pos, false)) {
            selected_items.delete(pos);
        } else {
            selected_items.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selected_items.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selected_items.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selected_items.size());
        for (int i = 0; i < selected_items.size(); i++) {
            items.add(selected_items.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        items.remove(position);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        current_selected_idx = -1;
    }

    public interface OnClickListener {
        void onItemClick(View view, Notification obj, int pos);

        void onItemLongClick(View view, Notification obj, int pos);
    }
}