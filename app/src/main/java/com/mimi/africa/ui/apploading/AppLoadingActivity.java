package com.mimi.africa.ui.apploading;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityAppLoadingBinding;
import com.mimi.africa.ui.common.BaseActivity;


public class AppLoadingActivity extends BaseActivity {

    //region Override Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityAppLoadingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_app_loading);
        // Init all UI
        initUI(binding);

    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
//        String CURRENT_LANG_CODE = preferences.getString(Constants.LANGUAGE_CODE, Config.DEFAULT_LANGUAGE);
//        String CURRENT_LANG_COUNTRY_CODE = preferences.getString(Constants.LANGUAGE_COUNTRY_CODE, Config.DEFAULT_LANGUAGE_COUNTRY_CODE);
//
//        super.attachBaseContext(MyContextWrapper.wrap(newBase, CURRENT_LANG_CODE, CURRENT_LANG_COUNTRY_CODE, true));
//    }
    //endregion


    //region Private Methods

    private void initUI(ActivityAppLoadingBinding binding) {

        // setup Fragment

        setupFragment(new AppLoadingFragment());
    }
}
//endregion


