package com.mimi.africa.ui.chat;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.Reply;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import java.util.List;


public class ChatReplyAdapter extends RecyclerView.Adapter<ChatReplyAdapter.ItemViewHolder> {
    private static List<Reply> dataList;
    private LayoutInflater mInflater;
    private Context context;

    private OnClickListener onClickListener = null;
    private OnClickListener OnLongClickListener = null;

    public ChatReplyAdapter(Context ctx, List<Reply> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout receiveLayout;
        private RelativeLayout sendLayout;
        private ImageView receiveImage;
        private TextView receiveMessage;
        private TextView receiveTime;
        private TextView sendMessage;
        private TextView sendTime;

        public ItemViewHolder(View itemView) {
            super(itemView);

            receiveLayout = itemView.findViewById(R.id.receiveLayout);
            sendLayout = itemView.findViewById(R.id.sendLayout);
            receiveImage = itemView.findViewById(R.id.receiveImage);
            receiveMessage = itemView.findViewById(R.id.receiveMessage);
            receiveTime = itemView.findViewById(R.id.receiveTime);
            sendMessage = itemView.findViewById(R.id.sendMessage);
            sendTime = itemView.findViewById(R.id.sendTime);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_reply_row, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {

        Reply reply = dataList.get(position);

//                    holder.receiveImage.setVisibility(View.GONE);
//                    holder.sendLayout.setVisibility(View.VISIBLE);
        holder.receiveLayout.setVisibility(View.VISIBLE);

        if (reply.getImages() != null && reply.getImages().size() > 0) {

            holder.receiveMessage.setVisibility(View.GONE);
            holder.receiveImage.setVisibility(View.VISIBLE);

            String url = Constants.IMAGES_BASE_URL + reply.getImages().get(0).getPath();
            Utils.LoadImage(context,holder.receiveImage, url);

        }else {
            holder.receiveMessage.setText(Html.fromHtml(reply.getReply()));

            holder.receiveMessage.setVisibility(View.VISIBLE);
            holder.receiveImage.setVisibility(View.GONE);
        }

        holder.receiveTime.setText(reply.getCreatedAt());

        holder.receiveLayout.setOnLongClickListener(v -> {
            if (onClickListener == null) return false;
            onClickListener.onItemLongClick(v,reply,position);
            return true;
        });

        holder.receiveMessage.setOnLongClickListener(v -> {
            if (onClickListener == null) return false;
            onClickListener.onItemLongClick(v,reply,position);
            return true;
        });

        holder.receiveImage.setOnLongClickListener(v -> {
            if (onClickListener == null) return false;
            onClickListener.onItemLongClick(v,reply,position);
            return true;
        });


//        if(dataList.get(position).isImage()) {
////            holder.receiveImage.setVisibility(View.VISIBLE);
//            holder.sendLayout.setVisibility(View.GONE);
//            holder.receiveLayout.setVisibility(View.GONE);
//        }else if(dataList.get(position).isSend()) {
////            holder.receiveImage.setVisibility(View.GONE);
//            holder.sendLayout.setVisibility(View.VISIBLE);
//            holder.receiveLayout.setVisibility(View.GONE);
//            holder.sendMessage.setText(dataList.get(position).getMessage());
//            holder.sendTime.setText(dataList.get(position).getTime());
//        }else{
////            holder.receiveImage.setVisibility(View.GONE);
//            holder.sendLayout.setVisibility(View.GONE);
//            holder.receiveLayout.setVisibility(View.VISIBLE);
//            holder.receiveMessage.setText(dataList.get(position).getMessage());
//            holder.receiveTime.setText(dataList.get(position).getTime());
//        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public interface OnClickListener {
        void onItemClick(View view, Reply obj, int pos);

        void onItemLongClick(View view, Reply obj, int pos);
    }

}
