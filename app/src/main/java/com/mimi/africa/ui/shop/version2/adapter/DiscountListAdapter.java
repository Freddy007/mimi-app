package com.mimi.africa.ui.shop.version2.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.mimi.africa.R;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DiscountListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Inventory> items = new ArrayList<>();

    private Context ctx;
    private DiscountListAdapter.OnItemClickListener mOnItemClickListener;
    private DiscountListAdapter.OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final DiscountListAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final DiscountListAdapter.OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public DiscountListAdapter(Context context, List<Inventory> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView imageCardView;
        public TextView shopNameTextView;
        public ImageView image;
        public ImageView productImageView;
        public TextView title;
        public TextView price;
        public TextView originalPrice;
        public TextView addressTextView;
        public TextView isSoldTextView;
        public TextView addedDateStrTextView;
        public ImageView profileCircleImageView;
        public ImageButton more;
        public View lyt_parent;
        public RatingBar ratingBar;
        public TextView ratingBarTextView;
        public TextView discountTextView;

        public OriginalViewHolder(@NonNull View v) {
            super(v);

            imageCardView =v.findViewById(R.id.newsHolderCardView);
            productImageView = v.findViewById(R.id.imageView);
            title = v.findViewById(R.id.news_title_textView);
            price = v.findViewById(R.id.priceTextView);
            originalPrice                        =  v.findViewById(R.id.originalPriceTextView);
            ratingBar                                     = v.findViewById(R.id.ratingBar);
            ratingBarTextView                   = v.findViewById(R.id.ratingBarTextView);
            discountTextView                   = v.findViewById(R.id.discountTextView);

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discount_list_adapter, parent, false);
        vh = new DiscountListAdapter.OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof DiscountListAdapter.OriginalViewHolder) {
            DiscountListAdapter.OriginalViewHolder view = (DiscountListAdapter.OriginalViewHolder) holder;
            final Inventory inventory = items.get(position);

            view.title.setText(inventory.getTitle());

            view.ratingBar.setRating((float) inventory.getRating());

            view.ratingBarTextView.setText(String.format("%s rating  (%d Reviews )", inventory.getRating(), inventory.getSumFeedbacks()));

            if (inventory.getImage() != null){
                if (inventory.getImage().getPath() != null) {
                    if (inventory.getImage().getPath().startsWith("http")){

                        Picasso.get().load(inventory.getImage().getPath()).into(view.productImageView);
//                        Picasso.with(ctx).load(inventory.getImage().getPath()).into(view.productImageView);
                    }else {
                        String imageUrl = Constants.IMAGES_BASE_URL +  inventory.getImage().getPath();
                        Picasso.get().load(imageUrl).into(view.productImageView);
//                        Picasso.with(ctx).load(imageUrl).into(view.productImageView);
                    }
                }
            }

            if ( inventory.getOfferPrice() != null){
                view.discountTextView.setVisibility(View.GONE);
                view.originalPrice.setVisibility(View.VISIBLE);
                view.originalPrice.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));

                view.originalPrice.setPaintFlags(view.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getOfferPrice()))));

//                int offerPrice =  Integer.parseInt(inventory.getOfferPrice());
//                int difference = Integer.parseInt(inventory.getSalePrice()) - offerPrice;
//                int percentage = (difference/offerPrice) * 100;

//                view.discountTextView.setText(String.format("%d%%", percentage));

            }else {
                view.discountTextView.setVisibility(View.GONE);
                view.originalPrice.setVisibility(View.GONE);
                view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));
            }

            view.productImageView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.productImageView, items.get(position), position);
                }
            });

            view.imageCardView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view.imageCardView, items.get(position), position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Inventory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, Inventory obj, MenuItem item);
    }

}
