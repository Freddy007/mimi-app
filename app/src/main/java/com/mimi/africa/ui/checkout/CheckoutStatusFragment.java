package com.mimi.africa.ui.checkout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.CheckoutFragmentStatusBinding;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;

public class CheckoutStatusFragment extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private AutoClearedValue<CheckoutFragmentStatusBinding> binding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CheckoutFragmentStatusBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.checkout_fragment_status, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);
        return binding.get().getRoot();
    }

    @Override
    public void onDispatched() {

    }

    @Override
    protected void initUIAndActions() {

//        if (getActivity() != null) {
//
//            String currencySymbol = ((MainCheckoutActivity) getActivity()).transactionObject.currencySymbol;
//
////            binding.get().reviewCardView.setOnClickListener(view -> navigationController.navigateToTransactionDetail(getActivity(), ((CheckoutActivity) getActivity()).transactionObject));
//
//            binding.get().transactionNumberTextView.setText(((MainCheckoutActivity) Objects.requireNonNull(getActivity())).transactionObject.transCode);
//
//            binding.get().imageView24.setOnClickListener(v -> {
//                ClipboardManager clipboard = (ClipboardManager) Objects.requireNonNull(getActivity()).getSystemService(Context.CLIPBOARD_SERVICE);
//                ClipData clip = ClipData.newPlainText(Constant.TRANSACTION_NUMBER, ((MainCheckoutActivity) Objects.requireNonNull(getActivity())).transactionObject.transCode);
//
//                if (clipboard != null && clip != null) {
//                    clipboard.setPrimaryClip(clip);
//
//                    Toast.makeText(getActivity(), getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
//                }
//            });
//
//
//            if (!((MainCheckoutActivity) Objects.requireNonNull(getActivity())).transactionObject.totalItemCount.equals(Constants.ZERO)) {
//                binding.get().totalItemCountValueTextView.setText(((MainCheckoutActivity) getActivity()).transactionObject.totalItemCount);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.totalItemAmount.equals(Constants.ZERO)) {
//
//                String totalValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.totalItemAmount));
//                binding.get().totalValueTextView.setText(totalValueString);
//
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.discountAmount.equals(Constants.ZERO)) {
//
//                String discountValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.discountAmount));
//                binding.get().discountValueTextView.setText(discountValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.subTotalAmount.equals(Constants.ZERO)) {
//                String subTotalValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.subTotalAmount));
//                binding.get().subtotalValueTextView.setText(subTotalValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.taxAmount.equals(Constants.ZERO)) {
//                String taxValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.taxAmount));
//                binding.get().taxValueTextView.setText(taxValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.shippingAmount.equals(Constants.ZERO)) {
//                String shippingTaxValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.shippingAmount));
//                binding.get().shippingTaxValueTextView.setText(shippingTaxValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.balanceAmount.equals(Constants.ZERO)) {
//                String finalTotalValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.balanceAmount));
//                binding.get().finalTotalValueTextView.setText(finalTotalValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.shippingMethodAmount.equals(Constants.ZERO)) {
//                String shippingCostValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.shippingMethodAmount));
//                binding.get().shippingCostValueTextView.setText(shippingCostValueString);
//            }
//
//            if (!((MainCheckoutActivity) getActivity()).transactionObject.couponDiscountAmount.equals(Constants.ZERO)) {
//                String couponDiscountValueString = currencySymbol + Constant.SPACE_STRING + Utils.format(Double.parseDouble(((MainCheckoutActivity) getActivity()).transactionObject.couponDiscountAmount));
//                binding.get().couponDiscountValueTextView.setText(couponDiscountValueString);
//            }
//
//            if (!overAllTaxLabel.equals(Constants.ZERO)) {
//
//                binding.get().textView22.setText(getString(R.string.tax, overAllTaxLabel));
//            }
//
//            if (!shippingTaxLabel.equals(Constants.ZERO)) {
//
//                binding.get().textView24.setText(getString(R.string.shipping_tax, shippingTaxLabel));
//            }
//        }
    }

    @Override
    protected void initViewModels() {

    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

        if (this.getActivity() != null) {

            User user = ((MainCheckoutActivity) this.getActivity()).getCurrentUser();
            String text = getString(R.string.checkout_status__thank_you) + Constant.SPACE_STRING + user.userName;

            binding.get().nameTitleTextView.setText(text);

            binding.get().shippingPhoneValueTextView.setText(user.shippingPhone);
            binding.get().shippingEmailValueTextView.setText(user.shippingEmail);
            binding.get().shippingAddressValueTextView.setText(user.shippingAddress1);

            binding.get().billingphoneValueTextView.setText(user.billingPhone);
            binding.get().billingEmailValueTextView.setText(user.billingEmail);
            binding.get().billingAddressValueTextView.setText(user.billingAddress1);

        }
    }
}
