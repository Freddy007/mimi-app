package com.mimi.africa.ui.notifications;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.Adapter.NotificationAdapter;
import com.mimi.africa.R;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.ItemAnimation;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.notification.NotificationViewModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class NotificationsFragment extends BaseFragment {

      private static final String TAG = NotificationsFragment.class.getSimpleName();

      private NotificationViewModel notificationViewModel;
      private NotificationAdapter mAdapter;
      private List<Notification> items = new ArrayList<>();
      private int animation_type = ItemAnimation.BOTTOM_UP;
      private RecyclerView recyclerView;
      private View view;
      private LinearLayout noNotificationsLinearLayout;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      @Inject
      MimiDb mimiDb;
      private LinearLayout progressLinearLayout;

      public NotificationsFragment() {}

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            view = inflater.inflate(R.layout.fragment_notifications2, container, false);
//            mimiDb = MimiDb.getInstance(getContext());
            return  view;
      }

      @Override
      protected void initUIAndActions() {
            Context context = getContext();
            recyclerView = view.findViewById(R.id.notifications_recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);
            noNotificationsLinearLayout = view.findViewById(R.id.no_notifications_linear_layout);
            progressLinearLayout = view.findViewById(R.id.lyt_progress);
            animation_type = ItemAnimation.BOTTOM_UP;
      }

      @Override
      protected void initViewModels() {
//            notificationViewModel =  ViewModelProviders.of(getActivity()).get(NotificationViewModel.class);
            notificationViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel.class);

      }

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {

            new Handler().postDelayed(() -> {
                  getNotificationsFromDatabase();
                  Utils.RemoveView(progressLinearLayout);
            },200);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(AddToCart event) {}

      private void getNotificationsFromDatabase(){
            notificationViewModel.getNotifications().observe(getActivity(), appNotifications -> {
                  items = appNotifications;

                  if (appNotifications.size() != 0){

                        setAdapter();
                        noNotificationsLinearLayout.setVisibility(View.GONE);

                  }else {
//                              progressLayout.setVisibility(View.GONE);
                              noNotificationsLinearLayout.setVisibility(View.VISIBLE);
                  }
            });
      }

      private void setAdapter() {
            //set data and list adapter
            try{
                  mAdapter = new NotificationAdapter(getContext(), items, animation_type);
                  recyclerView.setAdapter(mAdapter);
            }catch (Exception e){
                  if (mAdapter != null ){
//                        mAdapter = new AppNotificationAdapter(getContext(), items, animation_type);
//                        recyclerView.setAdapter(mAdapter);
                  }
            }
      }

}
