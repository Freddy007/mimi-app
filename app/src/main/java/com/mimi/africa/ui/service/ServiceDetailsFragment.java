package com.mimi.africa.ui.service;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.Adapter.ServiceCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ImageResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentServiceDetailsBinding;
import com.mimi.africa.event.ReviewsClickedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Rating;
import com.mimi.africa.model.Service;
import com.mimi.africa.ui.auth.LoginActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ServiceDetailsFragment extends BaseFragment {

      private static final String TAG =  ServiceDetailsFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentServiceDetailsBinding> binding;

      @Nullable
      private Service mService;

      @Inject
       APIService mAPIService;

      private String  messenger, whatsappNo;

      private boolean twist = false;

      private PSDialogMsg psDialogMsg;

      @Inject
      protected NavigationController navigationController;

      public ServiceDetailsFragment() {}

      @NonNull
      public static ServiceDetailsFragment newInstance(Service service){

            ServiceDetailsFragment fragment = new ServiceDetailsFragment();
            Bundle args = new Bundle();
            args.putParcelable(Constants.SERVICE_OBECT, service);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null){
                  mService = getArguments().getParcelable(Constants.SERVICE_OBECT);
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentServiceDetailsBinding serviceDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_details, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, serviceDetailsBinding);

            return serviceDetailsBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            psDialogMsg = new PSDialogMsg(getActivity(), false);


            messenger = "me";
            whatsappNo = "";
            String shopPhoneNumber = "";
            if (mService.getShop() != null && mService.getShop().getAddresses() != null) {
                  if (mService.getShop().getAddresses().size() > 0) {
                        shopPhoneNumber = mService.getShop().getAddresses().get(0).getPhone();
                        whatsappNo = shopPhoneNumber;
                  }

                  binding.get().productShop.setText(mService.getShop().getName());

                  binding.get().productShop.setOnClickListener(v -> {
                        Intent intent = new Intent(getContext(), StoreActivity.class);
                        intent.putExtra(Constants.SHOP_OBECT, mService.getShop());
                        startActivity(intent);
                  });
            }

            hideFloatingButton();

            (binding.get()).productTitle.setText(mService.getName());
            binding.get().productDescription.setText(Html.fromHtml(mService.getDescription()));

            try{

                  binding.get().price.setText(String.format("GHS%s", Utils.TwoDecimalPlace(Double.parseDouble(mService.getPrice()))));

            }catch (Exception ex){
                  Log.e(TAG, "initUIAndActions:  "  + ex.getMessage() );
            }

            if (mService.getImages().size() > 0) {
                  String imageUrl = Constants.IMAGES_BASE_URL + mService.getImages().get(0).getPath();
                  Picasso.get().load(imageUrl).into(binding.get().image);
//                  Picasso.with(getContext()).load(imageUrl).into(binding.get().image);

            }

//            binding.get().shopPage.setOnClickListener(v -> {
//                  Intent intent = new Intent(getContext(), StoreActivity.class);
//                  intent.putExtra(Constants.SHOP_OBECT, mService.getShop());
//                  startActivity(intent);
//            });

            if (mService.getFeedbacks() != null && mService.getFeedbacks().size() > 0) {

                  List<Feedback> feedbacks = mService.getFeedbacks();

                  int randomNum = ThreadLocalRandom.current().nextInt(0, feedbacks.size());

                  String reviewMessage = feedbacks.get(randomNum).getComment();
                  String customerName = null;
                  if (feedbacks.get(randomNum).getCustomer() != null) {
                        customerName = feedbacks.get(randomNum).getCustomer().getName();
                  }

                  binding.get().reviews.setText(String.format("%s - %s", reviewMessage, customerName));

                  binding.get().seeAllReviews.setOnClickListener(v -> {
                        EventBus.getDefault().post(new ReviewsClickedEvent(mService.getFeedbacks()));
                  });

            } else {
                  Utils.RemoveView(binding.get().seeAllReviews);
                  binding.get().reviews.setText("No reviews for this item!");
            }



                  binding.get().locateShop.setOnClickListener(v -> {

                        if (mService.getShop().getLat() != null || mService.getShop().getLng() != null) {

                              Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mService.getShop().getLat() + "," + mService.getShop().getLng());
                              Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                              mapIntent.setPackage("com.google.android.apps.maps");
                              if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                                    startActivity(mapIntent);
                              }

                        } else {
                              Utils.ShowSnackBar(getActivity(), "No location information fo this shop!", Snackbar.LENGTH_SHORT);
                        }

                  });

            if (mService.getShop().getAddresses() != null && mService.getShop() != null &&
                    mService.getShop().getAddresses().get(0).getPhone() != null) {

                  binding.get().phoneFloatingActionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              Utils.callPhone(getActivity(), mService.getShop().getAddresses().get(0).getPhone());
                        }
                  });
            }

            Utils.hideFab(binding.get().phoneFloatingActionButton);
            Utils.hideFab(binding.get().phoneTextView);

            binding.get().mainFloatingActionButton.setOnClickListener(v -> {
                  twist = Utils.twistFab(v, !twist);

                  if (twist) {
                        Utils.showFab(binding.get().whatsappFloatingActionButton);
                        Utils.showFab(binding.get().whatsAppTextView);
                        Utils.showFab(binding.get().phoneFloatingActionButton);
                        Utils.showFab(binding.get().phoneTextView);

                  } else {
                        Utils.hideFab(binding.get().whatsappFloatingActionButton);
                        Utils.hideFab(binding.get().whatsAppTextView);
                        Utils.hideFab(binding.get().phoneTextView);
                        Utils.hideFab(binding.get().phoneFloatingActionButton);
                  }

            });

            binding.get().whatsappFloatingActionButton.setOnClickListener(v -> {

                  Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                          ServiceDetailsFragment.this.getActivity(), navigationController, () -> {
                                Intent intent = new Intent(getContext(), MessageActivity.class);
                                intent.putExtra(Constants.SHOP_ID, mService.getShop().getId());
                                intent.putExtra(Constants.SHOP_NAME, mService.getShop().getName());
                                startActivity(intent);
                          });
            });

            binding.get().reviewsButton.setOnClickListener(v -> {

                  String tagClass = ServiceDetailsActivity.class.getSimpleName();

                  Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, tagClass,psDialogMsg, ServiceDetailsFragment.this.getActivity(), navigationController, new Utils.NavigateOnUserVerificationActivityCallback() {
                        @Override
                        public void onSuccess() {
                              showCustomDialog();
                        }
                  });
            });
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
            getImageListings(String.valueOf(mService.getId()));
            getRating();
      }

      private void getImageListings(String productId){

            mAPIService.getImageListings(productId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ImageResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ImageResponse listingResponse) {

                                if (listingResponse.getImages() != null && listingResponse.getImages().size() > 0) {

                                      Log.i(TAG, "onNext:  " + listingResponse.getImages().get(0).getPath());

                                      if (listingResponse.getImages().size() > 0) {

                                            for (int i = 0; i < listingResponse.getImages().size(); i++) {

                                                  ImageView imageView = new ImageView(getContext());
                                                  LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(90, 90);
                                                  lp.setMarginEnd(5);
                                                  lp.setMarginStart(5);
                                                  imageView.setLayoutParams(lp);
                                                  imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                                                  String path = Constants.IMAGES_BASE_URL + mService.getImages().get(i).getPath();

//                                                  Picasso.with(getContext())
                                                  Picasso.get()
                                                          .load(path).into(imageView);

                                                  Log.i(TAG, "onNext:  Image Uri  " + path  );
                                                  binding.get().galleryLinearLayout.addView(imageView);

                                                  imageView.setOnClickListener(v -> showDialogImage(path));
                                            }
                                      }
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, " onError:  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                                getRelatedServices();
                          }
                    });
      }

      private void showDialogImage(String url) {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_image_center);

            Utils.LoadImage(getContext(), dialog.findViewById(R.id.dialog_image), url);

            dialog.findViewById(R.id.close_image_dialog).setOnClickListener(v -> dialog.dismiss());

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.show();
      }

      private void hideFloatingButton() {

            Utils.hideFirstFab(binding.get().whatsappFloatingActionButton);
            Utils.hideFab(binding.get().whatsAppTextView);
      }

      private void showCustomDialog() {

            String customerId = loginUserId;
            String inventoryId  = String.valueOf(mService.getId());
            String approved = "1";
            String customerrName = userName;

            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_add_review);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            final TextView customer_name_textview = dialog.findViewById(R.id.customer_name);
            customer_name_textview.setText(customerrName);
            final EditText et_post = dialog.findViewById(R.id.et_post);
            final AppCompatRatingBar rating_bar = dialog.findViewById(R.id.rating_bar);
            dialog.findViewById(R.id.bt_cancel).setOnClickListener(v -> dialog.dismiss());

            dialog.findViewById(R.id.bt_submit).setOnClickListener(v -> {
                  String review = et_post.getText().toString().trim();
                  if (review.isEmpty()) {
                        Toast.makeText(getContext(), ("Please fill review text"), Toast.LENGTH_SHORT).show();
                  } else {
                        String rating = String.valueOf(rating_bar.getRating());
                        addFeedback(Constants.SERVICE_TYPE, customerId,inventoryId,rating,review,approved);

                        dialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.submitted), Toast.LENGTH_SHORT).show();
                  }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

      private void addFeedback(String type, String customerId, String typeId, String rating, String comment, String approved){
            mAPIService.saveProductFeedback(type, customerId,typeId, rating, comment,approved)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError:  " + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {

                                try {
                                      Utils.ShowSnackBar(getActivity(), "Successfully added a review", Snackbar.LENGTH_LONG);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " + e.getMessage() );
                                }
                          }
                    });
      }

      private void getRelatedServices() {
            mAPIService.getRelatedServices(String.valueOf(mService.getId()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ServiceResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                                try {
//                                      Utils.ShowView(binding.get().servicesProgressBar);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ServiceResponse serviceResponse) {

                                try {

                                      ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), serviceResponse.getServices());
                                      binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);
                                      serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.nameTextView)) {
                                                  if (obj.getShop().getFeedbacks() != null) {
                                                        Log.i(TAG, "onNext: Service feedback " + obj.getShop().getFeedbacks().get(0).getComment());
                                                  }
                                                  navigateToStore(obj.getShop());
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  startActivity(new Intent(getContext(), LocationActivity.class));
                                            } else if (view == view.findViewById(R.id.cardView12)) {
                                                  navigateToServiceDetailActivity(getActivity(), serviceResponse.getServices().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG,e, "Service Listings onError:  ");

                                try {
//                                      reloadSnackBar(binding.get().servicesProgressBar, getString(R.string.Oops));

                                } catch (Exception ex) {
                                      logError(TAG,e, "onError: ");
                                }
                          }

                          @Override
                          public void onComplete() {
//                                disposeAPIService(disposable);

                                try {
//                                      Utils.RemoveView(binding.get().servicesProgressBar);
//                                      Utils.ShowView(binding.get().servicesRecyclerView);

                                } catch (Exception e) {
                                      logError(TAG, e, "onComplete:  ");
                                }
                          }
                    });
      }

      private void getRating() {

            mAPIService.getAverageFeedbackRating(String.valueOf(mService.getId()), "product")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Rating>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) { disposable = d; }

                          @Override
                          public void onNext(@NonNull Rating rating) {
                                if (getContext() != null ) {
                                      if (rating.getAverageRating() != null) {
                                            binding.get().rating.setRating(Float.parseFloat(rating.getAverageRating()));
                                      }
                                      binding.get().ratingCount.setText(String.valueOf(mService.getFeedbacks().size()));
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {}

                          @Override
                          public void onComplete() {
                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }


      private boolean isUserLoggedIn() {
            return CustomSharedPrefs.getUserStatus(getContext());
      }

      private void navigateToLogin() {
            startActivity(new Intent(getContext(), LoginActivity.class));
      }


}
