package com.mimi.africa.ui.checkout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.JsonObject;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.CheckoutFragment2Binding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.ui.basket.viewModel.BasketViewModel;
import com.mimi.africa.ui.checkout.adapter.ShippingMethodsAdapter;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShippingMethodViewModel;
import com.mimi.africa.viewObject.ShippingMethod;
import com.mimi.africa.viewObject.ShippingProductContainer;
import com.mimi.africa.viewObject.coupon.CouponDiscountViewModel;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CheckoutFragment2 extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private BasketViewModel basketViewModel;
    private ShippingMethodViewModel shippingMethodViewModel;
    private CouponDiscountViewModel couponDiscountViewModel;

    private StringBuilder cartIds_sb;

    @VisibleForTesting
    private AutoClearedValue<CheckoutFragment2Binding> binding;
    private AutoClearedValue<ShippingMethodsAdapter> adapter;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @Inject
    protected APIService mAPIService;

    private PSDialogMsg psDialogMsg;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CheckoutFragment2Binding dataBinding = DataBindingUtil.inflate(inflater, R.layout.checkout_fragment_2, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    public void onDispatched() {

    }

    @Override
    protected void initUIAndActions() {

        cartIds_sb = new StringBuilder();

        psDialogMsg = new PSDialogMsg(this.getActivity(), false);

        if (getActivity() instanceof MainCheckoutActivity) {
            ((MainCheckoutActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
            ((MainCheckoutActivity) getActivity()).progressDialog.setCancelable(false);
        }

        binding.get().couponDiscountButton.setOnClickListener(v -> {
            if ((CheckoutFragment2.this.getActivity()) != null) {
                ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.couponDiscountText = binding.get().couponDiscountValueEditText.getText().toString();
            }
            couponDiscountViewModel.setCouponDiscountObj(binding.get().couponDiscountValueEditText.getText().toString());

            if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
                ((MainCheckoutActivity) getActivity()).progressDialog.setMessage(getString(R.string.check_coupon));
                ((MainCheckoutActivity) getActivity()).progressDialog.show();
            }

        });

        if (!overAllTaxLabel.isEmpty()) {
            binding.get().overAllTaxTextView.setText(getString(R.string.tax, overAllTaxLabel));
        }

        if (!shippingTaxLabel.isEmpty()) {
            binding.get().shippingTaxTextView.setText(getString(R.string.shipping_tax, shippingTaxLabel));
        }

    }

    @Override
    protected void initViewModels() {
        basketViewModel = ViewModelProviders.of(this, viewModelFactory ).get(BasketViewModel.class);
        shippingMethodViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShippingMethodViewModel.class);
        couponDiscountViewModel = ViewModelProviders.of(this, viewModelFactory).get(CouponDiscountViewModel.class);
    }

    @Override
    protected void initAdapters() {

        if (getActivity() != null) {

            ShippingMethodsAdapter nvAdapter = new ShippingMethodsAdapter(dataBindingComponent, shippingMethod -> {

                if (CheckoutFragment2.this.getActivity() != null) {

                    ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = Utils.round(shippingMethod.price, 2);
                    ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shippingMethodName = shippingMethod.name;
                    ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.selectedShippingId = shippingMethod.id;

                    CheckoutFragment2.this.calculateTheBalance();
                }
            }, shippingId, ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.selectedShippingId);

            adapter = new AutoClearedValue<>(this, nvAdapter);
            binding.get().shippingMethodsRecyclerView.setAdapter(adapter.get());
        }

    }

    @Override
    protected void initData() {

        if (shopNoShippingEnable.equals(Constant.ONE)) {
            binding.get().shippingMethodsCardView.setVisibility(View.GONE);
        }

        if (shopStandardShippingEnable.equals(Constant.ONE)) {
            binding.get().shippingMethodsCardView.setVisibility(View.VISIBLE);
            shippingMethodViewModel.setShippingMethodsObj();
        }

        binding.get().shippingMethodsCardView.setVisibility(View.VISIBLE);
        shippingMethodViewModel.setShippingMethodsObj();


//        shippingMethodViewModel.getshippingCostByCountryAndCityData().observe(this, result -> {
//
//            if (result != null) {
//                if (result.status == Status.SUCCESS) {
////                    progressDialog.get().cancel();
//                    if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
//                        ((MainCheckoutActivity) getActivity()).progressDialog.hide();
//                    }
//
//                    if (result.data != null) {
//
//                        if (CheckoutFragment2.this.getActivity() != null) {
//                            ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shippingMethodName = result.data.shippingZone.shippingZonePackageName;
//                            ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = Float.parseFloat(result.data.shippingZone.shippingCost);
//
//                            calculateTheBalance();
//                        }
//                    }
//                } else if (result.status == Status.ERROR) {
//                    if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
//                        ((MainCheckoutActivity) getActivity()).progressDialog.hide();
//                    }
//
//                }
//            }
//
//        });

        shippingMethodViewModel.getShippingMethodsData().observe(this, result -> {

            if (result.data != null) {
                switch (result.status) {

                    case SUCCESS:
                        CheckoutFragment2.this.replaceShippingMethods(result.data);

                        for (ShippingMethod shippingMethod : result.data) {

                            if ((CheckoutFragment2.this.getActivity()) != null) {
                                if (!shippingId.isEmpty()) {
                                    if (shippingMethod.id.equals(shippingId) && ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.selectedShippingId.isEmpty()) {
                                        if (CheckoutFragment2.this.getActivity() != null) {
                                            if (shopNoShippingEnable.equals(Constant.ONE)) {
                                                ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = 0;
                                            } else {
                                                ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = Utils.round(shippingMethod.price, 2);
                                            }
                                            CheckoutFragment2.this.calculateTheBalance();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        break;

                    case LOADING:
                        CheckoutFragment2.this.replaceShippingMethods(result.data);

                        for (ShippingMethod shippingMethod : result.data) {

                            if ((CheckoutFragment2.this.getActivity()) != null) {
                                if (!shippingId.isEmpty()) {
                                    if (shippingMethod.id.equals(shippingId) && ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.selectedShippingId.isEmpty()) {
                                        if (CheckoutFragment2.this.getActivity() != null) {
                                            if (shopNoShippingEnable.equals(Constant.ONE))
                                                ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = 0;
                                            else {
                                                ((MainCheckoutActivity) CheckoutFragment2.this.getActivity()).transactionValueHolder.shipping_cost = Utils.round(shippingMethod.price, 2);
                                            }
                                            CheckoutFragment2.this.calculateTheBalance();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        });

        couponDiscountViewModel.getCouponDiscountData().observe(this, result -> {

            if (result != null) {
                switch (result.status) {
                    case ERROR:

                        if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
                            ((MainCheckoutActivity) getActivity()).progressDialog.hide();
                        }

                        psDialogMsg.showErrorDialog(getString(R.string.error_coupon), getString(R.string.app__ok));
                        psDialogMsg.show();

                        break;

                    case SUCCESS:

                        if (result.data != null) {

                            if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
                                ((MainCheckoutActivity) getActivity()).progressDialog.hide();
                            }

                            psDialogMsg.showSuccessDialog(getString(R.string.checkout_detail__claimed_coupon), getString(R.string.app__ok));
                            psDialogMsg.show();

                            if (getActivity() != null) {
                                ((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount = Utils.round(Float.parseFloat(result.data.couponAmount), 2);
                                Utils.psLog("coupon5" + ((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount + "");
                            }

                            calculateTheBalance();
                        }

                        break;
                }
            }
        });

        basketViewModel.setBasketListWithProductObj();

        getCarts();

//        basketViewModel.getAllBasketWithProductList().observe(this, baskets -> {
//            if (baskets != null && baskets.size() > 0) {

//                if (getActivity() != null) {
//                    ((MainCheckoutActivity) getActivity()).transactionValueHolder.resetValues();

//                    for (Basket basket : baskets) {
//
//                        ((MainCheckoutActivity) getActivity()).transactionValueHolder.total_item_count += basket.count;
//                        ((MainCheckoutActivity) getActivity()).transactionValueHolder.total += Utils.round((basket.basketOriginalPrice) * basket.count, 2);
//
//                        ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol = Constants.DEFAULT_CURRENCY;
//
//                        ShippingProductContainer shippingProductContainer = new ShippingProductContainer(
//                                String.valueOf(basket.product.getId()),
//                                basket.count
//                        );
//                        shippingMethodViewModel.shippingProductContainer.add(shippingProductContainer);
//
//                    }

                    if (shopZoneShippingEnable.equals(Constant.ONE)) {
                        binding.get().shippingMethodsCardView.setVisibility(View.GONE);

                        if (getActivity() != null) {
                            //progressDialog.get().show();
                            if (getActivity() != null && getActivity() instanceof CheckoutActivity) {
                                ((MainCheckoutActivity) getActivity()).progressDialog.show();

//                                Utils.psLog(((CheckoutActivity) getActivity()).user.country.id + " - " + ((CheckoutActivity) getActivity()).user.city.id + " - " + shopId);


//                                shippingMethodViewModel.setshippingCostByCountryAndCityObj(new ShippingCostContainer(
//                                        ((  MainCheckoutActivity) getActivity()).user.country.id, ((CheckoutActivity) getActivity()).user.city.id, shopId,
//                                        shippingMethodViewModel.shippingProductContainer));
                            }
                        }
                    }
//                }
//                calculateTheBalance();
//            }
//        });

//        basketViewModel.getWholeBasketDeleteData().observe(this, result -> {
//                        if (result != null) {
//                            if (result.status == Status.SUCCESS) {
//                                Utils.psLog("Success");
//                            } else if (result.status == Status.ERROR) {
//                                Utils.psLog("Fail");
//                            }
//                        }
//                    });

    }

    private void replaceShippingMethods(List<ShippingMethod> shippingMethods) {
        this.adapter.get().replace(shippingMethods);
        this.binding.get().executePendingBindings();
    }

    private void calculateTheBalance() {

                    if (getActivity() != null) {

                        ((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total = Utils.round(((MainCheckoutActivity) getActivity()).transactionValueHolder.total - (((MainCheckoutActivity) getActivity()).transactionValueHolder.discount), 2);

                        if (((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount > 0) {
                            ((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total = Utils.round(((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total - ((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount, 2);
                        }

                        if (!overAllTax.isEmpty()) {
                            ((MainCheckoutActivity) getActivity()).transactionValueHolder.tax = Utils.round(((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total * Float.parseFloat(overAllTax), 2);
                        }

                        if (!shippingTax.isEmpty() && ((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost > 0) {
                            ((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_tax = Utils.round(((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost * Float.parseFloat(shippingTax), 2);
                        }

                        ((MainCheckoutActivity) getActivity()).transactionValueHolder.final_total = Utils.round(((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total + ((MainCheckoutActivity) getActivity()).transactionValueHolder.tax +
                                ((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_tax + ((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost, 2);
                        updateUI();
                    }

    }

    private void updateUI() {

        if (getActivity() != null) {

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.total_item_count > 0) {
                binding.get().totalItemCountValueTextView.setText(String.valueOf(((MainCheckoutActivity) getActivity()).transactionValueHolder.total_item_count));
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.total > 0) {
                String totalValueString = ((MainCheckoutActivity) getActivity()).
                        transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.total);
                binding.get().totalValueTextView.setText(totalValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount > 0) {
                String couponDiscountValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.coupon_discount);
                binding.get().couponDiscountValueTextView.setText(couponDiscountValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.discount > 0) {
                String discountValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.discount);
                binding.get().discountValueTextView.setText(discountValueString);
            }

            if (!((MainCheckoutActivity) getActivity()).transactionValueHolder.couponDiscountText.isEmpty()) {
                String couponDiscountValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.couponDiscountText;
                binding.get().couponDiscountValueEditText.setText(couponDiscountValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total > 0) {
                String subTotalValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.sub_total);
                binding.get().subtotalValueTextView.setText(subTotalValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.tax > 0) {
                String taxValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.tax);
                binding.get().taxValueTextView.setText(taxValueString);
            }

            if (!shippingTax.equals("0.0") && !shippingTax.equals(Constant.RATING_ZERO)) {
                String shippingTaxValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(Utils.round((((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_tax), 2));
                binding.get().shippingTaxValueTextView.setText(shippingTaxValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.final_total > 0.0) {
                String finalTotalValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.final_total);
                binding.get().finalTotalValueTextView.setText(finalTotalValueString);
            }

            if (((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost > 0) {
                String shippingCostValueString = ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol + " " + Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost);
                binding.get().shippingCostValueTextView.setText(shippingCostValueString);
            }

        }

    }

    private void getCarts() {

        mAPIService.getCarts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CartResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull CartResponse cartResponse) {

                        List<Basket> cartList = new ArrayList<>();
                        double total = 0;

                        for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                            cartIds_sb.append(cartResponse.getCarts().get(i).getId());

                            if ( i != (cartResponse.getCarts().size() -1) && cartResponse.getCarts().size() > 1){
                                cartIds_sb.append(",");
                            }

                            if (cartResponse.getCarts().get(i).getItems() != null) {

                                for (Inventory inventory : cartResponse.getCarts().get(i).getItems() ) {

                                    String attribute = "";
                                    JsonObject jsonObject = new JsonObject();
                                    if (inventory.getPivot() != null) {
                                        String description = inventory.getPivot().getItemDescription();
                                        String[] attributes = new String[3];
                                        if (description != null) {
                                            attributes = description.split(" - ");
                                            if (attributes.length == 3) {
                                                jsonObject.addProperty("1", attributes[1]);
                                            }else if (attributes.length == 4)
                                                jsonObject.addProperty("1", attributes[1]);
                                            jsonObject.addProperty("2", attributes[2]);
                                        }else if ( attributes.length > 4 ){
                                            jsonObject.addProperty("1", attributes[1]);
                                            jsonObject.addProperty("2", attributes[2]);
                                            jsonObject.addProperty("3", attributes[3]);
                                        }
                                    }

                                    Basket basket = new Basket(String.valueOf(inventory.getPivot().getCartId()), inventory.getPivot().getQuantity(), jsonObject.toString(), "1",
                                            "#000000","2", Float.valueOf(inventory.getPivot().getUnitPrice()), Float.valueOf(inventory.getPivot().getUnitPrice()),
                                            String.valueOf(1), "0" , inventory);
                                    cartList.add(basket);
                                }
                            }

                            if (cartResponse.getCarts().get(i) != null) {
                                int qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());
                                total += Double.parseDouble(cartResponse.getCarts().get(i).getTotal() );
                            }
                        }

                        if (getActivity() != null) {
                            ((MainCheckoutActivity) getActivity()).transactionValueHolder.resetValues();
                            for (Basket basket : cartList) {

                                ((MainCheckoutActivity) getActivity()).transactionValueHolder.total_item_count += basket.count;
                                ((MainCheckoutActivity) getActivity()).transactionValueHolder.total += Utils.round((basket.basketOriginalPrice) * basket.count, 2);

                                ((MainCheckoutActivity) getActivity()).transactionValueHolder.currencySymbol = Constants.DEFAULT_CURRENCY;

                                ShippingProductContainer shippingProductContainer = new ShippingProductContainer(
                                        String.valueOf(basket.product.getId()),
                                        basket.count
                                );
                                shippingMethodViewModel.shippingProductContainer.add(shippingProductContainer);

                            }
                        }

                        calculateTheBalance();

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
