package com.mimi.africa.ui.chat;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.mimi.africa.R;
import com.mimi.africa.ui.common.BaseActivity;

import java.net.URL;

import me.pushy.sdk.Pushy;

public class ChatActivity extends BaseActivity {

      private static final String TAG = ChatActivity.class.getSimpleName();

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_chat);

            if (!Pushy.isRegistered(getApplicationContext())) {
                  new RegisterForPushNotificationsAsync().execute();
            }

            // Check whether the user has granted us the READ/WRITE_EXTERNAL_STORAGE permissions
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                  // Request both READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE so that the
                  // Pushy SDK will be able to persist the device token in the external storage
                  ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }

      }

      private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
            protected Exception doInBackground(Void... params) {
                  try {
                        // Assign a unique token to this device
                        String deviceToken = Pushy.register(getApplicationContext());

                        // Log it for debugging purposes
                        Log.d(TAG, "Pushy device token: " + deviceToken);

                        // Send the token to your backend server via an HTTP GET request
                        new URL("https://{YOUR_API_HOSTNAME}/register/device?token=" + deviceToken).openConnection();
                  }
                  catch (Exception exc) {
                        // Return exc to onPostExecute
                        return exc;
                  }

                  // Success
                  return null;
            }

            @Override
            protected void onPostExecute(Exception exc) {
                  // Failed?
                  if (exc != null) {
                        // Show error as toast message
                        Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                        return;
                  }

                  // Succeeded, optionally do something to alert the user
            }
      }
}
