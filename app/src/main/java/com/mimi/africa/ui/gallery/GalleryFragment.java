package com.mimi.africa.ui.gallery;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mimi.africa.Adapter.GalleryAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ImageResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentGalleryBinding;
import com.mimi.africa.databinding.FragmentServiceDetailsBinding;
import com.mimi.africa.model.Image;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends BaseFragment {

      private static final String TAG =  GalleryFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentGalleryBinding> binding;
      private Shop mShop;

      @Inject
      public APIService mAPIService;


      public GalleryFragment() {
            // Required empty public constructor
      }

      public static GalleryFragment newInstance(Shop shop){
            GalleryFragment galleryFragment = new GalleryFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.SHOP_OBECT, shop);
            galleryFragment.setArguments(bundle);

            return galleryFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null){

                  mShop = getArguments().getParcelable(Constants.SHOP_OBECT);
            }

      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            FragmentGalleryBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false, dataBindingComponent);

            binding = new AutoClearedValue<>(this, dataBinding);

            return binding.get().getRoot();
      }

      @Override
      protected void initUIAndActions() {
            binding.get().imageList.setHasFixedSize(true);
            binding.get().imageList.setNestedScrollingEnabled(false);
            StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            binding.get().imageList.setLayoutManager(mLayoutManager);
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {
            List<Image> imageList = new ArrayList<>();

            if (mShop.getBannerImage() != null){
                  Image image = new Image();
                  image.setPath(mShop.getBannerImage());
                  imageList.add(image);
            }

            if (mShop.getImage() != null){
                  Image image = new Image();
                  image.setPath(mShop.getImage().getPath());
                  imageList.add(image);
            }

            GalleryAdapter galleryAdapter = new GalleryAdapter(getContext(), imageList);
            binding.get().imageList.setAdapter(galleryAdapter);


      }

      @Override
      protected void initData() {}

      private void getImageListings(String productId){

            mAPIService.getImageListings(productId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ImageResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ImageResponse listingResponse) {

                                if (listingResponse.getImages() != null && listingResponse.getImages().size() > 0) {

                                      Log.i(TAG, "onNext:  " + listingResponse.getImages().get(0).getPath());

                                      if (listingResponse.getImages().size() > 0) {

                                            for (int i = 0; i < listingResponse.getImages().size(); i++) {

                                            }
                                      }
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, " onError:  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {}
                    });
      }

}
