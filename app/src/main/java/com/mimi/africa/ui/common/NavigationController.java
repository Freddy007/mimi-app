package com.mimi.africa.ui.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.mimi.africa.R;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.login.UserLoginActivity;
import com.mimi.africa.ui.mainhome.MainHomeFragment;
import com.mimi.africa.ui.profile.ProfileFragment;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

public class NavigationController {

      private final int containerId;
      private RegFragments currentFragment;
      public Uri photoURI;

      //endregion


      //region Constructor
      @Inject
      public NavigationController() {

            // This setup is for MainActivity
            this.containerId = R.id.content_frame;
      }


      public void navigateToUserProfile(MainHomeActivity mainActivity) {
//            if (checkFragmentChange(RegFragments.HOME_USER_LOGIN)) {
                  try {
                        ProfileFragment fragment = new ProfileFragment();
                        mainActivity.getSupportFragmentManager().beginTransaction()
                                .replace(containerId, fragment)
                                .commitAllowingStateLoss();
                  } catch (Exception e) {
                        Utils.psErrorLog("Error! Can't replace fragment.", e);
                  }
//            }
      }

      //region default navigation

      public void navigateToUserLoginActivity(Activity activity, String origin) {
            Intent intent = new Intent(activity, UserLoginActivity.class);
            intent.putExtra(Constants.ORIGIN, origin );
            activity.startActivity(intent);
      }


//      public void navigateToUserLogin(MainHomeActivity mainActivity) {
//            if (checkFragmentChange(RegFragments.HOME_USER_LOGIN)) {
//                  try {
//                        UserLoginFragment fragment = new UserLoginFragment();
//                        mainActivity.getSupportFragmentManager().beginTransaction()
//                                .replace(containerId, fragment)
//                                .commitAllowingStateLoss();
//                  } catch (Exception e) {
//                        Utils.psErrorLog("Error! Can't replace fragment.", e);
//                  }
//            }
//      }

      public void navigateToHomeFragment(MainHomeActivity mainHomeActivity) {
            if (checkFragmentChange(RegFragments.MAIN_HOME_FRAGMENT)) {
                  try {
                        MainHomeFragment fragment = new MainHomeFragment();
                        mainHomeActivity.getSupportFragmentManager().beginTransaction()
                                .replace(containerId, fragment)
                                .commitAllowingStateLoss();
                  } catch (Exception e) {
                        Utils.psErrorLog("Error! Can't replace fragment.", e);
                  }
            }
      }

      //region Private methods
      private Boolean checkFragmentChange(RegFragments regFragments) {
            if (currentFragment != regFragments) {
                  currentFragment = regFragments;
                  return true;
            }

            return false;
      }

      /**
       * Remark : This enum is only for MainActivity,
       * For the other fragments, no need to register here
       **/
      private enum RegFragments {
            MAIN_HOME_FRAGMENT,
            PROMOS_FRAGMENT,
            MESSAGE_FRAGMENT,
            NOTIFICATION_FRAGMENT,
            FAVOURITES_FRAGMENT,
            SETTINGS_FRAGMENT
      }

//      public void navigateToVerifyEmailActivity(Activity activity) {
//            Intent intent = new Intent(activity, VerifyEmailActivity.class);
//            activity.startActivity(intent);
//      }
}
