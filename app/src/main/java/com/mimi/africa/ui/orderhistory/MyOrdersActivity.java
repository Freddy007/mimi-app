package com.mimi.africa.ui.orderhistory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.OrderBatchHistoryResponse;
import com.mimi.africa.databinding.ActivityOrderBinding;
import com.mimi.africa.model.OrderBatch;
import com.mimi.africa.model.OrderItem;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.orderhistory.adapter.OrderBatchHistoryAdapter;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyOrdersActivity extends BaseActivity {


    ActivityOrderBinding activityOrderBinding;

//    @BindView(R.id.recycle_address)
//    RecyclerView recycleAddress;

    UserData userData;
//    @BindView(R.id.txt_itmecount)
//    TextView txtItmecount;

    @Inject
    APIService mAPIService;

    @Inject
    protected SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order);
//        ButterKnife.bind(this);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("My Orders");
        }
        activityOrderBinding = DataBindingUtil.setContentView(this, R.layout.activity_order);

        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(MyOrdersActivity.this);
        activityOrderBinding.recycleAddress.setLayoutManager(recyclerLayoutManager);
        myOrder();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        if (ISORDER) {
//            ISORDER = false;
//            startActivity(new Intent(MyOrdersActivity.this, MainHomeActivity.class));
//            finish();
//        } else {
//            finish();
//        }

    }

    private void myOrder() {
        String loginUserId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);

        getOrderHistory(loginUserId);
//        GetService.showPrograss(MyOrdersActivity.this);
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("uid", userData.getId());
//            JsonParser jsonParser = new JsonParser();
//            Call<JsonObject> call = APIClient.getInterface().getOlist((JsonObject) jsonParser.parse(jsonObject.toString()));
//            GetResult getResult = new GetResult();
//            getResult.setMyListener(this);
//            getResult.callForLogin(call, "1");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

//    @Override
//    public void callback(JsonObject result, String callNo) {
//
//        if (callNo.equalsIgnoreCase("1")) {
//            try {
//                Gson gson = new Gson();
//                Orderdata myOrder = gson.fromJson(result.toString(), Orderdata.class);
//                if (myOrder.getOrderData().size() != 0) {
//                    List<OrderDatum> datumList = new ArrayList<>();
//                    for (int i = 0; i < myOrder.getOrderData().size(); i++) {
//                        OrderDatum datum = myOrder.getOrderData().get(i);
//                        datumList.add(datum);
//                    }
//                    txtItmecount.setText(datumList.size() + " orders");
//                    MyOrderAdepter myOrderAdepter = new MyOrderAdepter(datumList);
//                    recycleAddress.setAdapter(myOrderAdepter);
//                } else {
//                    txtItmecount.setText("0 orders");
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//
//            }
//
//        }
//
//    }

    private void getOrderHistory(String customer_id){

        final boolean[] empty = {false};

        mAPIService.getOrderBatchHistory(customer_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OrderBatchHistoryResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
//                        binding.get().setLoadingMore(true);

                    }

                    @Override
                    public void onNext(@NonNull OrderBatchHistoryResponse orderHistoryResponse) {
                        try {

                            if (orderHistoryResponse.getOrders().size() >0 ) {

                                List<OrderBatch> datumList = orderHistoryResponse.getOrders();

//                                for (int i = 0; i < myOrder.getOrderData().size(); i++) {
//                                    OrderDatum datum = myOrder.getOrderData().get(i);
//                                    datumList.add(datum);
//                                }
                                activityOrderBinding.txtItmecount.setText(datumList.size() + " orders");
                                MyOrderAdepter myOrderAdepter = new MyOrderAdepter(datumList);
                                activityOrderBinding.recycleAddress.setAdapter(myOrderAdepter);
                            } else {
                                activityOrderBinding.txtItmecount.setText("0 orders");
                            }

                        }catch (Exception e){
                            Log.e("MyOrdersActivity", "onNext:  " + e.getMessage() );
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        Log.e(TAG, "Order History onError:  "  + e.getMessage() );
//                        try {
//
//                            Utils.RemoveView(binding.get().progressBar);
//                            Utils.ShowSnackBar(getActivity(), "Failed to load order history!", Snackbar.LENGTH_LONG);
//                        }catch (Exception ex){
//                            Log.e(TAG, "onError:  " + e.getMessage() );
//                        }
                    }

                    @Override
                    public void onComplete() {
//                        try {

//                            if (!empty[0]) {
//
//                                Utils.RemoveView(binding.get().progressBar);
//                                Utils.ShowView(binding.get().orderHistoryRecyclerView);
//                                Utils.RemoveView(binding.get().noOrdersLinearLayout);
//                            }else {
//                                Utils.RemoveView(binding.get().progressBar);
//                                Utils.RemoveView(binding.get().orderHistoryRecyclerView);
//                                Utils.ShowView(binding.get().noOrdersLinearLayout);
//                            }
//                        }catch (Exception e){
//                            Log.e(TAG, "onComplete: " + e.getMessage() );
//                        }
//                        binding.get().setLoadingMore(false);

                    }
                });
    }

    public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

        private List<OrderBatch> orderlists;

        public OrderAdapter(List<OrderBatch> offersListIn) {
            orderlists = offersListIn;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_item, parent, false);

            ViewHolder viewHolder =
                    new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            OrderBatch oderList = orderlists.get(position);
            holder.txtOrderid.setText("#" + oderList.getOrderNumber());
            holder.txtStatus.setText("" );
            holder.txtPrice.setText("" + oderList.getTotalDeliveryCharge());
            holder.txtDate.setText("" + oderList.getCreatedAt());

        }

        @Override
        public int getItemCount() {
            return orderlists.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView txtOrderid;
            TextView txtStatus;
            TextView txtPrice;
            TextView txtDate;
//            TextView txtDateandstatus;

            public ViewHolder(View view) {
                super(view);
                txtOrderid = view.findViewById(R.id.txt_oderid);
                txtStatus = view.findViewById(R.id.txt_status);
                txtPrice = view.findViewById(R.id.txt_price);
                txtDate = view.findViewById(R.id.txt_date);
//                ButterKnife.bind(this, view);

            }
        }
    }

    public class MyOrderAdepter extends RecyclerView.Adapter<MyOrderAdepter.ViewHolder> {
        private List<OrderBatch> orderData;

        public MyOrderAdepter(List<OrderBatch> orderData) {
            this.orderData = orderData;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.myorder_item, parent, false);

            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder,
                                     int position) {
            Log.e("position", "" + position);
            OrderBatch order = orderData.get(position);
            holder.txtOderid.setText("Order #" + order.getOrderNumber());
            String subtotal = String.format("%.0f", Double.valueOf(order.getTotalDeliveryCharge()));
            String deliveryCharge = String.format("%.0f", Double.valueOf(order.getDeliveryCharge()));
            int grandTotal = Integer.parseInt(subtotal) + Integer.parseInt( deliveryCharge);
            holder.txtPricetotla.setText( "GHS  " + grandTotal);
            holder.txtDateandstatus.setText(" confirmed "  + " on " + order.getCreatedAt());
            setJoinPlayrList(holder.lvlItem, order.getOrderItems());

            holder.lvlClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.lvlItem.getVisibility() == View.VISIBLE) {

                        holder.imgRight.setBackgroundResource(R.drawable.rights);
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                0,
                                holder.lvlItem.getHeight());
                        animate.setDuration(500);
                        animate.setFillAfter(true);
                        holder.lvlItem.startAnimation(animate);
                        holder.lvlItem.setVisibility(View.GONE);
                    } else {
                        holder.lvlItem.setVisibility(View.VISIBLE);
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                holder.lvlItem.getHeight(),
                                0);
                        animate.setDuration(500);
                        animate.setFillAfter(true);
                        holder.lvlItem.startAnimation(animate);

                        holder.imgRight.setBackgroundResource(R.drawable.ic_expand);

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return orderData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.txt_oderid)
//            TextView txtOderid;
//            @BindView(R.id.txt_pricetotla)
//            TextView txtPricetotla;
//            @BindView(R.id.txt_dateandstatus)
//            TextView txtDateandstatus;
//            @BindView(R.id.lvl_item)
//            LinearLayout lvlItem;
//            @BindView(R.id.lvl_click)
//            LinearLayout lvlClick;
//            @BindView(R.id.img_right)
//            ImageView imgRight;

            public TextView txtOderid;
            public TextView txtPricetotla;
            public TextView txt_dateandstatus;
            public LinearLayout lvlClick;
            public LinearLayout lvlItem;
            public ImageView imgRight;
            public TextView txtDateandstatus;

            public ViewHolder(View view) {
                super(view);
                txtOderid = view.findViewById(R.id.txt_oderid);
                txtPricetotla = view.findViewById(R.id.txt_pricetotla);
//                txt_dateandstatus = view.findViewById(R.id.txt_dateandstatus);
                lvlItem = view.findViewById(R.id.lvl_item);
                lvlClick = view.findViewById(R.id.lvl_click);
                imgRight = view.findViewById(R.id.img_right);
                txtDateandstatus = view.findViewById(R.id.txt_dateandstatus);

//                ButterKnife.bind(this, view);
            }
        }
    }

    private void setJoinPlayrList(LinearLayout lnrView, List<OrderItem> listdata) {

        lnrView.removeAllViews();

        for (int i = 0; i < listdata.size(); i++) {
            OrderItem listdatum = listdata.get(i);
            LayoutInflater inflater = LayoutInflater.from(MyOrdersActivity.this);

            View view = inflater.inflate(R.layout.myordersub_item, null);
            ImageView imgView = view.findViewById(R.id.imageView);
            TextView txtTitle = view.findViewById(R.id.txt_title);
            TextView txtItems = view.findViewById(R.id.txt_items);
            TextView txtPrice = view.findViewById(R.id.txt_price);
//            Glide.with(MyOrdersActivity.this).load(Base_URL + listdatum.getImage()).placeholder(R.drawable.slider).into(imgView);
            Glide.with(MyOrdersActivity.this).load("").placeholder(R.drawable.slider).into(imgView);
//            txtTitle.setText("" + listdatum.getTitle());
            txtTitle.setText("" + listdatum.getItemDescription());
            txtItems.setText(" X " + listdatum.getQuantity() + " Items");
//            txtPrice.setText(sessionManager.getStringData(SessionManager.CURRNCY) + " " + listdatum.getPrice() + "");
            txtPrice.setText( "GHS " + listdatum.getUnitPrice() + "");

            lnrView.addView(view);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                if (ISORDER) {
//                    ISORDER = false;
//                    startActivity(new Intent(MyOrdersActivity.this, HomeActivity.class));
//                    finish();
//                } else {
//                    finish();
//                }
//                return true;
//            default:
//                break;
//        }
        return super.onOptionsItemSelected(item);
    }

}
