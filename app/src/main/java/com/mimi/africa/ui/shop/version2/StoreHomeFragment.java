package com.mimi.africa.ui.shop.version2;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.Adapter.AdapterImageSlider;
import com.mimi.africa.Adapter.ServiceCardAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxLayoutBinding;
import com.mimi.africa.databinding.FragmentMainStoreBinding;
import com.mimi.africa.event.ReviewItemClickedEvent;
import com.mimi.africa.event.ShopViewedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.home.adapter.ViewPagerAdapter;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.mainhome.MainHomeFragment;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.product.ProductListActivity;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.ui.shop.adapter.ProductCategoryAdapter;
import com.mimi.africa.ui.shop.version2.adapter.CategoryIconListAdapter;
import com.mimi.africa.ui.shop.version2.adapter.DiscountListAdapter;
import com.mimi.africa.ui.shop.version2.adapter.ProductHorizontalListAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.SliderTimer;
import com.mimi.africa.utils.SpeedSlowScroller;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Timer;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StoreHomeFragment extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    private static final String TAG = StoreHomeFragment.class.getSimpleName();
    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private AutoClearedValue<FragmentMainStoreBinding> binding;
    private ShopProductGridCardAdapter mProductsAdapter;
    private AutoClearedValue<ViewPagerAdapter> viewPagerAdapter;
    private AutoClearedValue<ProductHorizontalListAdapter> trendingAdapter, latestAdapter;
    private AutoClearedValue<DiscountListAdapter> discountListAdapter;
    private AutoClearedValue<CategoryIconListAdapter> categoryIconListAdapter;
    private GridLayoutManager gridLayoutManager, categoryGridLayoutManager;

    private PSDialogMsg psDialogMsg;
    private List<Feedback> mFeedBacks;

    private BottomSheetBehavior mBehavior;
    private View bottom_sheet;

    @Nullable
    private Shop mShop;
    private AdapterImageSlider adapterImageSlider;

    private ViewPager viewPager;

    @Nullable
    private Runnable runnable = null;
    @NonNull
    private Handler handler = new Handler();
    private Activity mActivity;
    private Context mContext;
    private boolean layoutDone = false;

    private AutoClearedValue<BottomBoxLayoutBinding> bottomBoxLayoutBinding;
    private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;

    @Inject
    protected NavigationController navigationController;


    @Inject
    APIService mAPIService;

    public StoreHomeFragment() {
    }

    @NonNull
    public static StoreHomeFragment newInstance(Shop shop) {
        StoreHomeFragment storeFragment = new StoreHomeFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.SHOP_OBECT, shop);
        storeFragment.setArguments(args);

        return storeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mShop = getArguments().getParcelable(Constants.SHOP_OBECT);

            String url = "";

            if (mShop != null) {
                if (mShop.getImages() != null && mShop.getImages().size() > 0) {
                    url = Constants.IMAGES_BASE_URL + mShop.getImages().get(0).getPath();
                    mShop.setBannerImage(url);
                } else {
                    mShop.setBannerImage(mShop.getBannerImage());

                }
            }

            EventBus.getDefault().post(new ShopViewedEvent(mShop));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentMainStoreBinding fragmentStoreBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_main_store, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, fragmentStoreBinding);

        mActivity = getActivity();
        mContext = getContext();

        return fragmentStoreBinding.getRoot();
    }

    @Override
    protected void initUIAndActions() {

        if (getContext() != null) {
            // Prepare Sorting Bottom Sheet
            mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
            bottomBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_box_layout, null, false));
            mBottomSheetDialog.get().setContentView(bottomBoxLayoutBinding.get().getRoot());
        }

        psDialogMsg = new PSDialogMsg(getActivity(), false);

        ViewPagerAdapter viewPagerAdapter1 = new ViewPagerAdapter(dataBindingComponent, product -> {
            navigateToItemDetailActivity((FragmentActivity) mActivity, product);
        });

        this.viewPagerAdapter = new AutoClearedValue<>(this, viewPagerAdapter1);

        binding.get().viewPager.setAdapter(viewPagerAdapter1);
        binding.get().viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (binding.get() != null && binding.get().viewPager != null) {
                    if (binding.get().viewPager.getChildCount() > 0) {
                        layoutDone = true;
//                                    loadingCount++;
//                                    hideLoading();
                        binding.get().viewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            }
        });

        CategoryIconListAdapter categoryIconListAdapter1 = new CategoryIconListAdapter(dataBindingComponent, category -> {
            if (mShop != null) {
                Intent intent = new Intent(getContext(), ProductListActivity.class);
                intent.putExtra(Constants.CATEGORY_NAME, (category.getName()));
                intent.putExtra(Constants.CATEGORY_ID, String.valueOf(category.getId()));
                intent.putExtra(Constants.SHOP_ID, mShop.getId());
                startActivity(intent);
            }
        }, this);
        this.categoryIconListAdapter = new AutoClearedValue<>(this, categoryIconListAdapter1);
        binding.get().categoryIconList.setAdapter(categoryIconListAdapter1);
        binding.get().categoryIconList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (binding.get() != null) {
                    if (binding.get().categoryIconList != null) {
                        if (binding.get().categoryIconList.getChildCount() > 0) {
                            layoutDone = true;
//                                          loadingCount++;
//                                          hideLoading();
                            binding.get().categoryIconList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                }
            }
        });

        binding.get().viewAllTrendingTextView.setOnClickListener(v -> {
            Intent intent = new Intent(StoreHomeFragment.this.getContext(), ProductListActivity.class);
            if (mShop != null) {
                intent.putExtra(Constants.SHOP_ID, String.valueOf(mShop.getId()));
                intent.putExtra(Constants.FILTERING_TYPE_NAME, "Trending Products");
                intent.putExtra(Constants.FILTERING_TYPE, Constants.TRENDING);
                startActivity(intent);
            }
        });
        binding.get().viewALlLatestTextView.setOnClickListener(v -> {
            Intent intent = new Intent(StoreHomeFragment.this.getContext(), ProductListActivity.class);
            if (mShop != null) {
                intent.putExtra(Constants.SHOP_ID, String.valueOf(mShop.getId()));
                intent.putExtra(Constants.FILTERING_TYPE_NAME, "Latest Products");
                intent.putExtra(Constants.FILTERING_TYPE, Constants.TRENDING);
                startActivity(intent);
            }
        });
        binding.get().viewAllDiscountTextView.setOnClickListener(v -> {
            Intent intent = new Intent(StoreHomeFragment.this.getContext(), ProductListActivity.class);
            if (mShop != null) {
                intent.putExtra(Constants.SHOP_ID, String.valueOf(mShop.getId()));
                intent.putExtra(Constants.FILTERING_TYPE_NAME, "Discounted Products");
                intent.putExtra(Constants.FILTERING_TYPE, Constants.HAS_OFFERS);
                startActivity(intent);
            }
        });
        Utils.RemoveView(binding.get().productList);
        Utils.RemoveView(binding.get().latestTitleTextView);
    }

    @Override
    protected void initViewModels() {}

    @Override
    protected void initAdapters() {}

    @Override
    protected void initData() {
        if (mShop != null ) {
            getShopListings("has_offers");
            getSliderListings();
            getAllCategories(String.valueOf(mShop.getId()));
            getShopService(String.valueOf(mShop.getId()));
            getTrendingCategories(String.valueOf(mShop.getId()));
        }
    }

    private void getAllCategories(String shop_id) {

        mAPIService.getShopCategories(shop_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {

                        try {

                            if (categoryResponse.getCategories().size() > 0) {
                                categoryGridLayoutManager = new GridLayoutManager(getContext(), 1);
                                categoryGridLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
                                binding.get().categoryIconList.setLayoutManager(categoryGridLayoutManager);
                                replaceCategoryIconList(categoryResponse.getCategories());
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (getActivity() != null )
                        ((StoreHomeActivity) getActivity()).progressDialog.hide();
                    }

                    @Override
                    public void onComplete() {
                        if (getActivity() != null )
                            ((StoreHomeActivity) getActivity()).progressDialog.hide();
                    }
                });
    }

    private void getShopService(String shop_id) {

        final boolean[] empty = {false};

        getShopServicesResponseObservable(mAPIService, "15", shop_id)
                .subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull ServiceResponse listingResponse) {

                        try {

                            if (listingResponse.getServices().size() > 0) {

                                Utils.ShowView(binding.get().servicesRecyclerView);
                                Utils.ShowView(binding.get().servicesText);

                                ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), listingResponse.getServices());
                                            binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);

                                if (listingResponse.getServices() != null) {
                                    for (int i = 0; i < listingResponse.getServices().size(); i++) {
                                        Log.i(TAG, "onNext:  Services " + listingResponse.getServices().get(0).getName());
                                    }
                                }
                                serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {

                                    if (view == view.findViewById(R.id.nameTextView)) {
                                        navigateToStore(obj.getShop());

                                    } else if (view == view.findViewById(R.id.addressTextView)) {

                                        goToShopLocation(obj.getShop());
                                    } else if (view == view.findViewById(R.id.cardView12)) {

                                        navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
                                    }
                                });
                            } else {
                                empty[0] = true;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onNext:  " + e.getMessage());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Service Listings onError:  " + e.getMessage());
                    }


                    @Override
                    public void onComplete() {
                        try {
                            if (!empty[0]) {
//                                            Utils.ShowView(binding.get().servicesRecyclerView);
//                                            Utils.ShowView(binding.get().allServices);
//                                            Utils.ShowView(binding.get().serviceHeadLayout);
                            } else {
//                                            Utils.RemoveView(binding.get().servicesRecyclerView);
//                                            Utils.RemoveView(binding.get().allServices);
//                                            Utils.RemoveView(binding.get().serviceHeadLayout);
                            }
//                                      Utils.RemoveView(binding.get().servicesProgressBar);

                        } catch (Exception e) {
                            Log.e(TAG, "onComplete:  " + e.getMessage());
                        }
                    }
                });
    }

    private void getShopListings(String all) {

        mAPIService.getSingleShopListing(String.valueOf(mShop.getId()), all)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {

                        ((StoreHomeActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                        ((StoreHomeActivity) getActivity()).progressDialog.setCancelable(false);
                        ((StoreHomeActivity) getActivity()).progressDialog.show();
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {
                        if (getActivity() != null) {
                            if (listingResponse.getInventories().size() > 0) {
                                Utils.ShowView(binding.get().discountTitleTextView);
                                Utils.ShowView(binding.get().discountList);
                                DiscountListAdapter discountListAdapter = new DiscountListAdapter(getContext(), listingResponse.getInventories());
                                binding.get().discountList.setAdapter(discountListAdapter);

                                discountListAdapter.setOnItemClickListener(new DiscountListAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, Inventory obj, int pos) {
                                        processItemClick(view, obj, pos, listingResponse);
                                    }
                                });

                            } else {
                                Utils.RemoveView(binding.get().discountList);
                                Utils.RemoveView(binding.get().discountTitleTextView);
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        logError(TAG, e, "onError: getListings  getSingleShopListing ");
                    }

                    @Override
                    public void onComplete() {
                        getTrendingProducts();
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                });
    }

    private void getSliderListings() {

        mAPIService.getSingleShopListing(String.valueOf(mShop.getId()), "all")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {
                        if (getContext() != null) {
                            if (viewPagerAdapter.get() != null) {
                                viewPagerAdapter.get().replaceFeaturedList(listingResponse.getInventories());
                                setUpAutoSlider(viewPager, viewPagerAdapter.get().getCount(),
                                        StoreHomeFragment.this.getActivity());
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        logError(TAG, e, "onError: getListings getSingleShopListing ");
                    }

                    @Override
                    public void onComplete() {
                        if (viewPagerAdapter.get() != null) {
                        }

                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                });
    }

    private void setUpAutoSlider(ViewPager viewPager, int size, Activity activity) {
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScrollerOne");
            mScroller.setAccessible(true);
            SpeedSlowScroller scroller = new SpeedSlowScroller(activity);
            mScroller.set(viewPager, scroller);
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new SliderTimer(viewPager, size, activity), 4000, 6000);
        } catch (Exception ignored) {
        }

    }

    private void getTrendingProducts() {

        mAPIService.getShopTrendingProducts(String.valueOf(mShop.getId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {

                        if (getActivity() != null) {
                            viewPagerAdapter.get().replaceFeaturedList(listingResponse.getInventories());
                            if (listingResponse.getInventories().size() > 0) {
                                Utils.ShowView(binding.get().trendingTitleTextView);
                                Utils.ShowView(binding.get().trendingList);
                                DiscountListAdapter discountListAdapter = new DiscountListAdapter(getContext(), listingResponse.getInventories());
                                binding.get().trendingList.setAdapter(discountListAdapter);

                                discountListAdapter.setOnItemClickListener(new DiscountListAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, Inventory obj, int pos) {
                                        processItemClick(view, obj, pos, listingResponse);
                                    }
                                });

                            } else {
                                Utils.RemoveView(binding.get().trendingList);
                                Utils.RemoveView(binding.get().trendingTitleTextView);
                            }
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        logError(TAG, e, "onError: getListings  getShopTrendingProducts");
                    }

                    @Override
                    public void onComplete() {
                        getLatestProducts();
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                });
    }

    private void processItemClick(View view, Inventory obj, int pos, @NonNull ListingResponse listingResponse) {

        if (view == view.findViewById(R.id.news_title_textView)) {

            navigateToStore(obj);

        } else if (view == view.findViewById(R.id.addressTextView)) {

            navigateToLocation(getActivity());

        } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {

            navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(pos));
        }
    }

    private void getLatestProducts() {
        mAPIService.getSingleShopListing(String.valueOf(mShop.getId()), "new_arrivals")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {

                        if (getContext() != null) {
                            if (listingResponse.getInventories().size() > 0) {
                                Utils.RemoveView(binding.get().productList);
                                Utils.ShowView(binding.get().latestTitleTextView);
                                DiscountListAdapter discountListAdapter = new DiscountListAdapter(getContext(), listingResponse.getInventories());
                                binding.get().productList.setAdapter(discountListAdapter);

                                discountListAdapter.setOnItemClickListener(new DiscountListAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, Inventory obj, int pos) {
                                        processItemClick(view, obj, pos, listingResponse);
                                    }
                                });

                            } else {
                                Utils.RemoveView(binding.get().productList);
                                Utils.RemoveView(binding.get().latestTitleTextView);
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        logError(TAG, e, "onError: getListings   getLatestProducts ");
                    }

                    @Override
                    public void onComplete() {
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                });
    }

    private void getTrendingCategories(String shop_id) {

        mAPIService.getShopCategoryWithProducts(shop_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {

                        try {

                            if (categoryResponse.getCategories().size() > 0) {
//                                Log.i(TAG, "onNext:  Category " + categoryResponse.getCategories().get(0).getName());
                                ProductCategoryAdapter productCategoryAdapter = new ProductCategoryAdapter(getContext(), categoryResponse.getCategories());
                                binding.get().trendingCategoryList.setAdapter(productCategoryAdapter);
                            }else {
                                Utils.RemoveView(binding.get().trendingTitleTextView);
                                Utils.RemoveView(binding.get().trendingCategoryList);
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (getActivity() != null )
                        ((StoreHomeActivity) getActivity()).progressDialog.hide();
                    }

                    @Override
                    public void onComplete() {
                        if (getActivity() != null ) {
                            ((StoreHomeActivity) getActivity()).progressDialog.hide();
                        }
                    }
                });
    }

    private void replaceCategoryIconList(List<ProductCategory> categoryList) {
        categoryIconListAdapter.get().replace(categoryList);
        binding.get().executePendingBindings();
    }

    @Override
    public void onDispatched() {}

    private void showCustomDialog() {

        String customerId = loginUserId;
        String inventoryId = String.valueOf(mShop.getId());
        String approved = "1";
        String customerrName = fullName;

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_review);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextView customer_name_textview = dialog.findViewById(R.id.customer_name);
        customer_name_textview.setText(customerrName);
        final EditText et_post = dialog.findViewById(R.id.et_post);
        final AppCompatRatingBar rating_bar = dialog.findViewById(R.id.rating_bar);
        dialog.findViewById(R.id.bt_cancel).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.bt_submit).setOnClickListener(v -> {
            String review = et_post.getText().toString().trim();
            if (review.isEmpty()) {
                Toast.makeText(getContext(), ("Please fill review text"), Toast.LENGTH_SHORT).show();
            } else {
                String rating = String.valueOf(rating_bar.getRating());
                addFeedback(Constants.SHOP_TYPE, customerId, String.valueOf(mShop.getId()), rating, review, approved);

                dialog.dismiss();
                Toast.makeText(getContext(), getString(R.string.submitted), Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void addFeedback(String type, String customerId, String typeId, String rating, String comment, String approved) {
        mAPIService.saveProductFeedback(type, customerId, typeId, rating, comment, approved)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "onError:  " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                        try {
                            Utils.ShowSnackBar(getActivity(), "Successfully added a review", Snackbar.LENGTH_LONG);
                        } catch (Exception e) {
                            Log.e(TAG, "onComplete:  " + e.getMessage());
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReviewAdded(ReviewItemClickedEvent event) {

        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg, StoreHomeFragment.this.getActivity(),
                navigationController, this::showCustomDialog);
    }

    private void goToShopLocation(Shop shop) {
        if (shop != null) {
            if (shop.getLat() != null || shop.getLng() != null) {

                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + shop.getLat() + "," + shop.getLng());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }

            } else {
                Utils.ShowSnackBar(getActivity(), "No location information fo this shop!", Snackbar.LENGTH_SHORT);
            }
        }
    }
}
