package com.mimi.africa.ui.location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mimi.africa.R;
import com.mimi.africa.utils.LocationPreferences;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = MapFragment.class.getSimpleName();
    private GoogleMap mMap;
    private double mLat;
    private double mLong;
    @Nullable
    private String mLocationName;


    public MapFragment() {
    }

    @NonNull
    public static MapFragment newInstance(double lat, double lng, String locationName){
        MapFragment mapFragment = new MapFragment();
        Bundle args = new Bundle();
        args.putDouble("LAT", lat);
        args.putDouble("LNG", lng);
        args.putString("Location", locationName);
        mapFragment.setArguments(args);
        return mapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            Bundle args = getArguments();
            mLat  = args.getDouble("LAT");
            mLong = args.getDouble("LNG");
            mLocationName = args.getString("Location");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (LocationPreferences.isLocationLatLonAvailable(getContext())){
            LatLng myLoc  = new LatLng(LocationPreferences.getLocationCoordinates(getContext())[0],
                    LocationPreferences.getLocationCoordinates(getContext())[1]);
            mMap.addMarker(new MarkerOptions().position(myLoc));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
        }

//        LatLng newLoc = new LatLng(mLat, mLong);
//        mMap.addMarker(new MarkerOptions().position(newLoc).title(mLocationName));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(newLoc));
//        mMap.moveCamera(CameraUpdateFactory.);

        mMap.setBuildingsEnabled(true);
        mMap.setMinZoomPreference(15);
    }
}
