package com.mimi.africa.ui.mainhome;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.ProductVerticalListAdapter;
import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomFavouriteOptionsLayoutBinding;
import com.mimi.africa.databinding.ProductsFragmentTabBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.LoadMoreProducts;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ProductsFragmentTab extends BaseFragment {

    public static final String  ALL_TAB = "All";
    public static final String PRODUCTS_TAB = "Products";
    public static final String SERVICES_TAB = "Services";
    public static final String STORES_TAB = "Stores";
    private static final String TAG = ProductsFragmentTab.class.getSimpleName();
    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;
    private  View parentView;
    private AutoClearedValue<ProductsFragmentTabBinding> binding;
    private String mTerm;

    private AutoClearedValue<BottomFavouriteOptionsLayoutBinding> bottomFavouriteOptionsLayoutBinding;
    private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
    private BasketCountViewModel basketCountViewModel;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;


    @Inject
    protected MimiDb mimiDb;

    private PSDialogMsg psDialogMsg;

    @Inject
    APIService mAPIService;

    private String mPage;
    private String mPreviousPage;

    public ProductsFragmentTab() {
    }

    @NonNull
    public static ProductsFragmentTab newInstance() {
        return new ProductsFragmentTab();
    }

    @NonNull
    public static ProductsFragmentTab newInstance(String term) {
        ProductsFragmentTab productsFragmentTab = new ProductsFragmentTab();
        Bundle bundle = new Bundle();
        bundle.putString("term", term);
        productsFragmentTab.setArguments(bundle);
        return productsFragmentTab;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null){
            mTerm = getArguments().getString("term");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ProductsFragmentTabBinding productsFragmentTabBinding =
//                DataBindingUtil.inflate(inflater, R.layout.products_fragment_search_tabs, container, false, dataBindingComponent);
                DataBindingUtil.inflate(inflater, R.layout.products_fragment_tab, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, productsFragmentTabBinding);

        return productsFragmentTabBinding.getRoot();
    }

    private Observable<ListingResponse> getOffersResponseObservable() {
        return mAPIService.getOffers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    protected void initUIAndActions() {
        psDialogMsg = new PSDialogMsg(getActivity(), false);
        parentView= getActivity().findViewById(android.R.id.content);

        mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
        bottomFavouriteOptionsLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_favourite_options_layout, null, false));
        mBottomSheetDialog.get().setContentView(bottomFavouriteOptionsLayoutBinding.get().getRoot());

    }

    @Override
    protected void initViewModels() {
        basketCountViewModel = ViewModelProviders.of(this,viewModelFactory).get(BasketCountViewModel.class);
    }

    @Override
    protected void initAdapters() {}

    @Override
    protected void initData() {
        getAllListings("1");
    }

    private void getAllListings(String page) {

        mAPIService.getShopsListingPage("all", page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        Utils.RemoveView(binding.get().loadPrevious);
                        binding.get().setLoadingMore(true);
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {
                        if (listingResponse.getLinks().getPrev() != null) {
                            char previousPageNum = listingResponse.getLinks().getPrev().charAt(listingResponse.getLinks().getPrev().length() - 1);
                            mPreviousPage = String.valueOf(previousPageNum);
                        }

                        mPage =     listingResponse.getLinks().getNext();
                        if (  mPage != null){
                            Log.i(TAG, "onNext: Page " + mPage.charAt(mPage.length()-1));
                            char pageNum = mPage.charAt(mPage.length()-1);
                            mPage = String.valueOf(pageNum);
                        }
                        initializeAdapter(listingResponse.getInventories());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        try{
                            Utils.RemoveView(binding.get().productsProgressBar);
                        }catch (Exception exx){
                            Log.e(TAG, "onError:  " + exx.getMessage() );
                        }

                        Snackbar snackbar = Snackbar.make(parentView,
                                "There was a problem loading products!", Snackbar.LENGTH_LONG)
                                .setAction("RELOAD!", view -> getAllListings("1"));
                        snackbar.show();

                        Log.e(TAG, "onError: getListings  "  + e.getMessage() );
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {

                        binding.get().setLoadingMore(false);

                        try {
                            Utils.ShowView(binding.get().productsRecyclerView);
                            Utils.RemoveView(binding.get().productsProgressBar);
                        }catch (Exception e){
                            Log.e(TAG, "onComplete: "   +e. getMessage());
                        }
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }

                    }
                });
    }

    private void initializeAdapter(@NonNull List<Inventory> inventoryList) {
        try {

            if (getActivity() != null)

                binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
                ProductVerticalListAdapter productVerticalListAdapter = new ProductVerticalListAdapter(getContext(), inventoryList);
                binding.get().productsRecyclerView.setAdapter(productVerticalListAdapter);

                binding.get().productsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        int visibleItemCount    = 0;
                        int totalItemCount      = 0;
                        int pastVisibleItems    = 0;

                        if (binding.get().productsRecyclerView.getLayoutManager() != null){
                            visibleItemCount = binding.get().productsRecyclerView.getLayoutManager().getChildCount();
                            totalItemCount  = binding.get().productsRecyclerView.getLayoutManager().getItemCount();
                            Log.i(TAG, "onScrolled: "+ visibleItemCount + " total Item Count " + totalItemCount );
//                            pastVisibleItems  = binding.get().productsRecyclerView.getLayoutManager().;
                        }
                    }
                });

                productVerticalListAdapter.setOnItemClickListener(new ProductVerticalListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Inventory obj, int pos) {

                        if (view == view.findViewById(R.id.nameTextView)) {

                            navigateToStore(obj);

                        } else if (view == view.findViewById(R.id.addressTextView)) {

                            navigateToLocation(getActivity());

                        } else if (view == view.findViewById(R.id.productImageView)) {

                            navigateToItemDetailActivity(getActivity(), inventoryList.get(pos));

                        }else {

                            navigateToItemDetailActivity(getActivity(), inventoryList.get(pos));
                        }

                    }

                    @Override
                    public void onItemLongClick(View view, Inventory obj, int pos) {
                        mBottomSheetDialog.get().show();
                        bottomFavouriteOptionsLayoutBinding.get().titleTextView.setText(obj.getTitle());
                        bottomFavouriteOptionsLayoutBinding.get().readStatus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                addToCart(1, obj.getSlug());
                            }
                        });

                        bottomFavouriteOptionsLayoutBinding.get().archiveMessage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                removeFromWishList(String.valueOf(obj.getProduct().getId()));
                            }
                        });
                    }
                });

        }catch (Exception e){
            Log.e(TAG, "onNext: " + e.getMessage());
        }
    }

    private void subscribeRequest() {
        try {

            Utils.ShowView(binding.get().productsProgressBar);
            Utils.RemoveView(   binding.get().noItemConstraintLayout);

        }catch (Exception e){
            Log.e(TAG, "onSubscribe: "  + e.getMessage() );
            Sentry.captureException(e);
        }
    }

    private void completeRequest(boolean[] empty) {
        try {
            if (empty[0]) {
                Utils.ShowView(binding.get().noItemConstraintLayout);
                Utils.RemoveView(binding.get().productsRecyclerView);
                Utils.RemoveView(binding.get().productsProgressBar);

            } else {
                Utils.ShowView(binding.get().productsRecyclerView);
                Utils.RemoveView(binding.get().productsProgressBar);
//                                binding.get().noItemConstraintLayout.setVisibility(View.VISIBLE);
                Utils.RemoveView(binding.get().noItemConstraintLayout);

            }
        } catch (Exception e) {
            Log.e(TAG, "onComplete: " + e.getMessage());
            Sentry.captureException(e);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadProducts(LoadMoreProducts loadMoreProducts){
        if (mPage != null) {
            getAllListings(mPage);
            Utils.ShowView(binding.get().loadPrevious);
            Log.i(TAG, "onLoadProducts: Page " + mPage);
        }

        binding.get().loadPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: Page " + mPreviousPage);

                    if (Integer.parseInt(mPreviousPage) > 1) {
                        getAllListings(String.valueOf(mPreviousPage));
                        Utils.ShowView(binding.get().loadPrevious);
                    } else if (Integer.parseInt(mPreviousPage) == 1)
                        getAllListings(mPreviousPage);
                    Utils.RemoveView(binding.get().loadPrevious);
                    Log.i(TAG, "onLoadProducts: Page " + mPreviousPage);
                }
        });

    }
}