//package com.mimi.mimi.ui.activities;
//
//import android.content.Intent;
//import android.content.res.Configuration;
//import android.graphics.PorterDuff;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.ActionBar;
//import androidx.appcompat.app.ActionBarDrawerToggle;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//import androidx.core.content.ContextCompat;
//import androidx.drawerlayout.widget.DrawerLayout;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.viewpager.widget.ViewPager;
//
//import com.google.android.material.navigation.NavigationView;
//import com.google.android.material.snackbar.Snackbar;
//import com.mimi.mimi.Adapter.AdapterGridShopCategory;
//import com.mimi.mimi.Adapter.AdapterGridShopProductCard;
//import com.mimi.mimi.Adapter.AdapterImageSlider;
//import com.mimi.mimi.R;
//import com.mimi.mimi.api.APIService;
//import com.mimi.mimi.api.response.SliderResponse;
//import com.mimi.mimi.model.ShopCategory;
//import com.mimi.mimi.ui.health.HealthCategoryActivity;
//import com.mimi.mimi.ui.search.SearchStoreActivity;
//import com.mimi.mimi.ui.category.CategoryListActivity;
//import com.mimi.mimi.utils.Constants;
//import com.mimi.mimi.utils.DataGenerator;
//import com.mimi.mimi.utils.SpacingItemDecoration;
//import com.mimi.mimi.utils.Tools;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import io.reactivex.Observer;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//
//
//public class MainHomeActivityCopy extends AppCompatActivity {
//
//    @Nullable
//    private ActionBar actionBar;
//    private Toolbar toolbar;
//    private View parent_view;
//    private ViewPager viewPager;
//    private LinearLayout layout_dots;
//    private AdapterImageSlider adapterImageSlider;
//    @Nullable
//    private Runnable runnable = null;
//    @NonNull
//    private Handler handler = new Handler();
//    private RecyclerView recyclerView;
//    private AdapterGridShopCategory mAdapter;
//
//    private RecyclerView productsRecyclerView;
//    private RecyclerView servicesRecyclerView;
//    private RecyclerView mallsRecyclerView;
//    private AdapterGridShopProductCard mProductsAdapter;
//
//    private LinearLayout categoriesLinearLayout;
//    private LinearLayout healthLinearLayout;
//
//    private ImageView categoriesImageView;
//    private TextView search_text;
//    private TextView cat_text;
//    private APIService mAPIService;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_home);
//        mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
//
//        initToolbar();
//        initNavigationMenu();
//        initComponent();
//
//          categoriesImageView = findViewById(R.id.categories_image);
//
//          categoriesImageView.setOnClickListener(v -> navigateToCategoryList());
//
//          cat_text.setOnClickListener(v -> Toast.makeText(MainHomeActivityCopy.this, "Worke", Toast.LENGTH_SHORT).show());
//
//          getListings("random");
//
//    }
//
//    private void initToolbar() {
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
//
////        actionBar.setTitle("Drawer News");
//        Tools.setSystemBarColor(this);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_cart_setting, menu);
//        return true;
//    }
//
//
//    private void initComponent() {
//
//        cat_text = findViewById(R.id.products_text);
//        parent_view = findViewById(R.id.parent_view);
//        search_text = findViewById(R.id.search_text);
//        categoriesLinearLayout = findViewById(R.id.categories_linear_layout);
//        healthLinearLayout = findViewById(R.id.health_linear_layout);
//        productsRecyclerView =  findViewById(R.id.products_recyclerView);
//        servicesRecyclerView =  findViewById(R.id.services_recyclerView);
//        mallsRecyclerView =  findViewById(R.id.malls_recyclerView);
//
//        categoriesLinearLayout.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View v) {
//                    navigateToCategoryList();
//              }
//        });
//
//        healthLinearLayout.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View v) {
//                    startActivity(new Intent(getApplicationContext(), HealthCategoryActivity.class));
//              }
//        });
//
//        search_text.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View v) {
//                    startActivity(new Intent(getApplicationContext(), SearchStoreActivity.class));
//              }
//        });
//
//        productsRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
//        servicesRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
//        mallsRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
//
////        List<ShopProduct> productItems = DataGenerator.getShoppingProduct(this);
//
////        mProductsAdapter = new AdapterGridShopProductCard(this, productItems);
//
////        productsRecyclerView.setAdapter(mProductsAdapter);
//
////        servicesRecyclerView.setAdapter(mProductsAdapter);
////        mallsRecyclerView.setAdapter(mProductsAdapter);
//
//        // on item list clicked
////        mProductsAdapter.setOnItemClickListener(new AdapterGridShopProductCard.OnItemClickListener() {
////            @Override
////            public void onItemClick(View view, ShopProduct obj, int position) {
////
////            }
////        });
//
//        recyclerView = findViewById(R.id.recyclerView);
//
//        int orientation = getResources().getConfiguration().orientation;
//
//        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            // In landscape
//
//            recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
//            recyclerView.addItemDecoration(new SpacingItemDecoration(3, Tools.dpToPx(this, 8), true));
//
//        } else {
//            // In portrait
//
//            recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
//            recyclerView.addItemDecoration(new SpacingItemDecoration(3, Tools.dpToPx(this, 8), true));
//        }
//
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setNestedScrollingEnabled(false);
//
//        List<ShopCategory> shopCategoryItems = DataGenerator.getShoppingCategory(this);
//
//        //set data and list adapter
//        mAdapter = new AdapterGridShopCategory(this, shopCategoryItems);
//        recyclerView.setAdapter(mAdapter);
//
//        mAdapter.setOnItemClickListener(new AdapterGridShopCategory.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, @NonNull ShopCategory obj, int position) {
//                Snackbar.make(parent_view, "Item " + obj.title + " clicked", Snackbar.LENGTH_SHORT).show();
//            }
//        });
//
//        layout_dots = findViewById(R.id.layout_dots);
//        viewPager = findViewById(R.id.pager);
//
//          adapterImageSlider = new AdapterImageSlider(this, new ArrayList<>());
//
//          getAllSliders();
//    }
//
//      private void navigateToCategoryList() {
//            startActivity(new Intent(getApplicationContext(), CategoryListActivity.class));
//      }
//
//      private void initNavigationMenu() {
//        NavigationView nav_view = findViewById(R.id.nav_view);
//        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//            }
//        };
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
//                Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
//                actionBar.setTitle(item.getTitle());
//                drawer.closeDrawers();
//                return true;
//            }
//        });
//
//        // open drawer at start
////        drawer.openDrawer(GravityCompat.START);
//    }
//
//      public void navigateToCategoryList(View view) {
//            startActivity(new Intent(getApplicationContext(), CategoryListActivity.class));
//      }
//
//    private void addBottomDots(@NonNull LinearLayout layout_dots, int size, int current) {
//        ImageView[] dots = new ImageView[size];
//
//        layout_dots.removeAllViews();
//        for (int i = 0; i < dots.length; i++) {
//            dots[i] = new ImageView(this);
//            int width_height = 15;
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
//            params.setMargins(10, 10, 10, 10);
//            dots[i].setLayoutParams(params);
//            dots[i].setImageResource(R.drawable.shape_circle);
//            dots[i].setColorFilter(ContextCompat.getColor(this, R.color.overlay_dark_10), PorterDuff.Mode.SRC_ATOP);
//            layout_dots.addView(dots[i]);
//        }
//
//        if (dots.length > 0) {
//            dots[current].setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryLight), PorterDuff.Mode.SRC_ATOP);
//        }
//    }
//
//    private void startAutoSlider(final int count) {
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                int pos = viewPager.getCurrentItem();
//                pos = pos + 1;
//                if (pos >= count) pos = 0;
//                viewPager.setCurrentItem(pos);
//                handler.postDelayed(runnable, 3000);
//            }
//        };
//        handler.postDelayed(runnable, 3000);
//    }
//
//      private void getAllSliders(){
//
//            mAPIService.getSliders()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<SliderResponse>() {
//                          @Override
//                          public void onSubscribe(Disposable d) {}
//
//                          @Override
//                          public void onNext(@NonNull SliderResponse slider) {
//
//                                adapterImageSlider.setItems(slider.getSlider());
//                                viewPager.setAdapter(adapterImageSlider);
//
//                                // displaying selected locationImage first
//                                viewPager.setCurrentItem(0);
//                                addBottomDots(layout_dots, adapterImageSlider.getCount(), 0);
//                                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                                      @Override
//                                      public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {}
//
//                                      @Override
//                                      public void onPageSelected(int pos) {
//                                            addBottomDots(layout_dots, adapterImageSlider.getCount(), pos);
//                                      }
//
//                                      @Override
//                                      public void onPageScrollStateChanged(int state) {
//                                      }
//                                });
//
//                                startAutoSlider(adapterImageSlider.getCount());
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                Log.e("MainHomeActivity", "onError:  "  + e.getMessage() );
//                          }
//
//                          @Override
//                          public void onComplete() {
//
//                          }
//                    });
//      }
//
//      private void getListings(String list){
//
////            mAPIService.getListings(list)
////                    .subscribeOn(Schedulers.io())
////                    .observeOn(AndroidSchedulers.mainThread())
////                    .subscribe(new Observer<ListingResponse>() {
////                          @Override
////                          public void onSubscribe(Disposable d) {
////
////                          }
////
////                          @Override
////                          public void onNext(ListingResponse listingResponse) {
////                                Log.i("MainHomePageActivity", "onNext:  " + listingResponse.getItems().get(0).getTitle() );
////
////                                mProductsAdapter = new AdapterGridShopProductCard(getApplicationContext(), listingResponse.getItems());
////                                productsRecyclerView.setAdapter(mProductsAdapter);
////
////                                servicesRecyclerView.setAdapter(mProductsAdapter);
////                                mallsRecyclerView.setAdapter(mProductsAdapter);
////
////                                // on item list clicked
////                                mProductsAdapter.setOnItemClickListener(new AdapterGridShopProductCard.OnItemClickListener() {
////                                      @Override
////                                      public void onItemClick(View view, Inventory obj, int position) {
////
////                                      }
////                                });
////
////                                mProductsAdapter.setOnMoreButtonClickListener(new AdapterGridShopProductCard.OnMoreButtonClickListener() {
////                                      @Override
////                                      public void onItemClick(View view, Inventory obj, MenuItem item) {
//////                                            Snackbar.make(parent_view, obj.getTitle() + " (" + item.getTitle() + ") clicked", Snackbar.LENGTH_SHORT).show();
////                                      }
////                                });
////                          }
////
////                          @Override
////                          public void onError(Throwable e) {
////
////                          }
////
////                          @Override
////                          public void onComplete() {
////
////                          }
////                    });
//      }
//}
