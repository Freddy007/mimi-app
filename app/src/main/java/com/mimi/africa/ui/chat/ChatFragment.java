package com.mimi.africa.ui.chat;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxLayoutBinding;
import com.mimi.africa.databinding.BottomChatLayoutBinding;
import com.mimi.africa.databinding.FragmentAllProductListBinding;
import com.mimi.africa.databinding.FragmentChatBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.AttachmentAdded;
import com.mimi.africa.event.ChatClickedEvent;
import com.mimi.africa.event.SendMessageEvent;
import com.mimi.africa.model.Chat;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ChatFragment extends BaseFragment implements View.OnClickListener  {

      private static final String TAG = ChatFragment.class.getSimpleName();
      /** Standard activity result: operation canceled. */
      public static final int RESULT_CANCELED    = 0;
      /** Standard activity result: operation succeeded. */
      public static final int RESULT_OK           = -1;


      ArrayList<Chat> rowListItem;
      ChatAdapter rcAdapter;

      private TextView storeNameTextView;
      private RecyclerView rView;
      private EditText mMessageEditText;
      private TextView mTimeView;
      private TextView mLastTime;
      private NestedScrollView nestedScrollView;
      private ImageView addAttachmentImageView;
      private Activity mActivity;
      private ImageView demoImageView;

      private int mShopId;
      private String mShopName;
      private String customer_id ;

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentChatBinding> binding;
      private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
      private AutoClearedValue<BottomChatLayoutBinding> bottomChatLayoutBinding;

      @Inject
      protected SharedPreferences pref;

      @Inject
      APIService mAPIService;

      @Inject
      MimiDb mimiDb;

      @Inject
      AppExecutors appExecutors;

      public ChatFragment() {}

      public static ChatFragment newInstance(String shopId, String name){
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SHOP_ID, shopId);
            bundle.putString(Constants.SHOP_NAME, name);
            chatFragment.setArguments(bundle);
            return chatFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null){
                  mShopId = Integer.parseInt(getArguments().getString(Constants.SHOP_ID));
                  mShopName = (getArguments().getString(Constants.SHOP_NAME));
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            FragmentChatBinding fragmentChatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container,false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentChatBinding);
            return fragmentChatBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            if (getContext() != null ) {
                  mBottomSheetDialog           = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  bottomChatLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_chat_layout, null, false));
                  mBottomSheetDialog.get().setContentView(bottomChatLayoutBinding.get().getRoot());
                  mMessageEditText = binding.get().sendMessage;
                  nestedScrollView = binding.get().scroll;
                  mLastTime = binding.get().lastTime;
                  addAttachmentImageView = binding.get().addAttachment;
                  demoImageView = binding.get().demoImageView;
                  customer_id = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
                  mActivity = getActivity();
            }

            if (CustomSharedPrefs.getLastChatTime(getActivity()) != 0){
                  mLastTime.setText(Utils.getDateString(CustomSharedPrefs.getLastChatTime(mActivity), "dd/MM/yyyy hh:mm:ss"));
            }

            mMessageEditText.setOnKeyListener((v, keyCode, event) -> {
                  // If the event is a key-down event on the "enter" button
                  if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                          (keyCode == KeyEvent.KEYCODE_ENTER)) {

                        sendMessage();

                        return true;
                  }
                  return false;
            });

            binding.get().buttonSend.setOnClickListener(v -> {
                  sendMessage();
            });
      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

            binding.get().recyclerView.setHasFixedSize(false);
            binding.get().recyclerView.setLayoutManager(layoutManager);
            binding.get().recyclerView.setNestedScrollingEnabled(false);

            getMessages();

            mMessageEditText.setOnClickListener(view -> new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        layoutManager.scrollToPosition(binding.get().recyclerView.getAdapter().getItemCount() -1);
                  }
            },3000));

            addAttachmentImageView.setOnClickListener(v -> {
//                  requestPermission();
                  EventBus.getDefault().post(new AttachmentAdded());
            });


      }

      @Override
      protected void initData() {

      }

      private void sendMessage() {
            if (!mMessageEditText.getText().toString().trim().isEmpty()) {
                  sendMessage(mMessageEditText.getText().toString());
                  EventBus.getDefault().post(new SendMessageEvent());

                  mMessageEditText.setText("");
//                  hideKeyboard();
            }
      }

      private void sendMessage(String message){

            String subject = "Message";
            String token = CustomSharedPrefs.getPushyToken(getContext());

            mAPIService.sendMessage(String.valueOf(mShopId),customer_id,subject,message,token )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {}

                          @Override
                          public void onComplete() {
                                Log.i(TAG, "onComplete:  " );
                                disposeAPIService(disposable);
                          }
                    });
      }

      private void getMessages() {

            final List<Chat>[] chats = new List[]{new ArrayList<>()};

            mAPIService.getMessages(String.valueOf(mShopId), customer_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<Chat>>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                if (getActivity() != null ) {
                                      ((MessageActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                                      ((MessageActivity) getActivity()).progressDialog.setCancelable(false);
                                      ((MessageActivity) getActivity()).progressDialog.show();
                                      disposable = d;
                                }
                          }

                          @Override
                          public void onNext(@NonNull List<Chat> chatList) {
                                if (getActivity() !=null ){
                                      rcAdapter = new ChatAdapter(getActivity(), chatList);
                                      binding.get().recyclerView.setAdapter(rcAdapter);
                                      chats[0] = chatList;

                                      rcAdapter.setOnClickListener(new ChatAdapter.OnClickListener() {
                                            @Override
                                            public void onItemClick(View view, Chat obj, int pos) {
                                                  Toast.makeText(mActivity, "Send Text ", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onItemLongClick(View view, Chat obj, int pos) {

                                                  mBottomSheetDialog.get().show();

                                                  bottomChatLayoutBinding.get().shareButton.setOnClickListener(v -> {
                                                        Intent sendIntent = new Intent();
                                                        sendIntent.setAction(Intent.ACTION_SEND);
                                                        sendIntent.putExtra(Intent.EXTRA_TEXT, obj.getMessage());
                                                        sendIntent.setType("text/plain");

                                                        Intent shareIntent = Intent.createChooser(sendIntent, null);
                                                        startActivity(shareIntent);
                                                        mBottomSheetDialog.get().dismiss();
                                                  });

                                                  bottomChatLayoutBinding.get().copyButton.setOnClickListener(v -> {
                                                        Toast.makeText(mActivity, "Copied text!", Toast.LENGTH_SHORT).show();
                                                        Tools.copyToClipboard(getActivity(), obj.getMessage());
                                                        mBottomSheetDialog.get().dismiss();
                                                  });

                                            }
                                      });

                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                ((MessageActivity) getActivity()).progressDialog.hide();
                          }

                          @Override
                          public void onComplete() {
                                ((MessageActivity) getActivity()).progressDialog.hide();
                                if (getActivity() != null ) {

                                      rcAdapter.notifyDataSetChanged();
                                      nestedScrollView.post(() -> {
                                            nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                      });
                                }
                          }
                    });
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

      private void requestPermission(){
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
      }



      @Subscribe( threadMode = ThreadMode.MAIN)
      public void onMessageSent(SendMessageEvent event){

            new Handler().postDelayed(() -> {
                  getMessages();
                  rcAdapter.notifyDataSetChanged();
                  nestedScrollView.post(() -> {
                        nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                  });
            },2000);
      }

      @Subscribe( threadMode = ThreadMode.MAIN)
      public void onMessageLongClicked(ChatClickedEvent event){
            mBottomSheetDialog.get().show();

            bottomChatLayoutBinding.get().shareButton.setOnClickListener(v -> {
                  Intent sendIntent = new Intent();
                  sendIntent.setAction(Intent.ACTION_SEND);
                  sendIntent.putExtra(Intent.EXTRA_TEXT, event.getText());
                  sendIntent.setType("text/plain");

                  Intent shareIntent = Intent.createChooser(sendIntent, null);
                  startActivity(shareIntent);
                  mBottomSheetDialog.get().dismiss();
            });

            bottomChatLayoutBinding.get().copyButton.setOnClickListener(v -> {
                  Toast.makeText(mActivity, "Copied text!", Toast.LENGTH_SHORT).show();
                  Tools.copyToClipboard(getActivity(), event.getText());
                  mBottomSheetDialog.get().dismiss();
            });
      }

      @Override
      public void onClick(View v) {
            if (v.getId() == R.id.buttonSend) {
                sendMessage();
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onSendMessage(SendMessageEvent sendMessageEvent){

            appExecutors.getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {
                        if (mimiDb.notificationDaoAccess().fetchNotificationByShopId(String.valueOf(mShopId)) != null ) {

                              Notification notification = mimiDb.notificationDaoAccess().fetchNotificationByShopId(String.valueOf(mShopId));
                              notification.setDateCreated(System.currentTimeMillis());
                              notification.setName("Message!");
                              notification.setMessage(mMessageEditText.getText().toString());
                              notification.setArchived(false);
                              notification.setRead(true);
                              mimiDb.notificationDaoAccess().updateNotification(notification);
                        }else {

                              Notification appNotification = new Notification();
                              appNotification.setName("Message");
                              appNotification.setMessage(mMessageEditText.getText().toString());
                              appNotification.setOwner(Notification.NOTIFICATION_CHATS);
                              appNotification.setSender(mShopName);
                              appNotification.setSenderId(String.valueOf(mShopId));
                              appNotification.setRead(false);
                              appNotification.setArchived(false);
                              appNotification.setDateCreated(System.currentTimeMillis());
                              mimiDb.notificationDaoAccess().insert(appNotification);

                        }
                  }
            });

      }
}
