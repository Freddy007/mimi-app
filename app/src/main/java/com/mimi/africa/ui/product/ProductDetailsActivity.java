package com.mimi.africa.ui.product;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.CartClicked;
import com.mimi.africa.event.OnClickEvent;
import com.mimi.africa.event.OnSizeGuideClicked;
import com.mimi.africa.event.ReviewsClickedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.reviews.ReviewsFragment;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.viewModels.BasketCountViewModel;
import com.viven.imagezoom.ImageZoomHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class ProductDetailsActivity extends BaseActivity implements View.OnClickListener {

      private static final String TAG = ProductDetailsActivity.class.getSimpleName();


      @Nullable
      private Inventory mInventory;

      private String mProductId;

      private ArrayAdapter adapter;
      private ImageZoomHelper imageZoomHelper;

      private BottomSheetBehavior mBehavior;
      private BottomSheetDialog mBottomSheetDialog;

      private BasketCountViewModel basketCountViewModel;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      public ProgressDialog progressDialog;

      TextView textCartItemCount;

      int mCartItemCount = 0;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_product_details);

            String title = "";
            if (getIntent() != null){
                  mInventory = getIntent().getParcelableExtra(Constants.INVENTORY_OBJECT);

                  if (getIntent().getStringExtra("product_id") != null){
                        mProductId = getIntent().getStringExtra("product_id");
                  }

                  if (mInventory != null) {
                        title = mInventory.getTitle();
                  }
            }

            initToolbar(title);
            initComponent();
            getBasketCountsViewModel();
      }

      private void initToolbar(String title) {
            Toolbar toolbar =  findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this);
      }

      private void initComponent() {

            basketCountViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketCountViewModel.class);

            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);

            Fragment productDetailsFragment = ProductDetailsFragment.newInstance(mInventory, mProductId);
            setupFragment(productDetailsFragment);
      }

      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_cart_setting, menu);
            FrameLayout actionView = (FrameLayout)menu.findItem(R.id.action_cart).getActionView();
            textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);
            setupBadge();

            MenuItem item = menu.findItem(R.id. action_cart);
//
            actionView.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onOptionsItemSelected(item);
                  }
            });

            return true;
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {

            if (item.getItemId() == android.R.id.home) {
                  finish();
            }else if (item.getItemId() == R.id.action_cart){
                  EventBus.getDefault().post(new CartClicked());
            }

            return super.onOptionsItemSelected(item);
      }

      private void setupBadge() {

            if (textCartItemCount != null) {
                  if (mCartItemCount == 0) {
                        if (textCartItemCount.getVisibility() != View.GONE) {
                              textCartItemCount.setVisibility(View.GONE);
                        }
                  } else {
                        textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                        if (textCartItemCount.getVisibility() != View.GONE) {
                              textCartItemCount.setVisibility(View.GONE);
                        }
                  }
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(AddToCart event) {
            new Handler().postDelayed(() -> {
                  basketCountViewModel.setBasketListObj();
                  setupBadge();
            },2000);

            Log.i(TAG, "onItemClicked: ");
      }

      @Subscribe( threadMode = ThreadMode.MAIN)
      public void onReviewsClicked(ReviewsClickedEvent event){
            ReviewsFragment reviewsFragment = ReviewsFragment.newInstance(event.getFeedbackList());
            showDialog(reviewsFragment);
      }

      private void showBottomSheetDialog(final Feedback feedback) {
            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                  mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);
            if (feedback.getCustomer() != null) {
                  ((TextView) view.findViewById(R.id.name)).setText(feedback.getCustomer().getName());
            }

            ((TextView) view.findViewById(R.id.address)).setText(feedback.getComment());
            (view.findViewById(R.id.bt_close)).setOnClickListener(view1 -> mBottomSheetDialog.dismiss());

            mBottomSheetDialog = new BottomSheetDialog(this);
            mBottomSheetDialog.setContentView(view);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }

            mBottomSheetDialog.show();
            mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                  @Override
                  public void onDismiss(DialogInterface dialog) {
                        mBottomSheetDialog = null;
                  }
            });
      }

      @Override
      public void onBackPressed() {
            super.onBackPressed();
//            Intent intent = new Intent(this, MainHomeActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
            finish();
      }

      private void getBasketCountsViewModel(){
            basketCountViewModel.getBasketCounts().observe(this, list -> {
                  Log.i(TAG, "onChanged:  size: " + list.size());

                  if (list.size() > 0 ) {
                        mCartItemCount = list.get(0).getCount();
                  }else {

                  }
            });
      }

      @Override
      protected void onResume() {
            super.onResume();
            basketCountViewModel.setBasketListObj();
            Log.i(TAG, "onResume:  " +mCartItemCount);
      }

      @Override
      public void onClick(View view) {
            EventBus.getDefault().post(new OnClickEvent(view));
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onSizeGuideClicked(OnSizeGuideClicked event){
            ProductSizeGuideFragment productSizeGuideFragment = new ProductSizeGuideFragment();
            showDialog(productSizeGuideFragment);

      }
}
