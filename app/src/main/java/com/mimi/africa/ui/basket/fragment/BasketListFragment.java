package com.mimi.africa.ui.basket.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentBasketListBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.OtherVariant;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.basket.adapter.BasketAdapter;
import com.mimi.africa.ui.basket.viewModel.BasketViewModel;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class BasketListFragment extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    //region Variables

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private PSDialogMsg psDialogMsg;

    @VisibleForTesting
    private AutoClearedValue<FragmentBasketListBinding> binding;
    private AutoClearedValue<BasketAdapter> basketAdapter;
    private BasketViewModel basketViewModel;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @Inject
    protected NavigationController navigationController;

    private BasketCountViewModel basketCountViewModel;

    @Inject
     APIService mAPIService;

    @Inject
    MimiDb mimiDb;


    private StringBuilder cartIds_sb;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentBasketListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_basket_list, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);
        cartIds_sb = new StringBuilder();

        return binding.get().getRoot();
    }

    @Override
    public void onDispatched() {}

    @Override
    protected void initUIAndActions() {

        psDialogMsg = new PSDialogMsg(getActivity(), false);

        if (getContext() != null ) {
            binding.get().checkoutButton.setText(binding.get().checkoutButton.getText().toString());

            binding.get().checkoutButton.setOnClickListener(view -> {
                doCheckOut();
            });
        }
    }

    private void doCheckOut() {

        if (getActivity() != null ) {
            String tagClass = BasketListActivity.class.getSimpleName();

            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, tagClass, psDialogMsg,
                    BasketListFragment.this.getActivity(), navigationController, () -> {
                        if (getActivity() != null) {
                            Intent intent = new Intent(getActivity(), MainCheckoutActivity.class);
                            getActivity().startActivityForResult(intent, Constants.REQUEST_CODE__BASKET_FRAGMENT);
                        }
                    });
        }
    }

    @Override
    protected void initViewModels() {
        basketViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketViewModel.class);
        basketCountViewModel = ViewModelProviders.of(this,viewModelFactory).get(BasketCountViewModel.class);

    }

    @Override
    protected void initAdapters() {

        //basket
        BasketAdapter basketAdapter1 = new BasketAdapter(dataBindingComponent, new BasketAdapter.BasketClickCallBack() {
            @Override
            public void onMinusClick(Basket basket, int count) {
//                Toast.makeText(getContext(), "clicked " + count, Toast.LENGTH_SHORT).show();
                    addToCart(-1, basket.product.getSlug());
            }

            @Override
            public void onAddClick(Basket basket) {
                addToCart(1, basket.product.getSlug());
            }

            @Override
            public void onDeleteConfirm(Basket basket) {

                psDialogMsg.showConfirmDialog(getString(R.string.delete_item_from_basket), getString(R.string.app__ok), getString(R.string.app__cancel));

                psDialogMsg.show();

                psDialogMsg.okButton.setOnClickListener(view -> {
                    if (getActivity() != null) {
                        removeItem(basket.productId, String.valueOf(basket.product.getId()), basket.count);
                    }

                    new Handler().postDelayed(() -> {
                        getCarts();
                        basketAdapter.get().notifyDataSetChanged();
                    },3000);

                    psDialogMsg.cancel();
                });
                psDialogMsg.cancelButton.setOnClickListener(view -> psDialogMsg.cancel());

            }

            @Override
            public void onClick(Basket basket) {
                if (getActivity() != null ) {
                    navigateToItemDetailActivity(getActivity(), basket.product);
                }
            }

        }, this);
        basketAdapter = new AutoClearedValue<>(this, basketAdapter1);
        bindingBasketAdapter(basketAdapter.get());

    }

    private void bindingBasketAdapter(BasketAdapter nvbasketAdapter) {
        if (getActivity() != null ) {
            this.basketAdapter = new AutoClearedValue<>(this, nvbasketAdapter);
            binding.get().basketRecycler.setAdapter(basketAdapter.get());
        }
    }

    @Override
    protected void initData() {

        if (getContext() != null) {
            binding.get().noItemTitleTextView.setText(getContext().getString(R.string.basket__no_item_title));
            binding.get().noItemDescTextView.setText(getContext().getString(R.string.basket__no_item_desc));
        }

        LoadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.REQUEST_CODE__BASKET_FRAGMENT
                && resultCode == Constant.RESULT_CODE__REFRESH_BASKET_LIST) {

                    loadLoginUserId();
                    Log.i(BasketListFragment.class.getSimpleName() , "run: Login User Id " + loginUserId);
                }

    }

    @Override
    public void onResume() {
     super.onResume();
}

    private void LoadData() {
        //load basket
        getCarts();
    }

    private void replaceProductSpecsData(List<Basket> basketList) {

        if (getActivity() != null ) {
            basketAdapter.get().replace(basketList);

            if (basketList != null) {
                basketAdapter.get().replace(basketList);

                if (basketList.size() > 0) {
                    basketViewModel.totalPrice = 0;

                    for (int i = 0; i < basketList.size(); i++) {
                        basketViewModel.totalPrice += basketList.get(i).basketPrice * basketList.get(i).count;
                    }

                    basketViewModel.basketCount = 0;

                    for (int i = 0; i < basketList.size(); i++) {
                        basketViewModel.basketCount += basketList.get(i).count;
                    }

                    String totalPriceString = "GHS " + " " + Utils.format(Utils.round(basketViewModel.totalPrice, 2));

                    binding.get().totalPriceTextView.setText(totalPriceString);
                    binding.get().countTextView.setText(String.valueOf(basketViewModel.basketCount));
                } else {
                    binding.get().totalPriceTextView.setText(String.valueOf(Constants.ZERO));
                    binding.get().countTextView.setText(String.valueOf(Constants.ZERO));

                    basketViewModel.totalPrice = 0;
                    basketViewModel.basketCount = 0;
//
                    if (basketList.size() > 0) {
                        for (int i = 0; i < basketList.size(); i++) {
                            basketViewModel.totalPrice += basketList.get(i).basketPrice * basketList.get(i).count;
                        }
                        for (int i = 0; i < basketList.size(); i++) {
                            basketViewModel.basketCount += basketList.get(i).count;
                        }

                        String totalPriceString = " GHS " + Utils.format(basketViewModel.totalPrice);
                        binding.get().totalPriceTextView.setText(totalPriceString);
                        binding.get().countTextView.setText(String.valueOf(basketViewModel.basketCount));
                    } else {
                        binding.get().totalPriceTextView.setText(String.valueOf(Constants.ZERO));
                        binding.get().countTextView.setText(String.valueOf(Constants.ZERO));
                    }
                    binding.get().executePendingBindings();

                }
            }

        }
    }

    private void getCarts() {

        mAPIService.getCarts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CartResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;

                        if (getContext() != null ) {
                            binding.get().setLoadingMore(true);
                        }
                    }

                    @Override
                    public void onNext(@NonNull CartResponse cartResponse) {

                        if (getActivity() != null ) {
                            List<Basket> cartList = new ArrayList<>();
                            double total = 0;

                            for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                                cartIds_sb.append(cartResponse.getCarts().get(i).getId());

                                if (i != (cartResponse.getCarts().size() - 1) && cartResponse.getCarts().size() > 1) {
                                    cartIds_sb.append(",");
                                }

                                if (cartResponse.getCarts().get(i).getItems() != null) {

                                    for (Inventory inventory : cartResponse.getCarts().get(i).getItems()) {

                                        String attribute = "";
                                        JsonObject jsonObject = new JsonObject();
                                        if (inventory.getPivot() != null) {
                                            String description = inventory.getPivot().getItemDescription();
                                            String[] attributes = new String[3];
                                            if (description != null) {
                                                attributes = description.split(" - ");
                                                if (attributes.length == 3) {
                                                    jsonObject.addProperty("1", attributes[1]);
                                                } else if (attributes.length == 4)
                                                    jsonObject.addProperty("1", attributes[1]);
                                                jsonObject.addProperty("2", attributes[2]);
                                            } else if (attributes.length > 4) {
                                                jsonObject.addProperty("1", attributes[1]);
                                                jsonObject.addProperty("2", attributes[2]);
                                                jsonObject.addProperty("3", attributes[3]);
                                            }
                                        }

                                        Basket basket = null;
                                        if (inventory.getPivot() != null) {
                                            Gson gson =  new Gson();
                                            OtherVariant[] otherVariant = gson.fromJson(String.valueOf(inventory.getPivot().getOtherVariants()), OtherVariant[].class);
                                            Log.i("BasketListFragment", "onNext: Variant Object " + otherVariant[0].getColorVariant());
                                            basket = new Basket(String.valueOf(inventory.getPivot().getCartId()), inventory.getPivot().getQuantity(),
                                                    jsonObject.toString(),  otherVariant[0].getImageVariant() ,
                                                    otherVariant[0].getColorVariant(), "2",
                                                    Float.valueOf(inventory.getPivot().getUnitPrice()), Float.valueOf(inventory.getPivot().getUnitPrice()),
                                                    String.valueOf(1), "0", inventory);
                                        }
                                        cartList.add(basket);
                                    }
                                }

                                if (cartResponse.getCarts().get(i) != null) {
                                    if (cartResponse.getCarts() != null) {
                                        int qty = 0;
                                        if (cartResponse.getCarts().get(i).getItems() != null) {
                                            qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());
//                                        total += Double.parseDouble(cartResponse.getCarts().get(i).getTotal() ) * qty;
                                            total += Double.parseDouble(cartResponse.getCarts().get(i).getItems().get(0).getPivot().getUnitPrice()) * qty;
                                        }
                                    }
                                }
                            }

                            if (cartList != null) {
                                if (cartList.size() > 0) {

                                    if (getContext() != null) {
                                        binding.get().noItemConstraintLayout.setVisibility(View.GONE);
                                        binding.get().checkoutConstraintLayout.setVisibility(View.VISIBLE);
                                    }

                                } else {

                                    if (getContext() != null) {
                                        binding.get().checkoutConstraintLayout.setVisibility(View.GONE);
                                        binding.get().noItemConstraintLayout.setVisibility(View.VISIBLE);
                                    }
                                }

                                replaceProductSpecsData(cartList);

                            } else {
                                if (cartList.size() == 0) {
                                    if (getContext() != null) {
                                        binding.get().checkoutConstraintLayout.setVisibility(View.GONE);
                                        binding.get().noItemConstraintLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("BasketListFragment", "onError: " + e.getMessage() );
                    }

                    @Override
                    public void onComplete() {
                        if (getContext() != null) {
                            binding.get().setLoadingMore(false);
                        }
                    }
                });
    }

    private void removeItem(String cartId, String item, int count) {

        mAPIService.remove(cartId, item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        if (getContext() != null) {
                            binding.get().setLoadingMore(true);
                        }
                    }

                    @Override
                    public void onNext(@NonNull JsonElement cartResponse) {

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                        if (getContext() != null) {
                            binding.get().setLoadingMore(false);

                            appExecutors.getDiskIO().execute(() -> {
                                if (mimiDb.basketCountDao().getBasketCount() > 0) {
                                    int oldCount = mimiDb.basketCountDao().getCount(1).getCount();
                                    int newCount = oldCount - (count);
                                    basketCountViewModel.setSaveToBasketListObj(newCount);
                                    mimiDb.basketCountDao().updateBasketCount(1, newCount);
                                }else {
//                                    BasketCount basketCount = new BasketCount();
//                                    basketCount.setCount(qty);
//                                    mimiDb.basketCountDao().insert(basketCount);
                                }
                            });

                        }
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                });
    }

    private void addToCart(int qty, String slug){
        mAPIService.addToCart(qty,"","","" ,slug)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        if (getContext() != null) {
                            binding.get().setLoadingMore(true);
                        }
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {

                        appExecutors.getDiskIO().execute(() -> {
                            if (mimiDb.basketCountDao().getBasketCount() > 0) {
                                int oldCount = mimiDb.basketCountDao().getCount(1).getCount();
                                int newCount = oldCount + qty;
                                basketCountViewModel.setSaveToBasketListObj(newCount);
                                mimiDb.basketCountDao().updateBasketCount(1, newCount);
                            }else {
                                BasketCount basketCount = new BasketCount();
                                basketCount.setCount(qty);
                                mimiDb.basketCountDao().insert(basketCount);
                            }
                        });
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {}

                    @Override
                    public void onComplete() {
                        getCarts();
                        if (getContext() != null) {
                            basketAdapter.get().notifyDataSetChanged();
                            binding.get().setLoadingMore(false);
                        }
                    }
                });
    }

}