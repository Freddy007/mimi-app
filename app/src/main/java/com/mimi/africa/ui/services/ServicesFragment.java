package com.mimi.africa.ui.services;


import android.content.Context;
import android.location.Address;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mimi.africa.Adapter.ShopCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentServicesBinding;
import com.mimi.africa.databinding.FragmentSingleCategoryBinding;
import com.mimi.africa.event.LocationSearchEvent;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.LocationUtilService;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShopViewModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class ServicesFragment extends BaseFragment {

      private static final String TAG = ServicesFragment.class.getSimpleName();
      private final DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentServicesBinding> binding;
      private Handler mHandler;
      private Runnable mRunnable;

      @Inject
       APIService mAPIService;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      private ShopViewModel shopsViewModel;

      public ServicesFragment() {}

      @Override
      public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentServicesBinding fragmentServicesBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_services, container, false,dataBindingComponent);

            binding = new AutoClearedValue<>(this, fragmentServicesBinding);

            mHandler = new Handler();

            return fragmentServicesBinding.getRoot();
      }


      @Override
      protected void initUIAndActions() {
            if (getActivity() != null)
            binding.get().storesAvailableRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
            binding.get().storesNearRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
            binding.get(). previouslyVisitedRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//            binding.get().suggestedProductsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
      }

      @Override
      protected void initViewModels() {
            shopsViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShopViewModel.class);
      }

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
            getAvailableShops();
      }

      private void getAvailableShops(){

            final boolean[] empty = {false};

            mAPIService.getAllCategoryProducts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<ProductCategory>>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {

                                ((ServicesActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                                ((ServicesActivity) getActivity()).progressDialog.setCancelable(false);
                                ((ServicesActivity) getActivity()).progressDialog.show();

                                disposable = d;
                          }

                          @Override
                          public void onNext(@NonNull List<ProductCategory> productCategories) {

                                            List<Shop> shopList = new ArrayList<>();

                                            try {

                                                  for (ProductCategory productCategory : productCategories) {

                                                        if (productCategory.getSubGroup() != null) {

                                                              if (productCategory.getSubGroup().getGroup().getSlug().toLowerCase().equals("services"))

                                                                    if (productCategory.getProducts() != null && productCategory.getProducts().size() > 0) {

                                                                          if (!shopList.contains(productCategory.getProducts().get(0).getShop())) {
                                                                                shopList.add(productCategory.getProducts().get(0).getShop());
                                                                          }
                                                                    }
                                                        }
                                                  }

                                                  if (shopList.size() > 0) {

                                                        ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopList);
                                                        binding.get().storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                                        shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                              navigateToStore(obj);
                                                        });
                                                  }else {
                                                        empty[0] = true;
                                                  }

                                            }catch (Exception e){
                                                  Log.e(TAG, "onNext:  " + e.getMessage() );
                                            }
//                                      }
//                                });
                          }
                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getShop Listing  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                                try {

                                      if (empty[0]) {
                                            Utils.ShowView(binding.get().noStoresAvailable);
                                            Utils.RemoveView(binding.get().storesAvailableRecyclerView);
                                      } else {
                                            Utils.RemoveView(binding.get().noStoresAvailable);
                                            Utils.ShowView(binding.get().storesAvailableRecyclerView);
                                      }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }

                                disposeAPIService(disposable);

                                getAShopsNearYou();
                          }
                    });
      }

      private void getShopsByAddress(String address) {

            mAPIService.getShopsByAddress(address)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {
                                ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());

                                try {
                                      binding.get(). storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                      Sentry.captureException(e);
                                }

                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                      navigateToStore(obj);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG, e, "onError:  " + e.getMessage());

//                                reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
//                                disposeAPIService(disposable);

                                try {

//                                      Utils.RemoveView(binding.get().shopsProgressBar);
//                                      Utils.ShowView(binding.get().mallsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                      Sentry.captureException(e);
                                }
                          }
                    });
      }

      private void getAShopsNearYou() {

            final boolean[] empty = {false};

            String locationAddress = "";
            if (getAddress(getContext()) != null) {
                  locationAddress = getAddress(getContext()).getAddressLine(0);
            }

                  binding.get().locationText.setText(locationAddress);

                  mAPIService.getShops()
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<ShopsResponse>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) { disposable = d; }

                                @Override
                                public void onNext(@NonNull ShopsResponse shopsResponse) {

                                Context ctx = getContext();
                                List<Shop> closestShops = new ArrayList<>();

                                      appExecutors.getDiskIO().execute(new Runnable() {
                                            @Override
                                            public void run() {

                                                  try {

                                                        for (Shop shop : shopsResponse.getShop()) {
                                                              if (shop.getLat() != null || shop.getLng() != null) {
                                                                    String lat = shop.getLat();
                                                                    String lng = shop.getLng();

                                                                    if (LocationPreferences.isLocationLatLonAvailable(ctx)) {

                                                                          double latitude_dbl = Double.parseDouble(lat);
                                                                          double longitude_dbl = Double.parseDouble(lng);

                                                                          android.location.Location endLocation = new android.location.Location("New Location");
                                                                          endLocation.setLatitude(latitude_dbl);
                                                                          endLocation.setLongitude(longitude_dbl);

                                                                          double shopLocationDistanceToYou = LocationUtilService.getDistanceInKm(ctx, endLocation);

                                                                          //if shop's location distaance to you is less than or  equal to 10km

                                                                          if (shopLocationDistanceToYou <= Constants.DEFAULT_RADIUS_DISTANCE) {
                                                                                closestShops.add(shop);
                                                                          }
                                                                    }
                                                              }
                                                        }

                                                  } catch (Exception e) {
                                                        Sentry.captureException(e);
                                                  }
                                            }
                                      });

                                      mHandler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

//                                                  if (closestShops != null) {

                                                  try {
                                                        ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), closestShops);

                                                        if (closestShops.size() > 0) {
                                                              binding.get().storesNearRecyclerView.setAdapter(shopCardAdapter);
                                                        } else {
                                                              empty[0] = true;
                                                        }

                                                        shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                              navigateToStore(obj);
                                                        });
                                                  }catch (Exception e){
                                                        Sentry.captureException(e);
                                                  }
                                            }
                                      },3000);
                                }

                                @Override
                                public void onError(Throwable e) {
                                      logError(TAG, e, "onError:  " + e.getMessage());
                                      Sentry.captureException(e);
                                }

                                @Override
                                public void onComplete() {
                                      disposeAPIService(disposable);

                                      try {
                                            if (empty[0]){
                                                  Utils.ShowView(binding.get().noStoresInYourArea);
                                                  Utils.RemoveView(   binding.get().storesNearRecyclerView);
                                            }else {
                                                  Utils.RemoveView(binding.get().noStoresInYourArea);
                                                  Utils.ShowView(   binding.get().storesNearRecyclerView);
                                            }

                                      }catch (Exception e){
                                            Sentry.captureException(e);
                                      }

                                      getPreviouslyVisitedShops();
                                }
                          });

      }

      private void getPreviouslyVisitedShops() {

            if (getActivity() != null)

            shopsViewModel.getShops().observe(getActivity(), shopList -> {

                  try {

                        if (shopList != null) {

                              ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopList);
                              binding.get().previouslyVisitedRecyclerView.setAdapter(shopCardAdapter);

                              shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                    navigateToStore(obj);
                              });

                              ((ServicesActivity) getActivity()).progressDialog.hide();
                        }

                  }catch (Exception e){
                        Sentry.captureException(e);
                  }
            });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onSearchQuery(LocationSearchEvent event){
            getShopsByAddress(event.getTerm());
            Toast.makeText(getContext(), event.getTerm(), Toast.LENGTH_SHORT).show();
      }

      private Address getAddress(Context context) {
            List<Address> addresses = LocationUtilService.getYourAddress(context);
            if (addresses.size() > 0 ) {
                  return addresses.get(0);
            }

            return null;
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

}
