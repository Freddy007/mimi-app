package com.mimi.africa.ui.apploading;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;


import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentAppLoadingBinding;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.viewModels.apploading.AppLoadingViewModel;
import com.mimi.africa.viewModels.clearalldata.ClearAllDataViewModel;
import com.mimi.africa.viewObject.PSAppInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class AppLoadingFragment extends BaseFragment {


    //region Variables

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private PSDialogMsg psDialogMsg;
    private String startDate = Constant.ZERO;
    private String endDate = Constant.ZERO;

    private AppLoadingViewModel appLoadingViewModel;
    private ClearAllDataViewModel clearAllDataViewModel;

    @VisibleForTesting
    private AutoClearedValue<FragmentAppLoadingBinding> binding;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    //endregion Variables

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentAppLoadingBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_app_loading, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }


    @Override
    protected void initUIAndActions() {

        psDialogMsg = new PSDialogMsg(getActivity(), false);

    }

    @Override
    protected void initViewModels() {
        appLoadingViewModel = ViewModelProviders.of(this, viewModelFactory).get(AppLoadingViewModel.class);
        clearAllDataViewModel = ViewModelProviders.of(this, viewModelFactory).get(ClearAllDataViewModel.class);
    }

    @Override
    protected void initAdapters() {
    }

    @Override
    protected void initData() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    startActivity(new Intent(AppLoadingFragment.this.getActivity(), MainHomeActivity.class));
                        getActivity().finish();
                }
            }
        },3000);

//        appLoadingViewModel.getDeleteHistoryData().observe(this, result -> {
//
//            if (result != null) {
//                switch (result.status) {
//
//                    case SUCCESS:
//
//                        if (result.data != null) {
//                            appLoadingViewModel.psAppInfo = result.data;
//                            checkVersionNumber(result.data);
//                            startDate = endDate;
//                        }
//                        break;
//
//                    case ERROR:
//
//                        break;
//                }
//            }
//        });
    }

    private void checkForceUpdate(PSAppInfo psAppInfo) {
        if (psAppInfo.psAppVersion.versionForceUpdate.equals(Constant.ONE)) {

            pref.edit().putString(Constant.APPINFO_PREF_VERSION_NO, psAppInfo.psAppVersion.versionNo).apply();
            pref.edit().putBoolean(Constant.APPINFO_PREF_FORCE_UPDATE, true).apply();
            pref.edit().putString(Constant.APPINFO_FORCE_UPDATE_TITLE, psAppInfo.psAppVersion.versionTitle).apply();
            pref.edit().putString(Constant.APPINFO_FORCE_UPDATE_MSG, psAppInfo.psAppVersion.versionMessage).apply();

//            navigationController.navigateToForceUpdateActivity(this.getActivity(), psAppInfo.psAppVersion.versionTitle, psAppInfo.psAppVersion.versionMessage);

        } else if (psAppInfo.psAppVersion.versionForceUpdate.equals(Constants.ZERO)) {

            pref.edit().putBoolean(Constant.APPINFO_PREF_FORCE_UPDATE, false).apply();

            psDialogMsg.showAppInfoDialog(getString(R.string.update), getString(R.string.app__cancel), psAppInfo.psAppVersion.versionTitle, psAppInfo.psAppVersion.versionMessage);
            psDialogMsg.show();
            psDialogMsg.okButton.setOnClickListener(v -> {
                psDialogMsg.cancel();
                startActivity(new Intent(AppLoadingFragment.this.getActivity(), MainHomeActivity.class));
//                navigationController.navigateToMainActivity(AppLoadingFragment.this.getActivity());
//                navigationController.navigateToPlayStore(AppLoadingFragment.this.getActivity());
                if (getActivity() != null) {
                    getActivity().finish();
                }

            });

            psDialogMsg.cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    psDialogMsg.cancel();

                    startActivity(new Intent(AppLoadingFragment.this.getActivity(), MainHomeActivity.class));

//                    navigationController.navigateToMainActivity(AppLoadingFragment.this.getActivity());

                    if (AppLoadingFragment.this.getActivity() != null) {
                        AppLoadingFragment.this.getActivity().finish();
                    }
                }
            });

            psDialogMsg.getDialog().setCancelable(false);
        }else {
//            navigationController.navigateToMainActivity(AppLoadingFragment.this.getActivity());
            startActivity(new Intent(AppLoadingFragment.this.getActivity(), MainHomeActivity.class));

            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    private void checkVersionNumber(PSAppInfo psAppInfo) {
        if (!Config.APP_VERSION.equals(psAppInfo.psAppVersion.versionNo)) {

            if (psAppInfo.psAppVersion.versionNeedClearData.equals(Constant.ONE)) {
                psDialogMsg.cancel();
                clearAllDataViewModel.setDeleteAllDataObj();
            } else {
                checkForceUpdate(appLoadingViewModel.psAppInfo);
            }
        } else {
            pref.edit().putBoolean(Constant.APPINFO_PREF_FORCE_UPDATE, false).apply();
//            navigationController.navigateToMainActivity(AppLoadingFragment.this.getActivity());
            startActivity(new Intent(AppLoadingFragment.this.getActivity(), MainHomeActivity.class));

            if (getActivity() != null) {
                getActivity().finish();
            }
        }

    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CANADA);
        Date date = new Date();
        return dateFormat.format(date);
    }


}
