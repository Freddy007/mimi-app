package com.mimi.africa.ui.shop.version2;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.mimi.africa.R;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;

public class HealthShopHomeActivity extends BaseActivity {

      private Shop mShop;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_health_shop_home);

            if (getIntent() != null ){
                  mShop = getIntent().getParcelableExtra(Constants.SHOP_OBECT);
            }
            Toolbar toolbar =  findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(mShop.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this);

            initComponents();
      }

      private void initComponents(){

            if (mShop != null ) {
                  HealthShopFragment healthShopFragment = HealthShopFragment.newInstance(mShop);
                  setupFragment(healthShopFragment);
            }
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                  finish();
            } else {
                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }
}