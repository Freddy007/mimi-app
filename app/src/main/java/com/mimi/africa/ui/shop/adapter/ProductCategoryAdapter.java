package com.mimi.africa.ui.shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mimi.africa.Adapter.ProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.databinding.ItemProductCategoryBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.ui.shop.version2.adapter.DiscountListAdapter;
import com.mimi.africa.utils.Constants;

import java.util.List;

public class ProductCategoryAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

      private ItemProductCategoryBinding itemProductCategoryBinding;

      private List<ProductCategory> mItems;
      private Context mContext;

      public ProductCategoryAdapter(Context context, List<ProductCategory> productCategoryList) {
            mContext = context;
            mItems      = productCategoryList;
      }

      public class OriginalViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            public Button button;
            public RecyclerView productsRecyclerView;

            public OriginalViewHolder(@NonNull View v) {
                  super(v);
                  title = itemProductCategoryBinding.productCategoryName;
                  button = itemProductCategoryBinding.button;
                  productsRecyclerView = itemProductCategoryBinding.productsRecyclerView;
            }
      }

      @NonNull
      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;

            itemProductCategoryBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product_category, parent, false);
            final View itemView = itemProductCategoryBinding.getRoot();
            vh = new OriginalViewHolder(itemView);
            return vh;
      }

      @Override
      public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            if (holder instanceof OriginalViewHolder) {
                  ProductCategoryAdapter.OriginalViewHolder view = (OriginalViewHolder) holder;
                  ProductCategory productCategory = mItems.get(position);

                  if (productCategory.getListings() != null &&
                          productCategory.getListings().size() > 0) {

                        view.title.setVisibility(View.VISIBLE);
                        view.productsRecyclerView.setVisibility(View.VISIBLE);
                        view.title.setText(productCategory.getName());
                        DiscountListAdapter mProductsAdapter = new DiscountListAdapter(mContext, productCategory.getListings());
                        view.productsRecyclerView.setAdapter(mProductsAdapter);

                        mProductsAdapter.setOnItemClickListener((v, obj, pos) -> {

                              if (v == v.findViewById(R.id.nameTextView)) {

                                    Intent intent = new Intent(mContext, StoreHomeActivity.class);
                                    intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
                                    mContext.startActivity(intent);

                              } else if (v == v.findViewById(R.id.addressTextView)) {

//                                    mContext.startActivity(new Intent(mContext, LocationActivity.class));

                              } else if (v == v.findViewById(R.id.newsHolderCardView)|| v == v.findViewById(R.id.imageView)) {

                                    Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                                    intent.putExtra(Constants.INVENTORY_OBJECT, obj );
                                    mContext.startActivity(intent);
                              }
                        });

                  }else {
                        view.title.setVisibility(View.GONE);
                        view.productsRecyclerView.setVisibility(View.GONE);
                  }
            }
      }

      @Override
      public int getItemCount() {
            return mItems.size();
      }

//      private void processItemClick(View view, Inventory obj, int pos, @NonNull ListingResponse listingResponse) {
//            if (view == view.findViewById(R.id.news_title_textView)) {
//
//                  navigateToStore(obj);
//
//            } else if (view == view.findViewById(R.id.addressTextView)) {
//
//                  navigateToLocation(getActivity());
//
//            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {
//
//                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(pos));
//            }
//      }

}
