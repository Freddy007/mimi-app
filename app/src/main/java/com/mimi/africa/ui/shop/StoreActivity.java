package com.mimi.africa.ui.shop;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.HomeCategoryAdapter;
import com.mimi.africa.Adapter.ImageSliderAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ShopResponse;
import com.mimi.africa.databinding.ActivityStoreBinding;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.ReviewItemClickedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Image;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.auth.LoginActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.reviews.ReviewsFragment;
import com.mimi.africa.ui.shop.version2.StoreHomeFragment;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class StoreActivity extends BaseActivity {

    private static final String TAG = StoreActivity.class.getSimpleName();

    public ActivityStoreBinding activityStoreBinding;

    @Nullable
    private  Shop shop;
    private String mProductId;

      @Nullable
      private Runnable runnable = null;
      @NonNull
      private Handler handler = new Handler();
      private ShopProductGridCardAdapter mProductsAdapter;
      @Nullable
      private ImageSliderAdapter adapterImageSlider;
      @Nullable
      private HomeCategoryAdapter homeCategoryAdapter;

    @Inject
    public APIService mAPIService;

    private List<Feedback> mFeedBacks;

      private BottomSheetBehavior mBehavior;
      private BottomSheetDialog mBottomSheetDialog;
      private View bottom_sheet;
      private ImageButton imageButton;
      private List<Image> images;

      private Toolbar toolbar;

    public ProgressDialog progressDialog;

    @NonNull
      private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
              = item -> {

            switch (item.getItemId()) {

                  case R.id.navigation_visit:
                        Utils.getDirectionsOnGoogleMap(StoreActivity.this, shop.getLat(), shop.getLng());
                        return true;

                  case R.id.navigation_reviews:
                        if (mFeedBacks != null && mFeedBacks.size() >0 ){
                              ReviewsFragment reviewsFragment = ReviewsFragment.newInstance(mFeedBacks);
                              showDialog(reviewsFragment);
                              return false;
                        }else {
                              Utils.ShowSnackBar(StoreActivity.this,"This shop has no reviews!", Snackbar.LENGTH_SHORT);
                              return false;
                        }
            }

            return false;
      };


      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         activityStoreBinding = DataBindingUtil.setContentView(this, R.layout.activity_store);

        initToolbar();
        initComponent();

    }

    private void initToolbar() {
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initComponent() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

          bottom_sheet = findViewById(R.id.bottom_sheet);
          mBehavior = BottomSheetBehavior.from(bottom_sheet);
          images = new ArrayList<>();

          mBottomSheetDialog = new BottomSheetDialog(this);

          if (getIntent() != null ){
             shop = getIntent().getParcelableExtra(Constants.SHOP_OBECT);

               if (shop != null && shop.getFeedbacks() != null) {
                     mFeedBacks = shop.getFeedbacks();
               }

               if (getIntent().getStringExtra(Constants.PRODUCT_ID) != null){
                   mProductId = getIntent().getStringExtra(Constants.PRODUCT_ID);
             }

                if (shop != null ) {

                      if (shop.getName() != null) {
                            activityStoreBinding.toolbarTitle.setText(shop.getName());
                      }
                      getShop(shop.getSlug());

                      activityStoreBinding.visitButton.setOnClickListener(v -> {
                            Utils.getDirectionsOnGoogleMap(StoreActivity.this, shop.getLat(), shop.getLng());
                      });

                      activityStoreBinding.chat.setOnClickListener(v -> {
                            try {

                                  if (!Utils.isUserLoggedIn(getApplicationContext())) {
//                                        Utils.navigateToLogin(getApplicationContext());
                                         startActivity(new Intent(StoreActivity.this, LoginActivity.class));

                                  } else {
//                                        Utils.navigateToChat(getApplicationContext(), shop);
                                        Intent intent = new Intent(StoreActivity.this, MessageActivity.class);
                                        intent.putExtra(Constants.SHOP_NAME, shop.getName());
                                        intent.putExtra(Constants.SHOP_ID, shop.getId());
                                        startActivity(intent);
                                  }

                            } catch (Exception e) {
                                  Sentry.captureException(e);
                            }
                      });
                }

             }

          activityStoreBinding.shopBottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

          activityStoreBinding.reviewsButton.setOnClickListener(v -> {
                if (mFeedBacks != null && mFeedBacks.size() >0 ){
                      ReviewsFragment reviewsFragment = ReviewsFragment.newInstance(mFeedBacks);
                      showDialog(reviewsFragment);
                }else {
                      Utils.ShowSnackBar(StoreActivity.this,"This shop has no reviews!", Snackbar.LENGTH_SHORT);
                }
          });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_without_location, menu);
        return true;
    }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {

          switch (item.getItemId()){
                case android.R.id.home:
                      finish();
                      break;
                case R.id.action_search:
                      startActivity(new Intent(StoreActivity.this, MainSearchActivity.class));
                      break;

          }

            if (item.getItemId() == android.R.id.home) {
                  finish();
            }else if (item.getItemId() == R.id.action_cart){

            } else {
//                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }

    private void getShop(String slug){

        mAPIService.getShop(slug)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShopResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onNext(@NonNull ShopResponse shopResponse) {

                          StoreHomeFragment storeFragment = StoreHomeFragment.newInstance(shopResponse.getShop());
                          setupFragment(storeFragment);


                          images.add(shopResponse.getShop().getImage());

                          if (shopResponse.getShop().getImages() != null  && shopResponse.getShop().getImages().size() > 0){

                                      for (Image img : shopResponse.getShop().getImages()){
                                            Image newImg = new  Image();
                                            newImg.setPath(Constants.IMAGES_BASE_URL+img.getPath());
                                            images.add(newImg);
                                      }

                                      Image bannerImage = new Image();
                                      bannerImage.setPath(shopResponse.getShop().getBannerImage());

                                      images.add(bannerImage);

                                adapterImageSlider = new ImageSliderAdapter(StoreActivity.this, images);
                                activityStoreBinding.pager.setAdapter(adapterImageSlider);

                                // displaying selected locationImage first
                                activityStoreBinding.pager.setCurrentItem(0);
                                activityStoreBinding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                      @Override
                                      public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
                                      }

                                      @Override
                                      public void onPageSelected(int pos) {
                                            Utils.addBottomDots(    activityStoreBinding.layoutDots, adapterImageSlider.getCount(), pos, getApplicationContext());
                                      }

                                      @Override
                                      public void onPageScrollStateChanged(int state) {
                                      }
                                });

                                startAutoSlider(adapterImageSlider.getCount());
                          }

                          activityStoreBinding.toolbarTitle.setText(shopResponse.getShop().getName());

                          if (getSupportActionBar() != null)
                               getSupportActionBar().setTitle(shopResponse.getShop().getName());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "onError: getShop  "  + e.getMessage() );
                    }

                    @Override
                    public void onComplete() {
                                activityStoreBinding.toolbarProgressBar.setVisibility(View.GONE);
                    }
                });
    }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(AddToCart event) {}

      @Override
      protected void onResumeFragments() {
            super.onResumeFragments();
      }

      @Override
      protected void onResume() {
            super.onResume();
      }

      private void showBottomSheetDialog(final Feedback feedback) {
            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                  mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);
            if (feedback.getCustomer() != null) {
                  ((TextView) view.findViewById(R.id.name)).setText(feedback.getCustomer().getName());
            }

            ((TextView) view.findViewById(R.id.address)).setText(feedback.getComment());
            (view.findViewById(R.id.bt_close)).setOnClickListener(view1 -> mBottomSheetDialog.dismiss());

            mBottomSheetDialog.setContentView(view);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }

            mBottomSheetDialog.show();
            mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                  @Override
                  public void onDismiss(DialogInterface dialog) {
                        mBottomSheetDialog = null;
                  }
            });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onReviewItemClicked(ReviewItemClickedEvent event){
            if (!mBottomSheetDialog.isShowing()){
                  showBottomSheetDialog(event.getFeedback());
            }
      }

      private void startAutoSlider(final int count) {
            runnable = () -> {
                  int pos =activityStoreBinding.pager.getCurrentItem();
                  pos = pos + 1;
                  if (pos >= count) pos = 0;
                  activityStoreBinding.pager.setCurrentItem(pos);
                  handler.postDelayed(runnable, 3000);
            };
            handler.postDelayed(runnable, 3000);
      }

}