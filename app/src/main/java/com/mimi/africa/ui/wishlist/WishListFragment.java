package com.mimi.africa.ui.wishlist;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentWishListBinding;
import com.mimi.africa.ui.common.BaseDialogFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;

public class WishListFragment extends BaseDialogFragment {

      private static final String TAG = WishListFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private  View parentView;
      private AutoClearedValue<FragmentWishListBinding> binding;
      private APIService mAPIService;



      public WishListFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {


            // Inflate the layout for this fragment
            FragmentWishListBinding fragmentWishListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_wish_list, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentWishListBinding);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }
            return fragmentWishListBinding.getRoot();

      }

      @Override
      protected void initUIAndActions() {
            binding.get().btClose.setOnClickListener(v -> dismiss());
      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {

      }
}
