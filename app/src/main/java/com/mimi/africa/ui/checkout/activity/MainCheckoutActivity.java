package com.mimi.africa.ui.checkout.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RaveUiManager;
import com.flutterwave.raveandroid.rave_java_commons.RaveConstants;
import com.flutterwave.raveandroid.rave_presentation.RavePayManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.databinding.ActivityCheckoutBinding;
import com.mimi.africa.event.SendMessageEvent;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.PaymentStatus;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.checkout.CheckoutFragment1;
import com.mimi.africa.ui.checkout.CheckoutFragment2;
import com.mimi.africa.ui.checkout.CheckoutFragment3;
import com.mimi.africa.ui.checkout.CheckoutStatusFragment;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.payment.FlutterPayActivity;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewObject.TransactionObject;
import com.mimi.africa.viewObject.holder.TransactionValueHolder;


import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class MainCheckoutActivity extends BaseActivity {

    private static final String TAG = MainCheckoutActivity.class.getSimpleName();
    public int number = 1;
    private int maxNumber = 4;
    public User user;
    BaseFragment fragment;
    public ActivityCheckoutBinding binding;
    public ProgressDialog progressDialog;
    private PSDialogMsg psDialogMsg;
    public TransactionValueHolder transactionValueHolder;
    public TransactionObject transactionObject;


    protected APIService mAPIService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_checkout);
        mAPIService = Constants.getRetrofit("https://api.flutterwave.com/",null).create(APIService.class);

        // Init all UI
        initUI(binding);

        progressDialog = new ProgressDialog(this);

        progressDialog.setCancelable(false);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE__MAIN_ACTIVITY
                && resultCode == Constant.RESULT_CODE__RESTART_MAIN_ACTIVITY) {

            finish();
            startActivity(new Intent(this, MainHomeActivity.class));

        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }

        /*
         *  We advise you to do a further verification of transaction's details on your server to be
         *  sure everything checks out before providing service or goods.
         */
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Toast.makeText(this, "SUCCESS " + message, Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RavePayActivity.RESULT_ERROR) {
                Toast.makeText(this, "ERROR " + message, Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                Toast.makeText(this, "CANCELLED " + message, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initUI(ActivityCheckoutBinding binding) {

        transactionValueHolder = new TransactionValueHolder();

        psDialogMsg = new PSDialogMsg(this, false);

        // click close locationImage button
        binding.closeImageButton.setOnClickListener(view -> {

            finish();
//
//            if (getCurrentUser() != null){
//                Log.i(TAG, "initUI:  loginUserId " + getCurrentUser().userId);
//                startActivity(new Intent(MainCheckoutActivity.this, BasketListActivity.class));
//            }

        });

        // fragment1 default initialize
        navigateFragment(binding, number);

        binding.nextButton.setOnClickListener(view -> {
            if (number < maxNumber) {
                number++;
                if (number == 3) {

                    navigateFragment(binding, number);

                } else if (number == 2) {

                        if (((CheckoutFragment1)fragment).checkShippingAddressEditTextIsEmpty()) {
                        psDialogMsg.showErrorDialog(getString(R.string.shipping_address_one_error_message), getString(R.string.app__ok));
                        psDialogMsg.show();
                        number--;
                    } else if (((CheckoutFragment1)fragment).checkBillingAddressEditTextIsEmpty()) {
                        psDialogMsg.showErrorDialog(getString(R.string.billing_address_one_error_message), getString(R.string.app__ok));
                        psDialogMsg.show();
                        number--;
                    } else if (((CheckoutFragment1)fragment).checkUserEmailEditTextIsEmpty()) {
                        psDialogMsg.showErrorDialog(getString(R.string.checkout__user_email_empty), getString(R.string.app__ok));
                        psDialogMsg.show();
                        number--;
                    }
                    else {
                        ((CheckoutFragment1) fragment).updateUserProfile();
                    }


                } else if (number == 4) {

                    if (((CheckoutFragment3) fragment).binding.get().checkBox.isChecked()) {

                        number--;

                        if (((CheckoutFragment3) fragment).clicked) {

                            psDialogMsg.showConfirmDialog(getString(R.string.confirm_to_Buy), getString(R.string.app__ok), getString(R.string.app__cancel));
                            psDialogMsg.show();

                            psDialogMsg.okButton.setOnClickListener(v -> {

                                psDialogMsg.cancel();

                                switch (((CheckoutFragment3) fragment).paymentMethod) {

                                    case Constants.PAYMENT_CASH_ON_DELIVERY:

                                        ((CheckoutFragment3) fragment).sendData();

                                        break;

                                    case Constants.PAYMENT_MOMO:
//                                        processMomoPayments();
                                        break;

                                    case Constants.PAYMENT_BANK:
                                        ((CheckoutFragment3) fragment).sendData();
                                        break;

                                    default:
                                    psDialogMsg.showErrorDialog(getString(R.string.checkout__choose_a_method), getString(R.string.app__ok));
                                    psDialogMsg.show();
                                }

                            });

                            psDialogMsg.cancelButton.setOnClickListener(v -> psDialogMsg.cancel());
                        } else {

                            psDialogMsg.showErrorDialog(getString(R.string.checkout__choose_a_method), getString(R.string.app__ok));
                            psDialogMsg.show();
                        }

                    } else {

                        number--;

                        psDialogMsg.showInfoDialog(getString(R.string.not_checked), getString(R.string.app__ok));
                        psDialogMsg.show();

                    }

                } else {

                    navigateFragment(binding, number);
                }

            }
        });

        binding.prevButton.setOnClickListener(view -> {
            if (number > 1) {
                number--;
                binding.shippingImageView.setImageResource(R.drawable.baseline_circle_line_uncheck_24);
                binding.paymentImageView.setImageResource(R.drawable.baseline_circle_black_uncheck_24);
                navigateFragment(binding, number);
            }

        });

//        binding.keepShoppingButton.setOnClickListener(v -> {
//
//            navigationController.navigateBackToBasketActivity(CheckoutActivity.this);
//            CheckoutActivity.this.finish();
//
//        });

    }

    public void navigateFragment(ActivityCheckoutBinding binding, int position) {
        updateCheckoutUI(binding);

        if (position == 1) {

            CheckoutFragment1 fragment = new CheckoutFragment1();
            this.fragment = fragment;
            setupFragment(fragment);

        } else if (position == 2) {

            CheckoutFragment2 fragment = new CheckoutFragment2();
            this.fragment = fragment;
            setupFragment(fragment);

        } else if (position == 3) {

            CheckoutFragment3 fragment = new CheckoutFragment3();
            this.fragment = fragment;
            setupFragment(fragment);

        } else if (position == 4) {
            setupFragment(new CheckoutStatusFragment());
        }
    }

    private void updateCheckoutUI(ActivityCheckoutBinding binding) {
        if (number == 1) {
            binding.nextButton.setVisibility(View.VISIBLE);
            binding.prevButton.setVisibility(View.GONE);
            binding.keepShoppingButton.setVisibility(View.GONE);
            binding.step2View.setBackgroundColor(getResources().getColor(R.color.md_grey_300));
            binding.step3View.setBackgroundColor(getResources().getColor(R.color.md_grey_300));
            binding.nextButton.setText(R.string.checkout__next);

        } else if (number == 2) {

            binding.nextButton.setVisibility(View.VISIBLE);
            binding.prevButton.setVisibility(View.VISIBLE);
            binding.step2View.setBackgroundColor(getResources().getColor(R.color.global__primary));
            binding.step3View.setBackgroundColor(getResources().getColor(R.color.md_grey_300));
            binding.keepShoppingButton.setVisibility(View.GONE);
            binding.paymentImageView.setImageResource(R.drawable.baseline_circle_line_uncheck_24);
            binding.shippingImageView.setImageResource(R.drawable.baseline_circle_line_check_24);

            binding.nextButton.setText(R.string.checkout__next);
            binding.prevButton.setText(R.string.back);

        } else if (number == 3) {
            binding.nextButton.setVisibility(View.VISIBLE);
            binding.prevButton.setVisibility(View.VISIBLE);
            binding.keepShoppingButton.setVisibility(View.GONE);
            binding.step3View.setBackgroundColor(getResources().getColor(R.color.global__primary));
            binding.paymentImageView.setImageResource(R.drawable.baseline_circle_line_check_24);
            binding.successImageView.setImageResource(R.drawable.baseline_circle_line_uncheck_24);

            binding.nextButton.setText(R.string.checkout);
            binding.prevButton.setText(R.string.back);
        } else if (number == 4) {
            binding.constraintLayout25.setVisibility(View.GONE);
            binding.nextButton.setVisibility(View.GONE);
            binding.prevButton.setVisibility(View.GONE);
            binding.keepShoppingButton.setVisibility(View.VISIBLE);
            binding.paymentImageView.setImageResource(R.drawable.baseline_circle_line_check_24);
            binding.successImageView.setImageResource(R.drawable.baseline_circle_line_check_24);
        }

    }

    public void setCurrentUser(User user) {
        this.user = user;
    }

    public User getCurrentUser() {
        return this.user;
    }

    public void goToFragment4() {
        navigateFragment(binding, 4);
        number = 4;
    }

    @Override
    public void onBackPressed() {}

    private void processMomoPayments(){

        sendPaymentDetails();

//        new RaveUiManager(this)
//                .setAmount(2)
//                .setCurrency("GHS")
//                .setEmail("f.ankamah988@gmail.com")
//                .setfName("Frederick")
//                .setlName("Ankamah")
//                .setNarration("Payments on Mimi GH")
////                .setPublicKey("FLWPUBK-d114fa66e74b45a093e7e101956187fa-X")
//                .setPublicKey("FLWPUBK_TEST-1c797580b156ea18fba9db4ed5af997e-X")
////                .setEncryptionKey("fa052b2c88cc7c6523ad349b")
//                .setEncryptionKey("FLWSECK_TESTa11b9d0d9cb7")
//                .setTxRef("Payments01")
//                .setPhoneNumber("0242953672", true)
//                    .acceptAccountPayments(true)
//                    .acceptCardPayments(false)
//                    .acceptMpesaPayments(false)
//                    .acceptAchPayments(false)
//                    .acceptGHMobileMoneyPayments(true)
//                    .acceptUgMobileMoneyPayments(false)
//                    .acceptZmMobileMoneyPayments(false)
//                    .acceptRwfMobileMoneyPayments(false)
//                    .acceptSaBankPayments(false)
//                    .acceptUkPayments(false)
//                    .acceptBankTransferPayments(false)
//                    .acceptUssdPayments(false)
//                    .acceptBarterPayments(false)
//                    .acceptFrancMobileMoneyPayments(false)
//                    .allowSaveCardFeature(false)
//                    .onStagingEnv(true)
////                    .setMeta(List<Meta>)
//                .withTheme(R.style.AppTheme)
//                .isPreAuth(true)
//                .shouldDisplayFee(true)
//                    .initialize();
    }

    private void sendPaymentRequest(RequestBody requestBody){

        String secret = " Bearer FLWSECK_TEST-04ebcad6562c5279fc516e7e1d868c0c-X";
        mAPIService.sendPaymentRequest(secret,requestBody )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PaymentStatus>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) { disposable = d; }

                    @Override
                    public void onNext(PaymentStatus paymentStatus) {
                        Log.i(TAG, "onNext:  " + paymentStatus.getMessage());
                        Log.i(TAG, "onNext:  " + paymentStatus.getData().getLink());
                        Intent intent = new Intent(MainCheckoutActivity.this, FlutterPayActivity.class);
                        intent.putExtra("payment_url", paymentStatus.getData().getLink());
                        startActivity(intent);
//                        Utils.ShowSnackBar(MainCheckoutActivity.this, paymentStatus.getData().getLink(), Snackbar.LENGTH_LONG );
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
//                        Utils.ShowSnackBar(MainCheckoutActivity.this, "Worked!", Snackbar.LENGTH_LONG);
                    }
                });
    }


    private void sendPaymentDetails(){

        try {
            MediaType mediaType = MediaType.parse("application/json");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("tx_ref", UUID.randomUUID()+ "mimi");
            jsonObject.addProperty("amount", "100");
            jsonObject.addProperty("currency", "GHS");
            jsonObject.addProperty("redirect_url", "https://webhook.site/9d0b00ba-9a69-44fa-a43d-a82c33c36fdc");
            jsonObject.addProperty("payment_options", "mobilemoney");
            JsonObject customerJsonObject = new JsonObject();
            customerJsonObject.addProperty("email", "frederickankamah988@gmail.com");
            customerJsonObject.addProperty("phonenumber", "0242953672");
            customerJsonObject.addProperty("name", "Frederick Ankamah");
            jsonObject.add("customer",  customerJsonObject);
            RequestBody requestBody = RequestBody.create(mediaType, jsonObject.toString());
            sendPaymentRequest(requestBody);

        }catch (Exception e){
            Sentry.captureException(e);
            Log.i(TAG, "sendPaymentDetails:  " + e.getMessage());
//            Utils.ShowSnackBar(mActivity, "Failed to send attachment!", Snackbar.LENGTH_LONG);
        }
    }


}
