package com.mimi.africa.ui.health;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mimi.africa.R;
import com.mimi.africa.ui.common.BaseFragment;


public class HealthFragment extends BaseFragment {


      public HealthFragment() {
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_health, container, false);
      }

      @Override
      protected void initUIAndActions() {

      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {

      }
}
