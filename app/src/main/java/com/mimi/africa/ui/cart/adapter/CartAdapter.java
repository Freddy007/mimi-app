package com.mimi.africa.ui.cart.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.ShopProduct;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CartAdapter.class.getSimpleName();

    private List<Inventory> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public CartAdapter(Context context, List<Inventory> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView price;
        public TextView qtyTextView;
        public ImageView itemImageView;
        public TextView dateCreatedTextView;
        public ImageButton more;
        public View lyt_parent;
        public ImageButton removeButton;
        public ImageButton addButton;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
//            locationImage = v.findViewById(R.id.locationImage);
            title = v.findViewById(R.id.nameTextView);
//            dateCreatedTextView = v.findViewById(R.id.dateTextView);
            price = v.findViewById(R.id.priceTextView);
            qtyTextView = v.findViewById(R.id.quantityTextView);
            itemImageView = v.findViewById(R.id.itemImageView);
            removeButton = v.findViewById(R.id.remove_item);
            addButton = v.findViewById(R.id.add_item);

//            more = v.findViewById(R.id.more);
//            lyt_parent = v.findViewById(R.id.lyt_parent);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_card, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final Inventory p = items.get(position);
            double unitPrice = 0;
            if (p.getPivot() != null) {
                if (p.getPivot().getUnitPrice() != null) {
                    unitPrice = Double.valueOf(p.getPivot().getUnitPrice());
                }
            }

            view.title.setText(p.getTitle());
            view.price.setText(String.format("GHS %s", Utils.TwoDecimalPlace(unitPrice)));
            view.qtyTextView.setText(String.valueOf(p.getPivot().getQuantity()));

            if (p.getImage() != null){
                String imageUrl = Constants.IMAGES_BASE_URL + p.getImage().getPath();

                try {

                    Utils.LoadImage(ctx, view.itemImageView,imageUrl);

                }catch (Exception e){
                    Log.e(TAG, "onBindViewHolder:  "  + e.getMessage() );
                }
            }

            view.removeButton.setOnClickListener(v -> {
                mOnItemClickListener.onItemClick(view.removeButton, p, position);

                    int qty = Integer.parseInt(view.qtyTextView.getText().toString());
                    if (qty > 1) {
                        qty--;
                        view.qtyTextView.setText(String.format("%d", qty));
                    }
                });

                view.addButton.setOnClickListener(v -> {
                    int qty = Integer.parseInt(view.qtyTextView.getText().toString());
                    if (qty < 100) {
                        qty++;
                        view.qtyTextView.setText(String.format("%d", qty));
                    }
                });
        }
    }

//    private void onMoreButtonClick(final View view, final ShopProduct p) {
//        PopupMenu popupMenu = new PopupMenu(ctx, view);
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                onMoreButtonClickListener.onItemClick(view, p, item);
//                return true;
//            }
//        });
//        popupMenu.inflate(R.menu.menu_product_more);
//        popupMenu.show();
//    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Inventory obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, ShopProduct obj, MenuItem item);
    }

}