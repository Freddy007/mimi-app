package com.mimi.africa.ui.shop.version2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.databinding.DataBindingUtil;
import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemHomeCategoryIconListBinding;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.ui.common.DataBoundViewHolder;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.Objects;


public class CategoryIconListAdapter extends DataBoundListAdapter<ProductCategory, ItemHomeCategoryIconListBinding> {

    private final androidx.databinding.DataBindingComponent dataBindingComponent;
    private final CategoryClickCallback callback;
    private DataBoundListAdapter.DiffUtilDispatchedInterface diffUtilDispatchedInterface;
    private int lastPosition = -1;


    public CategoryIconListAdapter(androidx.databinding.DataBindingComponent dataBindingComponent,
                           CategoryClickCallback callback,
                           DiffUtilDispatchedInterface diffUtilDispatchedInterface) {
        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
        this.diffUtilDispatchedInterface = diffUtilDispatchedInterface;
    }

    @Override
    protected ItemHomeCategoryIconListBinding createBinding(ViewGroup parent) {
        ItemHomeCategoryIconListBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_home_category_icon_list, parent, false,
                        dataBindingComponent);
        binding.getRoot().setOnClickListener(v -> {
            ProductCategory category = binding.getCat();
            if (category != null && callback != null) {
                callback.onClick(category);
            }
        });
        return binding;
    }

    @Override
    public void bindView(DataBoundViewHolder<ItemHomeCategoryIconListBinding> holder, int position) {
        super.bindView(holder, position);

//

        setAnimation(holder.itemView, position);
    }

    @Override
    protected void dispatched() {
        if (diffUtilDispatchedInterface != null) {
            diffUtilDispatchedInterface.onDispatched();
        }
    }

    @Override
    protected void bind(ItemHomeCategoryIconListBinding binding, ProductCategory item) {
        binding.setCat(item);

//        if (item.getImage() != null) {
//            String url = Constants.IMAGES_BASE_URL +item.getImage().getPath();
//            Picasso.get().load(url).transform(new CircleTransform()).into(binding.iconImageView);
//////                Picasso.with(ctx).load(url).transform(new CircleTransform()).into(view.image);
//        }
    }

    @Override
    protected boolean areItemsTheSame(ProductCategory oldItem, ProductCategory newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId())
                && oldItem.getName().equals(newItem.getName());
    }

    @Override
    protected boolean areContentsTheSame(ProductCategory oldItem, ProductCategory newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId())
                && oldItem.getName().equals(newItem.getName());
    }

    public interface CategoryClickCallback {
        void onClick(ProductCategory category);
    }


    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else {
            lastPosition = position;
        }
    }
}
