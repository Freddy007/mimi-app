package com.mimi.africa.ui.reviews;


import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mimi.africa.Adapter.FeedbackAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.event.ReviewItemClickedEvent;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.ui.common.BaseDialogFragment;
import com.mimi.africa.utils.LineItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ReviewsFragment extends BaseDialogFragment {

      private static final String TAG = ReviewsFragment.class.getSimpleName();

      private RecyclerView recyclerView;
      private FeedbackAdapter feedbackAdapter;
      private ActionModeCallback actionModeCallback;
      private ActionMode actionMode;
      private View view;
      List<Feedback> feedbackList;
      private AppCompatImageButton closeReviewsImageButton;
      private TextView reviewsCountTextView;
      private ImageButton addReviewButton;

      @Inject
      APIService mAPIService;


      public ReviewsFragment() {
            // Required empty public constructor
      }

      public static ReviewsFragment newInstance(List<Feedback> feedbackList){

            ReviewsFragment reviewsFragment = new ReviewsFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("FeedBackList", (ArrayList<? extends Parcelable>) feedbackList);
            reviewsFragment.setArguments(bundle);

            return reviewsFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null){
                  feedbackList = getArguments().getParcelableArrayList("FeedBackList");
                  if (feedbackList != null) {
                        Log.i(TAG, "onCreate:  " + feedbackList.toString());
                  }
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.fragment_reviews, container, false);

            return view;
      }

      @Override
      protected void initUIAndActions() {
            closeReviewsImageButton = view.findViewById(R.id.close_reviews_btn);
            recyclerView =  view.findViewById(R.id.recyclerView);
            reviewsCountTextView = view.findViewById(R.id.reviews_count);
            addReviewButton = view.findViewById(R.id.add_review_btn);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new LineItemDecoration(getContext(), LinearLayout.VERTICAL));
            recyclerView.setHasFixedSize(true);

            reviewsCountTextView.setText(String.format("User Reviews ( %d )", feedbackList.size()));

            feedbackAdapter = new FeedbackAdapter(getContext(), feedbackList);
            recyclerView.setAdapter(feedbackAdapter);

            feedbackAdapter.setOnClickListener(new FeedbackAdapter.OnClickListener() {
                  @Override
                  public void onItemClick(View view, Feedback obj, int pos) {

                        EventBus.getDefault().post(new ReviewItemClickedEvent(obj));
                  }

                  @Override
                  public void onItemLongClick(View view, Feedback obj, int pos) {
                        enableActionMode(pos);
                  }
            });

            closeReviewsImageButton.setOnClickListener(v -> {
                  dismiss();
            });

            actionModeCallback = new ActionModeCallback();

            addReviewButton.setOnClickListener(v -> {
                  Feedback feedback = new Feedback();
                  EventBus.getDefault().post(new ReviewItemClickedEvent(feedback));
            });
      }

      private void enableActionMode(int position) {
            toggleSelection(position);
      }

      private void toggleSelection(int position) {
            feedbackAdapter.toggleSelection(position);
            int count = feedbackAdapter.getSelectedItemCount();

            if (count == 0) {
                  actionMode.finish();
            } else {
                  actionMode.setTitle(String.valueOf(count));
                  actionMode.invalidate();
            }
      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {}

      private class ActionModeCallback implements ActionMode.Callback {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                  mode.getMenuInflater().inflate(R.menu.menu_delete, menu);
                  return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                  return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                  int id = item.getItemId();
                  if (id == R.id.action_delete) {
                        mode.finish();
                        return true;
                  }
                  return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                  feedbackAdapter.clearSelections();
                  actionMode = null;
//                  Tools.setSystemBarColor(getActivity(), getResources().getColor(R.color.red_600));
            }
      }


}
