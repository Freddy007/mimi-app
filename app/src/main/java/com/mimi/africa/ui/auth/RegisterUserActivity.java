package com.mimi.africa.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.mimi.africa.R;
import com.mimi.africa.api.AuthenticationAPIInterface;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.model.Authorization;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.AuthenticationUtil;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Session;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserActivity extends BaseActivity implements Session {

    private static final String TAG = RegisterUserActivity.class.getSimpleName();

    @Inject
    protected Connectivity connectivity;

    private ProgressBar progress_bar;
    private FloatingActionButton signUpFab;
    private View parent_view;
    private TextInputEditText editTextName;
    private TextInputEditText editTextEmail;
    private TextInputEditText editTextPassword;
    private LinearLayout loginButton;
    private AuthenticationAPIInterface authenticationAPIInterface;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        authenticationAPIInterface = Constants.getRetrofit(Constants.AUTHENTICATION_URL, null).create(AuthenticationAPIInterface.class);
        session = AuthenticationUtil.getSession(this);

        initViews();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginIntent);
            }
        });

        signUpFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AuthenticationUtil.hideKeyboard(RegisterUserActivity.this);
                registerAction();
            }
        });
    }

    private void registerAction() {
        progress_bar.setVisibility(View.VISIBLE);
        signUpFab.setAlpha(0f);

            String name      = editTextName.getText().toString().trim();
            String email     = editTextEmail.getText().toString().trim();
            String password  = editTextPassword.getText().toString().trim();
            if (!AuthenticationUtil.isEmpty(email) && !AuthenticationUtil.isEmpty(password)) {
                if (AuthenticationUtil.aBoolean && AuthenticationUtil.aBoolean1 && AuthenticationUtil.aBoolean2 && AuthenticationUtil.emailValidity(email)) {

                     registerNewAccountWithEmail(name, email, password, getApplicationContext());

                } else {
                    dismissProgressBar();
//                    Toast.makeText(getApplicationContext(), getString(R.string.check_input_field), Toast.LENGTH_LONG).show();
                }

            } else {
                dismissProgressBar();
//                Toast.makeText(getApplicationContext(), getString(R.string.fiil_field), Toast.LENGTH_LONG).show();
            }
    }

    private void initViews() {
        parent_view      = findViewById(android.R.id.content);
        progress_bar     = findViewById(R.id.progress_bar);
        signUpFab        = findViewById(R.id.signup_fab);
        editTextName     = findViewById(R.id.edit_text_name);
        editTextEmail    = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        loginButton = findViewById(R.id.login_button);

        TextView textView =  findViewById(R.id.login_text);
        SpannableString content = new SpannableString("Log In");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);

        AuthenticationUtil.checkFieldValidity(editTextEmail, editTextPassword, editTextName, this);

    }


    public void registerNewAccountWithEmail(final String name, final String email, final String password, final Context context) {
        if (connectivity.isConnected()) {

            Call<List<Authorization>> call = authenticationAPIInterface.registerAccount(name, email, password, password);

            call.enqueue(new Callback<List<Authorization>>() {
                @Override
                public void onResponse(Call<List<Authorization>> call, Response<List<Authorization>> response) {

                      dismissProgressBar();

                    if (response.isSuccessful()){

                        AuthenticationUtil.processLogin(response, email, password, session,RegisterUserActivity.this);

                    }else if (response.code() == 401 ){

                        Toast.makeText(RegisterUserActivity.this, "Required missing fields!", Toast.LENGTH_SHORT).show();

                    }else if (response.code() == 500 ){

                        Toast.makeText(RegisterUserActivity.this, "Account with that email already exists!", Toast.LENGTH_SHORT).show();

                    }else if (response.code() == 404){
                        Toast.makeText(RegisterUserActivity.this, "There was a problem creating an account!", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<List<Authorization>> call, Throwable t) {
                    Log.e(RegisterUserActivity.class.getSimpleName(), "onResponse: Failed: " + t.getMessage());

                }
            });

        } else {
            Toast.makeText(context, "Please check internet connection!", Toast.LENGTH_SHORT).show();
        }
    }


    private void dismissProgressBar() {
        progress_bar.setVisibility(View.GONE);
        signUpFab.setAlpha(1f);
    }

    @Override
    public void setLogin(boolean status) {

    }

    @Override
    public boolean isLoggedIn() {
        return false;
    }

    @Override
    public void saveToken(String token) {

    }

    @Nullable
    @Override
    public String getToken() {
        return null;
    }

    @Override
    public void saveUserId(String userId) {

    }

    @Nullable
    @Override
    public String getUserId() {
        return null;
    }

    @Override
    public void saveName(String name) {

    }

    @Nullable
    @Override
    public String getName() {
        return null;
    }

    @Override
    public void saveEmail(String email) {

    }

    @Nullable
    @Override
    public String getEmail() {
        return null;
    }

    @Override
    public void savePassword(String password) {

    }

    @Nullable
    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void saveOnBoardingStatus(boolean status) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDemo(DemoEvent ev){

    }
}
