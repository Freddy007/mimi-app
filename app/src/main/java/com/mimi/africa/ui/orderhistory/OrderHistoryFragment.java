package com.mimi.africa.ui.orderhistory;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.OrderHistoryAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.OrderBatchHistoryResponse;
import com.mimi.africa.api.response.OrderHistoryResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentOrderHistoryBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.orderhistory.adapter.OrderBatchHistoryAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class OrderHistoryFragment extends BaseFragment {

      private static final String TAG = OrderHistoryFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

      private AutoClearedValue<FragmentOrderHistoryBinding> binding;

      @Inject
      APIService mAPIService;

      public OrderHistoryFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            FragmentOrderHistoryBinding fragmentOrderHistoryBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_order_history, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentOrderHistoryBinding);

            return fragmentOrderHistoryBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            initComponent();
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {
            String userId = CustomSharedPrefs.getUserID(getContext());
            String customer_id = loginUserId;
//            getOrderHistoryData(userId);
            getOrderHistory(customer_id);
      }

      private void initComponent() {}

      public boolean toggleArrow(@NonNull View view) {
            if (view.getRotation() == 0) {
                  view.animate().setDuration(200).rotation(180);
                  return true;
            } else {
                  view.animate().setDuration(200).rotation(0);
                  return false;
            }
      }

      private void getOrderHistoryData(String customer_id){

            mAPIService.getOrderHistory(customer_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<OrderHistoryResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull OrderHistoryResponse orderHistoryResponse) {
                                try {
                                      binding.get().orderHistoryRecyclerView
                                              .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                      OrderHistoryAdapter orderHistoryAdapter = new OrderHistoryAdapter(getContext(), orderHistoryResponse.getOrders());
                                      binding.get().orderHistoryRecyclerView.setAdapter(orderHistoryAdapter);

                                      orderHistoryAdapter.setOnItemClickListener((view, obj, pos) -> {
//                                            Toast.makeText(getContext(), "Worked!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getContext(), InvoiceActivity.class);
                                            intent.putExtra("order", obj);
                                            startActivity(intent);
                                      });

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "Order History onError:  "  + e.getMessage() );
                                try {

                                      Utils.RemoveView(binding.get().progressBar);
                                      Utils.ShowSnackBar(getActivity(), "Failed to load order history!", Snackbar.LENGTH_LONG);
                                }catch (Exception ex){
                                      Log.e(TAG, "onError:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onComplete() {
                                try {

                                      Utils.RemoveView(binding.get().progressBar);
                                      Utils.ShowView(binding.get().orderHistoryRecyclerView);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete: " + e.getMessage() );
                                }
                          }
                    });
      }

      private void getOrderHistory(String customer_id){

            final boolean[] empty = {false};

            mAPIService.getOrderBatchHistory(customer_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<OrderBatchHistoryResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                binding.get().setLoadingMore(true);

                          }

                          @Override
                          public void onNext(@NonNull OrderBatchHistoryResponse orderHistoryResponse) {
                                try {

                                      if (orderHistoryResponse.getOrders().size() >0 ) {

                                            Log.i(TAG, "onNext:  Order History " + orderHistoryResponse.getOrders());
                                            binding.get().orderHistoryRecyclerView
                                                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            OrderBatchHistoryAdapter orderHistoryAdapter = new OrderBatchHistoryAdapter(getContext(), orderHistoryResponse.getOrders());
                                            binding.get().orderHistoryRecyclerView.setAdapter(orderHistoryAdapter);

                                            orderHistoryAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                  Intent intent = new Intent(getContext(), InvoiceActivity.class);
                                                  intent.putExtra("order", obj);
                                                  startActivity(intent);
                                            });
                                      }else {
                                            empty[0] = true;
                                      }

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "Order History onError:  "  + e.getMessage() );
                                try {

                                      Utils.RemoveView(binding.get().progressBar);
                                      Utils.ShowSnackBar(getActivity(), "Failed to load order history!", Snackbar.LENGTH_LONG);
                                }catch (Exception ex){
                                      Log.e(TAG, "onError:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onComplete() {
                                try {

                                      if (!empty[0]) {

                                            Utils.RemoveView(binding.get().progressBar);
                                            Utils.ShowView(binding.get().orderHistoryRecyclerView);
                                            Utils.RemoveView(binding.get().noOrdersLinearLayout);
                                      }else {
                                            Utils.RemoveView(binding.get().progressBar);
                                            Utils.RemoveView(binding.get().orderHistoryRecyclerView);
                                            Utils.ShowView(binding.get().noOrdersLinearLayout);
                                      }
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete: " + e.getMessage() );
                                }
                                binding.get().setLoadingMore(false);

                          }
                    });
      }

}
