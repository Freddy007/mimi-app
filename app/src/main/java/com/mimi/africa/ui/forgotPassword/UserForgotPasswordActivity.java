package com.mimi.africa.ui.forgotPassword;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityUserForgotPasswordBinding;
import com.mimi.africa.ui.common.BaseActivity;


public class UserForgotPasswordActivity extends BaseActivity {


    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUserForgotPasswordBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user_forgot_password);

        // Init all UI
        initUI(binding);

    }




    //region Private Methods

    private void initUI(ActivityUserForgotPasswordBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.forgot_password__title));

        // setup Fragment
        setupFragment(new UserForgotPasswordFragment());
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }

    //endregion


}