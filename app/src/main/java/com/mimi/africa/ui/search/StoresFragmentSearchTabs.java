package com.mimi.africa.ui.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.ShopCardVerticalAdapter;
import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.ServicesFragmentSearchTabsBinding;
import com.mimi.africa.databinding.StoresFragmentSearchTabsBinding;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class StoresFragmentSearchTabs extends BaseFragment {

    public static final String  ALL_TAB = "All";
    public static final String PRODUCTS_TAB = "Products";
    public static final String SERVICES_TAB = "Services";
    public static final String STORES_TAB = "Stores";

    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;
    private boolean reloadPage;


    private static final String TAG = ServicesFragmentSearchTabs.class.getSimpleName();

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private  View parentView;
    private AutoClearedValue<StoresFragmentSearchTabsBinding> binding;

    @Inject
    APIService mAPIService;


    public StoresFragmentSearchTabs() {}

    @NonNull
    public static StoresFragmentSearchTabs newInstance() {
        return new StoresFragmentSearchTabs();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        StoresFragmentSearchTabsBinding storesFragmentSearchTabsBinding = DataBindingUtil.inflate(inflater, R.layout.stores_fragment_search_tabs, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, storesFragmentSearchTabsBinding);

        return storesFragmentSearchTabsBinding.getRoot();
    }

    @Override
    protected void initUIAndActions() {
        reloadPage = false;
        if (getActivity() != null )
        parentView= getActivity().findViewById(android.R.id.content);
    }

    @Override
    protected void initViewModels() {}

    @Override
    protected void initAdapters() {}

    @Override
    protected void initData() {

        if (CustomSharedPrefs.getReloadPageStatus(getContext())) {
            getStores();

        }else {

            if ( getActivity() != null)
            Utils.ShowView(binding.get().noItemConstraintLayout);
            binding.get().noItemDescTextView.setText(getResources().getString(R.string.search_message));
            binding.get().noItemTitleTextView.setText("Search");
            Utils.RemoveView(binding.get().productsProgressBar);
        }
    }

    private void getStores(){
        mAPIService.getPromoShops("promos")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShopsResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;

                        try {
                            Utils.ShowView(binding.get().productsProgressBar);
                        } catch (Exception e) {
                            logError(TAG, e, "onSubscribe:  ");
                        }
                    }

                    @Override
                    public void onNext(@NonNull ShopsResponse shopsResponse) {
                        ShopCardVerticalAdapter shopCardAdapter = new ShopCardVerticalAdapter(getContext(), shopsResponse.getShop());

                        try {

                            binding.get().productsRecyclerView.
                                    setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));

                            binding.get().productsRecyclerView.setAdapter(shopCardAdapter);

                        } catch (Exception e) {
                            logError(TAG, e, "onNext:  ");
                        }

                        shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                            navigateToStore(obj);
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
                        logError(TAG,e, "onError:  " + e.getMessage());

                        reloadSnackBar(binding.get().productsProgressBar, "Oops, something went wrong!");

                    }

                    @Override
                    public void onComplete() {
                        disposeAPIService(disposable);

                        try {

                            Utils.RemoveView(binding.get().productsProgressBar);
                            Utils.ShowView(binding.get().productsRecyclerView);

                        }catch (Exception e){
                            logError(TAG,e, "onComplete:  ");
                        }
                    }
                });
    }

    private void disposeAPIService(Disposable disposable) {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private void reloadSnackBar(ProgressBar servicesProgressBar, String string) {
        Utils.RemoveView(servicesProgressBar);
        Snackbar snackbar = Snackbar.make(parentView, string, Snackbar.LENGTH_LONG)
                .setAction("RELOAD!", view -> {
//                    loadMainData();
                });
        snackbar.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(SearchEvent event) {

//        searchShops(event.getTerm());
        Toast.makeText(getContext(), " " + event.getTerm(), Toast.LENGTH_SHORT).show();
        getStores();
    }

}
