package com.mimi.africa.ui.common;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.libraries.places.api.Places;
import com.google.firebase.FirebaseApp;
import com.mimi.africa.R;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.NotificationUpdated;
import com.mimi.africa.event.ShopMoreClickedEvent;
import com.mimi.africa.event.ShopViewedEvent;
import com.mimi.africa.model.Notification;
import com.mimi.africa.model.Post;
import com.mimi.africa.model.Shop;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.GPSTracker;
import com.mimi.africa.utils.ShopContactBottomSheet;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import io.sentry.core.Sentry;

public abstract class BaseActivity extends AppCompatActivity implements HasAndroidInjector{

      private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
      private static final int MY_PERMISSIONS_REQUEST_PHONE_CALL = 101;
      private static final String TAG = BaseActivity.class.getSimpleName() ;
      public EventBus eventBus;
      public boolean registerEventBus = true;
      public boolean isInForeground = false;
      private Activity mActivity;

      @Inject
      DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

      @Inject
      protected AppExecutors appExecutors;

      @Inject
      protected MimiDb mimiDb;

      @Inject
      protected NavigationController navigationController;

            @Override
            public AndroidInjector<Object> androidInjector() {
                  return dispatchingAndroidInjector;
            }

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                  super.onCreate(savedInstanceState);

                  mActivity = this;

                  if(registerEventBus)
                        eventBus = EventBus.getDefault();

                  if (checkLocationPermission()) {
                        getGPSLocation();
                  }

                  Places.initialize(this, getResources().getString(R.string.google_places_key));

                  FirebaseApp.initializeApp(getApplicationContext());

//                  try {
//                        mimiDb = MimiDb.getInstance(this);
//                  }catch (Exception e){
//                        Sentry.captureException(e);
//                  }
            }

            @Nullable
            protected Toolbar initToolbar(@Nullable Toolbar toolbar, @NonNull String title, int color) {

                  if(toolbar != null) {

                        toolbar.setTitle(title);

                        if(color != 0) {
                              try {
                                    toolbar.setTitleTextColor(getResources().getColor(color));
                              }catch (Exception e){
                                    Utils.psErrorLog("Can't set color.", e);
                              }
                        }else {
                              try {
                                    toolbar.setTitleTextColor(getResources().getColor(R.color.text__white));
                              }catch (Exception e){
                                    Utils.psErrorLog("Can't set color.", e);
                              }
                        }

                        try {
                              setSupportActionBar(toolbar);
                        }catch (Exception e) {
                              Utils.psErrorLog("Error in set support action bar.", e);
                        }

                        try {
                              if (getSupportActionBar() != null) {
                                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                              }
                        }catch (Exception e) {
                              Utils.psErrorLog("Error in set display home as up enabled.", e);
                        }

                        if(!title.equals("")) {
                              setToolbarText(toolbar, title);
                        }

                  }else {
                        Utils.psLog("Toolbar is null");
                  }

                  return toolbar;
            }

            @Nullable
            protected Toolbar initToolbar(@Nullable Toolbar toolbar, @NonNull String title) {

                  if(toolbar != null) {

                        toolbar.setTitle(title);

                        try {
                              toolbar.setTitleTextColor(getResources().getColor(R.color.text__white));
                        }catch (Exception e){
                              Utils.psErrorLog("Can't set color.", e);
                        }

                        try {
                              setSupportActionBar(toolbar);
                        }catch (Exception e) {
                              Utils.psErrorLog("Error in set support action bar.", e);
                        }

                        try {
                              if (getSupportActionBar() != null) {
                                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                              }
                        }catch (Exception e) {
                              Utils.psErrorLog("Error in set display home as up enabled.", e);
                        }

                        if(!title.equals("")) {
                              setToolbarText(toolbar, title);
                        }

                  }else {
                        Utils.psLog("Toolbar is null");
                  }
                  return toolbar;
            }

            public void setToolbarText(Toolbar toolbar, String text) {
                  Utils.psLog("Set Toolbar Text : " + text);
//                  toolbar.setTitle(Utils.getSpannableString(toolbar.getContext(), text, Utils.Fonts.ROBOTO));
            }


            @Override
            public boolean onOptionsItemSelected(@Nullable MenuItem item) {

                  if(item != null) {
                        Utils.psLog("Clicked " + item.getItemId());
                        switch (item.getItemId()) {
                              case android.R.id.home:
                                    onBackPressed();
                                    return true;
                        }
                  }
                  return super.onOptionsItemSelected(item);
            }
            //endregion

            //region Setup Fragment

            protected void setupFragment(@NonNull Fragment fragment) {
                  try {
                        this.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, fragment)
                                .commitAllowingStateLoss();
                  }catch (Exception e){
                        Utils.psErrorLog("Error! Can't replace fragment.", e);
                  }
            }

            protected void setupFragment(@NonNull Fragment fragment, int frameId) {
                  try {
                        this.getSupportFragmentManager().beginTransaction()
                                .replace(frameId, fragment)
                                .commitAllowingStateLoss();
                  }catch (Exception e){
                        Utils.psErrorLog("Error! Can't replace fragment.", e);
                  }
            }

      private void loadFragment(@NonNull Fragment fragment) {
            // load fragment
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
      }

            //endregion

      @Override
      public void onStart() {
            super.onStart();
            Log.i(TAG, "onStart: ");
            if(registerEventBus)
                  eventBus.register(this);
      }
      //
      @Override
      public void onStop() {
            if(registerEventBus)
                  eventBus.unregister(this);
            super.onStop();
      }
      //
      @Override
      protected void onResume() {
            super.onResume();
            isInForeground = true;
      }

      @Override
      protected void onPause() {
            super.onPause();
            isInForeground = false;
      }

      private boolean checkLocationPermission() {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                  if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                          android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                        new android.app.AlertDialog.Builder(this)
                                .setTitle(R.string.alertmsg)
                                .setMessage(R.string.alert_msg2)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                            ActivityCompat.requestPermissions(mActivity,
                                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,  Manifest.permission.CALL_PHONE},
                                                    MY_PERMISSIONS_REQUEST_LOCATION);
                                      }
                                })
                                .create()
                                .show();

                  } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_LOCATION);
                  }
                  return false;
            } else {
                  return true;
            }
      }

      private void getGPSLocation() {
            GPSTracker gps = new GPSTracker(this);
            if (!gps.canGetLocation()) {
                  gps.showSettingsAlert();
            }

      }

      @Override
      public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            switch (requestCode) {
                  case MY_PERMISSIONS_REQUEST_LOCATION: {
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults.length > 0
                                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                              if (ContextCompat.checkSelfPermission(this,
                                      android.Manifest.permission.ACCESS_FINE_LOCATION)
                                      == PackageManager.PERMISSION_GRANTED) {
                                    //Request location updates:
                                    getGPSLocation();
                              }

                        } else {
                              checkLocationPermission();
                        }
                  }
                        break;

            }
      }

      public void showDialog(@NonNull Fragment fragment) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(android.R.id.content, fragment).addToBackStack(null).commit();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onShopViewed(ShopViewedEvent event) {
            insertShopViewed(event.getShop());
      }

      private void insertShopViewed(Shop shop){

            appExecutors.getDiskIO().execute(() -> {
                  if (mimiDb.shopDaoAccess().fetchShopById(shop.getId()) == null) {
                        mimiDb.shopDaoAccess().insertOnlySingleShop(shop);
                  }
            });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onNotificationUpdated(NotificationUpdated event) {
                  Notification appNotification = event.getNotification();
                  Post post = event.getPost();
                  String shopId = event.getShopId();

            if (mimiDb.notificationDaoAccess().fetchNotificationByShopId(shopId) != null ){

                  Notification notification = mimiDb.notificationDaoAccess().fetchNotificationByShopId(shopId);
                  notification.setDateCreated(System.currentTimeMillis());
                  notification.setName(post.getMessage().getTitle());
                  notification.setMessage(post.getMessage().getContent());
                  notification.setArchived(false);
                  notification.setRead(false);
                  mimiDb.notificationDaoAccess().updateNotification(notification);

            }else {
                  mimiDb.notificationDaoAccess().insert(appNotification);
            }
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onShopMoreClicked(ShopMoreClickedEvent event) {
            ShopContactBottomSheet fragment = new ShopContactBottomSheet();
            fragment.setShop(event.getShop());
            fragment.show(getSupportFragmentManager(), fragment.getTag());
      }

      @Override
      protected void onResumeFragments() {
            super.onResumeFragments();
            Log.i(TAG, "onResumeFragments: " );
      }

      @Override
      public void onAttachFragment(@NonNull Fragment fragment) {
            super.onAttachFragment(fragment);
            Log.i(TAG, "onAttachFragment:  " );
      }
}

