package com.mimi.africa.ui.category;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.event.LocationSearchEvent;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

public class SingleCategoryActivity extends BaseActivity {

    private ActionBar actionBar;
    private EditText et_search;
    private TextView searchResultsTextView;

    private static final String TAG = SingleCategoryFragment.class.getSimpleName();
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private String mCategorySlug;
    private int mCategoryId;
    private String mCategoryName;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_category);

        if (getIntent() != null){
            mCategorySlug = getIntent().getStringExtra("category_slug");
            mCategoryId = getIntent().getIntExtra("category_id",0);
            mCategoryName = getIntent().getStringExtra("category_name");
        }

        initComponent();
        initToolbar();

    }

    private void initToolbar() {
          initToolbar(findViewById(R.id.toolbar), mCategoryName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }


    private void initComponent() {
        searchResultsTextView = findViewById(R.id.search_results_text);
        searchResultsTextView.setVisibility(View.GONE);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        SingleCategoryFragment singleCategoryFragment = SingleCategoryFragment.newInstance(mCategorySlug, String.valueOf(mCategoryId));
        setupFragment(singleCategoryFragment);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }else if (item.getItemId() == R.id.action_search){
            autocompleteIntent();
        }else if (item.getItemId() == R.id.action_main_search){
            startActivity(new Intent(SingleCategoryActivity.this, MainSearchActivity.class));
            progressDialog.hide();
        }
        return super.onOptionsItemSelected(item);
    }

    private void autocompleteIntent(){

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .setCountry("GH")
                .build(getApplicationContext());

        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(DemoEvent event) {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                searchResultsTextView.setVisibility(View.VISIBLE);
                searchResultsTextView.setText(String.format("Searching results for %s", place.getName()));

                EventBus.getDefault().post(new LocationSearchEvent(place.getName()));

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Utils.ShowSnackBar(this,"Oops, there was a problem.", Snackbar.LENGTH_SHORT);

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Log.i(TAG, "Google Places Cancelled");

            }
        }
    }



}