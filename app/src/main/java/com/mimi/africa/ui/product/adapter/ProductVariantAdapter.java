package com.mimi.africa.ui.product.adapter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemProductVariantAdapterBinding;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.viewModels.ProductColor;
import com.squareup.picasso.Picasso;

import java.util.Objects;


public class ProductVariantAdapter extends DataBoundListAdapter<ProductColor, ItemProductVariantAdapterBinding> {
    private final androidx.databinding.DataBindingComponent dataBindingComponent;
    private variantClickCallBack callback;
    private DiffUtilDispatchedInterface diffUtilDispatchedInterface = null;


    public ProductVariantAdapter(androidx.databinding.DataBindingComponent dataBindingComponent, variantClickCallBack variantClickCallback) {
        this.dataBindingComponent = dataBindingComponent;
        this.callback = variantClickCallback;
    }

    @Override
    protected ItemProductVariantAdapterBinding createBinding(ViewGroup parent) {
        ItemProductVariantAdapterBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_product_variant_adapter, parent, false,
                        dataBindingComponent);

        binding.getRoot().setOnClickListener((View v) -> {
            ProductColor productColor = binding.getProductColor();
            if (productColor != null && callback != null) {

                callback.onClick(productColor, productColor.id,productColor.colorValue);
            }
        });
        return binding;
    }


    @Override
    protected void dispatched() {
        if (diffUtilDispatchedInterface != null) {
            diffUtilDispatchedInterface.onDispatched();
        }
    }

    @Override
    protected void bind(ItemProductVariantAdapterBinding binding, ProductColor item) {
        binding.setProductColor(item);

        Bitmap b = Bitmap.createBitmap(2, 2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
//        canvas.drawColor(Color.parseColor(item.colorValue));
//        dataBindingComponent.getFragmentBindingAdapters().bindCircleBitmap(binding.color1BgImageView, b);
//        binding.color1BgImageView;

//        Picasso.get().load(item.colorValue).into(binding.color1BgImageView);

        Picasso.get().load(item.colorValue).transform(new CircleTransform()).into(binding.color1BgImageView);

//        dataBindingComponent.getFragmentBindingAdapters().bindCircleBitmap(binding.color1BgImageView, b);

        if (item.isColorSelect) {
            binding.color1ImageView.setVisibility(View.VISIBLE);
        } else {
            binding.color1ImageView.setVisibility(View.GONE);
        }
    }

    @Override
    protected boolean areItemsTheSame(ProductColor oldItem, ProductColor newItem) {
        return Objects.equals(oldItem.id, newItem.id)
                && oldItem.productId.equals(newItem.productId)
                && oldItem.isColorSelect == newItem.isColorSelect;
    }

    @Override
    protected boolean areContentsTheSame(ProductColor oldItem, ProductColor newItem) {
        return Objects.equals(oldItem.id, newItem.id)
                && oldItem.productId.equals(newItem.productId)
                && oldItem.isColorSelect == newItem.isColorSelect;
    }

    public interface variantClickCallBack {
        void onClick(ProductColor productColor, String selectedColorId, String selectedColorValue);
    }

}
