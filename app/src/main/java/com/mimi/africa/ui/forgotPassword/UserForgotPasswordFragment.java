package com.mimi.africa.ui.forgotPassword;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentForgotPasswordBinding;
import com.mimi.africa.databinding.FragmentUserForgotPasswordBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.login.UserLoginActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.user.UserViewModel;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * UserForgotPasswordFragment
 */
public class UserForgotPasswordFragment extends BaseFragment {


    //region Variables

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);


    private UserViewModel userViewModel;
    private PSDialogMsg psDialogMsg;

    @VisibleForTesting
    private AutoClearedValue<FragmentForgotPasswordBinding> binding;

    private AutoClearedValue<ProgressDialog> prgDialog;

    //endregion

    //region Override Methods

    @Inject
    protected NavigationController navigationController;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @Inject
    APIService mAPIService;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
//        FragmentUserForgotPasswordBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_forgot_password, container, false, dataBindingComponent);
        FragmentForgotPasswordBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false, dataBindingComponent);

        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }

    @Override
    protected void initUIAndActions() {

//        dataBindingComponent.getFragmentBindingAdapters().bindFullImageDrawable(binding.get().bgImageView, getResources().getDrawable(R.drawable.login_app_bg));

        psDialogMsg = new PSDialogMsg(getActivity(), false);

        // Init Dialog
        prgDialog = new AutoClearedValue<>(this, new ProgressDialog(getActivity()));
        //prgDialog.get().setMessage(getString(R.string.message__please_wait));

        prgDialog.get().setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
        prgDialog.get().setCancelable(false);


        //fadeIn Animation
        fadeIn(binding.get().getRoot());


//        binding.get().loginButton.setOnClickListener(view -> {
//            Intent intent = new Intent(getActivity(), UserLoginActivity.class);
//            startActivity(intent);
//        });

        binding.get().forgotPasswordButton.setOnClickListener(view -> {
            if (connectivity.isConnected()) {
                forgotPassword();
            } else {

                psDialogMsg.showWarningDialog(getString(R.string.no_internet_error), getString(R.string.app__ok));
                psDialogMsg.show();
            }
        });
    }

    @Override
    protected void initViewModels() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

    }

    //endregion


    //region Private Methods

    private void updateForgotBtnStatus() {
        if (userViewModel.isLoading) {
            binding.get().forgotPasswordButton.setText(getResources().getString(R.string.message__loading));
        } else {
            binding.get().forgotPasswordButton.setText(getResources().getString(R.string.forgot_password__title));
        }
    }

    private void forgotPassword() {

        Utils.hideKeyboard(getActivity());

        String email = binding.get().emailEditText.getText().toString().trim();
        if (email.equals("")) {

            psDialogMsg.showWarningDialog(getString(R.string.error_message__blank_email), getString(R.string.app__ok));
            psDialogMsg.show();
            return;
        }

        postForgotPassword(email);

        new Handler().postDelayed(() -> {
            psDialogMsg.showSuccessDialog("Successfully sent a password reset link to your email!", "Log in");
            psDialogMsg.show();

            psDialogMsg.okButton.setOnClickListener(v1 -> {
                psDialogMsg.cancel();
                navigationController.navigateToUserLoginActivity(getActivity(), "");
            });
        },2000);

    }

    //endregion

    private void postForgotPassword(String email) {

            mAPIService.forgotPassword( email )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) { disposable = d; }

                          @Override
                          public void onNext(@NonNull JsonElement jsonElement) {

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {}

                          @Override
                          public void onComplete() {

                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
    }


}
