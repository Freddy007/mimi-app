package com.mimi.africa.ui.gallery;


import android.os.Bundle;

import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentGalleryDetailBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryDetailFragment extends BaseFragment {

      //region Variables

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

      @VisibleForTesting
      private AutoClearedValue<FragmentGalleryDetailBinding> binding;

      public GalleryDetailFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_gallery_detail, container, false);
      }

      @Override
      protected void initUIAndActions() {

            binding.get().viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                  public void onPageScrollStateChanged(int arg0) {

                  }

                  public void onPageScrolled(int arg0, float arg1, int arg2) {

                  }

                  public void onPageSelected(int currentPage) {

//                        if (imageViewModel.newsImageList != null) {
//                              if (currentPage >= imageViewModel.newsImageList.size()) {
//                                    currentPage = currentPage % imageViewModel.newsImageList.size();
//                              }
//
//                              binding.get().imgDesc.setText(imageViewModel.newsImageList.get(currentPage).imgDesc);
//                        }

                  }

            });
      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {

      }
}
