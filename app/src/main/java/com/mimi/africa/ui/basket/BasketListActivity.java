package com.mimi.africa.ui.basket;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityBasketListBinding;
import com.mimi.africa.ui.basket.fragment.BasketListFragment;
import com.mimi.africa.ui.common.BaseActivity;

public class BasketListActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBasketListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_basket_list);

        // Init all UI
        initUI(binding);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    //region Private Methods

    private void initUI(ActivityBasketListBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.basket__list));

        // setup Fragment
        setupFragment(new BasketListFragment());

    }

    //endregion
}
