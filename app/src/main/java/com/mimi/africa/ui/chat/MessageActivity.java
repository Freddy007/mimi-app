package com.mimi.africa.ui.chat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.databinding.ActivityMessageBinding;
import com.mimi.africa.event.AttachmentAdded;
import com.mimi.africa.event.SendMessageEvent;
import com.mimi.africa.model.Chat;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MessageActivity extends BaseActivity implements View.OnClickListener {

      private static final String TAG = MessageActivity.class.getSimpleName();
      public ActivityMessageBinding activityMessageBinding;

    @Inject
    APIService mAPIService;

      @Inject
      protected SharedPreferences pref;

      ArrayList<Chat> rowListItem;
    ChatAdapter rcAdapter;
    private Activity mActivity;
    private ImageView demoImageView;

    private int mShopId;
    private String customer_id ;

    public ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        activityMessageBinding = DataBindingUtil.setContentView(this, R.layout.activity_message);
        customer_id = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        mActivity = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

          if (CustomSharedPrefs.getLastChatTime(this) != 0){
                String time = TimeAgo.getTimeAgo(CustomSharedPrefs.getLastChatTime(mActivity));
              activityMessageBinding.time.setText(time);

//                mLastTime.setText(Utils.getDateString(CustomSharedPrefs.getLastChatTime(mActivity), "dd/MM/yyyy hh:mm:ss"));
          }


          String name = "";;

        if (getIntent() != null){
            name = getIntent().getStringExtra(Constants.SHOP_NAME);
            mShopId = getIntent().getIntExtra(Constants.SHOP_ID,0);
            String image = getIntent().getStringExtra(Constants.SHOP_IMAGE);

            if (image !=null && !image.isEmpty()) {
                Picasso.get().load(image).transform(new CircleTransform()).into(activityMessageBinding.shopImage);
//                Picasso.with(this).load(image).transform(new CircleTransform()).into(activityMessageBinding.shopImage);
            }
            activityMessageBinding.storeName.setText(name);
            setupFragment(ChatFragment.newInstance(String.valueOf(mShopId), name));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle("");
        }
      }

      @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_settings:
//                Toast.makeText(this, "action setting clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
          if (view.getId() == R.id.buttonSend) {
//                sendMessage();
          }
    }

      private void hideKeyboard() {
            View view = this.getCurrentFocus();
            if (view != null) {
                  InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                  imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
      }

    private void sendAttachmentAPI(MultipartBody.Part file, RequestBody requestBody, RequestBody customer_id, RequestBody shop_id, RequestBody token, RequestBody message){

        mAPIService.sendAttachment(file, requestBody, customer_id, shop_id, token )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;

                        showProgressIndicator();
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {}

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressDialog.hide();
                        EventBus.getDefault().post(new SendMessageEvent());
                    }

                    @Override
                    public void onComplete() {
                        Toast.makeText(mActivity, "Successful attachment!", Toast.LENGTH_SHORT).show();
                        progressDialog.hide();
                        disposeAPIService(disposable);
                        EventBus.getDefault().post(new SendMessageEvent());
                    }
                });
    }

    private void selectImage(Context context) {
        final CharSequence[] options = {  "Choose from Gallery","Cancel" };
//            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        try {

            android.app.AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Choose your profile picture");

            builder.setItems(options, (dialog, item) -> {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            });
            builder.show();

        }catch (Exception e){
            Sentry.captureException(e);
            Log.e(TAG, "selectImage:  " + e.getMessage() );
            Log.e(TAG, "selectImage Cause  :  " + e.getCause() );
        }
    }

    private void sendAttachment(String filePath){

        try {

            //Create a file object using file path
            File file = new File(filePath);
            // Create a request body with file and locationImage media type
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            // Create MultipartBody.Part using file request-body,file name and part name
            MultipartBody.Part part = MultipartBody.Part.createFormData("attachments", file.getName(), fileReqBody);
            //Create request body with text description and text media type
            RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");

            RequestBody customerid = RequestBody.create(MediaType.parse("text/plain"), customer_id);

            RequestBody subject = RequestBody.create(MediaType.parse("text/plain"), "Message");
            RequestBody token = RequestBody.create(MediaType.parse("text/plain"), CustomSharedPrefs.getPushyToken(this));
            RequestBody shop_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mShopId));
            RequestBody message = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mShopId));

            sendAttachmentAPI(part, description, customerid, shop_id, token, message);

//            new Handler().postDelayed(this::getMessages,3000);
        }catch (Exception e){
            Sentry.captureException(e);
            Utils.ShowSnackBar(mActivity, "Failed to send attachment!", Snackbar.LENGTH_LONG);
        }
    }

    private void disposeAPIService(Disposable disposable) {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttachmentAdded(AttachmentAdded event){
        requestPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");

                        if (selectedImage != null) {
                            Log.i(TAG, "onActivityResult:  file " + selectedImage.getWidth());
                            Log.i(TAG, "onActivityResult:  file " + selectedImage);
                            Log.i(TAG, "onActivityResult:  file  " + selectedImage.toString());
                        }
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                Log.i(TAG, "onActivityResult:  file decoded " + (picturePath));
                                sendAttachment(picturePath);
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    selectImage(mActivity);

                } else {

                    Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    private void showProgressIndicator() {
        this.progressDialog.setMessage((Utils.getSpannableString(this, getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
    }

}
