package com.mimi.africa.ui.mainSearch;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.search.AdapterSuggestionSearch;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.ViewAnimation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainSearchActivity extends BaseActivity {

    private static final String TAG = MainSearchActivity.class.getSimpleName();

    private ViewPager view_pager;
    private TabLayout tab_layout;
    private EditText et_search;
    private ImageButton bt_clear;
    private ImageButton back_button;
    private ProgressBar progress_bar;
    private RecyclerView recyclerSuggestion;
    public AdapterSuggestionSearch mAdapterSuggestion;
    private LinearLayout lyt_suggestion;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
        initComponent();
    }

    private void initComponent() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        setupFragment(new SearchHomeFragment());

        CustomSharedPrefs.removeLoadPageStatus(this);
        CustomSharedPrefs.setReloadPageStatus(this,  false);

        back_button =  findViewById(R.id.back_button);
//        progress_bar = findViewById(R.id.progress_bar);

        et_search = findViewById(R.id.et_search);
        bt_clear = findViewById(R.id.bt_clear);
        ImageButton search_btn = findViewById(R.id.search_button);

        search_btn.setOnClickListener(v -> searchAction());

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard();

                    searchAction();

                    return true;
                }
                return false;
            }
        });

        back_button.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), MainHomeActivity.class)));

        lyt_suggestion = findViewById(R.id.lyt_suggestion);

        if (getIntent() != null ){
            String queryText = getIntent().getStringExtra(Constants.SEARCH_TERM);
            et_search.setText(queryText);
        }

    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void searchAction() {
        ViewAnimation.collapse(lyt_suggestion);

        final String query = et_search.getText().toString().trim();
        if (!query.equals("")) {
            hideKeyboard();
            EventBus.getDefault().post(new SearchEvent((query)));

        } else {
            Toast.makeText(this, "Search field can not be empty!", Toast.LENGTH_SHORT).show();
        }
    }

      @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    private void showSuggestionSearch() {
        mAdapterSuggestion.refreshItems();
        ViewAnimation.expand(lyt_suggestion);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(AddToCart event) {
    }
}