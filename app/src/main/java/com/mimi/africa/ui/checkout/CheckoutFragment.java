package com.mimi.africa.ui.checkout;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentCheckoutBinding;
import com.mimi.africa.event.CheckOutReady;
import com.mimi.africa.model.Delivery;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.checkout.adapter.CheckoutCartItemAdapter;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

import static android.app.Activity.RESULT_CANCELED;
import static com.mimi.africa.utils.Constant.RESULT_OK;


public class CheckoutFragment extends BaseFragment {

      private static final String TAG = CheckoutFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private  View parentView;
      private AutoClearedValue<FragmentCheckoutBinding> binding;

      @Inject
      APIService mAPIService;
      private StringBuffer cartIds_sb;
      private Dialog progressDialog;
      private int AUTOCOMPLETE_REQUEST_CODE = 1;
      private int mDeliveryCharge;
      private String mTotalWithDeliveryCharge;
      private int mTotal;
      private Handler mHandler;
      private Context mContext;
      private Activity mActivity;
      private String mShopId;
      private  List<String> mShopIds;
      private   String mCoord;

      public CheckoutFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {


            FragmentCheckoutBinding fragmentCheckoutBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentCheckoutBinding);
            mDeliveryCharge = 0;
            mHandler = new Handler();
            mContext = getContext();
            mActivity = getActivity();
            mShopIds = new ArrayList<>();

            return fragmentCheckoutBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            progressDialog = new Dialog(mContext);

            String customer_name = CustomSharedPrefs.getUserName(getContext());
            String customer_phone = CustomSharedPrefs.getUserPhone(getContext());

            binding.get().nameTextView.setText(customer_name);
            binding.get().phoneNumberTextView.setText(customer_phone);

            if (LocationPreferences.isLocationLatLonAvailable(mContext)) {

                  try {

                        double[] coordinates = LocationPreferences.getLocationCoordinates(getContext());
                        Location location = new Location("new Location");
                        location.setLatitude(coordinates[0]);
                        location.setLongitude(coordinates[1]);
                        if (Utils.getAddresses(mContext).size() > 0) {
                              binding.get().deliveryAddress.setText((Utils.getAddresses(mContext).get(0)).
                                      getAddressLine(0));
                        }

                        mCoord = (location.getLatitude() +","+location.getLongitude());

                  }catch (Exception e){
                        Log.e(TAG, "initUIAndActions:  " + e.getMessage() );
                        Sentry.captureException(e);
                  }
            }

            binding.get().codbox.setOnClickListener(v -> {

                  if (!binding.get().codbox.isChecked()){
                        binding.get().orderButton.setEnabled(false);
                        Utils.ShowSnackBar(mActivity,getString(R.string.only_cash_on_delivery), Snackbar.LENGTH_SHORT);
                  }else {
                        binding.get().orderButton.setEnabled(true);
                  }
            });

            binding.get().changeLocation.setOnClickListener(v -> {
                  autocompleteIntent();
            });

            binding.get().orderButton.setOnClickListener(v -> {

                  String customer_id = CustomSharedPrefs.getUserID(mContext);

                  mHandler.postDelayed(() -> {
                        String cart_ids = CustomSharedPrefs.getUserCartIds(mContext);
                        checkout(customer_id, cart_ids, "1");
                  }, 2000);
            });
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {
            binding.get().itemsRecyclerview
                    .setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
      }

      @Override
      protected void initData() {
            getCarts();
      }

      private void getCarts(){

            mAPIService.getCarts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CartResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                try {
                                      Utils.ShowView(binding.get().progressBar);
                                }catch (Exception e){
                                      Log.e(TAG, "onSubscribe:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onNext(@NonNull CartResponse cartResponse) {

                                try {
                                      List<Inventory> cartList = new ArrayList<>();

                                      for (int i =0; i< cartResponse.getCarts().size(); i++ ){

                                            if (cartResponse.getCarts().get(i).getShop() != null) {
                                                  if (cartResponse.getCarts().get(i).getShop().getLat() != null && cartResponse.getCarts().get(i).getShop().getLng() != null) {
                                                        mShopIds.add(String.valueOf(cartResponse.getCarts().get(i).getShop().getId()));
                                                  }
                                            }

                                            if (cartResponse.getCarts().get(i).getItems() != null) {
                                                  cartList.addAll(cartResponse.getCarts().get(i).getItems());
                                            }

                                            if (cartResponse.getCarts().get(i) != null) {
                                                  int qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());

                                                  mTotal += Double.parseDouble(cartResponse.getCarts().get(i).getTotal() ) ;
                                            }
                                      }

                                      CheckoutCartItemAdapter cartAdapter = new CheckoutCartItemAdapter(mContext, cartList);
                                      binding.get().itemsRecyclerview.setAdapter(cartAdapter);
                                } catch (NumberFormatException e) {
                                      e.printStackTrace();
                                      Sentry.captureException(e);
                                }

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError:  "  + e.getMessage() );

                                Utils.ShowSnackBar(mActivity, "failed to load cart!", Snackbar.LENGTH_INDEFINITE);

                                try{
                                      binding.get().progressBar.setVisibility(View.GONE);
                                }catch (Exception ex){ Log.e(TAG, "onError:  "  + ex.getMessage() ); }
                          }

                          @Override
                          public void onComplete() {

                                try {
                                      mHandler.postDelayed(() -> {
                                            Utils.RemoveView(  binding.get().progressBar);
                                            Utils.ShowView(    binding.get().itemsRecyclerview);
                                      },1000);

                                      mHandler.postDelayed(() -> {
                                            getDeliveryCharge(Constants.POINTS, "1", mCoord);
                                      },3000);

                                }catch (Exception e){ Log.e(TAG, "onComplete:  " + e.getMessage() ); }
                          }
                    });
      }

      private void checkout(String customer_id, String cart_ids, String id){

            mAPIService.checkout(Constants.DEFAULT_PAYMENT_METHOD,
                    customer_id, cart_ids, id, String.valueOf(mDeliveryCharge), mTotalWithDeliveryCharge,"" )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                try {
                                      binding.get().orderButton.setEnabled(false);
                                      binding.get().orderButton.setText(getString(R.string.please_wait));
                                }catch (Exception e){
                                      Log.e(TAG, "onSubscribe:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onNext(@NonNull JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError:  "  + e.getMessage() );

                                try{
                                      Utils.ShowSnackBar(mActivity, "order failed!", Snackbar.LENGTH_LONG);
                                      binding.get().progressBar.setVisibility(View.GONE);
                                      binding.get().orderButton.setEnabled(true);
                                      binding.get().orderButton.setText(getString(R.string.try_again));
                                }catch (Exception ex){
                                      Log.e(TAG, "onError:  "  + ex.getMessage() );
                                }
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      new Handler().postDelayed(() -> {

                                            Utils.showProgressDialog(getContext(), progressDialog, R.layout.dialog_progress);

                                            progressDialog.findViewById(R.id.bt_close).setOnClickListener(v -> navigateToHome());

                                            Utils.RemoveView(  binding.get().progressBar);
                                            Utils.ShowSnackBar(mActivity, (getString(R.string.order_successfuly_placed)), Snackbar.LENGTH_INDEFINITE);
                                            binding.get().orderButton.setText(getString(R.string.order_successful));
                                            CustomSharedPrefs.removeCartStatus(getContext());
                                      },1000);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " + e.getMessage() );
                                }
                          }
                    });
      }

//      @NotNull
//      private Address getAddress(Context context, Location newLocation) {
//            List<Address> addresses = LocationUtilService.getAddresses(newLocation, context);
//             return addresses.get(0);
//      }

      private void autocompleteIntent(){
            int AUTOCOMPLETE_REQUEST_CODE = 1;

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .setCountry("GH")
                    .build(getContext());
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

            if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
                  if (resultCode == RESULT_OK) {
                        Place place = null;
                        if (data != null) {
                              place = Autocomplete.getPlaceFromIntent(data);
                              binding.get().deliveryAddress.setText(place.getName());
                        }

                        if (place != null) {
                              getDeliveryCharge("place_id", place.getId(),"");
                        }

                  } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        Status status = null;
                        if (data != null) {
                              status = Autocomplete.getStatusFromIntent(data);
                              if (status.getStatusMessage() != null) {
                                    Log.i(TAG, status.getStatusMessage());
                              }
                        }

                  } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                        Log.i(TAG, "onActivityResult:  Cancelled" );
                  }
            }
      }

      private void getDeliveryCharge(String type, String place_id, String coordinates ){
            mAPIService.getDeliveryCharge(type,mShopIds.get(0), place_id,  coordinates)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Delivery>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(Delivery delivery) { mDeliveryCharge = delivery.getCharge(); }

                          @Override
                          public void onError(Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
//                                EventBus.getDefault().post(new CheckOutReady());
                          }
                    });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onCheckoutReady(CheckOutReady event){

            mTotalWithDeliveryCharge =  (Utils.TwoDecimalPlace(mTotal + mDeliveryCharge));
            String totalStr = Utils.TwoDecimalPlace(mTotal + mDeliveryCharge);
            binding.get().totalAmount.setText(String.format("%s %s", Constants.DEFAULT_CURRENCY, totalStr));
            binding.get().deliveryCharge.setText((Constants.DEFAULT_CURRENCY + " "+ mDeliveryCharge));

//            Toast.makeText(mContext, "Checkout Ready! " + mTotalWithDeliveryCharge, Toast.LENGTH_SHORT).show();
      }

}
