package com.mimi.africa.ui.service;


import android.os.Bundle;

import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.ui.common.BaseActivity;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ServiceListActivity extends BaseActivity {

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_service_list);

            initComponents();
      }

      private void initComponents(){
            initToolbar(findViewById(R.id.toolbar),"Service list");
            setupFragment(new ServiceListFragment());
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void addToCart(AddToCart event){
      }
}
