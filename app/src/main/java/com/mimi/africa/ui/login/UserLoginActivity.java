package com.mimi.africa.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;


import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityUserLoginBinding;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.MyContextWrapper;

public class UserLoginActivity extends BaseActivity {

    private String mOrigin;
    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUserLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user_login);

        if (getIntent().getStringExtra(Constants.ORIGIN) != null){
            mOrigin = getIntent().getStringExtra(Constants.ORIGIN);
        }
        // Init all UI
        initUI(binding);

    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String CURRENT_LANG_CODE = preferences.getString(Constant.LANGUAGE_CODE, Config.DEFAULT_LANGUAGE);
        String CURRENT_LANG_COUNTRY_CODE = preferences.getString(Constant.LANGUAGE_COUNTRY_CODE, Config.DEFAULT_LANGUAGE_COUNTRY_CODE);

        super.attachBaseContext(MyContextWrapper.wrap(newBase, CURRENT_LANG_CODE, CURRENT_LANG_COUNTRY_CODE, true));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if(fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }
    //endregion


    //region Private Methods

    private void initUI(ActivityUserLoginBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.login__login));

        // setup Fragment
//        setupFragment(new UserLoginFragment());
        UserLoginFragment userLoginFragment = UserLoginFragment.newInstance(mOrigin);
        setupFragment(userLoginFragment);
        // Or you can call like this
        //setupFragment(new NewsListFragment(), R.id.content_frame);

    }


    //endregion


}