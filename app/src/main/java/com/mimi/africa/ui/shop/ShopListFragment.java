package com.mimi.africa.ui.shop;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.ShopCardVerticalAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxLayoutBinding;
import com.mimi.africa.databinding.BottomCategoriesBoxLayoutBinding;
import com.mimi.africa.databinding.FragmentShopListBinding;
import com.mimi.africa.databinding.StoresFragmentSearchTabsBinding;
import com.mimi.africa.event.OnOptionItemSelected;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.product.adapter.CategoryAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ShopListFragment extends BaseFragment {

      private static final String TAG = ShopListFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private  View parentView;
      private AutoClearedValue<FragmentShopListBinding> binding;
      private APIService mAPIService;
      private AutoClearedValue<BottomBoxLayoutBinding> bottomBoxLayoutBinding;
      private AutoClearedValue<BottomCategoriesBoxLayoutBinding> bottomCategoriesBoxLayoutBinding;
      private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
      private AutoClearedValue<BottomSheetDialog> mCategoryBottomSheetDialog;

      public ShopListFragment() {}

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentShopListBinding fragmentShopListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_list, container,
                    false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentShopListBinding);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }

            return fragmentShopListBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            parentView= getActivity().findViewById(android.R.id.content);

//            mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
//            mCategoryBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
//            bottomBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_box_layout, null, false));
//            bottomCategoriesBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_categories_box_layout, null, false));
//
//            mBottomSheetDialog.get().setContentView(bottomBoxLayoutBinding.get().getRoot());

//            mCategoryBottomSheetDialog.get().setContentView(bottomCategoriesBoxLayoutBinding.get().getRoot());
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
            getStores();
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

      private void reloadSnackBar(ProgressBar servicesProgressBar, String string) {
            Utils.RemoveView(servicesProgressBar);
            Snackbar snackbar = Snackbar.make(parentView, string, Snackbar.LENGTH_INDEFINITE)
                    .setAction("RELOAD!", view -> {});
            snackbar.show();
      }

      private void getStores(){
            mAPIService.getShops()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

                                try {
                                      Utils.ShowView(binding.get().productsProgressBar);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {

                                showNoItemView(shopsResponse);

                                ShopCardVerticalAdapter shopCardAdapter = new ShopCardVerticalAdapter(getContext(), shopsResponse.getShop());

                                try {

                                      binding.get().productsRecyclerView.
                                              setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));

                                      binding.get().productsRecyclerView.setAdapter(shopCardAdapter);

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext:  ");
                                }

                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                      navigateToStore(obj);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG,e, "onError:  " + e.getMessage());

                                reloadSnackBar(binding.get().productsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                try {

                                      Utils.RemoveView(binding.get().productsProgressBar);
                                      Utils.ShowView(binding.get().productsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      private void showNoItemView(@NonNull ShopsResponse shopsResponse) {
            if (getActivity() != null ) {
                  if (shopsResponse.getShop().size() < 1) {
                        Utils.ShowView(binding.get().noItemConstraintLayout);
                  } else {
                        Utils.RemoveView(binding.get().noItemConstraintLayout);
                  }
            }
      }


      private void getStoresByLocation(String location){
            mAPIService.getShopsByLocation(location)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

                                try {
                                      Utils.ShowView(binding.get().productsProgressBar);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {
                                ShopCardVerticalAdapter shopCardAdapter = new ShopCardVerticalAdapter(getContext(), shopsResponse.getShop());

                                showNoItemView(shopsResponse);
                                try {

                                      binding.get().productsRecyclerView.
                                              setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));

                                      binding.get().productsRecyclerView.setAdapter(shopCardAdapter);

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext:  ");
                                }

                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                      navigateToStore(obj);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG,e, "onError:  " + e.getMessage());

                                reloadSnackBar(binding.get().productsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                try {

                                      Utils.RemoveView(binding.get().productsProgressBar);
                                      Utils.ShowView(binding.get().productsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onOptionItemSelected(OnOptionItemSelected event){

            switch (event.getMenuItem().getItemId()){

                  case R.id.action_all:
                        getStores();
                        break;

                  case R.id.action_ashanti:
                        getStoresByLocation("Ashanti Region");
                        break;

                  case R.id.action_brong_ahafo:
                        getStoresByLocation("Brong Ahafo Region");
                        break;

                  case R.id.action_central:
                        getStoresByLocation("Central Region");
                        break;

                  case R.id.action_eastern:
                        getStoresByLocation("Eastern Region");
                        break;

                  case R.id.action_greater_accra:
                        getStoresByLocation("Accra");
                        break;

                  case R.id.action_northern:
                        getStoresByLocation("Northern Region");
                        break;

                  case R.id.action_upper_east:
                        getStoresByLocation("Upper East");
                        break;

                  case R.id.action_upper_west:
                        getStoresByLocation("Upper West");
                        break;

                  case R.id.action_volta:
                        getStoresByLocation("Volta");
                        break;

                  case R.id.action_western:
                        getStoresByLocation("Western Region");
                        break;
            }
      }


//      private void getAllCategories(){
//
//            mAPIService.getAllCategories()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<CategoryResponse>() {
//                          @Override
//                          public void onSubscribe(Disposable d) {}
//
//                          @Override
//                          public void onNext(@NonNull CategoryResponse categoryResponse) {
//
//                                try {
//
//                                      if (categoryResponse.getCategories().size() > 0) {
//                                            CategoryAdapter productCategoryAdapter = new CategoryAdapter(getContext(), categoryResponse.getCategories());
//                                            bottomCategoriesBoxLayoutBinding.get().productCategoriesRecyclerView
//                                                    .setAdapter(productCategoryAdapter);
//
//                                            productCategoryAdapter.setOnItemClickListener(new CategoryAdapter.OnItemClickListener() {
//                                                  @Override
//                                                  public void onItemClick(View view, ProductCategory obj, int pos) {
////                                                        getListings("", String.valueOf(obj.getId()));
//                                                        mCategoryBottomSheetDialog.get().dismiss();
//                                                  }
//                                            });
//                                      }
//
//                                }catch (Exception e){
//                                      Log.e(TAG, "onNext:  " + e.getMessage() );
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {}
//
//                          @Override
//                          public void onComplete() {}
//                    });
//      }

}
