package com.mimi.africa.ui.product;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxLayoutBinding;
import com.mimi.africa.databinding.BottomCategoriesBoxLayoutBinding;
import com.mimi.africa.databinding.FragmentAllProductListBinding;
import com.mimi.africa.databinding.FragmentProductListBinding;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.product.adapter.AllProductVerticalListAdapter;
import com.mimi.africa.ui.product.adapter.CategoryAdapter;
import com.mimi.africa.ui.shop.adapter.ProductCategoryAdapter;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class AllProductListFragment extends BaseFragment {

      private static final String TAG = AllProductListFragment.class.getSimpleName();

      private boolean typeClicked = false;
      private boolean filterClicked = false;
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentAllProductListBinding> binding;
      private AutoClearedValue<BottomBoxLayoutBinding> bottomBoxLayoutBinding;
      private AutoClearedValue<BottomCategoriesBoxLayoutBinding> bottomCategoriesBoxLayoutBinding;
      private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
      private AutoClearedValue<BottomSheetDialog> mCategoryBottomSheetDialog;

      @Inject
      APIService mAPIService;
      private String mFilteringType;
      private String mShopId;

      public AllProductListFragment() {
      }

      public static AllProductListFragment newInstance(String name, String type, String shop_id){

            AllProductListFragment allProductListFragment = new AllProductListFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FILTERING_TYPE,  type);
            bundle.putString(Constants.FILTERING_TYPE_NAME,  name);
            bundle.putString(Constants.SHOP_ID,  shop_id);
            allProductListFragment.setArguments(bundle);
            return allProductListFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mFilteringType = getArguments().getString(Constants.FILTERING_TYPE);
                  mShopId = getArguments().getString(Constants.SHOP_ID);
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentAllProductListBinding fragmentProductListBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_all_product_list, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProductListBinding);
            return fragmentProductListBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            parentView = getActivity().findViewById(android.R.id.content);

            if (getContext() != null) {

                  mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  mCategoryBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  bottomBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_box_layout, null, false));
                  bottomCategoriesBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_categories_box_layout, null, false));

                  mBottomSheetDialog.get().setContentView(bottomBoxLayoutBinding.get().getRoot());

                  mCategoryBottomSheetDialog.get().setContentView(bottomCategoriesBoxLayoutBinding.get().getRoot());
            }

            binding.get().typeButton.setOnClickListener(this::ButtonClick);

            binding.get().tuneButton.setOnClickListener(this::ButtonClick);

            binding.get().sortButton.setOnClickListener(this::ButtonClick);

            binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.view__primary_line));

            binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.global__primary));

            binding.get().swipeRefresh.setOnRefreshListener(() -> {
                  getListings(Constants.RANDOM);
            });

      }

      @Override
      protected void initViewModels() {
      }

      @Override
      protected void initAdapters() {
      }

      @Override
      protected void initData() {
            getListings(Constants.RANDOM);
      }

      private void getListings(String list) {

            final boolean[] empty = {true};

            Observable<ListingResponse> listingResponseObservable = mFilteringType != null ?
                    getShopListingResponseObservable(mFilteringType) : getListingResponseObservable() ;

            listingResponseObservable
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                if (getContext() != null ) {
                                      binding.get().setLoadingMore(true);
                                }
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                if (listingResponse.getInventories().size() > 0 ){
                                      empty[0] =false;
                                }
                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {
                                                  navigateToStore(obj);
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  navigateToLocation(getActivity());
                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {
                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      Log.e(TAG, "onNext: " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getListings  " + e.getMessage());
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                if (getContext() != null ) {
                                      binding.get().swipeRefresh.setRefreshing(false);
                                      binding.get().setLoadingMore(false);

                                      if (empty[0]){
                                            binding.get().noProductsLinearLayout.setVisibility(View.VISIBLE);
                                      }else {
                                            binding.get().noProductsLinearLayout.setVisibility(View.GONE);
                                      }

                                }

                          }
                    });
      }

      private Observable<ListingResponse> getListingResponseObservable() {
            return mAPIService.getListings()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      private Observable<ListingResponse> getShopListingResponseObservable(String filter) {

            return mAPIService.getSingleShopListing(mShopId, filter)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      private void tuneButtonClicked(Boolean b) {

            startActivity(new Intent(getContext(), MainSearchActivity.class));
      }

      private void ButtonClick(View view) {

            switch (view.getId()) {
                  case R.id.typeButton:

                        typeButtonClicked(typeClicked);

                        break;

                  case R.id.tuneButton:

                        tuneButtonClicked(filterClicked);

                        break;

                  case R.id.sortButton:

                        mBottomSheetDialog.get().show();
                        ButtonSheetClick();

                        break;

                  default:
                        Utils.psLog("No ID for Buttons");
            }
      }

      private void ButtonSheetClick() {

            bottomBoxLayoutBinding.get().popularButton.setOnClickListener(view -> {
                  getShopListings("new_arrivals");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().recentButton.setOnClickListener(view -> {
                  getShopListings("new_arrivals");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().lowestButton.setOnClickListener(view -> {
                  getShopListings("low_to_high");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().highestButton.setOnClickListener(view -> {
                  getShopListings("high_to_low");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().newButton.setOnClickListener(view -> {
                  getListingCondition("condition", "New");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().oldButton.setOnClickListener(view -> {
                  getListingCondition("condition", "Used");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().freeDeliveryButton.setOnClickListener(view -> {
                  getShopListings("free_shipping");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().hasOffers.setOnClickListener(view -> {
                  getShopListings("has_offers");
                  mBottomSheetDialog.get().dismiss();
            });
      }

      private void typeButtonClicked(Boolean b) {
            mCategoryBottomSheetDialog.get().show();

            getAllCategories();
      }

      private void getShopListings(String all) {

            final boolean[] empty = {true};

            mAPIService.getShopsListing(all)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                                try {
                                      binding.get().setLoadingMore(true);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                if (listingResponse.getInventories().size() > 0){
                                      empty[0] = false;
                                }
                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {
                                                  navigateToStore(obj);
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {
                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext: ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                binding.get().setLoadingMore(false);
                                logError(TAG, e, "onError: getListings  ");
                          }

                          @Override
                          public void onComplete() {
                                binding.get().setLoadingMore(false);

                                if (empty[0]){
                                      binding.get().noProductsLinearLayout.setVisibility(View.VISIBLE);
                                }else {
                                      binding.get().noProductsLinearLayout.setVisibility(View.GONE);
                                }

                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private void getListingCondition(String filter, String condition) {

            final boolean[] empty = {true};

            mAPIService.getListingCondition(filter, condition)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                                binding.get().setLoadingMore(true);
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                if (listingResponse.getInventories().size() > 0){
                                      empty[0] = false;
                                }
                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {

                                                  navigateToStore(obj);

                                            } else if (view == view.findViewById(R.id.addressTextView)) {

                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {

                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      binding.get().setLoadingMore(false);

                                      logError(TAG, e, "onNext: ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG, e, "onError: getListings  ");
                          }

                          @Override
                          public void onComplete() {
                                binding.get().setLoadingMore(false);

                                if (empty[0]){
                                      binding.get().noProductsLinearLayout.setVisibility(View.VISIBLE);
                                }else {
                                      binding.get().noProductsLinearLayout.setVisibility(View.GONE);
                                }

                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private void getAllCategories(){

            mAPIService.getAllCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CategoryResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull CategoryResponse categoryResponse) {

                                try {

                                      if (categoryResponse.getCategories().size() > 0) {
                                            CategoryAdapter productCategoryAdapter = new CategoryAdapter(getContext(), categoryResponse.getCategories());
                                            bottomCategoriesBoxLayoutBinding.get().productCategoriesRecyclerView
                                                    .setAdapter(productCategoryAdapter);

                                            productCategoryAdapter.setOnItemClickListener(new CategoryAdapter.OnItemClickListener() {
                                                  @Override
                                                  public void onItemClick(View view, ProductCategory obj, int pos) {
                                                        getListings("", String.valueOf(obj.getId()));
                                                        mCategoryBottomSheetDialog.get().dismiss();
                                                  }
                                            });
                                      }

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                          }

                          @Override
                          public void onComplete() {
                          }
                    });
      }

      private void getListings(String shop_id, String category_id) {

            getListingResponseObservable(category_id, shop_id)
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                if (getContext() != null) {
                                      binding.get().setLoadingMore(true);
                                }
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {
                                                  navigateToStore(obj);
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  navigateToLocation(getActivity());
                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {
                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      Log.e(TAG, "onNext: " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getListings  " + e.getMessage());
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                if (getContext() != null) {
                                      binding.get().setLoadingMore(false);
                                      binding.get().swipeRefresh.setRefreshing(false);
                                }
                          }

                    });
      }

      private Observable<ListingResponse> getListingResponseObservable(String category_id,  String shop_id) {
            return mAPIService.getProductsByShopCategory(category_id, shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }




}
