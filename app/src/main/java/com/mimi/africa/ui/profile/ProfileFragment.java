package com.mimi.africa.ui.profile;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentProfileBinding;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseDialogFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.user.UserViewModel;

import javax.inject.Inject;


public class ProfileFragment extends BaseDialogFragment {

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentProfileBinding> binding;
      private APIService mAPIService;

      private UserViewModel userViewModel;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      public ProfileFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            // Inflate the layout for this fragment
            FragmentProfileBinding fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProfileBinding);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }
            return fragmentProfileBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            Context context = getContext();

            binding.get().userNameTextView.setText(userName);
            binding.get().nameTextView.setText(fullName);
            binding.get().emailTextView.setText(email);
            binding.get().phoneNumberTextView.setText(phoneNumber);
            binding.get().addressTextView.setText(CustomSharedPrefs.getUserAddress(getContext()));
            binding.get().cityTextView.setText(city);

            binding.get().btClose.setOnClickListener(v -> dismiss());

            binding.get().btSaveInput.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                        pref.edit().putString(Constant.USER_NAME,   binding.get().userNameTextView.getText().toString()).apply();
                        pref.edit().putString(Constant.FULL_NAME,   binding.get().nameTextView.getText().toString()).apply();
                        pref.edit().putString(Constant.USER_EMAIL, binding.get().emailTextView.getText().toString()).apply();
                        pref.edit().putString(Constant.ADDRESS, binding.get().addressTextView.getText().toString()).apply();
                        pref.edit().putString(Constant.CITY, binding.get().cityTextView.getText().toString()).apply();

                        Utils.ShowSnackBar(getActivity(), "Successfully saved!", Snackbar.LENGTH_LONG);

                        getActivity().startActivity(new Intent(getContext(), MainHomeActivity.class));
                  }
            });
      }

      @Override
      protected void initViewModels() {
            userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {

      }

      private void initToolbar(View view) {
//            Toolbar toolbar =  view.findViewById(R.id.toolbar);
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setTitle(null);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            Tools.setSystemBarColor(this, R.color.pink_900);
      }
}
