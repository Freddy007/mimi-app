package com.mimi.africa.ui.chat;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.event.ChatClickedEvent;
import com.mimi.africa.listener.CustomTouchListener;
import com.mimi.africa.model.Chat;
import com.mimi.africa.model.Reply;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ItemViewHolder> {
    private static List<Chat> dataList;
    private LayoutInflater mInflater;
    private Context context;
    private OnClickListener onClickListener = null;
    private OnClickListener OnLongClickListener = null;

    public ChatAdapter(Context ctx, List<Chat> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }
    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnItemClickListener(Chat chat, OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnLongClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
//
    public void setOnItemLongClickListener(Chat chat, OnClickListener   onClickListener) {
        this.OnLongClickListener = onClickListener;
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout receiveLayout;
        private RelativeLayout sendLayout;
        private ImageView sendImage;
        private TextView receiveMessage;
        private TextView receiveTime;
        private TextView sendMessage;
        private LinearLayout sendMessageLayout;
        private TextView sendTime;
        private RecyclerView chatReplyRV;

        public ItemViewHolder(View itemView) {
            super(itemView);

            receiveLayout = itemView.findViewById(R.id.receiveLayout);
            sendLayout = itemView.findViewById(R.id.sendLayout);
            sendImage = itemView.findViewById(R.id.sendImage);
            sendMessage = itemView.findViewById(R.id.sendMessage);
            sendTime = itemView.findViewById(R.id.sendTime);
            chatReplyRV = itemView.findViewById(R.id.chatReplyRV);
            sendMessageLayout = itemView.findViewById(R.id.sendMessageLayout);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {

        Chat chat = dataList.get(position);

            holder.sendLayout.setVisibility(View.VISIBLE);
            holder.receiveLayout.setVisibility(View.VISIBLE);

            if (chat.getImages() != null && chat.getImages().size() > 0 ) {

                String url = Constants.IMAGES_BASE_URL + chat.getImages().get(0).getPath();
                Utils.LoadImage(context, holder.sendImage, url);
                holder.sendImage.setVisibility(View.VISIBLE);
                holder.sendMessage.setVisibility(View.GONE);

            }else {
                holder.sendMessage.setText(chat.getMessage());
                holder.sendImage.setVisibility(View.GONE);
                holder.sendMessage.setVisibility(View.VISIBLE);
            }
            holder.sendTime.setText(chat.getCreatedAt());

            if (chat.getReplies().size() > 0) {

                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

                holder.chatReplyRV.setHasFixedSize(false);
                holder.chatReplyRV.setLayoutManager(layoutManager);

                ChatReplyAdapter chatReplyAdapter = new ChatReplyAdapter(context, chat.getReplies());
                holder.chatReplyRV.setAdapter(chatReplyAdapter);
                chatReplyAdapter.setOnClickListener(new ChatReplyAdapter.OnClickListener() {
                    @Override
                    public void onItemClick(View view, Reply obj, int pos) {}

                    @Override
                    public void onItemLongClick(View view, Reply obj, int pos) {
//                        Toast.makeText(context, "from chat reply..", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new ChatClickedEvent(Html.fromHtml(obj.getReply()).toString()));
                    }
                });

            }else {
                holder.receiveLayout.setVisibility(View.GONE);
            }

        holder.sendMessage.setOnTouchListener(new CustomTouchListener());

            holder.sendMessage.setOnClickListener(v -> {
                if (onClickListener == null) return;
                onClickListener.onItemClick(v, chat, position);
            });

            holder.sendLayout.setOnLongClickListener(v -> {
                if (onClickListener == null) return false;
                onClickListener.onItemLongClick(v,chat,position);
                return true;
            });

        holder.sendMessage.setOnLongClickListener(v -> {
            if (onClickListener == null) return false;
            onClickListener.onItemLongClick(v,chat,position);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public interface OnClickListener {
        void onItemClick(View view, Chat obj, int pos);

        void onItemLongClick(View view, Chat obj, int pos);
    }

}
