package com.mimi.africa.ui.services;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityServicesBinding;
import com.mimi.africa.databinding.ActivityStoreBinding;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.event.LocationSearchEvent;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

public class ServicesActivity extends BaseActivity {

      private static final String TAG = ServicesActivity.class.getSimpleName();
      int AUTOCOMPLETE_REQUEST_CODE = 1;
      private TextView searchResultsTextView;


      private ActivityServicesBinding activityServicesBinding;

      public ProgressDialog progressDialog;


      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            activityServicesBinding = DataBindingUtil.setContentView(this, R.layout.activity_services);

            initComponents();
            initToolbar();
      }

      private void initToolbar() {
            initToolbar(findViewById(R.id.toolbar), "Services");
      }

      private void initComponents(){

            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);

            searchResultsTextView = findViewById(R.id.search_results_text);
            searchResultsTextView.setVisibility(View.GONE);

            ServicesFragment servicesFragment = new ServicesFragment();
            setupFragment(servicesFragment);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(DemoEvent event) {}

      private void autocompleteIntent(){

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.OVERLAY, fields)
                    .build(getApplicationContext());
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
                  if (resultCode == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place:  Google Places " + place.getName() + ", " + place.getId());

                        searchResultsTextView.setVisibility(View.VISIBLE);
                        searchResultsTextView.setText(String.format("Searching results for %s", place.getName()));

                        EventBus.getDefault().post(new LocationSearchEvent(place.getName()));

                  } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());
                        Log.i(TAG, "Google Places error ");

                        Utils.ShowSnackBar(this,"Oops, there was a problem.", Snackbar.LENGTH_SHORT);

                  } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                        Log.i(TAG, "Google Places Cancelled");

                  }
            }
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                  finish();
            }else if (item.getItemId() == R.id.action_search){
                  autocompleteIntent();
            }else if (item.getItemId() == R.id.action_main_search){
                  startActivity(new Intent(ServicesActivity.this, MainSearchActivity.class));
            }
            return super.onOptionsItemSelected(item);
      }

      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_search, menu);
            return true;
      }


}
