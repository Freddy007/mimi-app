package com.mimi.africa.ui.product;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentProductSizeBinding;
import com.mimi.africa.databinding.FragmentProfileBinding;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseDialogFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.user.UserViewModel;

import javax.inject.Inject;


public class ProductSizeGuideFragment extends BaseDialogFragment {

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentProductSizeBinding> binding;
      private APIService mAPIService;

      private UserViewModel userViewModel;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      public ProductSizeGuideFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            // Inflate the layout for this fragment
//            FragmentProfileBinding fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false, dataBindingComponent);
            FragmentProductSizeBinding fragmentProductSizeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_size, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProductSizeBinding);

            return fragmentProductSizeBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            Context context = getContext();

            binding.get().btClose.setOnClickListener(v -> dismiss());

      }

      @Override
      protected void initViewModels() {
      }

      @Override
      protected void initAdapters() {

      }



      @Override
      protected void initData() {

      }

}
