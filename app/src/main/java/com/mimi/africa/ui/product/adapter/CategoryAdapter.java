package com.mimi.africa.ui.product.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemProductVerticalListAdapterBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Product;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

      private List<ProductCategory> items = new ArrayList<>();

      private Context ctx;
      private OnItemClickListener mOnItemClickListener;
      private OnMoreButtonClickListener onMoreButtonClickListener;
      private String TAG = CategoryAdapter.class.getSimpleName();

      public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mOnItemClickListener = mItemClickListener;
      }

      public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
            this.onMoreButtonClickListener = onMoreButtonClickListener;
      }

      public CategoryAdapter(Context context, List<ProductCategory> items) {
            this.items = items;
            ctx = context;
      }

      public class OriginalViewHolder extends RecyclerView.ViewHolder {
            public CardView imageCardView;
            public TextView nameTextView;


            public OriginalViewHolder(@NonNull View v) {
                  super(v);

                  nameTextView = v.findViewById(R.id.name);

            }
      }

      @NonNull
      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_adapter, parent, false);
            vh = new OriginalViewHolder(v);
            return vh;
      }

      @Override
      public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof OriginalViewHolder) {
                  OriginalViewHolder view = (OriginalViewHolder) holder;
                  final ProductCategory productCategory = items.get(position);

                  view.nameTextView.setText(productCategory.getName());

                  view.nameTextView.setOnClickListener(v -> {
                        if (mOnItemClickListener != null) {
                              mOnItemClickListener.onItemClick(view.nameTextView, items.get(position), position);
                        }
                  });

            }
      }

      @Override
      public int getItemCount() {
            return items.size();
      }

      public interface OnItemClickListener {
            void onItemClick(View view, ProductCategory obj, int pos);
      }

      public interface OnMoreButtonClickListener {
            void onItemClick(View view, ProductCategory obj, MenuItem item);
      }

}