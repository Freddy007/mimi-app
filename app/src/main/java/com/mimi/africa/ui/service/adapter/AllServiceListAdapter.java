package com.mimi.africa.ui.service.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemVerticalListBinding;
import com.mimi.africa.model.Service;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AllServiceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

      private List<Service> items = new ArrayList<>();
      private ItemVerticalListBinding itemVerticalListBinding;

      private Context ctx;
      private OnItemClickListener mOnItemClickListener;
      private OnMoreButtonClickListener onMoreButtonClickListener;

      public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mOnItemClickListener = mItemClickListener;
      }

      public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
            this.onMoreButtonClickListener = onMoreButtonClickListener;
      }

      public AllServiceListAdapter(Context context, List<Service> items) {
            this.items = items;
            ctx = context;
      }

      public class OriginalViewHolder extends RecyclerView.ViewHolder {
            public CardView imageCardView;
            public TextView shopNameTextView;
            public ImageView image;
            public ImageView productImageView;
            public TextView title;
            public TextView price;
            public TextView addressTextView;
            public TextView isSoldTextView;
            public TextView addedDateStrTextView;
            public ImageButton more;
            public RatingBar ratingBar;
            public TextView ratingBarTextView;


            public OriginalViewHolder(@NonNull View v) {
                  super(v);
                   title =v.findViewById(R.id.news_title_textView);
                  imageCardView =v.findViewById(R.id.newsHolderCardView);
                  productImageView = v.findViewById(R.id.imageView);
                  title = v.findViewById(R.id.news_title_textView);
                  price = v.findViewById(R.id.priceTextView);
                  ratingBar                                     = v.findViewById(R.id.ratingBar);
                  ratingBarTextView                   = v.findViewById(R.id.ratingBarTextView);

            }
      }

      @NonNull
      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
//            itemVerticalListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product_vertical_list_adapter, parent, false);
//            final View itemView = itemVerticalListBinding.getRoot();

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_vertical_list_adapter, parent, false);
            vh = new OriginalViewHolder(v);
            return vh;
      }

      // Replace the contents of a view (invoked by the layout manager)
      @Override
      public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof OriginalViewHolder) {
                  OriginalViewHolder view = (OriginalViewHolder) holder;
                  final Service p = items.get(position);
                  view.title.setText(p.getName());
                  view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(p.getPrice()))));

                  view.ratingBar.setRating((float) p.getRating());

                  view.ratingBarTextView.setText(String.format("%s rating  (%d Reviews )", p.getRating(), p.getSumFeedbacks()));


//                  if (p.getShop() != null) {
//                        view.shopNameTextView.setText(p.getShop().getName());
//                  }

//            view.shopNameTextView.setText(p.getShop().getName());
//
//            if (p.getStockQuantity() == 0) {
//                view.isSoldTextView.setVisibility(View.VISIBLE);
//            }

//            String dateCreatedString = p.getCreatedAt().getDate() ;
//
//            long milliseconds = Utils.convertDateTimeToMilliseconds(dateCreatedString);
//
//            String timeAgo = TimeAgo.getTimeAgo(milliseconds);
//
//            view.addedDateStrTextView.setText(timeAgo);

                  try{
                        if (p.getImages().size() > 0) {
                              String imageUrl = Constants.IMAGES_BASE_URL + p.getImages().get(0).getPath();
                              Log.i("ServiceCardAdapter ", "onNext onBindViewHolder:  " + imageUrl);
                              Picasso.get().load(imageUrl).into(view.productImageView);
//                              Picasso.with(ctx).load(imageUrl).into(view.productImageView);

                        }
                  }catch (Exception e){
//                Log.e(TAG, "onBindViewHolder: ", );
                  }


//            if (p.getImage() != null){
//                Picasso.with(ctx).load(p.getImage().getPath()).into(view.productImageView);
//            }

//            Log.i(TAG, "onBindViewHolder:  Product id " + p.getProduct().getId());

                  view.imageCardView.setOnClickListener(v -> {
                        if (mOnItemClickListener != null) {
                              mOnItemClickListener.onItemClick(view.imageCardView, items.get(position), position);
                        }
                  });

//                  view.addressTextView.setOnClickListener(v -> {
//                        if (mOnItemClickListener != null) {
//                              mOnItemClickListener.onItemClick(view.addressTextView, items.get(position), position);
//                        }
//                  });

                  view.price.setOnClickListener(v -> {
                        if (mOnItemClickListener != null) {
                              mOnItemClickListener.onItemClick(view.price, items.get(position), position);
                        }
                  });

//                  view.shopNameTextView.setOnClickListener(v -> {
//                        if (mOnItemClickListener != null) {
//                              mOnItemClickListener.onItemClick(view.shopNameTextView, items.get(position), position);
//                        }
//                  });

//            view.title.setText(p.title);
//            view.price.setText(p.price);
//            Tools.displayImageOriginal(ctx, view.locationImage, p.locationImage);

//            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mOnItemClickListener != null) {
//                        mOnItemClickListener.onItemClick(view, items.get(position), position);
//                    }
//                }
//            });

//            view.more.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (onMoreButtonClickListener == null) return;
//                    onMoreButtonClick(view, p);
//                }
//            });
            }
      }

//    private void onMoreButtonClick(final View view, final ShopProduct p) {
//        PopupMenu popupMenu = new PopupMenu(ctx, view);
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                onMoreButtonClickListener.onItemClick(view, p, item);
//                return true;
//            }
//        });
//        popupMenu.inflate(R.menu.menu_product_more);
//        popupMenu.show();
//    }

      @Override
      public int getItemCount() {
            return items.size();
//        return 3;
      }

      public interface OnItemClickListener {
            void onItemClick(View view, Service obj, int pos);
      }

      public interface OnMoreButtonClickListener {
            void onItemClick(View view, Service obj, MenuItem item);
      }

}