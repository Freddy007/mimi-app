package com.mimi.africa.ui.shop;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.mimi.africa.R;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.OnOptionItemSelected;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.product.adapter.CategoryAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ShopListActivity extends BaseActivity {

      private Toolbar toolbar ;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_shop_list);
            initComponents();
      }

      private void initComponents(){
            toolbar = findViewById(R.id.toolbar);
            initToolbar(toolbar, "Shop list");
            setupFragment(new ShopListFragment());
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void addToCart(AddToCart event){}

      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.locations, menu);
            return true;
      }


      @Override
      public boolean onOptionsItemSelected(@Nullable MenuItem item) {

            EventBus.getDefault().post(new OnOptionItemSelected(item, ""));

            return super.onOptionsItemSelected(item);
      }
}
