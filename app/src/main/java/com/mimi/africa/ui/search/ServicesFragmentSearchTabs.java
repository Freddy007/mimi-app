package com.mimi.africa.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.Adapter.ServiceVerticalListAdapter;
import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomFavouriteOptionsLayoutBinding;
import com.mimi.africa.databinding.FragmentServiceListBinding;
import com.mimi.africa.databinding.ServicesFragmentSearchTabsBinding;
import com.mimi.africa.event.Reload;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.model.Service;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ServicesFragmentSearchTabs extends BaseFragment {

    public static final String  ALL_TAB = "All";
    public static final String PRODUCTS_TAB = "Products";
    public static final String SERVICES_TAB = "Services";
    public static final String STORES_TAB = "Stores";
    private boolean reloadPage;

    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;

    private static final String TAG = ServicesFragmentSearchTabs.class.getSimpleName();

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private  View parentView;
    private AutoClearedValue<ServicesFragmentSearchTabsBinding> binding;
    private AutoClearedValue<BottomFavouriteOptionsLayoutBinding> bottomFavouriteOptionsLayoutBinding;
    private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;

    @Inject
     APIService mAPIService;
    private String mTerm;


    public ServicesFragmentSearchTabs() {
    }

    @NonNull
    public static ServicesFragmentSearchTabs newInstance( String term) {
        ServicesFragmentSearchTabs servicesFragmentSearchTabs = new ServicesFragmentSearchTabs();
        Bundle bundle = new Bundle();
        bundle.putString("term", term);
        servicesFragmentSearchTabs.setArguments(bundle);
        return servicesFragmentSearchTabs;
    }

    @NonNull
    public static ServicesFragmentSearchTabs newInstance() {
        return new ServicesFragmentSearchTabs();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null ){
            mTerm = getArguments().getString("term");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ServicesFragmentSearchTabsBinding servicesFragmentSearchTabsBinding = DataBindingUtil.inflate(inflater, R.layout.services_fragment_search_tabs, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, servicesFragmentSearchTabsBinding);

        return servicesFragmentSearchTabsBinding.getRoot();

    }

    @Override
    protected void initUIAndActions() {
        reloadPage = false;
        parentView= getActivity().findViewById(android.R.id.content);

        mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
        bottomFavouriteOptionsLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_favourite_options_layout, null, false));
        mBottomSheetDialog.get().setContentView(bottomFavouriteOptionsLayoutBinding.get().getRoot());
    }

    @Override
    protected void initViewModels() {

    }

    @Override
    protected void initAdapters() {
        binding.get().productsRecyclerView
                .setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));

    }

    @Override
    protected void initData() {
        if (CustomSharedPrefs.getReloadPageStatus(getContext())) {
            if (mTerm != null && mTerm.equals("wishlists")){
                getWishLists();
            }else {
                getServiceListings();
            }

        }else {
            Utils.ShowView(binding.get().noItemConstraintLayout);
            binding.get().noItemDescTextView.setText(getResources().getString(R.string.search_message));
            binding.get().noItemTitleTextView.setText("Search");
            Utils.RemoveView(binding.get().productsProgressBar);
        }
    }

    private void getServiceListings(){

        final boolean[] empty = {true};

        getServiceOffersResponseObservable(mAPIService, "60")
                .subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                        try {

                            Utils.ShowView(binding.get().productsProgressBar);

                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onNext(@NonNull ServiceResponse listingResponse) {
                        initializeAdapter(listingResponse, empty);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Service Listings onError:  "  + e.getMessage() );

                        try {
                            Utils.RemoveView(binding.get().productsProgressBar);
                            Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading services!", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("RELOAD!", view -> getServiceListings());
                            snackbar.show();
                        }catch (Exception ex){
                            Log.e(TAG, "onError:  " +e.getMessage() );
                        }
                    }

                    @Override
                    public void onComplete() {
                        try{
                            if (empty[0] ){

                                Utils.RemoveView(binding.get().productsProgressBar);
                                Utils.RemoveView(binding.get().productsRecyclerView);
                                Utils.ShowView(binding.get().noItemConstraintLayout);
                                binding.get().noItemDescTextView.setText("No services offer!");

                            }else {
                                Utils.RemoveView(binding.get().productsProgressBar);
                                Utils.ShowView(binding.get().productsRecyclerView);
                                Utils.RemoveView(binding.get().noItemConstraintLayout);

                            }

                        }catch (Exception e){
                            Log.e(TAG, "onComplete:  " +e.getMessage() );
                        }
                    }
                });
    }

    private void initializeAdapter(@NonNull ServiceResponse listingResponse, boolean[] empty) {
        try {

            if (listingResponse.getServices().size() > 0 ) {

                empty[0] = false;

                ServiceVerticalListAdapter serviceVerticalListAdapter =
                        new ServiceVerticalListAdapter(getContext(), listingResponse.getServices());

                binding.get().productsRecyclerView.setAdapter(serviceVerticalListAdapter);

                serviceVerticalListAdapter.setOnItemClickListener(new ServiceVerticalListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Service obj, int pos) {

                        if (view == view.findViewById(R.id.nameTextView)) {
                            startActivity(new Intent(getContext(), StoreActivity.class));

                        } else if (view == view.findViewById(R.id.addressTextView)) {

                            startActivity(new Intent(getContext(), LocationActivity.class));

                            navigateToLocation(getActivity());

                        } else if (view == view.findViewById(R.id.productImageView)) {

                            navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(pos));
                        }else{
                            navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(pos));
                        }
                    }

                    @Override
                    public void onItemLongClick(View view, Service obj, int pos) {

                        bottomFavouriteOptionsLayoutBinding.get().readStatus.setVisibility(View.GONE);

                        mBottomSheetDialog.get().show();
                        bottomFavouriteOptionsLayoutBinding.get().titleTextView.setText(obj.getName());


                        bottomFavouriteOptionsLayoutBinding.get().archiveMessage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                removeFromWishList(String.valueOf(obj.getId()));
                            }
                        });

                    }
                });
            }

        }catch (Exception e){
            Log.e(TAG, "onNext:  " +e.getMessage() );
        }
    }

    private void searchServices(String term){

        final boolean[] empty = {false};

        getSearchServiceResponseObservable(mAPIService, term )
                .subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                        Utils.ShowView(binding.get().productsProgressBar);
                    }

                    @Override
                    public void onNext(@NonNull ServiceResponse listingResponse) {
                        initializeAdapter(listingResponse, empty);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Service Listings onError:  "  + e.getMessage() );

                        try {
                            Utils.RemoveView(binding.get().productsProgressBar);
                            Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading services!", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("RELOAD!", view -> getServiceListings());
                            snackbar.show();
                        }catch (Exception ex){
                            Log.e(TAG, "onError:  " +e.getMessage() );
                        }
                    }

                    @Override
                    public void onComplete() {
                        try{

                            if (empty[0]){
                                Utils.ShowView(   binding.get().noItemConstraintLayout);
                                Utils.RemoveView(binding.get().productsRecyclerView);
                                Utils.RemoveView(   binding.get().productsProgressBar);

                            }else {
                                Utils.ShowView(binding.get().productsRecyclerView);
                                Utils.RemoveView(binding.get().productsProgressBar);
                                Utils.RemoveView(   binding.get().noItemConstraintLayout);

                            }

                        }catch (Exception e){
                            Log.e(TAG, "onComplete:  " +e.getMessage() );
                        }

                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(SearchEvent event) {
        searchServices(event.getTerm());
    }

    private void getWishLists() {
//
        String customerId = loginUserId;
        final boolean[] empty = {false};

        mAPIService.getProductWishLists(customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ServiceResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;

                        Utils.ShowView(binding.get().productsProgressBar);
//                        subscribeRequest();

                    }

                    @Override
                    public void onNext(@NonNull ServiceResponse serviceResponse) {
                        initializeAdapter(serviceResponse, empty);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        try {
                            Utils.RemoveView(binding.get().productsProgressBar);
                            Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading services!", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("RELOAD!", view -> getServiceListings());
                            snackbar.show();
                        }catch (Exception ex){
                            Log.e(TAG, "onError:  " +e.getMessage() );
                        }

                    }

                    @Override
                    public void onComplete() {
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }

                        if (empty[0]){
                            Utils.ShowView(   binding.get().noItemConstraintLayout);
                            Utils.RemoveView(binding.get().productsRecyclerView);
                            Utils.RemoveView(   binding.get().productsProgressBar);

                        }else {
                            Utils.ShowView(binding.get().productsRecyclerView);
                            Utils.RemoveView(binding.get().productsProgressBar);
//                                binding.get().noItemConstraintLayout.setVisibility(View.VISIBLE);
                            Utils.RemoveView(   binding.get().noItemConstraintLayout);

                        }
//                        completeRequest(empty);
                    }
                });
    }

    private void removeFromWishList(String productId){
        String customer_id = loginUserId;
        mAPIService.removeFromWishList(customer_id,productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        mBottomSheetDialog.get().dismiss();
                        binding.get().setLoadingMore(true);
                        disposable = d;
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                        if (getActivity() != null)
                            Utils.ShowSnackBar(getActivity(), "Successfully removed from wishlist!", Snackbar.LENGTH_LONG);
                        getWishLists();
                        EventBus.getDefault().post(new Reload());
                        binding.get().setLoadingMore(false);

                    }
                });
    }



}