package com.mimi.africa.ui.shop;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.mimi.africa.Adapter.AdapterImageSlider;
import com.mimi.africa.Adapter.ServiceCardAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentStoreBinding;
import com.mimi.africa.event.ShopMoreClickedEvent;
import com.mimi.africa.event.ShopViewedEvent;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.shop.adapter.ProductCategoryAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CircleTransform;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.LocationUtilService;
import com.mimi.africa.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreFragment extends BaseFragment {

      private static final String TAG = StoreFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentStoreBinding>binding;
      private ShopProductGridCardAdapter mProductsAdapter;

      @Nullable
      private Shop mShop;
      private AdapterImageSlider adapterImageSlider;

      private ViewPager viewPager;

      @Nullable
      private Runnable runnable = null;
      @NonNull
      private Handler handler = new Handler();
      private Activity mActivity;
      private Context mContext;


      @Inject
       APIService mAPIService;

      public StoreFragment() {
            // Required empty public constructor
      }

      @NonNull
      public static StoreFragment newInstance(Shop shop){
            StoreFragment storeFragment = new StoreFragment();
            Bundle args = new Bundle();
            args.putParcelable(Constants.SHOP_OBECT, shop);
            storeFragment.setArguments(args);

            return storeFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null){
                  mShop = getArguments().getParcelable(Constants.SHOP_OBECT);
                  EventBus.getDefault().post(new ShopViewedEvent(mShop));
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            FragmentStoreBinding fragmentStoreBinding = DataBindingUtil
                    .inflate(inflater, R.layout.fragment_store, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentStoreBinding);

            mActivity = getActivity();
            mContext = getContext();

            return fragmentStoreBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            ImageButton imageButton = binding.get().addressImageButton;

          imageButton.setOnClickListener(v -> {
                EventBus.getDefault().post(new ShopMoreClickedEvent(mShop));
          });

            adapterImageSlider = new AdapterImageSlider(mActivity, new ArrayList<>());

            if (mShop != null ) {

                  if (mShop.getImage() != null) {
//                        Utils.LoadImage(getContext(), binding.get().logoImage, mShop.getImage().getPath());
                  }

                  if (mShop.getOwner() != null && mShop.getOwner().getImage() != null) {
                        String imageUrl = Constants.IMAGES_BASE_URL + mShop.getOwner().getImage().getPath();
                        Picasso.get().load(imageUrl).transform(new CircleTransform()).into(binding.get().merchantImage);
//                        Picasso.with(getContext()).load(imageUrl).transform(new CircleTransform()).into(binding.get().merchantImage);
                  }

                  if ( mShop.getLat() != null) {
                        String latitude = mShop.getLat();
                        String longitude = mShop.getLng();

                        if (LocationPreferences.isLocationLatLonAvailable(getContext())) {

                              double latitude_dbl = Double.parseDouble(latitude);
                              double longitude_dbl = Double.parseDouble(longitude);

                              android.location.Location endLocation = new android.location.Location("New Location");
                              endLocation.setLatitude(latitude_dbl);
                              endLocation.setLongitude(longitude_dbl);

                              double locationServiceLastLocation = LocationUtilService.getDistanceInKm(getContext(), endLocation);
                              //   binding.get().storeAddressTextview.setText(String.format("%s km away", String.format("%.2f", locationServiceLastLocation)));
                              binding.get().storeAddressTextview.setText(mShop.getAddress());
                        }
                  }

            }

      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {
            if (getActivity() != null)
            binding.get().productCategoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            binding.get().servicesRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
      }

      @Override
      protected void initData() {
            getAllCategories(String.valueOf(mShop.getId()));
            getShopService(String.valueOf(mShop.getId()));
      }


      private void getAllCategories(String shop_id){

            mAPIService.getCategoryShopListings(shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<ProductCategory>>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull List<ProductCategory> productCategories) {

                                try {
                                      ProductCategoryAdapter productCategoryAdapter = new ProductCategoryAdapter(getContext(), productCategories);
                                      binding.get().productCategoriesRecyclerView.setAdapter(productCategoryAdapter);

                                      Log.i(TAG, "onNext:  Categories size " + productCategories.size());

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {}

                          @Override
                          public void onComplete() {}
                    });
      }

      private void getCategoryListings(String slug){

            mAPIService.getCategoryListings(slug)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(ListingResponse listingResponse) {

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getShop Listing  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                          }
                    });

      }

      private void getShopService(String shop_id){

            final boolean[] empty = {false};

            getShopServicesResponseObservable(mAPIService, "15", shop_id)
                    .subscribe(new Observer<ServiceResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ServiceResponse listingResponse) {

                                try{

                                      if (listingResponse.getServices().size() > 0){
                                            ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), listingResponse.getServices());
                                            binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);

                                            serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {

                                                  if (view == view.findViewById(R.id.nameTextView)) {
//                                                  startActivity(new Intent(getContext(), StoreActivity.class));

                                                        navigateToStore(obj.getShop());

                                                  } else if (view == view.findViewById(R.id.addressTextView)){

                                                        startActivity(new Intent(getContext(), LocationActivity.class));

//
//                                                  navigateToLocation(getActivity());

                                                  }else if (view == view.findViewById(R.id.cardView12)){

                                                        navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
                                                  }

                                            });
                                      }else {
                                            empty[0] = true;
                                      }


                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " +e.getMessage() );
                                }

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "Service Listings onError:  "  + e.getMessage() );
                          }


                          @Override
                          public void onComplete() {
                                try {
                                      if (!empty[0]){
                                            Utils.ShowView(binding.get().servicesRecyclerView);
                                            Utils.ShowView(binding.get().allServices);
                                            Utils.ShowView(binding.get().serviceHeadLayout);
                                      }else {
                                            Utils.RemoveView(binding.get().servicesRecyclerView);
                                            Utils.RemoveView(binding.get().allServices);
                                            Utils.RemoveView(binding.get().serviceHeadLayout);
                                      }
//                                      Utils.RemoveView(binding.get().servicesProgressBar);

                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " + e.getMessage() );
                                }
                          }
                    });
      }

}
