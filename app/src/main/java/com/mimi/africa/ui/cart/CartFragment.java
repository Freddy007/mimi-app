package com.mimi.africa.ui.cart;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.ui.cart.adapter.CartAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentCartBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.auth.LoginActivity;
import com.mimi.africa.ui.checkout.CheckoutActivity;
import com.mimi.africa.ui.common.BaseDialogFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class CartFragment extends BaseDialogFragment {


      private static final String TAG = CartFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentCartBinding> binding;
      private StringBuffer cartIds_sb;
      private Activity mActivity;
      private Context mContext;
      private Handler mHandler;

      @Inject
       APIService mAPIService;

      public CartFragment() {}


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentCartBinding fragmentCartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentCartBinding);
            mActivity= getActivity();
            mContext = getContext();
            mHandler = new Handler();

            return fragmentCartBinding.getRoot();

      }

      @Override
      protected void initUIAndActions() {

            cartIds_sb = new StringBuffer();

            binding.get().checkoutButton.setOnClickListener(v -> {

                  if (Utils.isUserLoggedIn(mContext)) {
                        startActivity(new Intent(mContext, CheckoutActivity.class));
                  } else {
                        startActivity(new Intent(mContext, LoginActivity.class));
                  }

            });
            binding.get().btClose.setOnClickListener(v -> dismiss());

      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {
            getCarts();
      }

      private void getCarts() {

            mAPIService.getCarts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CartResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(@NonNull CartResponse cartResponse) {
                                binding.get().itemsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

                                List<Inventory> cartList = new ArrayList<>();
                                double total = 0;

                                for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                                      cartIds_sb.append(cartResponse.getCarts().get(i).getId());

                                      if ( i != (cartResponse.getCarts().size() -1) && cartResponse.getCarts().size() > 1){
                                            cartIds_sb.append(",");
                                      }

                                      if (cartResponse.getCarts().get(i).getItems() != null) {
                                            cartList.addAll(cartResponse.getCarts().get(i).getItems());
                                      }

                                      if (cartResponse.getCarts().get(i) != null) {
                                            int qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());
                                            total += Double.parseDouble(cartResponse.getCarts().get(i).getTotal() );
                                      }

                                }

                                CartAdapter cartAdapter = new CartAdapter(mContext, cartList);

                                try {

                                      binding.get().itemsRecyclerview.setAdapter(cartAdapter);

                                      cartAdapter.setOnItemClickListener((view, obj, pos) -> {
//                                            Utils.ShowSnackBar(getActivity(), "Hello ", Snackbar.LENGTH_LONG);
//                                            EventBus.getDefault().post(new AddToCart());
                                      });
                                      String str = String.format("%,d", Integer.parseInt(Utils.TwoDecimalPlace(total)));

                                      binding.get().totalAmount.setText(String.format("GHS %s", str));

                                } catch (Exception e) {

                                      Log.e(TAG, "onNext:  " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError:  " + e.getMessage());
                                Utils.ShowSnackBar(mActivity, "failed to load cart!", Snackbar.LENGTH_INDEFINITE);
                                try {
                                      binding.get().progressBar.setVisibility(View.GONE);
                                } catch (Exception ex) {
                                      Log.e(TAG, "onError:  " + ex.getMessage());
                                }
                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);
                                CustomSharedPrefs.removeCartIds(mContext);
                                CustomSharedPrefs.saveCartIds(getContext(), cartIds_sb.toString());

                                try {
                                      mHandler.postDelayed(() -> {
                                            try{
                                                  Utils.ShowView(binding.get().itemsRecyclerview);
                                                  Utils.ShowView(binding.get().checkoutButton);
                                                  Utils.ShowView(binding.get().totalLinearLayout);
                                                  Utils.RemoveView(binding.get().progressBar);
                                            }catch (Exception e){
                                                  Log.e(TAG, "onComplete: "  );
                                            }
                                      }, 1000);
                                } catch (Exception e) {
                                      Log.e(TAG, "onComplete:  " + e.getMessage());
                                }
                          }
                    });
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(@NonNull AddToCart event) {

                        addToCart(event.getQty(), event.getSlug());

                        CustomSharedPrefs.removeCartStatus(mContext);
                        CustomSharedPrefs.setCartStatus(getContext(), true);

      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

      private void addToCart(int qty, String slug){
          mAPIService.addToCart(qty,"","","" ,slug)
                  .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError:  " + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {

                                try {

                                      Utils.ShowSnackBar(mActivity, "Added to Cart!", Snackbar.LENGTH_LONG);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " + e.getMessage() );
                                }
                          }
                    });
      }


}
