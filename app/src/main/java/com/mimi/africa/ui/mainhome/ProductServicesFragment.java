package com.mimi.africa.ui.mainhome;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mimi.africa.Adapter.ProductVerticalListAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class ProductServicesFragment extends BaseFragment implements EcommerceStyle1ClickListener {

    private static final String TAG = ProductServicesFragment.class.getSimpleName();

    @Inject
    APIService mAPIService;

    int wizard_page_position;
    RecyclerView rView;



    public ProductServicesFragment() {
        // Required empty public constructor
    }


    public ProductServicesFragment(int position) {
      this.wizard_page_position = position;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_services, container, false);
        rView = view.findViewById(R.id.recyclerView);
        return view;
    }

    private void getNewArrivals() {

        mAPIService.getShopsListing("all")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {

                        try {
                            Log.i(TAG, "Product Services " + listingResponse.getInventories().size());

                            rView.setHasFixedSize(true);
                            rView.setNestedScrollingEnabled(false);
                            rView.setHasFixedSize(false);
                            rView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
                            ProductVerticalListAdapter productVerticalListAdapter = new ProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                            rView.setAdapter(productVerticalListAdapter);

//                            productVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

//                                if (getActivity() != null) {
//                                    if (view == view.findViewById(R.id.nameTextView)) {
//                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                        intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
//                                        startActivity(intent);
//
//                                    } else if (view == view.findViewById(R.id.addressTextView) || view == view.findViewById(R.id.imageView2)) {
//
////                                        goToShopLocation(obj.getShop());
//
//                                    } else if (view == view.findViewById(R.id.cardView12)) {
////                                        navigateToItemDetailActivity(mActivity, listingResponse.getInventories().get(position));
//
//                                    } else if (view == view.findViewById(R.id.favoriteImageView)) {
//
////                                        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg, MainHomeFragment.this.getActivity(), navigationController, () -> {
////                                            addToWishList(mAPIService, listingResponse.getInventories().get(position));
////                                            shopProductGridCardAdapter.notifyDataSetChanged();
////                                        });
//                                    }
//                                }
//                            });

                        } catch (Exception e) {
//                            logError(TAG, e, "onNext: ");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        logError(TAG, e, "onError: getListings  ");
                    }

                    @Override
                    public void onComplete() {
                        if (!disposable.isDisposed()) {
                            disposable.dispose();
                        }

                        try {
//                            if (getActivity() != null) {
//                                Utils.ShowView(binding.get().newArrivalsRecyclerView);
//                                Utils.RemoveView(binding.get().newArrivalsProgressBar);
//                            }

                        } catch (Exception e) {
                            Sentry.captureException(e);
                        }

                    }
                });
    }

    @Override
    protected void initUIAndActions() {

    }

    @Override
    protected void initViewModels() {

    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {
        getNewArrivals();
    }

    @Override
    public void itemClicked(View view, int position) {
        int num = position + 1;
        Toast.makeText(getActivity(), "Position " + num + " clicked!", Toast.LENGTH_SHORT).show();
    }
}