package com.mimi.africa.ui.product;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.api.response.FeedbackResponse;
import com.mimi.africa.api.response.ImageResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxBasketAndBuyBinding;
import com.mimi.africa.databinding.FragmentProductDetailsBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.CartClicked;
import com.mimi.africa.event.CheckOutReady;
import com.mimi.africa.event.OnClickEvent;
import com.mimi.africa.event.OnSizeGuideClicked;
import com.mimi.africa.event.ReviewItemClickedEvent;
import com.mimi.africa.event.ReviewsClickedEvent;
import com.mimi.africa.model.Attribute;
import com.mimi.africa.model.AttributeValue;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.model.Feedback;
import com.mimi.africa.model.Image;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Rating;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.product.adapter.ProductColorAdapter;
import com.mimi.africa.ui.product.adapter.ProductVariantAdapter;
import com.mimi.africa.ui.profile.ProfileFragment;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.BottomSheetDialogExpanded;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.utils.ViewAnimationUtil;
import com.mimi.africa.viewModels.BasketCountViewModel;
import com.mimi.africa.viewModels.ProductColor;
import com.mimi.africa.viewModels.ProductImageVariant;
import com.mimi.africa.viewModels.product.ProductColorViewModel;
import com.squareup.picasso.Picasso;
import com.viven.imagezoom.ImageZoomHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ProductDetailsFragment extends BaseFragment implements View.OnClickListener{

      @NonNull
      private String TAG = ProductDetailsActivity.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent =
              new FragmentDataBindingComponent(this);

      private AutoClearedValue<FragmentProductDetailsBinding> binding;
      private AutoClearedValue<BottomSheetDialogExpanded> mBottomSheetDialog;
      private AutoClearedValue<BottomBoxBasketAndBuyBinding> bottomBoxLayoutBinding;
      private AutoClearedValue<ProductColorAdapter> colorAdapter;
      private AutoClearedValue<ProductVariantAdapter> productImageVariantAdapter;
      private BasketCountViewModel basketCountViewModel;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;

      @Nullable
      private Inventory mInventory;
      private View parentView;
      Dialog progressDialog;
      private String mProductId;
      private ShopProductGridCardAdapter mProductsAdapter;
      private ImageZoomHelper imageZoomHelper;
      private ProductColorViewModel productColorViewModel;
      private ProductColorViewModel productImageVariantViewModel;
      private boolean twist = false;
      private int num = 1;
      private int minOrder = 1;
      private int mDeliveryCharge = 0;
      private   String mCoord;
      protected String  messenger, whatsappNo;
      private boolean colorSelected = false;
      private boolean variantSelected = false;
      private boolean sizeSelected = false;
      private boolean shoeSizeSelected = false;
      private boolean hasImageVariant = false;
      private boolean buyNowClicked = false;
      private String imageVariantSelected = "";
      private String shoeSizeVariantSelected = "";
      private String colorVariantSelected = "";

      @Inject
      APIService mAPIService;

      @Inject
      protected NavigationController navigationController;

      @Inject
      protected MimiDb mimiDb;

      private PSDialogMsg psDialogMsg;
      private List<Feedback> mFeedbacks;
      private List<String> mProductSizes;
      private List<String> mProductShoeSizes;
      private List<String> mProductGenders;
      private List<AttributeValue> attributeColors;
      private List<ProductColor>  mProductColors;
      private List<ProductColor> mProductImageVariants;
      private List<ProductColor> mTemporalImageLists;
      public ProductDetailsFragment() {}

      @NonNull
      public static  ProductDetailsFragment newInstance(Inventory inventory, String productId){

            ProductDetailsFragment fragment = new ProductDetailsFragment();
            Bundle args = new Bundle();
            args.putParcelable(Constants.INVENTORY_OBJECT, inventory );
            args.putString("product_id", productId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            attributeColors = new ArrayList<>();
            mProductId = "";

            if (getArguments() != null){
                  mInventory = getArguments().getParcelable(Constants.INVENTORY_OBJECT);
                  mProductId = getArguments().getString("product_id");
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentProductDetailsBinding fragmentProductDetailsBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProductDetailsBinding);

            View view =  fragmentProductDetailsBinding.getRoot();

            setHasOptionsMenu(true);

            basketCountViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketCountViewModel.class);

            return view;
      }

      @Override
      protected void initUIAndActions() {

            getBasketCountsViewModel();

            psDialogMsg = new PSDialogMsg(getActivity(), false);
            if (getContext() != null) {
                  mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialogExpanded(getContext()));
                  BottomBoxBasketAndBuyBinding bottomBoxLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_box_basket_and_buy, null, false, dataBindingComponent);
                  this.bottomBoxLayoutBinding = new AutoClearedValue<>(this, bottomBoxLayoutBinding);
                  mBottomSheetDialog.get().setContentView(this.bottomBoxLayoutBinding.get().getRoot());
            }

            messenger = "me";
            whatsappNo = "";
            String shopPhoneNumber = "";

            if (mInventory != null && mInventory.getShop() != null && mInventory.getShop().getAddresses() != null) {
                  if (mInventory.getShop().getAddresses().size() > 0) {
                        shopPhoneNumber = mInventory.getShop().getAddresses().get(0).getPhone();
                        whatsappNo = shopPhoneNumber;
                  }
            }

            progressDialog = new Dialog(getContext());

            hideFloatingButton();

            parentView = getActivity().findViewById(android.R.id.content);

            if (getContext() != null) {

                  binding.get().productTitle.setText(String.format("%s ( %s )", mInventory.getTitle(), mInventory.getCondition()));

                  double salePrice = Double.valueOf(mInventory.getSalePrice());

                  if (mInventory.getOfferPrice() != null) {
                        double offerPrice = Double.valueOf(mInventory.getOfferPrice());
                        binding.get().originalPriceTextView.setVisibility(View.VISIBLE);
                        binding.get().price.setText(String.format(" GHS %s ", Utils.TwoDecimalPlace(offerPrice)));
                        binding.get().originalPriceTextView.setPaintFlags(binding.get().originalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        binding.get().originalPriceTextView.setText((String.format(" GHS %s ", Utils.TwoDecimalPlace(salePrice))));
                  } else {
                        binding.get().originalPriceTextView.setVisibility(View.GONE);
                        binding.get().price.setText(String.format("GHS %s", Utils.TwoDecimalPlace(salePrice)));
                  }

                  if (mInventory.getShop() != null) {
                        binding.get().shopNameTextView.setText(mInventory.getShop().getName());
                        binding.get().shopName.setText(mInventory.getShop().getName());

                        binding.get().seeShop.setOnClickListener(view -> {
                              Intent intent = new Intent(getContext(), StoreHomeActivity.class);
                              intent.putExtra(Constants.SHOP_OBECT, mInventory.getShop());
                              startActivity(intent);
                        });
                  }

                  try {
                        binding.get().productDescription.setText(Html.fromHtml(mInventory.getDescription()));

                  } catch (Exception e) {
                        Log.e(TAG, "initUIAndActions:  " + e.getMessage());
                  }

                  try {

                        if (mInventory.getImages() != null && mInventory.getImages().size() > 0) {
                              String url = Constants.IMAGES_BASE_URL + mInventory.getImages().get(0).getPath();
                              Utils.LoadImage(getContext(), binding.get().image, url);

                              Utils.LoadImage(getContext(), bottomBoxLayoutBinding.get().productImageView, url);
                        } else if (mInventory.getImage() != null) {

                              String url = Constants.IMAGES_BASE_URL + mInventory.getImage().getPath();
                              Utils.LoadImage(getContext(), binding.get().image, url);

                              Utils.LoadImage(getContext(), bottomBoxLayoutBinding.get().productImageView, url);
                        }

                  } catch (Exception e) {
                        Log.e(TAG, "initUIAndActions: " + e.getMessage());
                  }

                  if (getContext() != null) {
                        binding.get().addReviewTv.setOnClickListener(v -> {
                              EventBus.getDefault().post(new ReviewItemClickedEvent(new Feedback()));
                        });

                        binding.get().fabQtySub.setOnClickListener(v -> {
                              int qty = Integer.parseInt(binding.get().tvQty.getText().toString());
                              if (qty > 1) {
                                    qty--;
                                    binding.get().tvQty.setText(String.format("%d", qty));
                              }
                        });

                        binding.get().fabQtyAdd.setOnClickListener(v -> {
                              int qty = Integer.parseInt(binding.get().tvQty.getText().toString());
                              if (qty < 100) {
                                    qty++;
                                    binding.get().tvQty.setText(String.format("%d", qty));
                              }
                        });

                        binding.get().addToBasketButton.setOnClickListener(v -> {

                              bottomBoxLayoutBinding.get().lowestButton.setText(getString(R.string.product_detail__add_to_busket));

                              mBottomSheetDialog.get().show();
                        });

                        mFeedbacks = mInventory.getFeedbacks();

                        pushFeedbacks();
                  }

                  Utils.hideFab(binding.get().phoneFloatingActionButton);
                  Utils.hideFab(binding.get().phoneTextView);


                  binding.get().mainFloatingActionButton.setOnClickListener(v -> {
                        twist = Utils.twistFab(v, !twist);

                        if (twist) {
                              Utils.showFab(binding.get().whatsappFloatingActionButton);
                              Utils.showFab(binding.get().whatsAppTextView);
                              Utils.showFab(binding.get().phoneFloatingActionButton);
                              Utils.showFab(binding.get().phoneTextView);

                        } else {
                              Utils.hideFab(binding.get().whatsappFloatingActionButton);
                              Utils.hideFab(binding.get().whatsAppTextView);
                              Utils.hideFab(binding.get().phoneTextView);
                              Utils.hideFab(binding.get().phoneFloatingActionButton);
                        }

                  });

                  if (mInventory.getShop() != null && mInventory.getShop().getAddresses() != null && mInventory.getShop() != null &&
                          mInventory.getShop().getAddresses().get(0).getPhone() != null) {

                        binding.get().phoneFloatingActionButton.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View view) {
                                    Utils.callPhone(getActivity(), mInventory.getShop().getAddresses().get(0).getPhone());
                              }
                        });

                  }

                  binding.get().contactShop.setOnClickListener(v -> {

                        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                                ProductDetailsFragment.this.getActivity(), navigationController, () -> {
                                      if (getActivity() != null) {

                                            if (mInventory.getShop() != null) {
                                                  Intent intent = new Intent(getContext(), MessageActivity.class);
                                                  intent.putExtra(Constants.SHOP_ID, mInventory.getShop().getId());
                                                  intent.putExtra(Constants.SHOP_NAME, mInventory.getShop().getName());
                                                  startActivity(intent);
                                            }
                                      }
                                });
                  });

                  binding.get().whatsappFloatingActionButton.setOnClickListener(v -> {

                        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                                ProductDetailsFragment.this.getActivity(), navigationController, () -> {
                                      if (getActivity() != null) {

                                            if (mInventory.getShop() != null) {
                                                  Intent intent = new Intent(getContext(), MessageActivity.class);
                                                  intent.putExtra(Constants.SHOP_ID, mInventory.getShop().getId());
                                                  intent.putExtra(Constants.SHOP_NAME, mInventory.getShop().getName());
                                                  startActivity(intent);
                                            }
                                      }
                                });
                  });

                  binding.get().textview56.setOnClickListener((View v) -> {
                        boolean show = Utils.toggleUpDownWithAnimation(binding.get().tabUpDownImageView);
                        if (show) {
                              expandTabFunction();
                        } else {
                              collapseTabFunction();
                        }
                  });

                  bottomBoxLayoutBinding.get().lowestButton.setOnClickListener(v -> {
                        if (bottomBoxLayoutBinding.get().lowestButton.getText().equals(getString(R.string.buy))) {
                              buyNowClicked = true;
                              EventBus.getDefault().post(new AddToCart(Integer.valueOf(bottomBoxLayoutBinding.get().qtyEditText.getText().toString()), mInventory.getSlug()));
                        } else {
                              EventBus.getDefault().post(new AddToCart(Integer.valueOf(bottomBoxLayoutBinding.get().qtyEditText.getText().toString()), mInventory.getSlug()));
                        }
                  });

                  binding.get().buyNow.setOnClickListener(v -> {
                        bottomBoxLayoutBinding.get().lowestButton.setText(getString(R.string.buy));
                        mBottomSheetDialog.get().show();
                  });

                  if (mInventory.getShop() != null) {
                        if (mInventory.getShop().getConfig() != null && mInventory.getShop().getConfig().getReturnRefund() != null) {
                              binding.get().returnPolicy.setText(Html.fromHtml(mInventory.getShop().getConfig().getReturnRefund()));
                        }
                  }
            }

            binding.get().shoeSizeGuide.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
//                        showDialogImage("","shirt_size");
//                        showDialogFragment();
                        EventBus.getDefault().post(new OnSizeGuideClicked("shirt_size"));
                  }
            });

      }

      private void pushFeedbacks() {
            if (mFeedbacks != null && mFeedbacks.size() > 0) {

                  int randomNum = ThreadLocalRandom.current().nextInt(0, mFeedbacks.size());

                  String reviewMessage = mFeedbacks.get(randomNum).getComment();
                  String customerName = null;
                  if (mFeedbacks.get(randomNum).getCustomer() != null) {
                        customerName = mFeedbacks.get(randomNum).getCustomer().getName();
                  }

                  binding.get().reviews.setText(String.format("%s - %s", reviewMessage, customerName));

                  binding.get().seeAllReviews.setOnClickListener(v -> {
                        EventBus.getDefault().post(new ReviewsClickedEvent(mInventory.getFeedbacks()));
                  });

            } else {
                  Utils.RemoveView(binding.get().seeAllReviews);
                  binding.get().reviews.setText("No reviews for this item!");
            }
      }

      @Override
      protected void initViewModels() {
            productColorViewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductColorViewModel.class);
            productImageVariantViewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductColorViewModel.class);
            mProductColors = new ArrayList<>();
            mProductSizes = new ArrayList<>();
            mProductShoeSizes = new ArrayList<>();
            mProductGenders = new ArrayList<>();
            mProductImageVariants = new ArrayList<>();
            mTemporalImageLists = new ArrayList<>();

            getAttributes();
            getImageListings(String.valueOf(mInventory.getId()));
            initializeProductColours();
            initializeProductImageVariants();

            if (hasImageVariant){
                  Utils.ShowView(binding.get().variantLayout);
            }else{
                  Utils.RemoveView(binding.get().variantLayout);
            }

      }

      private void initializeProductColours() {
            for (AttributeValue attributeValue : attributeColors) {
                  if (mInventory != null) {
                        ProductColor productColor1 =
                                new ProductColor(String.valueOf(attributeValue.getId()),
                                        String.valueOf(mInventory.getId()), attributeValue.getColor(),
                                        "", "", "", "", "", "");
                        mProductColors.add(productColor1);
                  }
            }
      }

      private void initializeProductImageVariants() {
            if (mInventory != null && mInventory.getImages() != null) {
                  for (Image image : mInventory.getImages()) {
                        if (mInventory != null) {
                              String path = image.getPath();
                              ProductColor productImageVariant =
                                      new ProductColor(String.valueOf(image.getId()), String.valueOf(image.getId()), path,
                                              "", "", "", "", "", "");
                              mProductImageVariants.add(productImageVariant);
                        }
                  }
            }
      }

      private void showSizeButtons(){

            if (getContext() != null) {
                  if (mProductSizes != null && mProductSizes.size() > 0) {
                        for (String size : mProductSizes) {
                              switch (size) {
                                    case "XS":
                                          Utils.ShowView(binding.get().buttonXS);
                                          break;

                                    case "S":
                                          Utils.ShowView(binding.get().buttonS);
                                          break;

                                    case "M":
                                          Utils.ShowView(binding.get().buttonM);
                                          break;

                                    case "L":
                                          Utils.ShowView(binding.get().buttonL);
                                          break;

                                    case "XL":
                                          Utils.ShowView(binding.get().buttonS);
                                          break;

                              }
                        }
                        Utils.ShowView(binding.get().sizeLayout);
                  } else {
                        Utils.RemoveView(binding.get().sizeLayout);
                  }

                  if (mProductShoeSizes != null && mProductShoeSizes.size() > 0) {
                        for (String size : mProductShoeSizes) {
                              switch (size) {
                                    case "40":
                                          Utils.ShowView(binding.get().buttonForty);
                                          break;

                                    case "41":
                                          Utils.ShowView(binding.get().buttonFortyOne);
                                          break;

                                    case "42":
                                          Utils.ShowView(binding.get().buttonFortyTwo);
                                          break;

                                    case "43":
                                          Utils.ShowView(binding.get().buttonFortyThree);
                                          break;

                                    case "44":
                                          Utils.ShowView(binding.get().buttonFortyFour);
                                          break;

                              }
                        }
                        Utils.ShowView(binding.get().shoeSizeLayout);
                  } else {
                        Utils.RemoveView(binding.get().shoeSizeLayout);
                  }

                  if (mProductGenders != null && mProductGenders.size() > 0 ){
                        for (String size : mProductGenders) {
                              switch (size) {
                                    case "female":
                                          Utils.ShowView(binding.get().buttonFemale);
                                          break;

                                    case "male":
                                          Utils.ShowView(binding.get().buttonMale);
                                          break;
                              }
                        }
                        Utils.ShowView(binding.get().genderLayout);
                  }else{
                        Utils.RemoveView(binding.get().genderLayout);
                  }

                  setGender();
            }
      }

      @Override
      protected void initAdapters() {

            try {
                  //color
                  ProductColorAdapter nvcolorAdapter = new ProductColorAdapter(dataBindingComponent,
                          (productColor, seletedColorId, selectetColorValue) -> {
                                productColorViewModel.colorSelectId = seletedColorId;
                                productColorViewModel.colorSelectValue = selectetColorValue;
                                colorVariantSelected = selectetColorValue;
                                mProductColors = new ArrayList<>();
                                initializeProductColours();

                                for (ProductColor productColor1 : mProductColors) {
                                      productColor1.isColorSelect = false;
                                      productColor1.isColorSelect = productColor.id.equals(productColor1.id);
                                      if (productColor.id.equals(productColor1.id)){
                                            colorSelected = true;
                                      }
                                };

                                productColorViewModel.proceededColorListData = ProductDetailsFragment.this.processDataList(productColorViewModel.proceededColorListData, productColorViewModel);
                                ProductDetailsFragment.this.replaceProductColorData(mProductColors);
                          });

                  this.colorAdapter = new AutoClearedValue<>(this, nvcolorAdapter);

                  ProductVariantAdapter pvcolorAdapter = new ProductVariantAdapter(dataBindingComponent,
                          (productColor, seletedColorId, selectetColorValue) -> {
                                productImageVariantViewModel.colorSelectId = seletedColorId;
                                productImageVariantViewModel.colorSelectValue = selectetColorValue;
                                Log.i(TAG, "initAdapters:  variant " + seletedColorId + " " + selectetColorValue);
                                imageVariantSelected = selectetColorValue;

                                mProductImageVariants = new ArrayList<>();
                                initializeProductImageVariants();

                                for (ProductColor productImageVariant : mProductImageVariants) {
                                      productImageVariant.isColorSelect = false;
                                      productImageVariant.isColorSelect = productColor.id.equals(productImageVariant.id);
                                      Log.i(TAG, "initAdapters:  variant selected " + productImageVariant.id + " " + productColor.id);
                                      if (productColor.id.equals(productImageVariant.id)){
//                                            colorSelected = true;
                                            variantSelected = true;

                                      }
                                }

                                productImageVariantViewModel.proceededColorListData = ProductDetailsFragment.this.processDataList(productImageVariantViewModel.proceededColorListData, productImageVariantViewModel);
                                ProductDetailsFragment.this.replaceProductVariantData(mProductImageVariants);
                          });


                  this.productImageVariantAdapter = new AutoClearedValue<>(this, pvcolorAdapter);

                  FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
                  layoutManager.setFlexDirection(FlexDirection.ROW);
                  layoutManager.setJustifyContent(JustifyContent.FLEX_START);

                  binding.get().colorRecyclerView.setLayoutManager(layoutManager);
                  binding.get().colorRecyclerView.setAdapter(colorAdapter.get());


                  FlexboxLayoutManager layoutManager3 = new FlexboxLayoutManager(getContext());
                  layoutManager.setFlexDirection(FlexDirection.ROW);
                  layoutManager.setJustifyContent(JustifyContent.FLEX_START);

                  binding.get().variantRecyclerView.setLayoutManager(layoutManager3);
                  binding.get().variantRecyclerView.setAdapter(productImageVariantAdapter.get());

                  //color in popup
                  FlexboxLayoutManager layoutManager2 = new FlexboxLayoutManager(getContext());
                  layoutManager2.setFlexDirection(FlexDirection.ROW);
                  layoutManager2.setJustifyContent(JustifyContent.FLEX_START);

                  bottomBoxLayoutBinding.get().colorRecycler.setLayoutManager(layoutManager2);
                  bottomBoxLayoutBinding.get().colorRecycler.setAdapter(colorAdapter.get());

                  bottomBoxLayoutBinding.get().prodNameTextView.setText(mInventory.getTitle());

                  bottomBoxLayoutBinding.get().colorRecycler.setNestedScrollingEnabled(false);
                  bottomBoxLayoutBinding.get().attributeHeaderRecycler.setNestedScrollingEnabled(false);

                  bottomBoxLayoutBinding.get().floatingbtnMinus.setOnClickListener(view -> {

                        if (num != 1) {

                              if (minOrder != 0) {
                                    if (num <= minOrder) {
                                          Toast.makeText(getContext(), getString(R.string.product_detail__min_order_error) + Constant.SPACE_STRING + minOrder, Toast.LENGTH_SHORT).show();
                                          return;
                                    }
                              }

                              num -= 1;
                              bottomBoxLayoutBinding.get().qtyEditText.setText(String.valueOf(num));
                        }
                  });

                  bottomBoxLayoutBinding.get().floatingbtnAdd.setOnClickListener(view -> {
                        num += 1;
                        bottomBoxLayoutBinding.get().qtyEditText.setText(String.valueOf(num));
                  });

                  bindProductDetailInfo(mInventory);
            }catch(Exception exception){
                  Sentry.captureException(exception);
            }
      }

      private void replaceProductColorData(List<ProductColor> productColorList) {
            colorAdapter.get().replace(productColorList);
            binding.get().executePendingBindings();
      }

      private void replaceProductVariantData(List<ProductColor> productColorList) {
            productImageVariantAdapter.get().replace(productColorList);
            binding.get().executePendingBindings();
      }

      @Override
      protected void initData() {
                        getListings("");
                        getRating();
                        loadData();
      }

      private void loadData(){
            if (mProductColors != null ) {
                  new Handler().postDelayed(() -> {
                        try {
                              if (mProductColors != null) {
                                    colorAdapter.get().replace(processDataList(mProductColors, productColorViewModel));
                              }
                        }catch(Exception exce){
                              Sentry.captureException(exce);
                        }
                  }, 3000);
            }

      }

      private List<ProductColor> processDataList(List<ProductColor> listResource, ProductColorViewModel productColorVm) {

            List<ProductColor> tmpColorList = new ArrayList<>();

            for (int i = 0; i < listResource.size(); i++) {

                  try {
                        tmpColorList.add((ProductColor) listResource.get(i).clone());
                        tmpColorList.get(i).isColorSelect = tmpColorList.get(i).id.equals(productColorVm.colorSelectId);
                  } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                  }
            }

            return tmpColorList;
      }

      private void bindProductDetailInfo(Inventory product) {

            if (getContext() != null ) {
                  binding.get().brandTextView.setText(product.getTitle());

                  if (product.getMinOrderQuantity() == 0) {
                        binding.get().productMinOrderValueTextView.setVisibility(View.GONE);
                        binding.get().productMinOrderTextView.setVisibility(View.GONE);
                  } else {
                        binding.get().productMinOrderValueTextView.setText(String.valueOf(product.getMinOrderQuantity()));
                  }

                  if (product.getProductUnit() == null || product.getProductUnit().equals("0") || product.getProductUnit().isEmpty()) {
                        binding.get().productUnitValueTextView.setVisibility(View.GONE);
                        binding.get().productUnitTextView.setVisibility(View.GONE);
                  } else {
                        binding.get().productUnitValueTextView.setText(String.valueOf(product.getProductUnit()));
                  }

                  if (product.getProductMeasurement() == null || product.getProductMeasurement().equals("0") || product.getProductMeasurement().isEmpty()) {
                        binding.get().productMeasurementTextView.setVisibility(View.GONE);
                        binding.get().productMeasurementValueTextView.setVisibility(View.GONE);
                  } else {
                        binding.get().productMeasurementValueTextView.setText(String.valueOf(product.getProductMeasurement()));
                  }

            }
      }

      private void getImageListings(String productId){
            getListingImages();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(@NonNull AddToCart event) {

            if (mInventory != null) {

                  if (mInventory.getStockQuantity() > 0) {

                        if (mProductColors.size() > 0 && colorSelected == false ||
                                mProductSizes.size() > 0 && sizeSelected ==false
                        || hasImageVariant && variantSelected == false) {

                              if (mProductColors.size() > 0 && !colorSelected) {
                                    psDialogMsg.showInfoDialog(getString(R.string.product_color_not_selected),
                                            getString(R.string.app__ok));
                                    psDialogMsg.show();
                              }else if ( mProductSizes.size() > 0 && !sizeSelected) {
                                    psDialogMsg.showInfoDialog(getString(R.string.product_size_not_selected),
                                            getString(R.string.app__ok));
                                    psDialogMsg.show();

                              } else if ( hasImageVariant  && !variantSelected) {
                              psDialogMsg.showInfoDialog(getString(R.string.variant_not_selected),
                                      getString(R.string.app__ok));
                              psDialogMsg.show();
                        }
                        }else{

                              addToCart(event.getQty(), event.getSlug());
                              CustomSharedPrefs.setCartStatus(getContext(), true);
                              setCartSize();

                              if (buyNowClicked){
                                    new Handler().postDelayed(new Runnable() {
                                          @Override
                                          public void run() {
                                                startActivity(new Intent(getActivity(), BasketListActivity.class));
                                          }
                                    },2000);
                              }
                        }


                  }else {

                        if (getActivity() != null) {
                              psDialogMsg.showWarningDialog(getString(R.string.product_out_of_stock),
                                      getString(R.string.app__ok));
                              psDialogMsg.show();
                        }
                  }
            }
      }

      private void addToCart(int qty, String slug){
            mAPIService.addToCart(qty,imageVariantSelected,shoeSizeVariantSelected,
                    colorVariantSelected ,slug)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                try {
                                      if (getContext() != null) {
                                            mBottomSheetDialog.get().dismiss();
                                            binding.get().setLoadingMore(true);

                                            appExecutors.getDiskIO().execute(new Runnable() {
                                                  @Override
                                                  public void run() {
                                                        if (mimiDb.basketCountDao().getBasketCount() > 0) {
                                                              int oldCount = mimiDb.basketCountDao().getCount(1).getCount();
                                                              int newCount = oldCount + qty;
                                                              basketCountViewModel.setSaveToBasketListObj(newCount);
                                                              mimiDb.basketCountDao().updateBasketCount(1, newCount);
                                                        } else {
                                                              BasketCount basketCount = new BasketCount();
                                                              basketCount.setCount(qty);
                                                              mimiDb.basketCountDao().insert(basketCount);
                                                        }
                                                  }
                                            });
                                      }
                                }catch(Exception exception){
                                      Sentry.captureException(exception);
                                }
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) { Log.e(TAG, "onError:  " + e.getMessage() ); }

                          @Override
                          public void onComplete() {
                                try {
                                      if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                      }
                                      if (getContext() != null) {
                                            binding.get().setLoadingMore(false);
                                      }
                                      psDialogMsg.showSuccessDialog(getString(R.string.product_detail__successfully_added), getString(R.string.app__ok));
                                      psDialogMsg.show();

                                }catch(Exception exception ){
                                      Sentry.captureException(exception);
                                }
                          }
                    });
      }

      private void addFeedback(String type, String customerId, String typeId, String rating, String comment, String approved){
            mAPIService.saveProductFeedback(type, customerId,typeId, rating, comment,approved)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {

                                try {
                                      if (getActivity() != null ) {
                                            Utils.ShowSnackBar(getActivity(), "Successfully added a review", Snackbar.LENGTH_LONG);
                                            getFeedBacks();
                                      }
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " + e.getMessage() );
                                }
                          }
                    });
      }

      private void showCustomDialog() {

            String customerId = loginUserId;
            String inventoryId  = String.valueOf(mInventory.getId());
            String approved = "1";
            String customerrName = fullName;

            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_add_review);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            final TextView customer_name_textview = dialog.findViewById(R.id.customer_name);
            customer_name_textview.setText(customerrName);
            final EditText et_post = dialog.findViewById(R.id.et_post);
            final AppCompatRatingBar rating_bar = dialog.findViewById(R.id.rating_bar);
            dialog.findViewById(R.id.bt_cancel).setOnClickListener(v -> dialog.dismiss());

            dialog.findViewById(R.id.bt_submit).setOnClickListener(v -> {
                  String review = et_post.getText().toString().trim();
                  if (review.isEmpty()) {
                        Toast.makeText(getContext(), ("Please fill review text"), Toast.LENGTH_SHORT).show();
                  } else {
                        String rating = String.valueOf(rating_bar.getRating());
                        addFeedback(Constants.INVENTORY_TYPE, customerId,inventoryId,rating,review,approved);
                        dialog.dismiss();
                  }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

      private void setCartSize() {

            mAPIService.getCarts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CartResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(@NonNull CartResponse cartResponse) {

                                try {
                                      int quantity = 0;

                                      for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                                            if (cartResponse.getCarts().get(i) != null) {
                                                  int qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());
                                                  quantity += qty;
                                            }
                                      }

                                      CustomSharedPrefs.setCartQty(getContext(), quantity);
                                }catch(Exception exception){
                                      Sentry.captureException(exception);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                          }

                    });
      }

      private void showDialogImage(String url, String type) {
            if (getContext() != null ) {
                  final Dialog dialog = new Dialog(getContext());
                  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                  dialog.setContentView(R.layout.dialog_image_center);

                  if (type.equals("product")) {
                        Utils.LoadImage(getContext(), dialog.findViewById(R.id.dialog_image), url);
                  }else if (type.equals("shirt_size")){
                        PhotoView photoView = dialog.findViewById(R.id.dialog_image);
                        photoView.setImageDrawable(getContext().getDrawable(R.drawable.shirt_size_guide));
                  }
                  dialog.findViewById(R.id.close_image_dialog).setOnClickListener(v -> {
                        dialog.dismiss();
                  });

                  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                  dialog.setCancelable(true);
                  dialog.show();
            }
      }

      private void hideFloatingButton() {
            try {
                  Utils.hideFirstFab(binding.get().whatsappFloatingActionButton);
                  Utils.hideFab(binding.get().whatsAppTextView);
            }catch(Exception exception){
                  Sentry.captureException(exception);
            }
      }

      private void getListings(String list) {

            if (mInventory != null) {
                  mAPIService.getRelatedProducts(String.valueOf(mInventory.getId()))
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<List<Inventory>>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {
                                      disposable = d;
                                }

                                @Override
                                public void onNext(@NonNull List<Inventory> inventoryList) {

                                      try {

                                            if (inventoryList.size() < 1){
                                                  Utils.RemoveView(binding.get().alsoBuyTextView);
                                                  Utils.RemoveView(binding.get().alsoBuyRecyclerView);
                                            }else {
                                                  Utils.ShowView(binding.get().alsoBuyTextView);
                                                  Utils.ShowView(binding.get().alsoBuyRecyclerView);
                                            }

                                            mProductsAdapter = new ShopProductGridCardAdapter(getContext(), inventoryList);
                                            binding.get().alsoBuyRecyclerView.setAdapter(mProductsAdapter);
                                            mProductsAdapter.setOnItemClickListener((view, obj, position) -> {

                                                  if (view == view.findViewById(R.id.nameTextView)) {

                                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
                                                        intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
                                                        startActivity(intent);

                                                  } else if (view == view.findViewById(R.id.addressTextView)) {
                                                        startActivity(new Intent(getContext(), LocationActivity.class));
                                                  } else if (view == view.findViewById(R.id.cardView12)) {
                                                        navigateToItemDetailActivity(getActivity(), inventoryList.get(position));
                                                  }
                                            });

                                            mProductsAdapter.setOnMoreButtonClickListener((view, obj, item) -> {
                                            });

                                      } catch (Exception e) {
                                            logError(TAG, e, "onNext: ");
                                      }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {
                                      logError(TAG, e, "onError: getListings  ");
                                }

                                @Override
                                public void onComplete() {
                                      if (!disposable.isDisposed()) {
                                            disposable.dispose();

                                      }

                                }
                          });
            }
      }

      private void getRating() {

            mAPIService.getAverageFeedbackRating(String.valueOf(mInventory.getId()), "inventory")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Rating>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) { disposable = d; }

                          @Override
                          public void onNext(@NonNull Rating rating) {
                                try{
                                      if (rating.getAverageRating() != null ) {
                                            binding.get().rating.setRating(Float.parseFloat(rating.getAverageRating()));
                                      }
                                      binding.get().ratingCount.setText(String.valueOf(mInventory.getFeedbacks().size()));

                                }catch (Exception e){
                                      Log.e(TAG, "onNext: " + e.getMessage() );
                                }

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {}

                          @Override
                          public void onComplete() {
                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private void collapseTabFunction() {
            if (getContext() != null ) {
                  binding.get().tabRecyclerView.setVisibility(View.GONE);
                  ViewAnimationUtil.collapse(binding.get().findBySimilarFactTextView);

                  binding.get().alsoBuyRecyclerView.setVisibility(View.GONE);
                  ViewAnimationUtil.collapse(binding.get().alsoBuyTextView);
            }
      }

      private void expandTabFunction() {
            if (getContext() != null ) {
                  ViewAnimationUtil.expand(binding.get().findBySimilarFactTextView);
                  binding.get().tabRecyclerView.setVisibility(View.VISIBLE);
                  ViewAnimationUtil.expand(binding.get().alsoBuyTextView);
                  binding.get().alsoBuyRecyclerView.setVisibility(View.VISIBLE);
                  binding.get().alsoBuyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

                  binding.get().tabRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            }
      }

      @Subscribe( threadMode = ThreadMode.MAIN)
      public void onCartClicked(CartClicked event){
            Intent intent = new Intent(ProductDetailsFragment.this.getActivity(), BasketListActivity.class);
            startActivity(intent);
      }

      private void getBasketCountsViewModel(){
            try {
                  basketCountViewModel.getBasketCounts().observe(this, list -> {
                        if (list.size() > 0) {
                              int cartItemSize = list.get(0).getCount();
                        } else {
                        }
                  });
            }catch(Exception exception){
                  Sentry.captureException(exception);
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onCheckoutReady(CheckOutReady event){
            if (getContext() != null) {
                  String currencySymbol = Constants.DEFAULT_CURRENCY;
                  String shippingCostString = currencySymbol + Constant.SPACE_STRING + mDeliveryCharge;
                  binding.get().productShippingCostValueTextView.setText(shippingCostString);
            }
      }

      private void getFeedBacks() {

            if (mInventory != null) {
                  mAPIService.getFeedbacks(String.valueOf(mInventory.getId()), "product")
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<FeedbackResponse>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {
                                      disposable = d;
                                }

                                @Override
                                public void onNext(@NonNull FeedbackResponse feedbackResponse){
                                       mFeedbacks  = new ArrayList<>();
                                       mFeedbacks = feedbackResponse.getFeedbacks();
                                      pushFeedbacks();
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {
                                }

                                @Override
                                public void onComplete() {}
                          });
            }
      }

      @Override
      public void onPause() {
            super.onPause();

            try {
                  pushFeedbacks();
            }catch (Exception exception){
                  Sentry.captureException(exception);
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onAddReviewClicked(ReviewItemClickedEvent event){

            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                    ProductDetailsFragment.this.getActivity(), navigationController, () -> {
                          checkCustomerOrder();
                    });

      }

      private void checkCustomerOrder() {

            if (mInventory != null) {

                  mAPIService.getCheckCustomerOrder(loginUserId,String.valueOf(mInventory.getId()))
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<JsonElement>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {

                                      if (getActivity() != null ) {
                                            ((ProductDetailsActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                                            ((ProductDetailsActivity) getActivity()).progressDialog.setCancelable(false);
                                            ((ProductDetailsActivity) getActivity()).progressDialog.show();
                                      }

                                      disposable = d;
                                }

                                @Override
                                public void onNext(@NonNull JsonElement jsonElement) {
                                      if (getActivity() != null ) {
                                            ((ProductDetailsActivity) getActivity()).progressDialog.hide();
                                            showCustomDialog();
                                      }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {
                                      if (getActivity() != null ) {
                                            ((ProductDetailsActivity) getActivity()).progressDialog.hide();
                                            psDialogMsg.showWarningDialog("You need to buy this product to review!", getString(R.string.app__ok));
                                            psDialogMsg.show();
                                      }
                                }

                                @Override
                                public void onComplete() {
                                      if (!disposable.isDisposed()) {
                                            disposable.dispose();
                                      }
                                }
                          });
            }
      }

      @Override
      public void onClick(View view) {
            Toast.makeText(getContext(), " " + view.getId(), Toast.LENGTH_SHORT).show();
      }

      private void getAttributes(){

            if (mInventory != null) {
                  mAPIService.getProductAttributes(String.valueOf(mInventory.getSlug()))
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<List<Attribute>>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {
                                      disposable = d;
                                }

                                @Override
                                public void onNext(@NonNull List<Attribute> attributes) {

                                      try {
                                            if (attributes.size() > 0) {
                                                  for (Attribute attribute : attributes) {
                                                        if (attribute.getName().equals("Sizes")) {
                                                              for (AttributeValue attributeValue : attribute.getAttributeValues()) {
                                                                    mProductSizes.add(attributeValue.getValue());
                                                              }
                                                        } else if (attribute.getName().equals("Gender")) {
                                                              for (AttributeValue attributeValue : attribute.getAttributeValues()) {
                                                                    mProductGenders.add(attributeValue.getValue());
                                                              }
                                                        } else if (attribute.getName().equals("Color")) {
                                                              for (AttributeValue attributeValue : attribute.getAttributeValues()) {
                                                                    ProductColor productColor1 =
                                                                            new ProductColor(String.valueOf(attributeValue.getId()), String.valueOf(mInventory.getId()), attributeValue.getColor(), "", "", "",
                                                                                    "", "", "");
                                                                    mProductColors.add(productColor1);
                                                                    attributeColors.add(attributeValue);
                                                              }
                                                        }else if (attribute.getName().equals("Shoe sizes")){

                                                              for (AttributeValue attributeValue : attribute.getAttributeValues()) {
                                                                    mProductShoeSizes.add(attributeValue.getValue());
//                                                                    attributeColors.add(attributeValue);
                                                              }
                                                        }else if (attribute.getName().equals("other")){
                                                              hasImageVariant = true;
                                                        }
                                                  }
                                            }
                                      }catch (Exception exception){
                                            Sentry.captureException(exception);
                                      }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {
                                      logError(TAG, e, "onError: getListings  ");
                                }

                                @Override
                                public void onComplete() {

                                      try{
                                            if (mProductColors.size() > 0 ){
                                                  Utils.ShowView(binding.get().colorRecyclerView);
                                                  Utils.ShowView(binding.get().cangetColorTextView);
                                                  Utils.ShowView(bottomBoxLayoutBinding.get().cangetColorTextView);
                                                  Utils.ShowView(bottomBoxLayoutBinding.get().colorRecycler);
                                            }else {
                                                  Utils.RemoveView(binding.get().colorRecyclerView);
                                                  Utils.RemoveView(binding.get().cangetColorTextView);
                                                  Utils.RemoveView(bottomBoxLayoutBinding.get().cangetColorTextView);
                                                  Utils.RemoveView(bottomBoxLayoutBinding.get().colorRecycler);
                                            }

                                            showSizeButtons();

                                      }catch (Exception e){
                                            Sentry.captureException(e);
                                      }

                                      if (!disposable.isDisposed()) {
                                            disposable.dispose();
                                      }
                                }
                          });
            }

      }

      private void getListingImages(){

          if (mInventory != null) {
                if (mInventory.getProduct() != null) {
                      mAPIService.getProductImages(String.valueOf(mInventory.getProduct().getId()))
                              .subscribeOn(Schedulers.io())
                              .observeOn(AndroidSchedulers.mainThread())
                              .subscribe(new Observer<ImageResponse>() {
                                    Disposable disposable;

                                    @Override
                                    public void onSubscribe(Disposable d) {
                                          disposable = d;
                                    }

                                    @Override
                                    public void onNext(@NonNull ImageResponse imageResponse) {

                                          mProductImageVariants = new ArrayList<>();
                                          mTemporalImageLists = new ArrayList<>();


                                          try {
                                                if (getContext() !=null )
                                                if (imageResponse.getImages().size() > 0) {

                                                      mInventory.setImages(imageResponse.getImages());

                                                      for (int i = 0; i < imageResponse.getImages().size(); i++) {

                                                            ImageView imageView = new ImageView(getContext());
                                                            LayoutParams lp = new LayoutParams(90, 90);
                                                            lp.setMarginEnd(5);
                                                            lp.setMarginStart(5);
                                                            imageView.setLayoutParams(lp);
                                                            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                                                            String path = imageResponse.getImages().get(i).getPath();

                                                            Picasso.get().load(path).into(imageView);

                                                            Image image = imageResponse.getImages().get(i);

                                                            ProductColor productImageVariant =
                                                                    new ProductColor(String.valueOf(image.getId()),String.valueOf( image.getId()),path,
                                                                            "","","","","","");


                                                            mProductImageVariants.add(productImageVariant);

                                                            mTemporalImageLists.add(productImageVariant);

                                                            binding.get().galleryLinearLayout.addView(imageView);

                                                            imageView.setOnClickListener(v -> {
                                                                  showDialogImage(path, "product");
                                                            });
                                                      }
                                                }
                                          }catch (Exception ex){
                                                Sentry.captureException(ex);
                                          }
                                    }

                                    @Override
                                    public void onError(@NonNull Throwable e) {
                                          logError(TAG, e, "onError: getListings  ");
                                    }

                                    @Override
                                    public void onComplete() {

                                          try{

                                                if (mProductImageVariants.size() > 0 ){
                                                      Utils.ShowView(binding.get().variantRecyclerView);
                                                      Utils.ShowView(binding.get().variantLayout);
                                                }else {
                                                      Utils.RemoveView(binding.get().variantRecyclerView);
                                                      Utils.RemoveView(binding.get().variantLayout);
                                                }

                                                showSizeButtons();

                                          }catch (Exception e){
                                                Sentry.captureException(e);
                                          }


                                                if (mProductImageVariants != null) {
                                                      new Handler().postDelayed(() -> {
                                                            try {
                                                                  if (hasImageVariant) {

                                                                        if (mProductImageVariants != null) {
                                                                              productImageVariantAdapter.get().replace(processDataList(mProductImageVariants, productImageVariantViewModel));
                                                                              Utils.ShowView(binding.get().variantLayout);
                                                                        }
                                                                  }else{
                                                                        Utils.RemoveView(binding.get().variantLayout);
                                                                  }
                                                            } catch (Exception exce) {
                                                                  Sentry.captureException(exce);
                                                            }
                                                      }, 1000);

                                          }

                                          if (!disposable.isDisposed()) {
                                                disposable.dispose();
                                          }
                                    }
                              });
                }
          }

      }

      private void setSize(View btnClick){
            if (getContext() != null ) {
                  binding.get().buttonXS.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonS.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonM.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonL.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonXL.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonXS.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonS.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonM.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonL.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonXL.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));

                  btnClick.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ecommerce11_selected_bg));
                  TextView textActive = (TextView) btnClick;
                  textActive.setTextColor(Color.WHITE);
            }
      }

      private void setShoeSize(View btnClick){
            if (getContext() != null ) {
                  binding.get().buttonForty.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonFortyOne.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonFortyTwo.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonFortyThree.setBackgroundColor(Color.TRANSPARENT);
                  binding.get().buttonFortyFour.setBackgroundColor(Color.TRANSPARENT);

                  binding.get().buttonForty.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonFortyOne.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonFortyTwo.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonFortyThree.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));
                  binding.get().buttonFortyFour.setTextColor(ContextCompat.getColor(getContext(), R.color.ecommerce11fontSize));

                  btnClick.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ecommerce11_selected_bg));
                  TextView textActive = (TextView) btnClick;
                  textActive.setTextColor(Color.WHITE);
            }
      }

      private void setGender(){
            if (getContext() != null ) {
                  binding.get().buttonFemale.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ecommerce11_selected_bg));
                  binding.get().buttonMale.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ecommerce11_selected_bg));
                  binding.get().buttonFemale.setTextColor(Color.WHITE);
                  binding.get().buttonMale.setTextColor(Color.WHITE);
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onButtonClicked(OnClickEvent event){
            switch (event.getView().getId()){
                  case R.id.button_XS:
                        shoeSizeVariantSelected = "XS";

                  case R.id.button_S:
                        shoeSizeVariantSelected = "S";

                  case R.id.button_M:
                        shoeSizeVariantSelected = "M";

                  case R.id.button_L:
                        shoeSizeVariantSelected = "L";

                  case R.id.button_XL:
                        setSize(event.getView());
                        sizeSelected= true;
                        shoeSizeVariantSelected = "L";
                        break;

                  case R.id.button_forty:
                        shoeSizeVariantSelected = "40";

                  case R.id.button_forty_one:
                        shoeSizeVariantSelected = "41";

                  case R.id.button_forty_two:
                        shoeSizeVariantSelected = "42";

                  case R.id.button_forty_three:
                        shoeSizeVariantSelected = "43";

                  case R.id.button_forty_four:
                        setShoeSize(event.getView());
                        shoeSizeSelected= true;
                        shoeSizeVariantSelected = "43";
                        break;
            }
      }

      private void showDialogFragment() {
//            ProductSizeGuideFragment productSizeGuideFragment = new ProductSizeGuideFragment();
//            showDialog(productSizeGuideFragment);
      }


      private void showDialog(Fragment fragment){
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(android.R.id.content, fragment).addToBackStack(null).commit();
      }


}
