package com.mimi.africa.ui.orderhistory;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import com.mimi.africa.R;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Tools;

public class OrderHistoryActivity extends BaseActivity {


      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_order_history);

//            setupFragment(new OrderHistoryFragment());
      }

      private void initToolbar() {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_menu);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Basic");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this);
      }



      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_setting, menu);
            return true;
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                  finish();
            } else {
                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }
}
