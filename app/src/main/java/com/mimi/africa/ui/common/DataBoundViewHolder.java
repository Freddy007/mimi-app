package com.mimi.africa.ui.common;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A generic ViewHolder that works with a {@link ViewDataBinding}.
 * @param <T> The type of the ViewDataBinding.
 */
public class DataBoundViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    @NonNull
    public final T binding;
    DataBoundViewHolder(@NonNull T binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
