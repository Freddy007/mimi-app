package com.mimi.africa.ui.service;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentProductListBinding;
import com.mimi.africa.databinding.FragmentServiceListBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.service.adapter.AllServiceListAdapter;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class ServiceListFragment extends BaseFragment {


      private static final String TAG = ServiceListFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private  View parentView;
      private AutoClearedValue<FragmentServiceListBinding> binding;
      private APIService mAPIService;


      public ServiceListFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentServiceListBinding fragmentServiceListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_list, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentServiceListBinding);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }
            return fragmentServiceListBinding.getRoot();

      }

      @Override
      protected void initUIAndActions() {
            parentView= getActivity().findViewById(android.R.id.content);
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {
            binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
      }

      @Override
      protected void initData() {
            getServiceListings();
      }

      private void getServiceListings(){

            getServiceResponseObservable(mAPIService, "60")
                    .subscribe(new Observer<ServiceResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                Utils.ShowView(binding.get().productsProgressBar);
                          }

                          @Override
                          public void onNext(@NonNull ServiceResponse listingResponse) {
                                AllServiceListAdapter allServiceListAdapter = new AllServiceListAdapter(getContext(), listingResponse.getServices());

                                try {


                                      binding.get().productsRecyclerView.setAdapter(allServiceListAdapter);
                                      allServiceListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {
                                                  startActivity(new Intent(getContext(), StoreActivity.class));

                                            } else if (view == view.findViewById(R.id.addressTextView)) {

                                                  startActivity(new Intent(getContext(), LocationActivity.class));

                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.newsHolderCardView)) {

                                                  navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
                                            }
                                      });

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " +e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "Service Listings onError:  "  + e.getMessage() );

                                try {
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                      Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading services!", Snackbar.LENGTH_INDEFINITE)
                                              .setAction("RELOAD!", view -> getServiceListings());
                                      snackbar.show();
                                }catch (Exception ex){
                                      Log.e(TAG, "onError:  " +e.getMessage() );
                                }
                          }

                          @Override
                          public void onComplete() {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                      Utils.ShowView(binding.get().productsRecyclerView);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete:  " +e.getMessage() );
                                }

                          }
                    });
      }
}
