package com.mimi.africa.ui.product.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemProductVerticalListAdapterBinding;
import com.mimi.africa.databinding.ItemVerticalListBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AllProductVerticalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

      private List<Inventory> items = new ArrayList<>();
      private ItemProductVerticalListAdapterBinding itemProductVerticalListAdapterBinding;

      private Context ctx;
      private OnItemClickListener mOnItemClickListener;
      private OnMoreButtonClickListener onMoreButtonClickListener;
      private String TAG = AllProductVerticalListAdapter.class.getSimpleName();

      public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mOnItemClickListener = mItemClickListener;
      }

      public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
            this.onMoreButtonClickListener = onMoreButtonClickListener;
      }

      public AllProductVerticalListAdapter(Context context, List<Inventory> items) {
            this.items = items;
            ctx = context;
      }

      public class OriginalViewHolder extends RecyclerView.ViewHolder {
            public CardView imageCardView;
            public TextView shopNameTextView;
            public ImageView image;
            public ImageView productImageView;
            public TextView title;
            public TextView price;
            public TextView originalPrice;
            public TextView addressTextView;
            public TextView isSoldTextView;
            public TextView addedDateStrTextView;
            public ImageView profileCircleImageView;
            public ImageButton more;
            public View lyt_parent;
            public RatingBar ratingBar;
            public TextView ratingBarTextView;
            public TextView discountTextView;

            public OriginalViewHolder(@NonNull View v) {
                  super(v);

//                  shopNameTextView = itemVerticalLi stBinding.nameTextView;
                  imageCardView =v.findViewById(R.id.newsHolderCardView);
//                  locationImage = itemVerticalListBinding.imageView2;
                  productImageView = v.findViewById(R.id.imageView);
                  title = v.findViewById(R.id.news_title_textView);
                  price = v.findViewById(R.id.priceTextView);
//                  addressTextView = itemVerticalListBinding.addressTextView;
//                  isSoldTextView = itemVerticalListBinding.isSoldTextView;
//                  addedDateStrTextView = itemVerticalListBinding.addedDateStrTextView;
//                  profileCircleImageView = A
                  originalPrice                        =  v.findViewById(R.id.originalPriceTextView);
                  ratingBar                                     = v.findViewById(R.id.ratingBar);
                  ratingBarTextView                   = v.findViewById(R.id.ratingBarTextView);
                  discountTextView                   = v.findViewById(R.id.discountTextView);
            }
      }

      @NonNull
      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_vertical_list_adapter, parent, false);
            vh = new OriginalViewHolder(v);
            return vh;
      }

      @Override
      public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof OriginalViewHolder) {
                  OriginalViewHolder view = (OriginalViewHolder) holder;
                  final Inventory inventory = items.get(position);

                  view.title.setText(inventory.getTitle());

                  view.ratingBar.setRating((float) inventory.getRating());

                  view.ratingBarTextView.setText(String.format("%s rating  (%d Reviews )", inventory.getRating(), inventory.getSumFeedbacks()));

                  if (inventory.getImage() != null){
                        if (inventory.getImage().getPath() != null) {
                              if (inventory.getImage().getPath().startsWith("http")){

//                                    Picasso.with(ctx).load(inventory.getImage().getPath()).into(view.productImageView);
                                    Picasso.get().load(inventory.getImage().getPath()).into(view.productImageView);
                              }else {
                                    String imageUrl = Constants.IMAGES_BASE_URL +  inventory.getImage().getPath();
                                    Picasso.get().load(imageUrl).into(view.productImageView);
//                                    Picasso.with(ctx).load(imageUrl).into(view.productImageView);
                              }
                        }else{
                              view.productImageView.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
                        }
                  }else{
                        view.productImageView.setImageDrawable(ctx.getDrawable(R.drawable.default_image));
                  }

                  if ( inventory.getOfferPrice() != null){
                        view.discountTextView.setVisibility(View.GONE);
                        view.originalPrice.setVisibility(View.VISIBLE);
                        view.originalPrice.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));

                        view.originalPrice.setPaintFlags(view.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getOfferPrice()))));

//                        discounted_price = original_price - (original_price * discount / 100)

//                        int offerPrice =  Integer.parseInt(inventory.getOfferPrice());
                        int offerPrice = Integer.parseInt(String.format("%.0f", Double.valueOf(inventory.getOfferPrice())));
                        int salePrice = Integer.parseInt(String.format("%.0f", Double.valueOf(inventory.getSalePrice())));

                        int difference = salePrice- offerPrice;
                        int percentage = (difference / offerPrice);

                        Log.i(TAG, "onBindViewHolder:  Percentage "  + difference / offerPrice  + " " + salePrice  + " " + offerPrice + " difference " + difference);

                        view.discountTextView.setText(String.format("%d%%", percentage));
                  }else {
                        view.discountTextView.setVisibility(View.GONE);
                        view.originalPrice.setVisibility(View.GONE);
                        view.price.setText(String.format("GHS%s", String.format("%.2f", Double.valueOf(inventory.getSalePrice()))));
                  }

                  view.productImageView.setOnClickListener(v -> {
                        if (mOnItemClickListener != null) {
                              mOnItemClickListener.onItemClick(view.productImageView, items.get(position), position);
                        }
                  });

                  view.imageCardView.setOnClickListener(v -> {
                        if (mOnItemClickListener != null) {
                              mOnItemClickListener.onItemClick(view.imageCardView, items.get(position), position);
                        }
                  });

            }
      }

      @Override
      public int getItemCount() {
            return items.size();
      }

      public interface OnItemClickListener {
            void onItemClick(View view, Inventory obj, int pos);
      }

      public interface OnMoreButtonClickListener {
            void onItemClick(View view, Inventory obj, MenuItem item);
      }

}