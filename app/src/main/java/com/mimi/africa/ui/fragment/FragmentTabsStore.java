package com.mimi.africa.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.model.ShopProduct;
import com.mimi.africa.utils.DataGenerator;
import com.mimi.africa.utils.Tools;

import java.util.List;


public class FragmentTabsStore extends Fragment {

    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;


    public FragmentTabsStore() {
    }

    @NonNull
    public static FragmentTabsStore newInstance() {
        FragmentTabsStore fragment = new FragmentTabsStore();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabs_store, container, false);

//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_1), R.drawable.image_8);
//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_2), R.drawable.image_9);
        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_1), R.drawable.image_15);
        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_2), R.drawable.image_15);
        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_3), R.drawable.image_15);
//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_4), R.drawable.image_14);
//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_5), R.drawable.image_12);

//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_6), R.drawable.image_2);
//        Tools.displayImageOriginal(getActivity(), (ImageView) root.findViewById(R.id.image_7), R.drawable.image_5);

        recyclerView = root.findViewById(R.id.card_recyclerView);

//        List<ShopCategory> shopCategoryItems = DataGenerator.getShoppingCategory(this);
        List<ShopProduct> productItems = DataGenerator.getShoppingProduct(getContext());

        //set data and list adapter
        shopProductCardAdapter = new ShopProductCardAdapter(getContext(), productItems, 0);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
//        recyclerView.addItemDecoration(new SpacingItemDecoration(3, Tools.dpToPx(getContext(), 8), true));
        recyclerView.setAdapter(shopProductCardAdapter);

        return root;
    }
}