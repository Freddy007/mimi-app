package com.mimi.africa.ui.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.model.ShopProduct;
import com.mimi.africa.utils.DataGenerator;
import com.mimi.africa.utils.Tools;

import java.util.List;


public class FragmentSearchTabs extends Fragment {

    public static final String  ALL_TAB = "All";
    public static final String PRODUCTS_TAB = "More Products";
    public static final String SERVICES_TAB = "More Services";
    public static final String STORES_TAB = "Stores";

    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;


    public FragmentSearchTabs() {
    }

    @NonNull
    public static FragmentSearchTabs newInstance() {
        return new FragmentSearchTabs();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search_tabs, container, false);

        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_1), R.drawable.image_15);
        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_2), R.drawable.image_15);
        Tools.displayImageOriginal(getActivity(), root.findViewById(R.id.image_3), R.drawable.image_15);

        recyclerView = root.findViewById(R.id.card_recyclerView);

        List<ShopProduct> productItems = DataGenerator.getShoppingProduct(getContext());

//        AdapterGridShopProductCard shopProductCards = new AdapterGridShopProductCard(getContext(), productItems);
        //set data and list adapter
        shopProductCardAdapter = new ShopProductCardAdapter(getContext(), productItems, 0);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
//        recyclerView.setAdapter(shopProductCards);

        return root;
    }
}