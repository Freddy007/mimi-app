package com.mimi.africa.ui.home.adapter;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;


import com.mimi.africa.R;
import com.mimi.africa.databinding.ItemViewPagerAdapterBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private List<Inventory> featuredProducts;
    private androidx.databinding.DataBindingComponent dataBindingComponent;
    private ItemClick callback;

    public ViewPagerAdapter(androidx.databinding.DataBindingComponent dataBindingComponent, ItemClick callback) {
        this.dataBindingComponent = dataBindingComponent;
        this.callback = callback;
    }

    public void replaceFeaturedList(List<Inventory> featuredProductList) {
        this.featuredProducts = featuredProductList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {

        if (featuredProducts != null && featuredProducts.size() != 0) {
            return featuredProducts.size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        ItemViewPagerAdapterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.item_view_pager_adapter, container, false, dataBindingComponent);

        binding.setInventory(featuredProducts.get(position));

        Inventory inventory = featuredProducts.get(position);

        if (featuredProducts.get(position).isHasOffer()) {

            changeVisibilityOfDiscountTextView(binding, View.VISIBLE);
            binding.oldDiscountPriceTextView.setPaintFlags(binding.oldDiscountPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            int discountPercent = 0;
            if (inventory.getOfferPrice() != null && (inventory.getSalePrice() != null)) {

                binding.newDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getOfferPrice()))));
                binding.oldDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getSalePrice()))));            binding.featuredIconImageView.setVisibility(View.VISIBLE);
                binding.featuredIconImageView.setVisibility(View.VISIBLE);
            }

        } else {
            changeVisibilityOfDiscountTextView(binding, View.GONE);
            binding.oldDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getSalePrice()))));
            binding.newDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getSalePrice()))));

        }

        binding.ratingBar.setRating((float) (featuredProducts.get(position).getRating()));

        if (inventory.getImages() != null && inventory.getImages().size() > 0) {
            String url = Constants.IMAGES_BASE_URL + inventory.getImages().get(0).getPath();
            Utils.LoadImage(container.getContext(), binding.imageView25, url );
        }

        String currencySymbol = "GHS ";
//        binding.newDiscountPriceTextView.setText(String.format("%s %s", currencySymbol, Utils.format(Double.valueOf(featuredProducts.get(position).getOfferPrice()))));
//        binding.newDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getOfferPrice()))));
//        binding.oldDiscountPriceTextView.setText(String.format("GHS %s", String.format("%.2f", Double.valueOf(featuredProducts.get(position).getSalePrice()))));

//        binding.oldDiscountPriceTextView.setText(String.format("%s %s", currencySymbol, Utils.format(Double.valueOf(featuredProducts.get(position).getSalePrice()))));


        binding.ratingValueTextView.setText(binding.getRoot().getResources().getString(R.string.discount__rating5,
                String.valueOf(Utils.round((float) featuredProducts.get(position).getRating(), 2)),
//                String.valueOf(featuredProducts.get(position).ratingDetails.totalRatingCount)));
                String.valueOf(inventory.getSumFeedbacks())));

        binding.getRoot().setOnClickListener(view -> callback.onClick(featuredProducts.get(position)));

        container.addView(binding.getRoot());

        return binding.getRoot();
    }

    public interface ItemClick {
        void onClick(Inventory product);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }

    private void changeVisibilityOfDiscountTextView(ItemViewPagerAdapterBinding binding, int status) {

        binding.discountPercentTextView.setVisibility(status);

        binding.oldDiscountPriceTextView.setVisibility(status);
    }
}
