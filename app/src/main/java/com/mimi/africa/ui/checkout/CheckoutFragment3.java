package com.mimi.africa.ui.checkout;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.flutterwave.raveandroid.RaveUiManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.CheckoutFragment3Binding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.CheckOutReady;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.PaymentStatus;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.ui.basket.viewModel.BasketViewModel;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.ui.payment.FlutterPayActivity;
//import com.mimi.africa.ui.payment.RaveUIManager;
import com.mimi.africa.ui.payment.RaveUIManager;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;
import com.mimi.africa.viewModels.paypal.PaypalViewModel;
import  com.mimi.africa.viewModels.shop.ShopViewModel;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.viewModels.transaction.TransactionListViewModel;
import com.mimi.africa.viewObject.BasketProductListToServerContainer;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class CheckoutFragment3 extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private PaypalViewModel paypalViewModel;

    public AutoClearedValue<CheckoutFragment3Binding> binding;

    private BasketViewModel basketViewModel;
    private TransactionListViewModel transactionListViewModel;
    private ShopViewModel shopViewModel;
    private User user;
    private List<Basket> basketList;
    private BasketProductListToServerContainer basketProductListToServerContainer = new BasketProductListToServerContainer();
    private String clientTokenString;
    public String paymentMethod = Constants.PAYMENT_CASH_ON_DELIVERY;
    private String payment_method_nonce;
    private CardView oldCardView;
    private TextView oldTextView;
    private ImageView oldImageView;
    public boolean clicked = false;

    private PSDialogMsg psDialogMsg;
    private StringBuilder cartIds_sb;
    private Dialog progressDialog;

    private double total = 0;

    @Inject
    MimiDb mimiDb;

    @Inject
    protected APIService mAPIService;

    private APIService mPaymentService;

    BasketCountViewModel basketCountViewModel;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CheckoutFragment3Binding dataBinding = DataBindingUtil.inflate(inflater, R.layout.checkout_fragment_3, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);

        return binding.get().getRoot();
    }


    @Override
    public void onDispatched() {}

    @Override
    protected void initUIAndActions() {

        mPaymentService = Constants.getRetrofit(Constants.PAYMENTS_BASE_URL,null).create(APIService.class);

        cartIds_sb = new StringBuilder();

         progressDialog = new Dialog(getContext());

        binding.get().cashImageView.setColorFilter(getResources().getColor(R.color.md_grey_500));

        if (getActivity() instanceof MainCheckoutActivity) {
            ((MainCheckoutActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.com_facebook_loading), Utils.Fonts.MM_FONT)));
            ((MainCheckoutActivity) getActivity()).progressDialog.setCancelable(false);
        }

        binding.get().cashCardView.setOnClickListener(v -> {

            if (!clicked) {
                clicked = true;

                oldCardView = binding.get().cashCardView;
                oldImageView = binding.get().cashImageView;
                oldTextView = binding.get().cashTextView;

                changeToOrange(oldCardView, oldTextView, oldImageView);
            } else {

                if (oldCardView != null && oldImageView != null && oldTextView != null) {
                    changeToWhite(oldCardView, oldTextView, oldImageView);
                    changeToOrange(binding.get().cashCardView, binding.get().cashTextView, binding.get().cashImageView);

                    oldCardView = binding.get().cashCardView;
                    oldImageView = binding.get().cashImageView;
                    oldTextView = binding.get().cashTextView;
                }
            }

            binding.get().warningTitleTextView.setText(R.string.checkout__information__COD);

            paymentMethod = Constants.PAYMENT_CASH_ON_DELIVERY;

        });

        binding.get().momoCardView.setOnClickListener(v -> {

            if (!clicked) {
                clicked = true;

                oldCardView = binding.get().momoCardView;
                oldImageView = binding.get().momImageView;
                oldTextView = binding.get().momoTextView;

                changeToOrange(oldCardView, oldTextView, oldImageView);
            } else {

                if (oldCardView != null && oldImageView != null && oldTextView != null) {
                    changeToWhite(oldCardView, oldTextView, oldImageView);
                    changeToOrange(binding.get().momoCardView, binding.get().momoTextView, binding.get().momImageView);

                    oldCardView = binding.get().momoCardView;
                    oldImageView = binding.get().momImageView;
                    oldTextView = binding.get().momoTextView;
                }
            }

            binding.get().warningTitleTextView.setText(R.string.checkout__information__STRIPE);
//            binding.get().warningTitleTextView.setText(R.string.checkout__information__MOMO);

            paymentMethod = Constants.PAYMENT_MOMO;
            sendPaymentDetails();
        });

        psDialogMsg = new PSDialogMsg(this.getActivity(), false);


        if (cod.equals(Constant.ONE)) {
            binding.get().cashCardView.setVisibility(View.VISIBLE);

            if (binding.get().noPaymentTextView.getVisibility() == View.VISIBLE) {
                binding.get().noPaymentTextView.setVisibility(View.GONE);
            }
        }


        if (stripe.equals(Constant.ONE)) {
            binding.get().momoCardView.setVisibility(View.VISIBLE);
            if (binding.get().noPaymentTextView.getVisibility() == View.VISIBLE) {
                binding.get().noPaymentTextView.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void initViewModels() {
//        transactionListViewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionListViewModel.class);
        basketCountViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketCountViewModel.class);
    }

    @Override
    protected void initAdapters() {}



    public void sendData() {

        if (getActivity() != null) {
            user = ((MainCheckoutActivity) CheckoutFragment3.this.getActivity()).getCurrentUser();

//            if (paymentMethod.equals(Constants.PAYMENT_MOMO)){
//                psDialogMsg.showErrorDialog(getString(R.string.momo_not_supported), getString(R.string.app__ok));
//                psDialogMsg.show();
//                return;
//            }
                if (user.shippingAddress1.isEmpty()) {
                psDialogMsg.showErrorDialog(getString(R.string.shipping_address_one_error_message), getString(R.string.app__ok));
                psDialogMsg.show();
                return;
            }


        }


        if (basketList != null) {

            if (basketList.size() > 0) {

                if (getActivity() != null) {
                    switch (paymentMethod) {

                        case Constants.PAYMENT_PAYPAL:
                            break;

                        case Constants.PAYMENT_STRIPE:
                            break;

                        case Constants.PAYMENT_CASH_ON_DELIVERY:
                            EventBus.getDefault().post(new CheckOutReady("cod"));
                            break;
                    }
                }

            } else {
                psDialogMsg.showErrorDialog(getString(R.string.basket__no_item_desc), getString(R.string.app__ok));
                psDialogMsg.show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.REQUEST_CODE__PAYPAL) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);

                if (result.getPaymentMethodNonce() != null) {
                    onPaymentMethodNonceCreated(result.getPaymentMethodNonce());
                }
            }

        } else if (requestCode == Constant.REQUEST_CODE__STRIPE_ACTIVITY && resultCode == Constant.RESULT_CODE__STRIPE_ACTIVITY) {
            if (this.getActivity() != null) {

                payment_method_nonce = data.getStringExtra(Constant.PAYMENT_TOKEN);

//                progressDialog.show();
                if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
                    ((MainCheckoutActivity) getActivity()).progressDialog.show();
                }
                sendData();

            }
        }
    }


    @Override
    protected void initData() {
        getCarts();
    }


    private void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        if (getActivity() != null && getActivity() instanceof MainCheckoutActivity) {
            ((MainCheckoutActivity) getActivity()).progressDialog.show();
        }

        payment_method_nonce = paymentMethodNonce.getNonce();

        sendData();

//        }
        /*else {
            // Send nonce to server
            String nonce = paymentMethodNonce.getNonce();
        }*/
    }

    private void changeToOrange(CardView cardView, TextView textView, ImageView imageView) {

        cardView.setCardBackgroundColor(getResources().getColor(R.color.global__primary));
        imageView.setColorFilter(getResources().getColor(R.color.md_white_1000));
        textView.setTextColor(getResources().getColor(R.color.md_white_1000));
    }

    private void changeToWhite(CardView cardView, TextView textView, ImageView imageView) {

        cardView.setCardBackgroundColor(getResources().getColor(R.color.md_white_1000));
        imageView.setColorFilter(getResources().getColor(R.color.md_grey_500));
        textView.setTextColor(getResources().getColor(R.color.md_grey_700));
    }

    private void getCarts() {

        mAPIService.getCarts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CartResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull CartResponse cartResponse) {

                        List<Basket> cartList = new ArrayList<>();

                        for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                            cartIds_sb.append(cartResponse.getCarts().get(i).getId());

                            if ( i != (cartResponse.getCarts().size() -1) && cartResponse.getCarts().size() > 1){
                                cartIds_sb.append(",");
                            }

                            if (cartResponse.getCarts().get(i).getItems() != null) {

                                for (Inventory inventory : cartResponse.getCarts().get(i).getItems() ) {

                                    String attribute = "";
                                    JsonObject jsonObject = new JsonObject();
                                    if (inventory.getPivot() != null) {
                                        String description = inventory.getPivot().getItemDescription();
                                        String[] attributes = new String[3];
                                        if (description != null) {
                                            attributes = description.split(" - ");
                                            if (attributes.length == 3) {
                                                jsonObject.addProperty("1", attributes[1]);
                                            }else if (attributes.length == 4)
                                                jsonObject.addProperty("1", attributes[1]);
                                            jsonObject.addProperty("2", attributes[2]);
                                        }else if ( attributes.length > 4 ){
                                            jsonObject.addProperty("1", attributes[1]);
                                            jsonObject.addProperty("2", attributes[2]);
                                            jsonObject.addProperty("3", attributes[3]);
                                        }
                                    }

                                    Basket basket = new Basket(String.valueOf(inventory.getPivot().getCartId()), inventory.getPivot().getQuantity(), jsonObject.toString(), "1",
                                            "#000000","2", Float.valueOf(inventory.getPivot().getUnitPrice()), Float.valueOf(inventory.getPivot().getUnitPrice()),
                                            String.valueOf(1), "0" , inventory);
                                    cartList.add(basket);
                                }
                            }

                            if (cartResponse.getCarts().get(i) != null) {
                                int qty = (cartResponse.getCarts().get(i).getItems().get(0).getPivot().getQuantity());
//                                total += Double.parseDouble(cartResponse.getCarts().get(i).getTotal() );
                                total += Double.parseDouble(cartResponse.getCarts().get(i).getItems().get(0).getPivot().getUnitPrice()) * qty;

                            }
                        }

                        if (cartList != null && cartList.size() > 0) {
                            basketList = cartList;

                            for (Basket basket : cartList) {
                                //                    currency = basket.product.currencyShortForm;
                                HashMap<String, String> map = new Gson().fromJson(basket.selectedAttributes, new TypeToken<HashMap<String, String>>() {
                                }.getType());
                                Iterator<String> keyIterator = map.keySet().iterator();
                                StringBuilder keyStr = new StringBuilder();
                                StringBuilder nameStr = new StringBuilder();
                                while (keyIterator.hasNext()) {
                                    String key = keyIterator.next();
                                    if (!key.equals("-1")) {
                                        if (map.containsKey(key)) {

                                            if (!keyStr.toString().equals("")) {
                                                keyStr.append(Config.ATTRIBUTE_SEPARATOR);
                                                nameStr.append(Config.ATTRIBUTE_SEPARATOR);
                                            }
                                            keyStr.append(key);
                                            nameStr.append(map.get(key));

                                        }
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void checkout(String customer_id, String cart_ids, String id){

        String deliveryCost = Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost);

        mAPIService.checkout(Constants.DEFAULT_PAYMENT_METHOD,
                customer_id, cart_ids, id, deliveryCost, String.valueOf(total),"")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {

                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull JsonElement jsonElement) {}

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (getActivity() != null) {
                            ((MainCheckoutActivity) getActivity()).progressDialog.hide();
                            Utils.ShowSnackBar(getActivity(), "Failed!", Snackbar.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (getActivity() != null) {
                            appExecutors.getDiskIO().execute(new Runnable() {
                                @Override
                                public void run() {
                                    if (mimiDb.basketCountDao().getBasketCount() > 0) {
                                        int oldCount = mimiDb.basketCountDao().getCount(1).getCount();
                                        int newCount = 0;
                                        basketCountViewModel.setSaveToBasketListObj(newCount);
                                        mimiDb.basketCountDao().updateBasketCount(1, newCount);
                                    }
                                }
                            });
                            ((MainCheckoutActivity) getActivity()).progressDialog.hide();
//                            Utils.ShowSnackBar(getActivity(), "Successful", Snackbar.LENGTH_LONG);
                            Utils.showProgressDialog(getContext(), progressDialog, R.layout.dialog_progress);
                            progressDialog.findViewById(R.id.bt_close).setOnClickListener(v -> navigateToHome());
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckoutReady(CheckOutReady checkOutReady){
                if (getActivity() != null) {
                    ((MainCheckoutActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                    ((MainCheckoutActivity) getActivity()).progressDialog.setCancelable(false);
                    ((MainCheckoutActivity) getActivity()).progressDialog.show();
                }

                 new Handler().postDelayed(() -> {
                     String cart_ids = CustomSharedPrefs.getUserCartIds(getContext());
                     checkout(loginUserId, cartIds_sb.toString(), "1");
                     }, 3000);
    }

    private void sendPaymentDetails(){

        try {
            String pushyToken = CustomSharedPrefs.getPushyToken(getContext());
            user = ((MainCheckoutActivity) CheckoutFragment3.this.getActivity()).getCurrentUser();
            String deliveryCost = Utils.format(((MainCheckoutActivity) getActivity()).transactionValueHolder.shipping_cost);
            MediaType mediaType = MediaType.parse("application/json");
            double total_with_delivery_charge = total + Integer.valueOf(deliveryCost);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("tx_ref", UUID.randomUUID()+ "mimi");
            jsonObject.addProperty("amount", total + Integer.valueOf(deliveryCost));
            jsonObject.addProperty("currency", "GHS");
            jsonObject.addProperty("redirect_url",
                    "https://www.mimi.africa/api/payments/"+(cartIds_sb.toString())+"/"+deliveryCost+"/"+total_with_delivery_charge+"/"+pushyToken+"/"+loginUserId);
            jsonObject.addProperty("payment_options", "mobilemoney");
            jsonObject.addProperty("delivery_charge", deliveryCost);
            JsonObject metaJsonObject = new JsonObject();
            metaJsonObject.addProperty("delivery_charge", deliveryCost);
            metaJsonObject.addProperty("total_with_delivery_charge", total + Integer.valueOf(deliveryCost));
            metaJsonObject.addProperty("cart_ids", cartIds_sb.toString());
            metaJsonObject.addProperty("token", pushyToken);
            metaJsonObject.addProperty("user_id", loginUserId);
            jsonObject.add("meta", metaJsonObject);

            JsonObject customerJsonObject = new JsonObject();
            customerJsonObject.addProperty("email", user.userEmail);
//            customerJsonObject.addProperty("email", "frederickankamah988@gmail.com");
            customerJsonObject.addProperty("phonenumber", user.userPhone);
            customerJsonObject.addProperty("name", user.billingFirstName );
            jsonObject.add("customer",  customerJsonObject);
            RequestBody requestBody = RequestBody.create(mediaType, jsonObject.toString());
            sendPaymentRequest(requestBody);

            Log.i("CheckoutFragment3", "sendPaymentDetails: delivery cost " + Integer.valueOf(deliveryCost));
            Log.i("CheckoutFragment3", "sendPaymentDetails: total " + total);
            Log.i("CheckoutFragment3", "sendPaymentDetails: Amount " + jsonObject.get("amount"));
            Log.i("CheckoutFragment3", "sendPaymentDetails: redirect " + jsonObject.get("redirect_url"));

        }catch (Exception e){
            Sentry.captureException(e);
        }
    }

    private void sendPaymentRequest(RequestBody requestBody){
        String secret = " Bearer FLWSECK_TEST-04ebcad6562c5279fc516e7e1d868c0c-X";

        mPaymentService.sendPaymentRequest(secret,requestBody )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PaymentStatus>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) { disposable = d; }

                    @Override
                    public void onNext(PaymentStatus paymentStatus) {
                        Intent intent = new Intent(getActivity(), FlutterPayActivity.class);
                        intent.putExtra("payment_url", paymentStatus.getData().getLink());
                        startActivity(intent);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
}













































