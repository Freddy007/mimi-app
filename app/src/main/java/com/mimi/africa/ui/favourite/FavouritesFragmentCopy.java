//package com.mimi.africa.ui.favourite;
//
//
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.databinding.DataBindingUtil;
//import androidx.recyclerview.widget.GridLayoutManager;
//
//import com.mimi.africa.Adapter.ProductVerticalListAdapter;
//import com.mimi.africa.R;
//import com.mimi.africa.api.APIService;
//import com.mimi.africa.api.response.ListingResponse;
//import com.mimi.africa.binding.FragmentDataBindingComponent;
//import com.mimi.africa.databinding.FragmentFavouritesBinding;
//import com.mimi.africa.ui.common.BaseFragment;
//import com.mimi.africa.utils.AutoClearedValue;
//import com.mimi.africa.utils.Utils;
//
//import javax.inject.Inject;
//
//import io.reactivex.Observer;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//
//
//public class FavouritesFragmentCopy extends BaseFragment {
//
//      private static final String TAG = FavouritesFragmentCopy.class.getSimpleName();
//      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
//      private View parentView;
//      private AutoClearedValue<FragmentFavouritesBinding> binding;
//
//      @Inject
//      APIService mAPIService;
//
//      public FavouritesFragmentCopy() {}
//
//      @Override
//      public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                               Bundle savedInstanceState) {
//
//            // Inflate the layout for this fragment
//            FragmentFavouritesBinding fragmentFavouritesBinding =
//                    DataBindingUtil.inflate(inflater, R.layout.fragment_favourites, container, false, dataBindingComponent);
//            binding = new AutoClearedValue<>(this, fragmentFavouritesBinding);
//
//            return fragmentFavouritesBinding.getRoot();
//      }
//
//      @Override
//      protected void initUIAndActions() {}
//
//      @Override
//      protected void initViewModels() {}
//
//      @Override
//      protected void initAdapters() {}
//
//      @Override
//      protected void initData() {
//            getWishLists();
//      }
//
//      private void getWishLists() {
//
//            String customerId = loginUserId;
//            final boolean[] empty = {false};
//
//            mAPIService.getInventoryWishList(customerId)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ListingResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().lytProgress);
//                                } catch (Exception e) {
////                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ListingResponse listingResponse) {
//
//                                empty[0] = (listingResponse.getInventories().size() <= 0);
//
//                                try {
//                                      binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
//                                      ProductVerticalListAdapter productVerticalListAdapter = new ProductVerticalListAdapter(getContext(), listingResponse.getInventories());
//                                      binding.get().productsRecyclerView.setAdapter(productVerticalListAdapter);
//
//                                      productVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (view == view.findViewById(R.id.nameTextView)) {
//
//                                                  navigateToStore(obj);
//
//                                            } else if (view == view.findViewById(R.id.addressTextView)) {
//
//                                                  navigateToLocation(getActivity());
//
//                                            } else if (view == view.findViewById(R.id.cardView12)) {
//
//                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
//                                            }
//
//                                      });
//
//                                      productVerticalListAdapter.setOnMoreButtonClickListener((view, obj, item) -> {});
//
//                                }catch (Exception e){
//                                      Log.e(TAG, "onNext: " + e.getMessage());
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                try {
////                                      reloadSnackBar(binding.get().productsProgressBar, "There was a problem loading products!");
//
//                                } catch (Exception ex) {
//                                      logError(TAG, e, "onError: ");
//                                }
//
//                                logError(TAG, e, "onError: getListings  ");
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                if (!disposable.isDisposed()) {
//                                      disposable.dispose();
//                                }
//
//                                try {
//                                      if (empty[0]){
//                                            Utils.ShowView(binding.get().noFavouritesLinearLayout);
//                                            Utils.RemoveView(binding.get().productsRecyclerView);
//                                            Utils.RemoveView(binding.get().lytProgress);
//                                      }else {
//                                            Utils.RemoveView(binding.get().noFavouritesLinearLayout);
//                                            Utils.ShowView(binding.get().productsRecyclerView);
//                                            Utils.RemoveView(binding.get().lytProgress);
//                                      }
//
//
//                                } catch (Exception e) {
//                                      logError(TAG,e, "onComplete:  ");
//                                }
//                          }
//                    });
//      }
//
//}
