package com.mimi.africa.ui.orderhistory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.R;
import com.mimi.africa.model.Order;
import com.mimi.africa.model.OrderItem;
import com.mimi.africa.model.ShopProduct;
import com.mimi.africa.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class InvoiceItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<OrderItem> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnMoreButtonClickListener onMoreButtonClickListener;

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setOnMoreButtonClickListener(final OnMoreButtonClickListener onMoreButtonClickListener) {
        this.onMoreButtonClickListener = onMoreButtonClickListener;
    }

    public InvoiceItemAdapter(Context context, List<OrderItem> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView itemNameTextView;
        public TextView itemAmountTextView;

        public OriginalViewHolder(@NonNull View v) {
            super(v);
//            locationImage = v.findViewById(R.id.locationImage);

            itemNameTextView = v.findViewById(R.id.item_name);
            itemAmountTextView = v.findViewById(R.id.item_amount);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoice, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final OrderItem orderItem = items.get(position);

//            ((OriginalViewHolder) holder).itemNameTextView.setText(inventory.getTitle());
            ((OriginalViewHolder) holder).itemNameTextView.setText(orderItem.getItemDescription());
//            if (inventory.getPivot() != null) {

//                float unitPrice = Utils.roundDouble(Double.valueOf(inventory.getPivot().getUnitPrice()), 3);
                float unitPrice = Utils.roundDouble(Double.valueOf(orderItem.getUnitPrice()), 3);

                ((OriginalViewHolder) holder).itemAmountTextView
                         .setText(String.format("GHS %s x %s", unitPrice, orderItem.getQuantity()));
//            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Order obj, int pos);
    }

    public interface OnMoreButtonClickListener {
        void onItemClick(View view, ShopProduct obj, MenuItem item);
    }

}