package com.mimi.africa.ui.product;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.ProductVerticalListAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentProductListBinding;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ProductListFragment extends BaseFragment {

      private static final String TAG = ProductListFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private  View parentView;
      private AutoClearedValue<FragmentProductListBinding> binding;
      private APIService mAPIService;

      public ProductListFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentProductListBinding fragmentProductListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProductListBinding);

            if (mAPIService == null){
                  mAPIService = Constants.getRetrofit(Constants.BASE_URL, null).create(APIService.class);
            }
            return fragmentProductListBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            parentView= getActivity().findViewById(android.R.id.content);
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
            getListings("random");
      }

      private void getListings(String list){

            getListingResponseObservable()
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                Utils.ShowView(binding.get().productsProgressBar);
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {
                                Log.i(TAG, "onNext:  " + listingResponse.getInventories().get(0).getTitle() );

                                try {
                                      binding.get().productsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                      ProductVerticalListAdapter productVerticalListAdapter = new ProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().productsRecyclerView.setAdapter(productVerticalListAdapter);

                                      productVerticalListAdapter.setOnItemClickListener(new ProductVerticalListAdapter.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(View view, Inventory obj, int pos) {

                                                  if (view == view.findViewById(R.id.nameTextView)) {

                                                        navigateToStore(obj);

                                                  } else if (view == view.findViewById(R.id.addressTextView)) {

                                                        navigateToLocation(getActivity());

                                                  } else if (view == view.findViewById(R.id.cardView12)) {

                                                        navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(pos));
                                                  }

                                            }

                                            @Override
                                            public void onItemLongClick(View view, Inventory obj, int pos) {

                                            }
                                      });

//                                      productVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (view == view.findViewById(R.id.nameTextView)) {
//
//                                                  navigateToStore(obj);
//
//                                            } else if (view == view.findViewById(R.id.addressTextView)) {
//
//                                            navigateToLocation(getActivity());
//
//                                            } else if (view == view.findViewById(R.id.cardView12)) {
//
//                                            navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
//                                            }
//                                      });

                                }catch (Exception e){
                                      Log.e(TAG, "onNext: " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Utils.RemoveView(binding.get().productsProgressBar);
                                Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RELOAD!", view -> getListings("random"));
                                snackbar.show();

                                Log.e(TAG, "onError: getListings  "  + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      Utils.ShowView(binding.get().productsRecyclerView);
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception e){
                                      Log.e(TAG, "onComplete: "   +e. getMessage());
                                }
                          }
                    });
      }

      private Observable<ListingResponse> getListingResponseObservable() {
            return mAPIService.getListings()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }


}
