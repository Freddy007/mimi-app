package com.mimi.africa.ui.category;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.GroupResponse;
import com.mimi.africa.api.response.SubGroupResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentCategoryListBinding;
import com.mimi.africa.ui.category.adapter.GroupListAdapter;
import com.mimi.africa.ui.category.adapter.SubGroupCategoryListAdapter;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class SubGroupListFragment extends BaseFragment {

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

      @Inject
      APIService mAPIService;

      @VisibleForTesting
      private AutoClearedValue<FragmentCategoryListBinding> binding;
      private AutoClearedValue<GroupListAdapter> adapter;

      public SubGroupListFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            // Inflate the layout for this fragment
//            return inflater.inflate(R.layout.fragment_category_list, container, false);

            FragmentCategoryListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_list, container, false, dataBindingComponent);

            binding = new AutoClearedValue<>(this, dataBinding);

            return binding.get().getRoot();
      }

      @Override
      protected void initUIAndActions() {
            binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.view__primary_line));
            binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.global__primary));
//            binding.get().swipeRefresh.setOnRefreshListener(getSubGroups(""));
          binding.get().swipeRefresh.setOnRefreshListener(() -> getSubGroups("all"));

          binding.get().allGroupsTextview.setOnChipClicked(view -> getSubGroups("all"));
      }

      @Override
      protected void initViewModels() {
      }

      @Override
      protected void initAdapters() {
      }

      @Override
      protected void initData() {
            getSubGroups("all");
            getGroups();
      }

      private void getSubGroups(String group_id){
            mAPIService.getSubGroups(group_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SubGroupResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                              if (getActivity() != null) {
                                  ((SubGroupListActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                                  ((SubGroupListActivity) getActivity()).progressDialog.setCancelable(false);
                                  ((SubGroupListActivity) getActivity()).progressDialog.show();
                              }
                          }

                          @Override
                          public void onNext(@NonNull SubGroupResponse subGroupResponse) {


                                SubGroupCategoryListAdapter subGroupCategoryListAdapter = new SubGroupCategoryListAdapter(getContext(), subGroupResponse.getSubGroups());
                                binding.get().subCategoryList.setAdapter(subGroupCategoryListAdapter);

                                subGroupCategoryListAdapter.setOnItemClickListener((view, obj, position) -> {
                                      Intent intent = new Intent(getContext(), CategoryListActivity.class);
                                      intent.putExtra(Constants.SUB_GROUP_ID,  obj.getId());
                                      intent.putExtra(Constants.SUB_GROUP_NAME,  obj.getName());
                                      startActivity(intent);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                              if (getActivity() != null) {
                                  binding.get().swipeRefresh.setRefreshing(false);
                                  ((SubGroupListActivity) getActivity()).progressDialog.hide();
                              }
                          }
                    });
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

          private void getGroups(){
            mAPIService.getGroups()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GroupResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onNext(@NonNull GroupResponse groupResponse) {

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        binding.get().groupsRecyclerView.setLayoutManager(linearLayoutManager);
                        binding.get().groupsRecyclerView.setHasFixedSize(true);
                        binding.get().groupsRecyclerView.setNestedScrollingEnabled(false);
                       GroupListAdapter groupListAdapter = new GroupListAdapter(getContext(), groupResponse.getGroups());
                        binding.get().groupsRecyclerView.setAdapter(groupListAdapter);

//                          Toast.makeText(getContext(), " " + groupResponse.getGroups().size(), Toast.LENGTH_SHORT).show();

                        groupListAdapter.setOnItemClickListener((view, obj, position) -> {
                            getSubGroups(String.valueOf(obj.getId()));
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
//                        Log.e(TAG, "onError: get Categories  "  + e.getMessage() );
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
//                        Utils.RemoveView(progressBar);
//                        Utils.ShowView(recyclerView);
                    }
                });
    }
}
