package com.mimi.africa.ui.search;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.Adapter.ProductVerticalListAdapter;
import com.mimi.africa.Adapter.ShopProductCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomFavouriteOptionsLayoutBinding;
import com.mimi.africa.databinding.BottomMessageOptionsLayoutBinding;
import com.mimi.africa.databinding.ProductsFragmentSearchTabsBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.Reload;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ProductsFragmentSearchTabs extends BaseFragment {

    public static final String  ALL_TAB = "All";
    public static final String PRODUCTS_TAB = "Products";
    public static final String SERVICES_TAB = "Services";
    public static final String STORES_TAB = "Stores";
    private static final String TAG = ProductsFragmentSearchTabs.class.getSimpleName();
    private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    private RecyclerView recyclerView;
    private ShopProductCardAdapter shopProductCardAdapter;
    private  View parentView;
    private AutoClearedValue<ProductsFragmentSearchTabsBinding> binding;
    private String mTerm;

    private AutoClearedValue<BottomFavouriteOptionsLayoutBinding> bottomFavouriteOptionsLayoutBinding;
    private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
    private BasketCountViewModel basketCountViewModel;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;


    @Inject
    protected MimiDb mimiDb;

    private PSDialogMsg psDialogMsg;

    @Inject
    APIService mAPIService;
    private List<Inventory> mWishlists;


    public ProductsFragmentSearchTabs() {
    }

    @NonNull
    public static ProductsFragmentSearchTabs newInstance() {
        return new ProductsFragmentSearchTabs();
    }

    @NonNull
    public static ProductsFragmentSearchTabs newInstance( String term) {
        ProductsFragmentSearchTabs productsFragmentSearchTabs = new ProductsFragmentSearchTabs();
        Bundle bundle = new Bundle();
        bundle.putString("term", term);
        productsFragmentSearchTabs.setArguments(bundle);
        return productsFragmentSearchTabs;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null){
            mTerm = getArguments().getString("term");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ProductsFragmentSearchTabsBinding fragmentSearchTabsBinding =
                DataBindingUtil.inflate(inflater, R.layout.products_fragment_search_tabs, container, false, dataBindingComponent);
        binding = new AutoClearedValue<>(this, fragmentSearchTabsBinding);

        return fragmentSearchTabsBinding.getRoot();
    }


    private Observable<ListingResponse> getListingResponseObservable() {
        return mAPIService.getListings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<ListingResponse> getOffersResponseObservable() {
        return mAPIService.getOffers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<ListingResponse> getSearchResponseObservable(String term) {
        return mAPIService.searchProducts(term)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    protected void initUIAndActions() {
        psDialogMsg = new PSDialogMsg(getActivity(), false);
        parentView= getActivity().findViewById(android.R.id.content);

        mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
        bottomFavouriteOptionsLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_favourite_options_layout, null, false));
        mBottomSheetDialog.get().setContentView(bottomFavouriteOptionsLayoutBinding.get().getRoot());
    }

    @Override
    protected void initViewModels() {
        basketCountViewModel = ViewModelProviders.of(this,viewModelFactory).get(BasketCountViewModel.class);
    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {


        if (CustomSharedPrefs.getReloadPageStatus(getContext())) {

            if (mTerm != null && mTerm.equals("wishlists") ){
                getWishLists();
            }else{
                getListings("random");
            }

        }else {
            Utils.ShowView(binding.get().noItemConstraintLayout);
            binding.get().noItemDescTextView.setText(getResources().getString(R.string.search_message));
            binding.get().noItemTitleTextView.setText("Search");
            Utils.RemoveView(binding.get().productsProgressBar);
        }
    }

    private void getListings(String list){

        getOffersResponseObservable()
                .subscribe(new Observer<ListingResponse>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;

                        try {

                            Utils.ShowView(binding.get().productsProgressBar);
                        }catch (Exception e){
                            Log.e(TAG, "onSubscribe: "  + e.getMessage() );
                        }
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {

                        initializeAdapter(listingResponse.getInventories());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try{
                            Utils.RemoveView(binding.get().productsProgressBar);
                        }catch (Exception exx){
                            Log.e(TAG, "onError:  " + exx.getMessage() );
                        }

                        Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG)
                                .setAction("RELOAD!", view -> getListings("random"));
                        snackbar.show();

                        Log.e(TAG, "onError: getListings  "  + e.getMessage() );

                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                        try {
                            Utils.ShowView(binding.get().productsRecyclerView);
                            Utils.RemoveView(binding.get().productsProgressBar);
                        }catch (Exception e){
                            Log.e(TAG, "onComplete: "   +e. getMessage());
                        }
                    }
                });
    }

    private void initializeAdapter(@NonNull List<Inventory> inventoryList) {
        try {

            if (getActivity() != null)

                binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
                ProductVerticalListAdapter productVerticalListAdapter = new ProductVerticalListAdapter(getContext(), inventoryList);
                binding.get().productsRecyclerView.setAdapter(productVerticalListAdapter);

                productVerticalListAdapter.setOnItemClickListener(new ProductVerticalListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Inventory obj, int pos) {

                        if (view == view.findViewById(R.id.nameTextView)) {

                            navigateToStore(obj);

                        } else if (view == view.findViewById(R.id.addressTextView)) {

                            navigateToLocation(getActivity());

                        } else if (view == view.findViewById(R.id.productImageView)) {

                            navigateToItemDetailActivity(getActivity(), inventoryList.get(pos));

                        }else {

                            navigateToItemDetailActivity(getActivity(), inventoryList.get(pos));
                        }

                    }

                    @Override
                    public void onItemLongClick(View view, Inventory obj, int pos) {
                        mBottomSheetDialog.get().show();
                        bottomFavouriteOptionsLayoutBinding.get().titleTextView.setText(obj.getTitle());
                        bottomFavouriteOptionsLayoutBinding.get().readStatus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                addToCart(1, obj.getSlug());
                            }
                        });

                        bottomFavouriteOptionsLayoutBinding.get().archiveMessage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                removeFromWishList(String.valueOf(obj.getProduct().getId()));
                            }
                        });
                    }
                });

        }catch (Exception e){
            Log.e(TAG, "onNext: " + e.getMessage());
        }
    }

    private void search(String term){

        final boolean[] empty = {false};

        getSearchResponseObservable(term)
                .subscribe(new Observer<ListingResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                        subscribeRequest();
                    }

                    @Override
                    public void onNext(@NonNull ListingResponse listingResponse) {
                        initAdapters();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try{
                            Utils.RemoveView(binding.get().productsProgressBar);
                        }catch (Exception exx){
                            Log.e(TAG, "onError:  " + exx.getMessage() );
                            Sentry.captureException(e);
                        }

                        Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG)
                                .setAction("RELOAD!", view -> {
//                                    getListings("random");
                                    search(term);
                                });

                        Sentry.captureException(e);

                    }

                    @Override
                    public void onComplete() {
                        completeRequest(empty);
                    }
                });
    }

    private void subscribeRequest() {
        try {

            Utils.ShowView(binding.get().productsProgressBar);
            Utils.RemoveView(   binding.get().noItemConstraintLayout);

        }catch (Exception e){
            Log.e(TAG, "onSubscribe: "  + e.getMessage() );
            Sentry.captureException(e);
        }
    }

    private void getWishLists() {

            String customerId = loginUserId;
            final boolean[] empty = {false};

            mAPIService.getInventoryWishList(customerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

                              subscribeRequest();

                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                empty[0] = (listingResponse.getInventories().size() <= 0);

                              initializeAdapter(listingResponse.getInventories());

                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try {

                                } catch (Exception ex) {
                                      logError(TAG, e, "onError: ");
                                }

                                logError(TAG, e, "onError: getListings  ");
                          }

                          @Override
                          public void onComplete() {
                              if (!disposable.isDisposed()) {
                                  disposable.dispose();
                              }

                              completeRequest(empty);
                          }
                    });
      }

    private void completeRequest(boolean[] empty) {
        try {
            if (empty[0]) {
                Utils.ShowView(binding.get().noItemConstraintLayout);
                Utils.RemoveView(binding.get().productsRecyclerView);
                Utils.RemoveView(binding.get().productsProgressBar);

            } else {
                Utils.ShowView(binding.get().productsRecyclerView);
                Utils.RemoveView(binding.get().productsProgressBar);
//                                binding.get().noItemConstraintLayout.setVisibility(View.VISIBLE);
                Utils.RemoveView(binding.get().noItemConstraintLayout);

            }
        } catch (Exception e) {
            Log.e(TAG, "onComplete: " + e.getMessage());
            Sentry.captureException(e);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(SearchEvent event) {
        search(event.getTerm());
    }

    private void addToCart(int qty, String slug){
        mAPIService.addToCart(qty,"","","" ,slug)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (getContext() != null) {
                            mBottomSheetDialog.get().dismiss();
                            binding.get().setLoadingMore(true);

                            appExecutors.getDiskIO().execute(new Runnable() {
                                @Override
                                public void run() {
                                    if (mimiDb.basketCountDao().getBasketCount() > 0) {
                                        int oldCount = mimiDb.basketCountDao().getCount(1).getCount();
                                        int newCount = oldCount + qty;
                                        basketCountViewModel.setSaveToBasketListObj(newCount);
                                        mimiDb.basketCountDao().updateBasketCount(1, newCount);
                                    }else {
                                        BasketCount basketCount = new BasketCount();
                                        basketCount.setCount(qty);
                                        mimiDb.basketCountDao().insert(basketCount);
                                    }
                                }
                            });

                        }
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {}

                    @Override
                    public void onError(@NonNull Throwable e) { Log.e(TAG, "onError:  " + e.getMessage() ); }

                    @Override
                    public void onComplete() {
                        if (getContext() != null) {
                                      mBottomSheetDialog.get().dismiss();
                            binding.get().setLoadingMore(false);
                        }
                        psDialogMsg.showSuccessDialog(getString(R.string.product_detail__successfully_added), getString(R.string.app__ok));
                        psDialogMsg.show();

                    }
                });
    }

    public void removeFromWishList(String productId){
        String customer_id = loginUserId;
        mAPIService.removeFromWishList(customer_id,productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                    Disposable disposable;
                    @Override
                    public void onSubscribe(Disposable d) {
                        mBottomSheetDialog.get().dismiss();
                        binding.get().setLoadingMore(true);
                        disposable = d;
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {}

                    @Override
                    public void onError(Throwable e) {
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {
                        if (getActivity() != null)
                            Utils.ShowSnackBar(getActivity(), "Successfully removed from wishlist!", Snackbar.LENGTH_LONG);
                            getWishLists();
                            EventBus.getDefault().post(new Reload());
                          binding.get().setLoadingMore(false);

                    }
                });
    }



}