package com.mimi.africa.ui.help;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentFavouritesBinding;
import com.mimi.africa.databinding.FragmentHelpBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.search.FragmentSearchTabs;
import com.mimi.africa.ui.search.ProductsFragmentSearchTabs;
import com.mimi.africa.ui.search.ServicesFragmentSearchTabs;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.viewModels.BasketCountViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class HelpFragment extends BaseFragment {

      private static final String TAG = HelpFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentHelpBinding> binding;


      @Inject
      APIService mAPIService;

      public HelpFragment() {}

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentHelpBinding fragmentHelpBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentHelpBinding);

            return fragmentHelpBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
//            WebView myWebView = (WebView) findViewById(R.id.webview);
            binding.get().webview.loadUrl("https://www.mimi.africa/selling#faqs");

            WebSettings webSettings = binding.get().webview.getSettings();
            webSettings.setJavaScriptEnabled(true);

      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
      }


}
