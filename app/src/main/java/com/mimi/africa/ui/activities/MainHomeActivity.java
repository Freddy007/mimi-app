package com.mimi.africa.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mimi.africa.Adapter.AdapterGridShopCategory;
import com.mimi.africa.BuildConfig;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.databinding.ActivityMainHomeBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.LogOutEvent;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.ui.basket.BasketListActivity;
import com.mimi.africa.ui.common.NavigationController;
import com.mimi.africa.ui.favourite.FavouritesFragment;
import com.mimi.africa.ui.help.HelpFragment;
import com.mimi.africa.ui.login.UserLoginActivity;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.message.MessageFragment;
import com.mimi.africa.ui.notifications.NotificationsFragment;
import com.mimi.africa.ui.orderhistory.MyOrdersActivity;
import com.mimi.africa.ui.profile.ProfileFragment;
import com.mimi.africa.ui.promo.PromoFragment;
import com.mimi.africa.R;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.mainhome.MainHomeFragment;
import com.mimi.africa.ui.orderhistory.OrderHistoryFragment;
import com.mimi.africa.ui.settings.AppSettingsFragment;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;
import com.mimi.africa.viewModels.ShopViewModel;
import com.mimi.africa.viewModels.user.UserViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;
import me.pushy.sdk.Pushy;


public class MainHomeActivity extends BaseActivity {

    private static final String TAG = MainHomeActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private UserViewModel userViewModel;

    @Inject
      protected ViewModelProvider.Factory viewModelFactory;

      @Inject
     APIService mAPIService;

      ActivityMainHomeBinding activityMainHomeBinding;

        @Nullable
        private ActionBar actionBar;
        private Toolbar toolbar;
        private AdapterGridShopCategory mAdapter;
        private TextView search_text;
        private BottomNavigationView navigationView;
        private NavigationView nav_view;
        private DrawerLayout drawer;
        private Activity mActivity;
        private Context context ;
        private TextView txtSpeechInput;
        private ImageButton btnSpeak;
        private final int REQ_CODE_SPEECH_INPUT = 100;

        private boolean doubleBackToExitPressedOnce;
        private Handler mHandler = new Handler();
        private ShopViewModel shopViewModel;
        private BasketCountViewModel basketCountViewModel;
        private String loginUserId;
        private  String userIdToVerify;

        private PSDialogMsg psDialogMsg;

        private TextView searchTextView;

        private TextView mtxtfirst;

        public ProgressDialog progressDialog;

    @Inject
    protected MimiDb mimiDb;

    @Inject
    protected SharedPreferences pref;

    @Inject
    protected Connectivity connectivity;

    @Inject
    NavigationController navigationController;

    protected String userName,fullName, phoneNumber,email, address,city;

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };


    @NonNull
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Utils.ShowView(btnSpeak);
                        search_text.setText(getString(R.string.search_for_offers));
                        setupFragment(new MainHomeFragment());
                        return true;

                    case R.id.navigation_promo:
                        Utils.RemoveView(btnSpeak);
                        search_text.setText(getString(R.string.promotions));
                        setupFragment(new PromoFragment());
                        return true;

                    case R.id.navigation_cart:
                            startActivity(new Intent(MainHomeActivity.this, BasketListActivity.class));
                            return true;

                    case R.id.navigation_message:
                        Utils.RemoveView(btnSpeak);
                        search_text.setText(getString(R.string.messages));
                        setupFragment(new MessageFragment());
                        return true;

                    case R.id.navigation_notifications:
                        unCheckSideMenuItems();
                        Utils.RemoveView(btnSpeak);
                        search_text.setText(getString(R.string.notifications));
                        setupFragment(new NotificationsFragment());
                        return true;
                }

                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_home);

        Pushy.listen(this);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        userName = pref.getString(Constant.USER_NAME, Constant.EMPTY_STRING);
        email = pref.getString(Constant.USER_EMAIL, Constant.EMPTY_STRING);
        fullName = pref.getString(Constant.FULL_NAME, Constant.EMPTY_STRING);
        phoneNumber = pref.getString(Constant.USER_PHONE, Constant.EMPTY_STRING);
        loginUserId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        userIdToVerify = pref.getString(Constant.USER_ID_TO_VERIFY, Constant.EMPTY_STRING);

        psDialogMsg = new PSDialogMsg(this, false);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        mActivity = this;
//        nav_view = findViewById(R.id.nav_view);
        nav_view = activityMainHomeBinding.navView;
//        drawer = findViewById(R.id.drawer_layout);
        drawer = activityMainHomeBinding.drawerLayout;

        shopViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShopViewModel.class);
        basketCountViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketCountViewModel.class);

        initToolbar();
        initNavigationMenu();
        initComponent();

        checkSelectedBottomMenuSelected();
//        btnSpeak = findViewById(R.id.btnSpeak);
        btnSpeak = activityMainHomeBinding.btnSpeak;

        btnSpeak.setOnClickListener(v -> promptSpeechInput());

        getShopsFromViewModel();
        getBasketCountsViewModel();
        getCarts();

        Log.i(TAG, "onCreate:  " );

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        if (data != null && data.toString().contains("message")){
            setupFragment(new MessageFragment());
        }

    }

    private void initToolbar() {
//        toolbar =  findViewById(R.id.toolbar);
        toolbar =  activityMainHomeBinding.toolbar;
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setHomeButtonEnabled(true);
        Tools.setSystemBarColor(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void initComponent() {
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);

        userViewModel.setLoginUser();

        userViewModel.getLoginUser().observe(this, data -> {

            if (data != null) {

                if (data.size() > 0) {
                    userViewModel.user = data.get(0).user;
                }
            }

        });

        navigationView = activityMainHomeBinding.navigation;

        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        search_text = activityMainHomeBinding.searchText;

          search_text.setOnClickListener(v -> {
              startActivity(new Intent(getApplicationContext(), MainSearchActivity.class));
          });

          MainHomeFragment mainHomeFragment = new MainHomeFragment();
          setupFragment(mainHomeFragment);
    }

    private void initNavigationMenu() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (!fullName.isEmpty() || !fullName.equals("")) {
            char first = fullName.charAt(0);

            View header = activityMainHomeBinding.navView.getHeaderView(0);
            TextView textview = (TextView) header.findViewById(R.id.txtfirst);
            TextView nameTextview = header.findViewById(R.id.txt_name);
            TextView emailTextview = header.findViewById(R.id.txt_email);
            TextView mobTextview = header.findViewById(R.id.txt_mob);
            textview.setText("" + first);
            nameTextview.setText("" + fullName);
            emailTextview.setText("" + email);
            mobTextview.setText("" + phoneNumber);
        }


        nav_view.setNavigationItemSelectedListener(item -> {

            switch (item.getItemId()){
                case R.id.nav_home:
                    Utils.ShowView(btnSpeak);
                    search_text.setText(getString(R.string.search_for_offers));
                    setupFragment(new MainHomeFragment());
                    break;

                case R.id.nav_order_history:

                    Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                            MainHomeActivity.this, navigationController, () -> {
                                Utils.RemoveView(btnSpeak);
//                                search_text.setText(getString(R.string.order_history));
//                                setupFragment(new OrderHistoryFragment());
                                startActivity(new Intent(MainHomeActivity.this, MyOrdersActivity.class));
                            });
                    break;

//                case R.id.nav_profile:
//                    Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
//                            MainHomeActivity.this, navigationController, this::showProfileDialogFragment);
//                    break;

                case R.id.nav_invite_link:
                        shareContent();
                    break;

                case R.id.navigation_favorites:
                    Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg,
                            MainHomeActivity.this, navigationController, new Utils.NavigateOnUserVerificationActivityCallback() {
                                @Override
                                public void onSuccess() {
                                    setupFragment(new FavouritesFragment());
                                    Utils.RemoveView(btnSpeak);
                                    search_text.setText(getString(R.string.favourites));
                                }
                            });

                    break;

                case R.id.nav_settings:
                    Utils.RemoveView(btnSpeak);
                    search_text.setText(getString(R.string.settings));
                    setupFragment(new AppSettingsFragment());
                    break;

                case R.id.nav_help:
                    Utils.RemoveView(btnSpeak);
                    search_text.setText("FAQ'S");
                    setupFragment(new HelpFragment());
                    break;

                case R.id.nav_logout:
                        logout();

                    break;

                    default:
            }

            actionBar.setTitle(item.getTitle());
            drawer.closeDrawers();
            return true;

        });

        logoutButtonVisibility();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(AddToCart event) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoggedOut(LogOutEvent event) {
//        logout();

        Menu menu = nav_view.getMenu();
        MenuItem target = menu.findItem(R.id.nav_logout);

        if (nav_view != null) {
            target.setVisible(false);
        }
    }

    private void checkSelectedBottomMenuSelected(){
//        navigationView.getMenu().findItem(navigationView.getSelectedItemId()).setCheckable(true);
    }

    private void checkCurrentPage(){
        try {
            navigationView.getMenu().findItem(R.id.navigation_home).setCheckable(true);
            navigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);
        }catch (Exception e){
            Sentry.captureException(e);
        }
    }
//
    private void unCheckSideMenuItems(){
        for (int i =0; i < nav_view.getMenu().size(); i++){
            nav_view.getMenu().getItem(i).setCheckable(true);
            nav_view.getMenu().getItem(i).setChecked(false);
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        getCarts();

        new Handler().postDelayed(() -> {
            checkCurrentPage();
        },1000);

        Log.i(TAG, "onResumeFragments: ");
        basketCountViewModel.setBasketListObj();

    }

    private void logout() {
        View parent_view = findViewById(android.R.id.content);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(" Logout?");
        builder.setPositiveButton("Yes", (dialogInterface, i) -> appExecutors.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                mimiDb.userDao().deleteUserLogin();

                pref.edit().putString(Constant.FACEBOOK_ID, "").apply();
                pref.edit().putString(Constant.GOOGLE_ID, "").apply();
                pref.edit().putString(Constant.USER_PHONE, "").apply();
                pref.edit().putString(Constant.USER_ID, "").apply();
                pref.edit().putString(Constant.USER_NAME, "").apply();
                pref.edit().putString(Constant.FULL_NAME, "").apply();
                pref.edit().putString(Constant.USER_EMAIL, "").apply();
                pref.edit().putString(Constant.ADDRESS, "").apply();
                pref.edit().putString(Constant.CITY, "").apply();
                pref.edit().putString(Constant.USER_PASSWORD, "").apply();

                startActivity(new Intent(getApplicationContext(), UserLoginActivity.class));
//                  .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                EventBus.getDefault().post(new LogOutEvent());
            }
        }));
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();

        logoutButtonVisibility();
    }

    private void logoutButtonVisibility() {
        Menu menu = nav_view.getMenu();
        MenuItem target = menu.findItem(R.id.nav_logout);

        if (nav_view != null) {

            if (loginUserId.isEmpty()) {
                target.setVisible(false);
            } else {
                target.setVisible(true);
            }
        }
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Log.i(TAG, "onActivityResult:  " + data + "resultt code " + REQ_CODE_SPEECH_INPUT);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (result != null) {
                        Intent intent = new Intent(getApplicationContext(), MainSearchActivity.class);
                        intent.putExtra(Constants.SEARCH_TERM, result.get(0) );
                        startActivity(intent);
                        new Handler().postDelayed(() -> EventBus.getDefault().post(new SearchEvent((result.get(0)))),2000);
                    }
                }
                break;
            }
        }
    }

    private void getShopsFromViewModel(){
        shopViewModel.getShops().observe(this, shopList -> {
//            Log.i(TAG, "onChanged:  size: " + shopList.size());
        });
    }

    private void getBasketCountsViewModel(){
        basketCountViewModel.getBasketCounts().observe(this, list -> {
//            Log.i(TAG, "onChanged:  size: " + list.size());

            if (list.size() > 0 ) {
                int cartItemSize = list.get(0).getCount();
                Utils.showBadge(getApplicationContext(), navigationView, R.id.navigation_cart, String.valueOf(cartItemSize));
            }else {
                Utils.showBadge(getApplicationContext(), navigationView, R.id.navigation_cart, String.valueOf(0));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

            if(fragment != null)
            {
                if(fragment instanceof MainHomeFragment)
                {
                    String message = getBaseContext().getString(R.string.message__want_to_quit);
                    String okStr =getBaseContext().getString(R.string.message__ok_close);
                    String cancelStr = getBaseContext().getString(R.string.message__cancel_close);

                    psDialogMsg.showConfirmDialog(message, okStr,cancelStr );

                    psDialogMsg.show();

                    psDialogMsg.okButton.setOnClickListener(view -> {
                        psDialogMsg.cancel();
                        MainHomeActivity.this.finish();
                        System.exit(0);
                    });
                    psDialogMsg.cancelButton.setOnClickListener(view -> psDialogMsg.cancel());
                }else {
                    finish();
                }

            }
        }
        return  true;
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
        Log.i(TAG, "onAttachFragment:  " );
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddToCart(AddToCart addToCart){
            basketCountViewModel = null;
            basketCountViewModel = ViewModelProviders.of(MainHomeActivity.this).get(BasketCountViewModel.class);
            getBasketCountsViewModel();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void getCarts() {
        mAPIService.getCarts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CartResponse>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull CartResponse cartResponse) {

                        try {
                            int quantity = 0;

                            for (int i = 0; i < cartResponse.getCarts().size(); i++) {

                                if (cartResponse.getCarts().get(i) != null) {

                                    if (cartResponse.getCarts().get(i).getItems().get(0).getPivot() != null) {

                                        for (int x = 0; x < cartResponse.getCarts().get(i).getItems().size(); x++) {
                                            int qty = (cartResponse.getCarts().get(i).getItems().get(x).getPivot().getQuantity());
                                            quantity = quantity + qty;
                                        }
                                    }
                                }
                            }


                            int finalQuantity = quantity;
                            appExecutors.getDiskIO().execute(() -> {
                                basketCountViewModel.setSaveToBasketListObj(finalQuantity);
                                mimiDb.basketCountDao().updateBasketCount(1, finalQuantity);
                            });

                            Log.i(TAG, "onNext:  Cart qty " + quantity);

                            basketCountViewModel.setBasketListObj();
                        }catch (Exception ex) {
                            Sentry.captureException(ex);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {}

                });
    }

    private void shareContent(){
        Uri imageUri = Uri.parse("android.resource://" + getPackageName()
                + "/drawable/" + "ic_launcher");

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.share_message) + ":https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }

}
