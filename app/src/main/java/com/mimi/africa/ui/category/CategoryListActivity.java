package com.mimi.africa.ui.category;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mimi.africa.Adapter.AdapterListShopCategory;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.R;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.utils.Utils;
import com.pchmn.materialchips.ChipView;


import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class CategoryListActivity extends BaseActivity {

    private static final String TAG = CategoryListActivity.class.getSimpleName();
    private View parent_view;

    private RecyclerView recyclerView;
    private RecyclerView subGroupsRecyclerview;
    private AdapterListShopCategory mAdapter;

    private TextView noCategoriesTextview;
    private ChipView allCategoriesTextview;

    private Handler mHandler;

    @Inject
    APIService mAPIService;
    private ProgressBar progressBar;

    private Context mContext;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_category_list);
        parent_view = findViewById(R.id.parent_view);
        progressBar = findViewById(R.id.progress_bar);
        noCategoriesTextview = findViewById(R.id.no_categories_textview);
        allCategoriesTextview = findViewById(R.id.all_categories_textview);
        mContext = this;
        mActivity = this;
        mHandler = new Handler();
        initComponent();
    }

    private void initToolbar(String name) {
        Toolbar toolbar = findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    private void initComponent() {

        View view = new View(this);

        subGroupsRecyclerview = view.findViewById(R.id.sub_groups_recyclerView);
        subGroupsRecyclerview = findViewById(R.id.sub_groups_recyclerView);
        subGroupsRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
        subGroupsRecyclerview.setHasFixedSize(false);
        subGroupsRecyclerview.setNestedScrollingEnabled(false);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
//        allCategoriesTextview.setOnChipClicked(v -> getCategories());

//        getSubGroups();
//        getCategories();

          if (getIntent() != null ){
                int id = getIntent().getIntExtra(Constants.SUB_GROUP_ID, 0);
                String name = getIntent().getStringExtra(Constants.SUB_GROUP_NAME);

                initToolbar(name);

                mHandler.postDelayed(new Runnable() {
                      @Override
                      public void run() {
                            getSubGroupCategories(String.valueOf(id));
                      }
                },1000);
          }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(DemoEvent event) {
    }

    private void getSubGroupCategories(String sub_group_id){

          final boolean[] empty = {true};

        mAPIService.getSubGroupCategories(sub_group_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {

                      Disposable disposable ;

                    @Override
                    public void onSubscribe(Disposable d) {
                          disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {

                          if (categoryResponse.getCategories().size() >0 )
                                empty[0] = false;
                        mAdapter = new AdapterListShopCategory(getApplicationContext(), categoryResponse.getCategories());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();


                        mAdapter.setOnItemClickListener((view, obj, position) -> {
                            Intent intent = new Intent(getApplicationContext(), SingleCategoryActivity.class);
                            intent.putExtra("category_slug", obj.getSlug());
                            intent.putExtra("category_id", obj.getId());
                            intent.putExtra("category_name", obj.getName());
                            startActivity(intent); });
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: get Categories  "  + e.getMessage() );
                        Sentry.captureException(e);
                    }

                    @Override
                    public void onComplete() {

                          if (empty[0]){
                                Utils.RemoveView(progressBar);
                                Utils.RemoveView(recyclerView);
                                Utils.ShowView(noCategoriesTextview);
                                noCategoriesTextview.setText("No categories for this subgroup!");
                          }else {
                                Utils.RemoveView(progressBar);
                                Utils.ShowView(recyclerView);
                                Utils.RemoveView(noCategoriesTextview);
                          }
                    }
                });
    }

      private void getCategories(){
            final boolean[] empty = {true};

            mAPIService.getCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CategoryResponse>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

                                try {
                                      Utils.ShowView(progressBar);
                                } catch (Exception e) {
                                      Sentry.captureException(e);
                                      Log.e(TAG, "onSubscribe: " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onNext(@NonNull CategoryResponse categoryResponse) {

                                if (categoryResponse.getCategories().size() >0 )
                                      empty[0] = false;
                                mAdapter = new AdapterListShopCategory(getApplicationContext(), categoryResponse.getCategories());
                                recyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();

                                mAdapter.setOnItemClickListener((view, obj, position) -> {
                                      Intent intent = new Intent(getApplicationContext(), SingleCategoryActivity.class);
                                      intent.putExtra("category_slug", obj.getSlug());
                                      intent.putExtra("category_id", obj.getId());
                                      startActivity(intent); });
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.e(TAG, "onError: get Categories  "  + e.getMessage() );
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                if (empty[0]){
                                      Utils.RemoveView(progressBar);
                                      Utils.RemoveView(recyclerView);
                                      Utils.ShowView(noCategoriesTextview);
                                      noCategoriesTextview.setText("No categories for this subgroup!");
                                }else {
                                      Utils.RemoveView(progressBar);
                                      Utils.ShowView(recyclerView);
                                      Utils.RemoveView(noCategoriesTextview);
                                }
                          }
                    });
      }

//      private void getSubGroups(){
//        mAPIService.getSubGroups()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<SubGroupResponse>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {}
//
//                    @Override
//                    public void onNext(@NonNull SubGroupResponse subGroupResponse) {
//
//                       GroupListAdapter subGroupListAdapter = new GroupListAdapter(mContext, subGroupResponse.getSubGroups());
//                        subGroupsRecyclerview.setAdapter(subGroupListAdapter);
//
//                          Toast.makeText(mContext, " " + subGroupResponse.getSubGroups().size(), Toast.LENGTH_SHORT).show();
//
//                        subGroupListAdapter.setOnItemClickListener((view, obj, position) -> {
//                              getSubGroupCategories(String.valueOf(obj.getId()));
//                        });
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e(TAG, "onError: get Categories  "  + e.getMessage() );
//                        Sentry.captureException(e);
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Utils.RemoveView(progressBar);
//                        Utils.ShowView(recyclerView);
//                    }
//                });
//    }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }


}
