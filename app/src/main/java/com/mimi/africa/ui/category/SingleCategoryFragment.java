package com.mimi.africa.ui.category;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.mimi.africa.Adapter.ShopCardAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryListResponse;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentSingleCategoryBinding;
import com.mimi.africa.event.LocationSearchEvent;
import com.mimi.africa.event.ProductCategoryViewedEvent;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.product.adapter.AllProductVerticalListAdapter;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.LocationPreferences;
import com.mimi.africa.utils.LocationUtilService;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShopViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public class SingleCategoryFragment extends BaseFragment {

      private static final String TAG = SingleCategoryFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentSingleCategoryBinding> binding;
      private List<Inventory> mCategoryLists;

      @Inject
      APIService mAPIService;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;

      private ShopViewModel shopsViewModel;
      private TextView locationTextView;
      private String mCategorySlug;
      private String mCategoryId;
      private Activity mActivity;
      private Context mContext;
      private Handler mHandler;

      public SingleCategoryFragment() {}

      public static SingleCategoryFragment newInstance(String slug, String id){
            SingleCategoryFragment singleCategoryFragment = new SingleCategoryFragment();
            Bundle bundle = new Bundle();
            bundle.putString("category_slug", slug);
            bundle.putString("category_id", id);
            singleCategoryFragment.setArguments(bundle);

            return singleCategoryFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            mHandler = new Handler();

            if (getArguments() != null){
                  mCategorySlug = getArguments().getString("category_slug");
                  mCategoryId = getArguments().getString("category_id");
//                  Utils.ShowSnackBar(getActivity(), "Category id " + mCategoryId, Snackbar.LENGTH_LONG);

                  ProductCategory productCategory = new ProductCategory();
                  productCategory.setName(mCategorySlug);
                  EventBus.getDefault().post(new ProductCategoryViewedEvent(productCategory));
            }
      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentSingleCategoryBinding fragmentSingleCategoryBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_single_category, container, false, dataBindingComponent);

            binding = new AutoClearedValue<>(this, fragmentSingleCategoryBinding);
            shopsViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShopViewModel.class);

            mActivity = getActivity();
            mContext = getContext();

            return fragmentSingleCategoryBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            mCategoryLists = new ArrayList<>();
            if (getActivity() != null) {
                  binding.get().storesAvailableRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                  binding.get().storesNearRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                  binding.get().previouslyVisitedRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                  binding.get().suggestedProductsRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                  binding.get().productsAvailableRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
            }
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {
            getCategoryListings(mCategoryId);
            getShopsWithCategory();
      }

      private void getShopsByAddress(String address) {

            mAPIService.getShopsByAddress(address)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {
                                ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());

                                try {
                                      binding.get(). storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }

                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                      navigateToStore(obj);
                                });
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG, e, "onError:  " + e.getMessage());

//                                reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
//                                disposeAPIService(disposable);

                                try {

//                                      Utils.RemoveView(binding.get().shopsProgressBar);
//                                      Utils.ShowView(binding.get().mallsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      private void getAShopsNearYou() {

            final boolean[] empty = {false};
                  mAPIService.getShops()
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<ShopsResponse>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) { disposable = d; }

                                @Override
                                public void onNext(@NonNull ShopsResponse shopsResponse) {

                                      List<Shop> closestShops = new ArrayList<>();

                                      appExecutors.getDiskIO().execute(() -> {
                                            try {
                                                  for (Shop shop : shopsResponse.getShop()) {

                                                        if (shop.getLat() != null || shop.getLng() != null) {
                                                              String lat = shop.getLat();
                                                              String lng = shop.getLng();
                                                              if (LocationPreferences.isLocationLatLonAvailable(mContext)) {

                                                                    android.location.Location endLocation = new android.location.Location("New End Location");
                                                                    endLocation.setLatitude( Double.parseDouble(lat));
                                                                    endLocation.setLongitude(Double.parseDouble(lng));

                                                                    double shopLocationDistanceToYou = LocationUtilService.getDistanceInKm(mContext, endLocation);

                                                                    if (shopLocationDistanceToYou <= Constants.DEFAULT_RADIUS_DISTANCE) {
                                                                          closestShops.add(shop);
                                                                    }
                                                              }
                                                        }
                                                  }
                                            } catch (Exception e) {
                                                  Sentry.captureException(e);
                                            }
                                      });

                                      try {

                                            mHandler.postDelayed(() -> {
                                                  try {
                                                        ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), closestShops);
                                                        if (closestShops.size() > 0){
                                                              binding.get().storesNearRecyclerView.setAdapter(shopCardAdapter);

                                                              shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                                    navigateToStore(obj);
                                                              });

                                                        }else {
                                                              empty[0] = true;
                                                        }
                                                  } catch (Exception e) {
                                                        e.printStackTrace();
                                                        Sentry.captureException(e);
                                                  }
                                            },3000);

                                      } catch (Exception e) {
                                            logError(TAG,e, "onNext:  ");
                                      }
                                }

                                @Override
                                public void onError(Throwable e) {
                                      logError(TAG, e, "onError:  " + e.getMessage());
                                }

                                @Override
                                public void onComplete() {
                                      try {

                                            if (empty[0]) {
                                                  Utils.ShowView(binding.get().noStoresInYourArea);
                                                  Utils.RemoveView(binding.get().storesNearRecyclerView);
                                            } else {
                                                  Utils.RemoveView(binding.get().noStoresInYourArea);
                                                  Utils.ShowView(binding.get().storesNearRecyclerView);
                                            }

                                            ((SingleCategoryActivity) getActivity()).progressDialog.hide();

                                      }catch (Exception e){
                                            logError(TAG, e, "onComplete");
                                      }
                                }
                          });

            getPreviouslyVisitedShops();
      }

      private void getPreviouslyVisitedShops() {

                  if (getActivity() != null)
                        shopsViewModel.getShops().observe(getActivity(), shopList -> {
                              if (shopList != null) {
                                    if (shopList.size() > 0) {

                                          try {

                                                if (binding.get().noHistory != null)
                                                      binding.get().noHistory.setVisibility(View.GONE);

                                                ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopList);

                                                if (binding.get().previouslyVisitedRecyclerView != null)

                                                      binding.get().previouslyVisitedRecyclerView.setAdapter(shopCardAdapter);

                                                shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                      navigateToStore(obj);
                                                });
                                          }catch (Exception e){
                                                logError(TAG, e,"getPreviouslyVisitedShops");
                                    }
                                    } else {
                                          if (binding.get().noHistory != null)
                                                binding.get().noHistory.setVisibility(View.VISIBLE);
                                    }
                              }
                        });


      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onSearchQuery(LocationSearchEvent event){
            getShopsByAddress(event.getTerm());
            Toast.makeText(getContext(), event.getTerm(), Toast.LENGTH_SHORT).show();
      }

      private void getShopsWithCategory(){

            final boolean[] empty = {false};

            mAPIService.getShopsByCategoryList(mCategorySlug)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<Shop>>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                if (getActivity() != null) {
                                      ((SingleCategoryActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                                      ((SingleCategoryActivity) getActivity()).progressDialog.setCancelable(false);
                                      ((SingleCategoryActivity) getActivity()).progressDialog.show();
                                }
                          }
                          @Override
                          public void onNext(@NonNull List<Shop> shopsResponse) {

                                try {

                                      if (shopsResponse.size() > 0) {

                                            ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse);
                                            binding.get().storesAvailableRecyclerView.setAdapter(shopCardAdapter);

                                            shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                                  navigateToStore(obj);
                                            });
                                      }else {
                                            empty[0] = true;
                                      }

                                }catch (Exception e){
                                      logError(TAG, e, "onNext" );
                                }
                          }
                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG, e, "onError: getShop Listing  ");
                          }

                          @Override
                          public void onComplete() {
                                try {

                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noStoresAvailable);
                                            Utils.RemoveView(binding.get().storesAvailableRecyclerView);
                                      }else {
                                            Utils.RemoveView(binding.get().noStoresAvailable);
                                            Utils.ShowView(binding.get().storesAvailableRecyclerView);
                                      }

                                }catch (Exception e){
                                      logError(TAG, e, "onComplete:  ");
                                }
                                getAShopsNearYou();
                          }
                    });
      }

      private void getCategoryListings(String category_id) {

            final boolean[] empty = {true};

            mAPIService.getProductsByShopCategory(category_id, "")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                                try {
//                                      Utils.ShowView(binding.get().productsProgressBar);
                                } catch (Exception e) {
                                      logError(TAG, e, "onSubscribe:  ");
                                }
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {

                                      if (getActivity() != null ) {
                                            if (listingResponse.getInventories().size() > 0) {
                                                  empty[0] = false;
                                                  mCategoryLists = listingResponse.getInventories();
                                            }
                                            ShopProductGridCardAdapter mProductsAdapter = new ShopProductGridCardAdapter(getContext(), listingResponse.getInventories());
                                            binding.get().productsAvailableRecyclerView.setAdapter(mProductsAdapter);

                                            mProductsAdapter.setOnItemClickListener((view, obj, position) -> {

                                                  if (view == view.findViewById(R.id.nameTextView)) {
                                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
                                                        intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
                                                        startActivity(intent);

                                                  } else if (view == view.findViewById(R.id.addressTextView)) {
                                                        startActivity(new Intent(getContext(), LocationActivity.class));
                                                  } else if (view == view.findViewById(R.id.cardView12)) {
                                                        navigateToItemDetailActivity(SingleCategoryFragment.this.getActivity(), listingResponse.getInventories().get(position));
                                                  } else
                                                        addToWishList(mAPIService, listingResponse.getInventories().get(position));
                                            });
                                      }

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext: ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG, e, "onError: getListings  ");
                          }

                          @Override
                          public void onComplete() {
                                if (getActivity() != null){
                                      if (empty[0]) {
                                            Utils.ShowView(binding.get().noProductsTv);
                                      }
                                      getRelatedProducts();
                                }
                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private Observable<ListingResponse> getListingResponseObservable(String category_id, String shop_id) {
            return mAPIService.getProductsByShopCategory(category_id, shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }


      private void getRelatedProducts() {

            if (mCategoryLists != null && mCategoryLists.size() > 0) {
                  int randomNum = ThreadLocalRandom.current().nextInt(0, mCategoryLists.size());

                  mAPIService.getRelatedProducts(String.valueOf(mCategoryLists.get(randomNum).getId()))
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<List<Inventory>>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {
                                      disposable = d;
                                }

                                @Override
                                public void onNext(@NonNull List<Inventory> inventoryList) {

                                      try {

                                            if (getActivity() != null) {
                                                  if (inventoryList.size() < 1) {
                                                        Utils.RemoveView(binding.get().alsoBuyTextView);
                                                        Utils.RemoveView(binding.get().suggestedProductsRecyclerView);
                                                  } else {
                                                        Utils.ShowView(binding.get().alsoBuyTextView);
                                                        Utils.ShowView(binding.get().suggestedProductsRecyclerView);
                                                  }

                                                  ShopProductGridCardAdapter mProductsAdapter = new ShopProductGridCardAdapter(getContext(), inventoryList);
                                                  binding.get().suggestedProductsRecyclerView.setAdapter(mProductsAdapter);
                                                  mProductsAdapter.setOnItemClickListener((view, obj, position) -> {

                                                        if (view == view.findViewById(R.id.nameTextView)) {

                                                              Intent intent = new Intent(getContext(), StoreHomeActivity.class);
                                                              intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
                                                              startActivity(intent);

                                                        } else if (view == view.findViewById(R.id.addressTextView)) {
                                                              startActivity(new Intent(getContext(), LocationActivity.class));
                                                        } else if (view == view.findViewById(R.id.cardView12)) {
                                                              navigateToItemDetailActivity(getActivity(), inventoryList.get(position));
                                                        }
                                                  });

                                                  mProductsAdapter.setOnMoreButtonClickListener((view, obj, item) -> {
                                                  });
                                            }

                                      } catch (Exception e) {
                                            logError(TAG, e, "onNext: ");
                                      }
                                }

                                @Override
                                public void onError(@NonNull Throwable e) {
                                      logError(TAG, e, "onError: getListings  ");
                                }

                                @Override
                                public void onComplete() {
                                      if (!disposable.isDisposed()) {
                                            disposable.dispose();

                                      }
                                }
                          });
            }
      }
}
