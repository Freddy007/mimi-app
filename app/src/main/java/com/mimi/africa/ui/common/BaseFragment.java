package com.mimi.africa.ui.common;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.di.Injectable;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.event.Reload;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Product;
import com.mimi.africa.model.Service;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.checkout.CheckoutActivity;
import com.mimi.africa.ui.health.HealthCategoryActivity;
import com.mimi.africa.ui.category.CategoryListActivity;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.product.ProductListActivity;
import com.mimi.africa.ui.service.ServiceDetailsActivity;
import com.mimi.africa.ui.service.ServiceListActivity;
import com.mimi.africa.ui.services.ServicesActivity;
import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
import com.mimi.africa.utils.AppExecutors;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;

public abstract class BaseFragment  extends Fragment implements Injectable {

      private boolean isFadeIn = false;

//      @Inject
//      protected ViewModelProvider.Factory viewModelFactory;

      @Inject
      protected AppExecutors appExecutors;

      @Inject
      protected Connectivity connectivity;

      @Inject
      protected SharedPreferences pref;

      protected String loginUserId;

      protected String shippingId;

      protected String shippingTax;

      protected String overAllTax;

      protected String shippingTaxLabel;

      protected String overAllTaxLabel;

      protected String versionNo;

      protected Boolean force_update = false;

      protected String force_update_msg;

      protected String force_update_title;

      protected String cod, paypal, stripe, messenger, whatsappNo;

      protected String consent_status;

      protected String userName,fullName, phoneNumber,email, address,city;

      protected String userEmailToVerify, userPasswordToVerify , userNameToVerify, userIdToVerify;

      protected String shopId, shopPhoneNumber, shopStandardShippingEnable, shopNoShippingEnable, shopZoneShippingEnable;

      @Override
      public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            loadLoginUserId();
            initUIAndActions();
            initViewModels();
            initAdapters();
            initData();
      }

      protected abstract void initUIAndActions();

      protected abstract void initViewModels();

      protected abstract void initAdapters();

      protected abstract void initData();

      protected void fadeIn(@NonNull View view) {

            if (!isFadeIn) {
                  view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
                  isFadeIn = true; // Fade in will do only one time.
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(DemoEvent event) {
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      protected void navigateToItemDetailActivity(@NonNull FragmentActivity fragmentActivity, Inventory inventory) {

            Intent intent = new Intent(fragmentActivity, ProductDetailsActivity.class);

            intent.putExtra(Constants.INVENTORY_OBJECT, inventory );
            fragmentActivity.startActivity(intent);
      }

      protected void navigateToItemDetailActivity(@NonNull FragmentActivity fragmentActivity, Inventory inventory, Product product) {

            Intent intent = new Intent(fragmentActivity, ProductDetailsActivity.class);

            intent.putExtra(Constants.INVENTORY_OBJECT, inventory );
            intent.putExtra("product_id", product.getId() );
            fragmentActivity.startActivity(intent);
      }

      protected void navigateToStore(@NonNull Inventory obj){
            Intent intent = new Intent(getContext(), StoreHomeActivity.class);
            intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
            startActivity(intent);
      }

      protected void navigateToStore(Shop obj){
//            Intent intent = new Intent(getContext(), StoreActivity.class);
            Intent intent = new Intent(getContext(), StoreHomeActivity.class);
            intent.putExtra(Constants.SHOP_OBECT, obj);

            if (obj.getFeedbacks() != null) {
//                  Log.i("BaseFragment", "navigateToStore:  " + obj.getFeedbacks().get(0).getComment() );
            }
            startActivity(intent);
      }


      protected void navigateToServiceDetailActivity(@NonNull FragmentActivity fragmentActivity, Service service) {

            Intent intent = new Intent(fragmentActivity, ServiceDetailsActivity.class);
            intent.putExtra(Constants.SERVICE_OBECT, service);
            intent.putExtra(Constant.HISTORY_FLAG, Constant.ONE);
            fragmentActivity.startActivity(intent);
      }

      protected void navigateToLocation(@NonNull FragmentActivity fragmentActivity) {
            Intent intent = new Intent(fragmentActivity, LocationActivity.class);
//            intent.putExtra(Constant.HISTORY_FLAG, Constant.ONE);
            fragmentActivity.startActivity(intent);
      }

      public Observable<ServiceResponse> getServiceResponseObservable(@NonNull APIService mAPIService, String limit) {
            return mAPIService.getServiceListings(limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      public Observable<ServiceResponse> getServiceOffersResponseObservable(@NonNull APIService mAPIService, String limit) {
            return mAPIService.getServiceOffers(limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      public Observable<ServiceResponse> getShopServicesResponseObservable(@NonNull APIService mAPIService, String limit, String shop_id) {
            return mAPIService.getShopServices(limit, shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      public Observable<ServiceResponse> getSearchServiceResponseObservable(@NonNull APIService mAPIService, String term) {
            return mAPIService.searchServices(term)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      public void addServiceToWishList(APIService mAPIService, Service product){
            String customer_id = loginUserId;
            String product_id = String.valueOf(product != null ? product.getId() : "");
            mAPIService.addToWishList(customer_id, "", product_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                if (getActivity() != null)
                                      Utils.ShowSnackBar(getActivity(), "Successfully added to wishlist!", Snackbar.LENGTH_LONG);
                                EventBus.getDefault().post(new Reload());
                          }
                    });
      }

      public void addToWishList(APIService mAPIService, Inventory inventory){
            String customer_id = loginUserId;
            String product_id = String.valueOf(inventory.getProduct() != null ? inventory.getProduct().getId() : null);
            mAPIService.addToWishList(customer_id, String.valueOf(inventory.getId()), product_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                if (getActivity() != null)
                                      Utils.ShowSnackBar(getActivity(), "Successfully added to wishlist!", Snackbar.LENGTH_LONG);
                                EventBus.getDefault().post(new Reload());
                          }
                    });
      }

      protected void navigateToProductListActivity(){
            startActivity(new Intent(getContext(), ProductListActivity.class));
      }

      protected void navigateToServiceListActivity(){
            startActivity(new Intent(getContext(), ServiceListActivity.class));
      }

      protected void navigateToCategoryListActivity(){
            startActivity(new Intent(getContext(), CategoryListActivity.class));
      }

      protected void navigateToHealthCategory (){
            startActivity(new Intent(getContext(), HealthCategoryActivity.class));
      }

      protected void navigateToServicesCategory (){
            startActivity(new Intent(getContext(), ServicesActivity.class));
      }

      protected void navigateToCheckoutActivity (){
            startActivity(new Intent(getContext(), CheckoutActivity.class));
      }

      protected void navigateToHome (){
            startActivity(new Intent(getContext(), MainHomeActivity.class));
      }

      public void logError(String TAG, @NonNull Throwable e, String s) {
            Log.e(TAG, s + e.getMessage());
            Sentry.captureException(e);
      }

      protected void loadLoginUserId() {
            try {

                  if (getActivity() != null && getActivity().getBaseContext() != null) {
                        loginUserId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
                        userName = pref.getString(Constant.USER_NAME, Constant.EMPTY_STRING);
                        email = pref.getString(Constant.USER_EMAIL, Constant.EMPTY_STRING);
                        fullName = pref.getString(Constant.FULL_NAME, Constant.EMPTY_STRING);
                        phoneNumber = pref.getString(Constant.USER_PHONE, Constant.EMPTY_STRING);
                        address = pref.getString(Constant.ADDRESS, Constant.EMPTY_STRING);
                        city = pref.getString(Constant.CITY, Constant.EMPTY_STRING);

                        shippingId = pref.getString(Constant.SHIPPING_ID, "");
                        shippingTax = pref.getString(Constant.PAYMENT_SHIPPING_TAX, "");
                        versionNo = pref.getString(Constant.APPINFO_PREF_VERSION_NO, "");
                        force_update = pref.getBoolean(Constant.APPINFO_PREF_FORCE_UPDATE, false);
                        force_update_msg = pref.getString(Constant.APPINFO_FORCE_UPDATE_MSG, "");
                        force_update_title = pref.getString(Constant.APPINFO_FORCE_UPDATE_TITLE, "");
//                        overAllTax = pref.getString(Constant.PAYMENT_OVER_ALL_TAX, Constant.EMPTY_STRING);
                        overAllTax = "0";
                        overAllTaxLabel = pref.getString(Constant.PAYMENT_OVER_ALL_TAX_LABEL, Constant.EMPTY_STRING);
                        shippingTaxLabel = pref.getString(Constant.PAYMENT_SHIPPING_TAX_LABEL, Constant.EMPTY_STRING);
//                        cod = pref.getString(Constants.PAYMENT_CASH_ON_DELIVERY, Constant.ZERO);
                        cod = "1";
                        paypal = pref.getString(Constants.PAYMENT_PAYPAL, Constant.ZERO);
//                        stripe = pref.getString(Constants.PAYMENT_STRIPE, Constant.ZERO);
                        stripe = "1";
                        messenger = pref.getString(Constant.MESSENGER, Constant.ZERO);
                        whatsappNo = pref.getString(Constant.WHATSAPP, Constant.ZERO);
                        consent_status = pref.getString(Config.CONSENTSTATUS_CURRENT_STATUS, Config.CONSENTSTATUS_CURRENT_STATUS);
                        userEmailToVerify = pref.getString(Constant.USER_EMAIL_TO_VERIFY, Constant.EMPTY_STRING);
                        userPasswordToVerify = pref.getString(Constant.USER_PASSWORD_TO_VERIFY, Constant.EMPTY_STRING);
                        userNameToVerify = pref.getString(Constant.USER_NAME_TO_VERIFY, Constant.EMPTY_STRING);
                        userIdToVerify = pref.getString(Constant.USER_ID_TO_VERIFY, Constant.EMPTY_STRING);
                        shopId = pref.getString(Constants.SHOP_ID, Constant.EMPTY_STRING);
                        shopPhoneNumber = pref.getString(Constant.SHOP_PHONE_NUMBER, Constant.EMPTY_STRING);
                        shopNoShippingEnable = pref.getString(Constant.SHOP_NO_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                        shopZoneShippingEnable = pref.getString(Constant.SHOP_ZONE_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                        shopStandardShippingEnable = pref.getString(Constant.SHOP_STANDARD_SHIPPING_ENABLE, Constant.EMPTY_STRING);
                  }

            } catch (NullPointerException ne) {
                  Utils.psErrorLog("Null Pointer Exception.", ne);
                  Sentry.captureException(ne);
            } catch (Exception e) {
                  Utils.psErrorLog("Error in getting notification flag data.", e);
                  Sentry.captureException(e);
            }
      }

}
