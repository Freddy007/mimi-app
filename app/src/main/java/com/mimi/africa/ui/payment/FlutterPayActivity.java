package com.mimi.africa.ui.payment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityFlutterPayBinding;
import com.mimi.africa.event.CheckOutReady;
import com.mimi.africa.event.PaymentSuccess;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.checkout.activity.MainCheckoutActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Tools;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FlutterPayActivity extends BaseActivity {

    private static final String TAG = FlutterPayActivity.class.getSimpleName();
    ActivityFlutterPayBinding binding;
    private String mUrl;
    public ProgressDialog progressDialog;
    private PSDialogMsg psDialogMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_flutter_pay);

        setContentView(R.layout.activity_flutter_pay);

        psDialogMsg = new PSDialogMsg(this, false);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Payments page");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Tools.setSystemBarColor(this);

        mUrl = getIntent().getStringExtra("payment_url");

        WebView myWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        myWebView.setWebViewClient(
                new SSLTolerantWebViewClient()
        );

        myWebView.loadUrl(mUrl);

    }

    class SSLTolerantWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

            handler.proceed(); // Ignore SSL certificate errors
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressDialog = new ProgressDialog(FlutterPayActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading payment page..");
            Utils.showProgressDialog(FlutterPayActivity.this, progressDialog, R.layout.dialog_progress);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressDialog.hide();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.loadUrl("javascript:(function(){"+
                            "l=document.getElementById('no-cookie-btn');"+
                            "e=document.createEvent('HTMLEvents');"+
                            "e.initEvent('click',true,true);"+
                            "l.dispatchEvent(e);"+
                            "})()");
//                    view.loadUrl("javascript:(function(){document.getElementById('no-cookie-btn').click();})();");
                }
            },5000);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPaymentMade(PaymentSuccess paymentSuccess){
        Log.i(TAG, "onPaymentMade:  Payment successful!");
        psDialogMsg.showSuccessDialog(getString(R.string.payment__successfully_made), getString(R.string.app__ok));
        psDialogMsg.show();
        psDialogMsg.okButton.setOnClickListener(v -> startActivity(new Intent(FlutterPayActivity.this, MainHomeActivity.class)));
        }

}
