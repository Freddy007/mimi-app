package com.mimi.africa.ui.mainhome.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.mimi.africa.R;
import com.mimi.africa.model.Slider;
import com.mimi.africa.ui.product.ProductListActivity;
import com.mimi.africa.utils.ProportionalImageView;

import java.util.List;


public class BannerAdapter extends PagerAdapter {

    private Context context;
    private List<Slider> bannerList;

    public BannerAdapter(Context activity, List<Slider> imagesArray) {

        this.context = activity;
        this.bannerList = imagesArray;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View viewItem = inflater.inflate(R.layout.image_item, container, false);
        ProportionalImageView imageView = viewItem.findViewById(R.id.imageView);
        Glide.with(context).load( bannerList.get(position).getImage().getPath()).placeholder(R.drawable.slider).into(imageView);
        container.addView(viewItem);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                context.startActivity(new Intent(context, ProductListActivity.class));
            }
        });

        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}