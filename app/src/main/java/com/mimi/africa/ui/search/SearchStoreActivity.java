package com.mimi.africa.ui.search;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.ViewAnimation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class SearchStoreActivity extends BaseActivity {

    private static final String TAG = SearchStoreActivity.class.getSimpleName();
    
    private ViewPager view_pager;
    private TabLayout tab_layout;
    private EditText et_search;
    private ImageButton bt_clear;
    private ImageButton back_button;
    private ProgressBar progress_bar;
    private RecyclerView recyclerSuggestion;
    public AdapterSuggestionSearch mAdapterSuggestion;
    private LinearLayout lyt_suggestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_store);

        initComponent();
//        Tools.setSystemBarColor(this, R.color.blue_grey_600);

    }

    private void initComponent() {

        CustomSharedPrefs.removeLoadPageStatus(this);
        CustomSharedPrefs.setReloadPageStatus(this,  false);

        back_button =  findViewById(R.id.back_button);
        progress_bar = findViewById(R.id.progress_bar);
        view_pager = findViewById(R.id.view_pager);
        setupViewPager(view_pager);

        tab_layout = findViewById(R.id.tab_layout);
        tab_layout.setupWithViewPager(view_pager);

        et_search = findViewById(R.id.et_search);
        bt_clear = findViewById(R.id.bt_clear);
        ImageButton search_btn = findViewById(R.id.search_button);

        search_btn.setOnClickListener(v -> searchAction());

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard();

                    searchAction();

                    return true;
                }
                return false;
            }
        });

        back_button.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), MainHomeActivity.class)));

        lyt_suggestion = findViewById(R.id.lyt_suggestion);

    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void searchAction() {
        progress_bar.setVisibility(View.VISIBLE);
        ViewAnimation.collapse(lyt_suggestion);
        view_pager.setVisibility(View.VISIBLE);

        final String query = et_search.getText().toString().trim();
        if (!query.equals("")) {
            hideKeyboard();
            EventBus.getDefault().post(new SearchEvent((query)));

        } else {
            Toast.makeText(this, "Search field can not be empty!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupViewPager(@NonNull ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

//        adapter.addFragment(AllFragmentSearchTabs.newInstance(), FragmentSearchTabs.ALL_TAB);
        adapter.addFragment(ProductsFragmentSearchTabs.newInstance(),  FragmentSearchTabs.PRODUCTS_TAB);
        adapter.addFragment(ServicesFragmentSearchTabs.newInstance(),  FragmentSearchTabs.SERVICES_TAB);
        adapter.addFragment(StoresFragmentSearchTabs.newInstance(),  FragmentSearchTabs.STORES_TAB);

        viewPager.setAdapter(adapter);
    }

    private void setupViewPagerWithTerm(@NonNull ViewPager viewPager, String term) {
            SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

//        adapter.addFragment(AllFragmentSearchTabs.newInstance(), FragmentSearchTabs.ALL_TAB);
            adapter.addFragment(ProductsFragmentSearchTabs.newInstance(term),  FragmentSearchTabs.PRODUCTS_TAB);
            adapter.addFragment(ServicesFragmentSearchTabs.newInstance(),  FragmentSearchTabs.SERVICES_TAB);
            adapter.addFragment(StoresFragmentSearchTabs.newInstance(),  FragmentSearchTabs.STORES_TAB);

            viewPager.setAdapter(adapter);
      }


      @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public androidx.fragment.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void showSuggestionSearch() {
        mAdapterSuggestion.refreshItems();
        ViewAnimation.expand(lyt_suggestion);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(AddToCart event) {
    }
}