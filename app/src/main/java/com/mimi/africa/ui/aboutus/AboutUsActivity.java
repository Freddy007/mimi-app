package com.mimi.africa.ui.aboutus;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

//import com.cscode.radytocook.R;
//import com.cscode.radytocook.utils.SessionManager;

import com.mimi.africa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

//import static com.cscode.radytocook.utils.SessionManager.ABOUT_US;


public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.txt_about)
    TextView txtAbout;
//    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abouts);
        ButterKnife.bind(this);
//        sessionManager=new SessionManager(this);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            txtAbout.setText(getString(R.string.));
            } else {
//            txtAbout.setText(Html.fromHtml(sessionManager.getStringData(ABOUT_US)));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
