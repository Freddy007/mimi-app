package com.mimi.africa.ui.category.services;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.SubGroupResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentCategoryListBinding;
import com.mimi.africa.ui.category.CategoryListActivity;
import com.mimi.africa.ui.category.adapter.SubGroupCategoryListAdapter;
import com.mimi.africa.ui.category.adapter.GroupListAdapter;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class ServiceCategoryListFragment extends BaseFragment {

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

      @Inject
      APIService mAPIService;

      @VisibleForTesting
      private AutoClearedValue<FragmentCategoryListBinding> binding;
      private AutoClearedValue<GroupListAdapter> adapter;

      public ServiceCategoryListFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentCategoryListBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_list, container, false, dataBindingComponent);

            binding = new AutoClearedValue<>(this, dataBinding);

            return binding.get().getRoot();
      }

      @Override
      protected void initUIAndActions() {
            binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.view__primary_line));
            binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.global__primary));
            binding.get().swipeRefresh.setOnRefreshListener(this::getSubGroups);
      }

      @Override
      protected void initViewModels() {
      }

      @Override
      protected void initAdapters() {
      }

      @Override
      protected void initData() {
            getSubGroups();
      }

      private void getSubGroups(){
            mAPIService.getServiceSubGroups()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SubGroupResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                              ((ServiceSubGroupListActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                              ((ServiceSubGroupListActivity) getActivity()).progressDialog.setCancelable(false);
                              ((ServiceSubGroupListActivity) getActivity()).progressDialog.show();
                          }

                          @Override
                          public void onNext(@NonNull SubGroupResponse subGroupResponse) {

                              try {

                                      SubGroupCategoryListAdapter subGroupCategoryListAdapter =
                                              new SubGroupCategoryListAdapter(getContext(), subGroupResponse.getSubGroups());
                                      binding.get().subCategoryList.setAdapter(subGroupCategoryListAdapter);

                                      subGroupCategoryListAdapter.setOnItemClickListener((view, obj, position) -> {
                                          Intent intent = new Intent(getContext(), CategoryListActivity.class);

                                          intent.putExtra(Constants.SUB_GROUP_ID, obj.getId());
                                          startActivity(intent);
                                      });

                              } catch (Exception e) {
                                  e.printStackTrace();
                                  Sentry.captureException(e);
                              }
                          }

                          @Override
                          public void onError(Throwable e) {
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                binding.get().swipeRefresh.setRefreshing(false);
                              ((ServiceSubGroupListActivity) getActivity()).progressDialog.hide();
                          }
                    });
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }
}
