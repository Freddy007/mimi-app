package com.mimi.africa.ui.favourite;


import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.mimi.africa.Adapter.ProductVerticalListAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentFavouritesBinding;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.promo.PromoFragment;
import com.mimi.africa.ui.search.FragmentSearchTabs;
import com.mimi.africa.ui.search.ProductsFragmentSearchTabs;
import com.mimi.africa.ui.search.ServicesFragmentSearchTabs;
import com.mimi.africa.ui.search.StoresFragmentSearchTabs;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.BasketCountViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class FavouritesFragment extends BaseFragment {

      private static final String TAG = FavouritesFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentFavouritesBinding> binding;
      private BasketCountViewModel basketCountViewModel;


      @Inject
      APIService mAPIService;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;

      public FavouritesFragment() {}

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentFavouritesBinding fragmentFavouritesBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_favourites, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentFavouritesBinding);

            return fragmentFavouritesBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            FragmentManager fragmentManager = getChildFragmentManager();
            setupViewPager(binding.get().viewPager, fragmentManager);
            binding.get().tabLayout.setupWithViewPager(binding.get().viewPager);

            CustomSharedPrefs.setReloadPageStatus(getContext(), true);
      }

      @Override
      protected void initViewModels() {
            basketCountViewModel = ViewModelProviders.of(this, viewModelFactory).get(BasketCountViewModel.class);

      }

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
//            getWishLists();
      }



      private void setupViewPager(@NonNull ViewPager viewPager, FragmentManager fragmentManager) {
            FavouritesFragment.SectionsPagerAdapter adapter = new FavouritesFragment.SectionsPagerAdapter(fragmentManager);

            adapter.addFragment(ProductsFragmentSearchTabs.newInstance("wishlists"),  FragmentSearchTabs.PRODUCTS_TAB);
            adapter.addFragment(ServicesFragmentSearchTabs.newInstance("wishlists"),  FragmentSearchTabs.SERVICES_TAB);

            viewPager.setAdapter(adapter);
      }

      private class SectionsPagerAdapter extends FragmentPagerAdapter {

            private final List<Fragment> mFragmentList = new ArrayList<>();
            private final List<String> mFragmentTitleList = new ArrayList<>();

            public SectionsPagerAdapter(FragmentManager manager) {
                  super(manager);
            }

            @Override
            public androidx.fragment.app.Fragment getItem(int position) {
                  return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                  return mFragmentList.size();
            }

            public void addFragment(Fragment fragment, String title) {
                  mFragmentList.add(fragment);
                  mFragmentTitleList.add(title);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                  return mFragmentTitleList.get(position);
            }
      }

}
