//package com.mimi.africa.ui.mainhome;
//
//
//import android.app.Activity;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.view.Display;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ViewTreeObserver;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.PopupMenu;
//import android.widget.ProgressBar;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.databinding.DataBindingUtil;
//import androidx.fragment.app.FragmentActivity;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.viewpager.widget.PagerAdapter;
//import androidx.viewpager.widget.ViewPager;
//
//import com.google.android.material.snackbar.Snackbar;
//import com.mimi.africa.Adapter.AdapterImageSlider;
//import com.mimi.africa.Adapter.HomeCategoryAdapter;
//import com.mimi.africa.Adapter.ServiceCardAdapter;
//import com.mimi.africa.Adapter.ShopCardAdapter;
//import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
//import com.mimi.africa.R;
//import com.mimi.africa.api.APIService;
//import com.mimi.africa.api.response.ListingResponse;
//import com.mimi.africa.api.response.ServiceResponse;
//import com.mimi.africa.api.response.ShopsListingResponse;
//import com.mimi.africa.api.response.ShopsResponse;
//import com.mimi.africa.api.response.SliderResponse;
//import com.mimi.africa.binding.FragmentDataBindingComponent;
//import com.mimi.africa.databinding.FragmentMainHomeBinding;
//import com.mimi.africa.event.Reload;
//import com.mimi.africa.model.Inventory;
//import com.mimi.africa.model.ProductCategory;
//import com.mimi.africa.model.Shop;
//import com.mimi.africa.ui.category.SubGroupListActivity;
//import com.mimi.africa.ui.category.services.ServiceSubGroupListActivity;
//import com.mimi.africa.ui.common.BaseFragment;
//import com.mimi.africa.ui.common.DataBoundListAdapter;
//import com.mimi.africa.ui.common.NavigationController;
//import com.mimi.africa.ui.home.adapter.ViewPagerAdapter;
//import com.mimi.africa.ui.location.LocationActivity;
//import com.mimi.africa.ui.mainhome.adapter.BannerAdapter;
//import com.mimi.africa.ui.shop.ShopListActivity;
//import com.mimi.africa.ui.shop.StoreActivity;
//import com.mimi.africa.ui.shop.version2.HealthShopHomeActivity;
//import com.mimi.africa.ui.shop.version2.StoreHomeActivity;
//import com.mimi.africa.utils.AutoClearedValue;
//import com.mimi.africa.utils.Constants;
//import com.mimi.africa.utils.PSDialogMsg;
//import com.mimi.africa.utils.SliderTimer;
//import com.mimi.africa.utils.SpeedSlowScroller;
//import com.mimi.africa.utils.Utils;
//
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.Timer;
//
//import javax.inject.Inject;
//
//import io.reactivex.Observer;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//import io.sentry.core.Sentry;
//
//
//public class MainHomeFragment2 extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {
//
//      private static final String TAG = MainHomeFragment2.class.getSimpleName();
//      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
//      private View parentView;
//      private AutoClearedValue<FragmentMainHomeBinding> binding;
//      private boolean layoutDone = false;
//      private int loadingCount = 0;
//
//      private PSDialogMsg psDialogMsg;
//
//
//      private FragmentActivity mActivity;
//
//      @Nullable
//      private Runnable runnable = null;
//      @NonNull
//      private Handler handler = new Handler();
//      private ShopProductGridCardAdapter mProductsAdapter;
//      @Nullable
//      private AdapterImageSlider adapterImageSlider;
//      @Nullable
//      private HomeCategoryAdapter homeCategoryAdapter;
//      private ViewPager viewPager;
//
//      private AutoClearedValue<ViewPagerAdapter> viewPagerAdapter;
//      private int dotsCount = 0;
//      private ImageView[] dots;
//
//      static int y = 0;
//      private final Handler mHandler = new Handler();
//
//      @Inject
//      APIService mAPIService;
//
//      @Inject
//      NavigationController navigationController;
//
//      public MainHomeFragment2() {}
//
//      @Override
//      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                               Bundle savedInstanceState) {
//
//            FragmentMainHomeBinding fragmentMainHomeBinding =
////                    DataBindingUtil.inflate(inflater, R.layout.fragment_main_home_2, container, false, dataBindingComponent);
//                    DataBindingUtil.inflate(inflater, R.layout.fragment_main_home, container, false, dataBindingComponent);
//            binding = new AutoClearedValue<>(this, fragmentMainHomeBinding);
//            mActivity = getActivity();
//
//            return fragmentMainHomeBinding.getRoot();
//      }
//
//      @Override
//      protected void initUIAndActions() {
//
//            psDialogMsg = new PSDialogMsg(MainHomeFragment2.this.getActivity(), false);
//
//            parentView = getActivity().findViewById(android.R.id.content);
//
//            binding.get().viewpager.startAutoScroll();
//            binding.get().viewpager.setInterval(4000);
//            binding.get().viewpager.setCycle(true);
//            binding.get().viewpager.setStopScrollWhenTouch(true);
//
//
//            HomeSlider();
//            setHeight();
//
//            adapterImageSlider = new AdapterImageSlider(getActivity(), new ArrayList<>());
//
//            binding.get().seeAllProductsButton.setOnClickListener(v -> {
//                  navigateToProductListActivity();
//            });
//
//            binding.get().seeAllServicesButton.setOnClickListener(v -> {
//                  navigateToServiceListActivity();
//            });
//
//            binding.get().seeAllShops.setOnClickListener(v -> {
//                  startActivity(new Intent(getActivity(), ShopListActivity.class));
//            });
//
//            binding.get().productsMore.setOnClickListener(v -> {
//                  PopupMenu popupMenu = new PopupMenu(getContext(), v);
//
//                  popupMenu.setOnMenuItemClickListener(item -> {
//                        switch (item.getItemId()){
//
//                              case R.id.action_arrivals:
//                                    getShopListings("new_arrivals");
//                                    break;
//
//                              case R.id.action_low_to_high:
//                                  getShopListings("low_to_high");
//                                    break;
//
//                              case R.id.action_high_to_low:
//                                    getShopListings("high_to_low");
//                                    break;
//
//                              case R.id.action_has_offers:
//                                    getShopListings("has_offers");
//                                    break;
//
//                              case R.id.action_condition_new:
//                                    getListingCondition("condition","New" );
//                                    break;
//
//                              case R.id.action_condition_old:
////                                    filterCondition(mTerm, "Used");
//                                    getListingCondition("condition","Used" );
//                                    break;
//
//                              case R.id.action_free_delivery:
//                                    getShopListings("free_shipping");
//
//                                    break;
//
//                        }
//                        return true;
//                  });
//
//                  popupMenu.inflate(R.menu.products_menu);
//                  popupMenu.show();
//            });
//
//            binding.get().shopsMore.setOnClickListener(new View.OnClickListener() {
//                  @Override
//                  public void onClick(View v) {
//                        PopupMenu popupMenu = new PopupMenu(getContext(), v);
//
//                        popupMenu.setOnMenuItemClickListener(item -> {
//                              switch (item.getItemId()){
//
//                                    case R.id.action_all:
//                                          getShops();
//                                          break;
//
//                                    case R.id.action_ashanti:
//                                          getStoresByLocation("Ashanti Region");
//                                          break;
//
//                                    case R.id.action_brong_ahafo:
//                                          getStoresByLocation("Brong Ahafo");
//                                          break;
//
//                                    case R.id.action_central:
//                                          getStoresByLocation("Central Region");
//                                          break;
//
//                                    case R.id.action_eastern:
//                                          getStoresByLocation("Eastern Region");
//                                          break;
//
//                                    case R.id.action_greater_accra:
//                                          getStoresByLocation("Accra");
//                                          break;
//
//                                    case R.id.action_northern:
//                                          getStoresByLocation("Northern Region");
//                                          break;
//
//                                    case R.id.action_upper_east:
//                                          getStoresByLocation("Upper East");
//                                          break;
//
//                                    case R.id.action_upper_west:
//                                          getStoresByLocation("Upper West");
//                                          break;
//
//                                    case R.id.action_volta:
//                                          getStoresByLocation("Volta");
//                                          break;
//
//                                    case R.id.action_western:
//                                          getStoresByLocation("Western Region");
//                                          break;
//                              }
//                              return true;
//                        });
//
//                        popupMenu.inflate(R.menu.locations);
//                        popupMenu.show();
//
//                  }
//            });
//
//            binding.get().servicesMore.setOnClickListener(new View.OnClickListener() {
//                  @Override
//                  public void onClick(View v) {
//                        PopupMenu popupMenu = new PopupMenu(getContext(), v);
//
//                        popupMenu.setOnMenuItemClickListener(item -> {
//                              switch (item.getItemId()){
//
//                                    case R.id.action_arrivals:
//                                          getShopListings("new_arrivals");
//                                          break;
//
//                              }
//                              return true;
//                        });
//
//                        popupMenu.inflate(R.menu.services_menu);
//                        popupMenu.show();
//
//                  }
//            });
//
//            ViewPagerAdapter viewPagerAdapter1 = new ViewPagerAdapter(dataBindingComponent, product -> {
//                  navigateToItemDetailActivity(mActivity, product);
//            });
//
//            this.viewPagerAdapter = new AutoClearedValue<>(this, viewPagerAdapter1);
//            binding.get().pager.setAdapter(viewPagerAdapter1);
////            binding.get().viewPagerCountDots.setVisibility(View.VISIBLE);
//
//            binding.get().pager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                  @Override
//                  public void onGlobalLayout() {
//                        if (binding.get() != null && binding.get().pager != null) {
//                              if (binding.get().pager.getChildCount() > 0) {
//                                    layoutDone = true;
//                                    loadingCount++;
//                                    hideLoading();
//                                    binding.get().pager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                              }
//                        }
//                  }
//            });
//      }
//
//      @Override
//      protected void initViewModels() {}
//
//      @Override
//      protected void initAdapters() {
//
//            binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//            binding.get().servicesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//            binding.get().mallsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//            binding.get().categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//            viewPager = binding.get().pager;
//
//            List<ProductCategory> categoryItems = new ArrayList<>();
//            categoryItems.add(new ProductCategory(1, Constants.CATEGORIES, Constants.EMPTY));
//            categoryItems.add(new ProductCategory(2, Constants.SERVICES, Constants.EMPTY));
//            categoryItems.add(new ProductCategory(3, Constants.STORES, Constants.EMPTY));
//
//            //set data and list adapter
//            homeCategoryAdapter = new HomeCategoryAdapter(getContext(), categoryItems);
//
//            binding.get().categoriesRecyclerView.setAdapter(homeCategoryAdapter);
//
//            homeCategoryAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                  if (obj.getName() != null) {
//                        switch (obj.getName()) {
//                              case "Categories":
//                                    startActivity(new Intent(getContext(), SubGroupListActivity.class));
//                                    break;
//
//                              case "Services":
//                                    startActivity(new Intent(getContext(), ServiceSubGroupListActivity.class));
//                                    break;
//
//                              case "Stores":
//                                    startActivity(new Intent(getActivity(), ShopListActivity.class));
//                                    break;
//
//                        }
//                  }
//            });
//      }
//
//      @Override
//      protected void initData() {
//            if (connectivity.isConnected()) {
//                  loadMainData();
//            } else {
//
//            }
//      }
//
//      private void loadMainData() {
//            getNewArrivals();
//            getShopListings("all");
//
//      }
//
//      private void getListings(String list) {
//
//            mAPIService.getAllListings()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ShopsListingResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().productsProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ShopsListingResponse listingResponse) {
//                                List<Inventory> inventoryList = new ArrayList<>();
//
//                                try {
//
//                                      for (Shop shop : listingResponse.getShops()){
//
//                                            if (shop.getInventories() != null) {
//                                                  inventoryList.addAll(shop.getInventories());
//                                            }
//                                      }
//
//                                      Collections.shuffle(inventoryList);
//                                      mProductsAdapter = new ShopProductGridCardAdapter(getContext(), inventoryList);
//                                      binding.get().productsRecyclerView.setAdapter(mProductsAdapter);
//                                      mProductsAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (view == view.findViewById(R.id.nameTextView)) {
//
//                                                  Intent intent = new Intent(getContext(), StoreActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
//                                                  startActivity(intent);
//
//                                            } else if (view == view.findViewById(R.id.addressTextView)) {
//
//                                                  startActivity(new Intent(getContext(), LocationActivity.class));
//
//                                            } else if (view == view.findViewById(R.id.cardView12)) {
//
//                                                  navigateToItemDetailActivity(getActivity(), inventoryList.get(position));
//
//                                            }
//                                      });
//
//
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onNext: ");
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                try {
//                                      reloadSnackBar(binding.get().productsProgressBar, "There was a problem loading products!");
//
//                                      getServiceListings();
//
//                                } catch (Exception ex) {
//                                      logError(TAG, e, "onError: ");
//                                }
//
//                                logError(TAG, e, "onError: getListings  ");
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                if (!disposable.isDisposed()) {
//                                      disposable.dispose();
//                                }
//
//                                try {
//                                      Utils.ShowView(binding.get().productsRecyclerView);
//                                      Utils.RemoveView(binding.get().productsProgressBar);
//
//                                      getServiceListings();
//
//                                } catch (Exception e) { Sentry.captureException(e); }
//
//                          }
//                    });
//      }
//
//      private void getNewArrivals() {
//
//            mAPIService.getShopsListing("new_arrivals")
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ListingResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().newArrivalsProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ListingResponse listingResponse) {
//
//                                try {
//                                      ShopProductGridCardAdapter shopProductGridCardAdapter = new ShopProductGridCardAdapter(getContext(), listingResponse.getInventories());
//                                      binding.get().newArrivalsRecyclerView.setAdapter(shopProductGridCardAdapter);
//
////                                      replaceFeaturedData(listingResponse.getInventories());
//
//                                      shopProductGridCardAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (getActivity() != null) {
//                                                  if (view == view.findViewById(R.id.nameTextView)) {
//                                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                        intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
//                                                        startActivity(intent);
//
//                                                  } else if (view == view.findViewById(R.id.addressTextView) || view == view.findViewById(R.id.imageView2)) {
//
//                                                        goToShopLocation(obj.getShop());
//
//                                                  } else if (view == view.findViewById(R.id.cardView12)) {
//                                                        navigateToItemDetailActivity(mActivity, listingResponse.getInventories().get(position));
//
//                                                  } else if (view == view.findViewById(R.id.favoriteImageView)){
//
//                                                        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg, MainHomeFragment2.this.getActivity(), navigationController, () -> {
//                                                              addToWishList(mAPIService, listingResponse.getInventories().get(position));
//                                                              shopProductGridCardAdapter.notifyDataSetChanged();
//                                                        });
//                                                  }
//                                            }
//                                      });
//
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onNext: ");
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                try {
////                                      reloadSnackBar(binding.get().productsProgressBar, "There was a problem loading products!");
//
////                                      getServiceListings();
//
//                                } catch (Exception ex) {
//                                      logError(TAG, e, "onError: ");
//                                }
//
//                                logError(TAG, e, "onError: getListings  ");
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                if (!disposable.isDisposed()) {
//                                      disposable.dispose();
//                                }
//
//                                try {
//                                      if (getActivity() != null ) {
//                                            Utils.ShowView(binding.get().newArrivalsRecyclerView);
//                                            Utils.RemoveView(binding.get().newArrivalsProgressBar);
//
////                                            startAutoSlider(viewPagerAdapter.get().getCount());
//
////                                            setUpAutoSlider(viewPager, viewPagerAdapter.get().getCount(), MainHomeFragment.this.getActivity());
//
//
////                                            getServiceListings();
//                                      }
//
//                                } catch (Exception e) { Sentry.captureException(e); }
//
//                          }
//                    });
//      }
//
//      private void getShopListings(String all) {
//
//            mAPIService.getShopsListing(all)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ListingResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().productsProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ListingResponse listingResponse) {
//
//                                try {
//                                      mProductsAdapter = new ShopProductGridCardAdapter(getContext(), listingResponse.getInventories());
//                                      binding.get().productsRecyclerView.setAdapter(mProductsAdapter);
//
//                                      replaceFeaturedData(listingResponse.getInventories());
//
//                                      mProductsAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (getActivity() != null) {
//                                                  if (view == view.findViewById(R.id.nameTextView)) {
//                                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                        intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
//                                                        startActivity(intent);
//
//                                                  } else if (view == view.findViewById(R.id.addressTextView) || view == view.findViewById(R.id.imageView2)) {
//
//                                                        goToShopLocation(obj.getShop());
//
//                                                  } else if (view == view.findViewById(R.id.cardView12)) {
//                                                        navigateToItemDetailActivity(mActivity, listingResponse.getInventories().get(position));
//
//                                                  } else if (view == view.findViewById(R.id.favoriteImageView)){
//
//                                                        Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg, MainHomeFragment2.this.getActivity(), navigationController, () -> {
//                                                              addToWishList(mAPIService, listingResponse.getInventories().get(position));
//                                                              mProductsAdapter.notifyDataSetChanged();
//                                                        });
//                                                  }
//                                            }
//                                      });
//
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onNext: ");
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                try {
//                                      reloadSnackBar(binding.get().productsProgressBar, "There was a problem loading products!");
//
//                                      getServiceListings();
//
//                                } catch (Exception ex) {
//                                      logError(TAG, e, "onError: ");
//                                }
//
//                                logError(TAG, e, "onError: getListings  ");
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                if (!disposable.isDisposed()) {
//                                      disposable.dispose();
//                                }
//
//                                try {
//                                      if (getActivity() != null ) {
//                                            Utils.ShowView(binding.get().productsRecyclerView);
//                                            Utils.RemoveView(binding.get().productsProgressBar);
//
//                                            startAutoSlider(viewPagerAdapter.get().getCount());
//
//                                            setUpAutoSlider(viewPager, viewPagerAdapter.get().getCount(), MainHomeFragment2.this.getActivity());
//
//
//                                            getServiceListings();
//                                      }
//
//                                } catch (Exception e) { Sentry.captureException(e); }
//
//                          }
//                    });
//      }
//
//      private void getListingCondition(String filter,  String condition) {
//
//            mAPIService.getListingCondition(filter, condition)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ListingResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().productsProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ListingResponse listingResponse) {
//
//                                try {
//                                      mProductsAdapter = new ShopProductGridCardAdapter(getContext(), listingResponse.getInventories());
//                                      binding.get().productsRecyclerView.setAdapter(mProductsAdapter);
//                                      mProductsAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (view == view.findViewById(R.id.nameTextView)) {
//                                                  Intent intent = new Intent(getContext(), StoreActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj.getShop());
//                                                  startActivity(intent);
//
//                                            } else if (view == view.findViewById(R.id.addressTextView)) {
//                                                  startActivity(new Intent(getContext(), LocationActivity.class));
//                                            } else if (view == view.findViewById(R.id.cardView12)) {
//                                                  navigateToItemDetailActivity(mActivity, listingResponse.getInventories().get(position));
//                                            }
//                                      });
//
//
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onNext: ");
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                try {
//
//                                      reloadSnackBar(binding.get().productsProgressBar, "There was a problem loading products!");
//                                      getServiceListings();
//
//                                } catch (Exception ex) {
//                                      logError(TAG, e, "onError: ");
//                                }
//
//                                logError(TAG, e, "onError: getListings  ");
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                if (!disposable.isDisposed()) {
//                                      disposable.dispose();
//                                }
//
//                                try {
//                                      Utils.ShowView(binding.get().productsRecyclerView);
//                                      Utils.RemoveView(binding.get().productsProgressBar);
//
//                                      getServiceListings();
//
//                                } catch (Exception e) { Sentry.captureException(e); }
//
//                          }
//                    });
//      }
//
//      private void reloadSnackBar(ProgressBar servicesProgressBar, String string) {
//            Utils.RemoveView(servicesProgressBar);
//            Snackbar snackbar = Snackbar.make(parentView, string, Snackbar.LENGTH_LONG)
//                    .setAction("RELOAD!", view -> loadMainData());
//            snackbar.show();
//      }
//
//      private void getServiceListings() {
//            getServiceResponseObservable(mAPIService, "15")
//                    .subscribe(new Observer<ServiceResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//                                try {
//                                      Utils.ShowView(binding.get().servicesProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ServiceResponse listingResponse) {
//
//                                try {
//
//                                      ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), listingResponse.getServices());
//                                      binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);
//                                      serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {
//
//                                            if (view == view.findViewById(R.id.nameTextView)) {
//                                                  if (obj.getShop().getFeedbacks() != null) {
//                                                        Log.i(TAG, "onNext: Service feedback " + obj.getShop().getFeedbacks().get(0).getComment());
//                                                  }
//                                                  navigateToStore(obj.getShop());
//
//                                            }  else if (view == view.findViewById(R.id.addressTextView) || view == view.findViewById(R.id.imageView2)) {
//
//                                                  goToShopLocation(obj.getShop());
//
//                                            } else if (view == view.findViewById(R.id.cardView12)) {
//                                                  navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
//                                            } else if (view == view.findViewById(R.id.favoriteImageView)) {
//
//                                                  Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, TAG, psDialogMsg, MainHomeFragment2.this.getActivity(), navigationController, () -> {
//                                                        addServiceToWishList(mAPIService, listingResponse.getServices().get(position));
//                                                        serviceCardAdapter.notifyDataSetChanged();
//
//                                                  });
//
//                                            }
//                                      });
//
//                                } catch (Exception e) {
//                                      logError(TAG,e, "onNext:  ");
//                                }
//                          }
//
//                          @Override
//                          public void onError(@NonNull Throwable e) {
//                                logError(TAG,e, "Service Listings onError:  ");
//
//                                try {
//                                      reloadSnackBar(binding.get().servicesProgressBar, getString(R.string.Oops));
//
//                                } catch (Exception ex) {
//                                      logError(TAG,e, "onError: ");
//                                }
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                disposeAPIService(disposable);
//
//                                try {
//                                      Utils.RemoveView(binding.get().servicesProgressBar);
//                                      Utils.ShowView(binding.get().servicesRecyclerView);
//
//                                      getShops();
//
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onComplete:  ");
//                                }
//                          }
//                    });
//      }
//
//      private void goToShopLocation(Shop shop) {
//            if (shop != null) {
//                  if (shop.getLat() != null || shop.getLng() != null) {
//
//                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + shop.getLat() + "," + shop.getLng());
//                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                        mapIntent.setPackage("com.google.android.apps.maps");
//                        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
//                              startActivity(mapIntent);
//                        }
//
//                  } else {
//                        Utils.ShowSnackBar(getActivity(), "No location information fo this shop!", Snackbar.LENGTH_SHORT);
//                  }
//            }
//      }
//
//      private void getShops() {
//
//            mAPIService.getShops()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ShopsResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//
//                                try {
//                                      Utils.ShowView(binding.get().shopsProgressBar);
//                                } catch (Exception e) {
//                                      Sentry.captureException(e);
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ShopsResponse shopsResponse) {
//
//                                try {
//
//                                      if (shopsResponse.getShop().size() < 1){
//                                            Utils.ShowView(binding.get().shopEmptyText);
//                                      }else{
//                                            Utils.RemoveView(binding.get().shopEmptyText);
//                                      }
//
//                                      ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());
//
//                                      binding.get().mallsRecyclerView.setAdapter(shopCardAdapter);
//
//                                      shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
//                                            if (obj.getShopType() != null){
//                                            if (obj.getShopType().equals(Constants.SPECIALIST_CLINIC) || obj.getShopType().equals(Constants.HEALTH_FACILITY) || obj.getShopType().equals(Constants.PHARMACY)) {
//                                                  Intent intent = new Intent(getContext(), HealthShopHomeActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj);
//
//                                            }else {
//                                                  Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj);
//                                                  startActivity(intent);
//                                            }
//
//                                            }else{
//                                                  Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj);
//                                                  startActivity(intent);
//                                            }
//                                      });
//
//                                } catch (Exception e) { Sentry.captureException(e);}
//                          }
//
//                          @Override
//                          public void onError(Throwable e) {
//
//                                if (getActivity() != null)
//                                      reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");
//
//                                Sentry.captureException(e);
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                disposeAPIService(disposable);
//
//                                try {
//
//                                      Utils.RemoveView(binding.get().shopsProgressBar);
//                                      Utils.ShowView(binding.get().mallsRecyclerView);
//
//                                }catch (Exception e){
//                                      Sentry.captureException(e);
//                                }
//                          }
//                    });
//      }
//
//      private void getStoresByLocation(String location) {
//
//            mAPIService.getShopsByLocation(location)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<ShopsResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//
//                                try {
//                                      Utils.ShowView(binding.get().shopsProgressBar);
//                                } catch (Exception e) {
//                                      Sentry.captureException(e);
//                                }
//                          }
//
//                          @Override
//                          public void onNext(@NonNull ShopsResponse shopsResponse) {
//
//                                try {
//
//                                      if (shopsResponse.getShop().size() < 1){
//                                            Utils.ShowView(binding.get().shopEmptyText);
//                                      }else{
//                                            Utils.RemoveView(binding.get().shopEmptyText);
//                                      }
//
//                                      ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());
//
//                                      binding.get().mallsRecyclerView.setAdapter(shopCardAdapter);
//
//                                      shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
//                                            if (obj.getShopType() != null){
//                                                  if (obj.getShopType().equals(Constants.SPECIALIST_CLINIC) || obj.getShopType().equals(Constants.HEALTH_FACILITY) || obj.getShopType().equals(Constants.PHARMACY)) {
//                                                        Intent intent = new Intent(getContext(), HealthShopHomeActivity.class);
//                                                        intent.putExtra(Constants.SHOP_OBECT, obj);
//                                                        startActivity(intent);
//                                                  }else {
//                                                        Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                        intent.putExtra(Constants.SHOP_OBECT, obj);
//                                                        startActivity(intent);
//                                                  }
//
//                                            }else{
//                                                  Intent intent = new Intent(getContext(), StoreHomeActivity.class);
//                                                  intent.putExtra(Constants.SHOP_OBECT, obj);
//                                                  startActivity(intent);
//                                            }
//                                      });
//
//                                } catch (Exception e) { Sentry.captureException(e);}
//                          }
//
//                          @Override
//                          public void onError(Throwable e) {
//
//                                if (getActivity() != null) {
//                                      reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");
//                                }
//
//                                Sentry.captureException(e);
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                disposeAPIService(disposable);
//
//                                try {
//
//                                      Utils.RemoveView(binding.get().shopsProgressBar);
//                                      Utils.ShowView(binding.get().mallsRecyclerView);
//
//                                }catch (Exception e){
//                                      Sentry.captureException(e);
//                                }
//                          }
//                    });
//      }
//
//      private void disposeAPIService(Disposable disposable) {
//            if (!disposable.isDisposed()) {
//                  disposable.dispose();
//            }
//      }
//
//      private void startAutoSlider(final int count) {
//            if (getActivity() != null) {
//                  runnable = () -> {
//                        if (binding.get().pager != null) {
//                              int pos = binding.get().pager.getCurrentItem();
//                              pos = pos + 1;
//                              if (pos >= count) pos = 0;
//                              binding.get().pager.setCurrentItem(pos);
//                              handler.postDelayed(runnable, 3000);
//                        }
//                        handler.postDelayed(runnable, 3000);
//                  };
//            }
//      }
//
//      private void replaceFeaturedData(List<Inventory> featuredProductList) {
//            viewPagerAdapter.get().replaceFeaturedList(featuredProductList);
//            setupSliderPagination();
//            binding.get().executePendingBindings();
//      }
//
//      private void setupSliderPagination() {
//
//            if (getActivity() != null )
//            dotsCount = viewPagerAdapter.get().getCount();
//
//            if (dotsCount > 0 && dots == null) {
//
//                  dots = new ImageView[dotsCount];
//
//                  if (binding.get() != null) {
//                        if (binding.get().viewPagerCountDots.getChildCount() > 0) {
//                              binding.get().viewPagerCountDots.removeAllViewsInLayout();
//                        }
//                  }
//
//                  for (int i = 0; i < dotsCount; i++) {
//                        dots[i] = new ImageView(getContext());
//                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
//
//                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                                LinearLayout.LayoutParams.WRAP_CONTENT,
//                                LinearLayout.LayoutParams.WRAP_CONTENT
//                        );
//                        params.setMargins(4, 0, 4, 0);
//                        binding.get().viewPagerCountDots.addView(dots[i], params);
//                  }
//                  dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
//            }
//      }
//
//      private void setUpAutoSlider(ViewPager viewPager, int size, Activity activity){
//            try {
//                  Field mScroller = ViewPager.class.getDeclaredField("mScroller");
//                  mScroller.setAccessible(true);
//                  SpeedSlowScroller scroller = new SpeedSlowScroller(activity);
//                  mScroller.set(viewPager, scroller);
//                  Timer timer = new Timer();
//                  timer.scheduleAtFixedRate(new SliderTimer(viewPager, size, activity), 4000, 6000);
//            } catch (Exception ignored) {
//            }
//
//      }
//
//      private void hideLoading() {
//
//            if (loadingCount == 3 && layoutDone) {
//
//                  binding.get().loadingView.setVisibility(View.VISIBLE);
//                  binding.get().loadHolder.setVisibility(View.VISIBLE);
//            }
//      }
//
//      @Override
//      public void onDispatched() {}
//
//      @Subscribe(threadMode = ThreadMode.MAIN)
//      public void onItemClicked(Reload reload) {
//            loadMainData();
//      }
//
//      public void HomeSlider(){
//
//            mAPIService.getSliders()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Observer<SliderResponse>() {
//                          Disposable disposable;
//
//                          @Override
//                          public void onSubscribe(Disposable d) {
//                                disposable = d;
//
//                          }
//
//                          @Override
//                          public void onNext(@NonNull SliderResponse sliderResponse) {
//
//                                if (getActivity() !=null ){
//                                      PagerAdapter adapter = new BannerAdapter(getActivity(), sliderResponse.getSlider());
//                                      binding.get().viewpager.setAdapter(adapter);
//                                }
//                          }
//
//                          @Override
//                          public void onError(Throwable e) {
//
//                          }
//
//                          @Override
//                          public void onComplete() {
//                                disposeAPIService(disposable);
//
//                          }
//                    });
//
//      }
//
//      public void setHeight() {
//            if(getActivity() != null ) {
//                  Display display = getActivity().getWindowManager().getDefaultDisplay();
//                  int width = (display.getWidth());
//                  double height = (display.getHeight());
//                  height = height / 2.6;
//                  LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, (int) height);
//                  binding.get().lvlBanner.setLayoutParams(parms);
//            }
//      }
//
//
//}
