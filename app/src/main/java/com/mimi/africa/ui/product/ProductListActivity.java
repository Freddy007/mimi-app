package com.mimi.africa.ui.product;


import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ProductListActivity extends BaseActivity {

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_product_list);

            initComponents();
      }

      @Subscribe(threadMode =ThreadMode.MAIN)
      public void addToCart(AddToCart event){}

      private void initToolbar(String title) {
            Toolbar toolbar =  findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this);
      }

      private void initComponents(){
                  if (getIntent().getStringExtra(Constants.CATEGORY_ID) != null ) {
                        String category_name = getIntent().getStringExtra(Constants.CATEGORY_NAME);
                        String category_id = getIntent().getStringExtra(Constants.CATEGORY_ID);
                        int shop_id = getIntent().getIntExtra(Constants.SHOP_ID, 0);
                        initToolbar(category_name);
                        CategoryProductsListFragment categoryProductsListFragment = CategoryProductsListFragment.newInstance(category_id, String.valueOf(shop_id));
                        setupFragment(categoryProductsListFragment);
                  }else if ((getIntent().getStringExtra(Constants.FILTERING_TYPE) != null ) ) {
                        String filtering_type_name = getIntent().getStringExtra(Constants.FILTERING_TYPE_NAME);
                        String filtering_type = getIntent().getStringExtra(Constants.FILTERING_TYPE);
                        String shop_id = getIntent().getStringExtra(Constants.SHOP_ID);
                        initToolbar(filtering_type_name);
                        AllProductListFragment allProductListFragment = AllProductListFragment.newInstance(filtering_type_name,filtering_type, String.valueOf(shop_id));
                        setupFragment(allProductListFragment);
            }else {
                  initToolbar("Product Listings");
                  setupFragment(new AllProductListFragment());
            }

      }



}
