package com.mimi.africa.ui.mainSearch;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.mimi.africa.Adapter.ServiceCardAdapter;
import com.mimi.africa.Adapter.ShopCardAdapter;
import com.mimi.africa.Adapter.ShopProductGridCardAdapter;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.FragmentMainHomeBinding;
import com.mimi.africa.databinding.FragmentSearchHomeBinding;
import com.mimi.africa.event.SearchEvent;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.common.DataBoundListAdapter;
import com.mimi.africa.ui.location.LocationActivity;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.shop.ShopListActivity;
import com.mimi.africa.ui.shop.StoreActivity;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class SearchHomeFragment extends BaseFragment implements DataBoundListAdapter.DiffUtilDispatchedInterface {

      private static final String TAG = SearchHomeFragment.class.getSimpleName();
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentSearchHomeBinding> binding;

      @Nullable
      private Runnable runnable = null;
      @NonNull
      private Handler handler = new Handler();
      private ShopProductGridCardAdapter mProductsAdapter;
      @Nullable

      private String mTerm;


      @Inject
      APIService mAPIService;

      public SearchHomeFragment() {}

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            // Inflate the layout for this fragment
            FragmentSearchHomeBinding fragmentMainHomeBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_search_home, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentMainHomeBinding);

           mTerm = mTerm == null ? "all" : mTerm;

            return fragmentMainHomeBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {

            if (getActivity() != null)
            parentView = getActivity().findViewById(android.R.id.content);

            binding.get().seeAllProductsButton.setOnClickListener(v -> {
                  navigateToProductListActivity();
            });

            binding.get().seeAllServicesButton.setOnClickListener(v -> {
                  navigateToServiceListActivity();
            });

            binding.get().seeAllShops.setOnClickListener(v -> {
                  startActivity(new Intent(getActivity(), ShopListActivity.class));
            });

            binding.get().productsMore.setOnClickListener(v -> {
                  PopupMenu popupMenu = new PopupMenu(getContext(), v);

                  popupMenu.setOnMenuItemClickListener(item -> {
                        switch (item.getItemId()){

                              case R.id.action_low_to_high:
                                    searchLowToHigh(mTerm);
                                    break;

                              case R.id.action_high_to_low:
                                    filterHighToLow(mTerm);
                                    break;

                              case R.id.action_has_offers:
                                    searchHasOffers(mTerm, "true");
                                    break;

                              case R.id.action_condition_new:
                                    filterCondition(mTerm, "New");
                                    break;

                              case R.id.action_condition_old:
                                    filterCondition(mTerm, "Used");
                                    break;

                              case R.id.action_free_delivery:
                                    filterFreeDelivery(mTerm);
                                    break;

                        }
                        return true;
                  });

                  popupMenu.inflate(R.menu.products_menu);
                  popupMenu.show();
            });
      }

      @Override
      protected void initViewModels() {}

      @Override
      protected void initAdapters() {

            if (getActivity() != null )
            binding.get().productsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
            binding.get().servicesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
            binding.get().mallsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
      }

      @Override
      protected void initData() {
            if (connectivity.isConnected()) {
                  loadMainData();
            }
      }

      @Override
      public void onResume() {
            super.onResume();

            if ( mTerm != null) {
                  search(mTerm);
            }

      }

      private void loadMainData() {

            search("all");

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        getServiceListings();
                  }
            },3000);
      }

      private void getServiceListings() {
            getServiceResponseObservable(mAPIService, "15")
                    .subscribe(new Observer<ServiceResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) { disposable = d; }

                          @Override
                          public void onNext(@NonNull ServiceResponse listingResponse) {

                                try {

                                      ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), listingResponse.getServices());
                                      binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);
                                      serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (getActivity() != null)
                                            if (view == view.findViewById(R.id.nameTextView)) {
                                                  navigateToStore(obj.getShop());
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  startActivity(new Intent(getContext(), LocationActivity.class));
                                            } else if (view == view.findViewById(R.id.cardView12)) {
                                                  navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {

                                try {
                                      reloadSnackBar(binding.get().servicesProgressBar, getString(R.string.Oops));

                                } catch (Exception ex) {
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                try {
//                                      Utils.RemoveView(binding.get().servicesProgressBar);
                                      Utils.ShowView(binding.get().servicesRecyclerView);

                                      getShops();

                                } catch (Exception e) {
                                      Sentry.captureException(e);
                                }
                          }
                    });
      }

      private void getShops() {

            mAPIService.getShops()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;


                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {

                                try {

                                      ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());

                                      binding.get().mallsRecyclerView.setAdapter(shopCardAdapter);

                                      shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                            navigateToStore(obj);
                                      });

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }
                          }

                          @Override
                          public void onError(Throwable e) {
                                logError(TAG, e, "onError:  " + e.getMessage());

//                                      reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");

                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                try {

                                      Utils.RemoveView(binding.get().shopsProgressBar);
                                      Utils.ShowView(binding.get().mallsRecyclerView);

                                    ((MainSearchActivity) getActivity()).progressDialog.hide();

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      @Override
      public void onDispatched() {}

      private void search(String term){
            final boolean[] empty = {false};

            getSearchResponseObservable(term)
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                              ((MainSearchActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                              ((MainSearchActivity) getActivity()).progressDialog.setCancelable(false);
                              ((MainSearchActivity) getActivity()).progressDialog.show();
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true;}

                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                              search(term);
                                        });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {

                              ((MainSearchActivity) getActivity()).progressDialog.hide();

                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
//                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
//                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private Observable<ListingResponse> getSearchResponseObservable(String term) {
            return mAPIService.searchProducts(term)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      private void searchServices(String term){

            final boolean[] empty = {false};

            getSearchServiceResponseObservable(mAPIService, term )
                    .subscribe(new Observer<ServiceResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
//                                Utils.ShowView(binding.get().servicesProgressBar);
                          }

                          @Override
                          public void onNext(@NonNull ServiceResponse listingResponse) {

                                try {
                                      ServiceCardAdapter serviceCardAdapter = new ServiceCardAdapter(getContext(), listingResponse.getServices());

                                      if (getActivity() != null)

                                      binding.get().servicesRecyclerView.setAdapter(serviceCardAdapter);
                                      serviceCardAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.nameTextView)) {
                                                  startActivity(new Intent(getContext(), StoreActivity.class));

                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.cardView12)) {
                                                  navigateToServiceDetailActivity(getActivity(), listingResponse.getServices().get(position));
                                            }
                                      });

                                      if (listingResponse.getServices().size() == 0){ empty[0] =true; }

                                }catch (Exception e){
                                    Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {

                                try {
                                      Utils.RemoveView(binding.get().servicesProgressBar);
                                      Snackbar snackbar = Snackbar.make(parentView, "There was a problem loading services!", Snackbar.LENGTH_SHORT)
                                              .setAction("RELOAD!", view -> getServiceListings());
                                      snackbar.show();
                                }catch (Exception ex){
                                      Sentry.captureException(e);
                                }

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try{

                                      if (empty[0]){

                                            Utils.ShowView(binding.get().noServicesTv);
                                            Utils.RemoveView(binding.get().servicesRecyclerView);
                                      }else {
                                            Utils.ShowView(binding.get().noServicesTv);
                                            Utils.RemoveView(binding.get().servicesRecyclerView);
                                      }

                                      Utils.RemoveView(binding.get().servicesProgressBar);

                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }
                    });
      }

      private void searchShops(String term) {

            mAPIService.searchShops(term)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ShopsResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;

//                                try {
//                                      Utils.ShowView(binding.get().shopsProgressBar);
//                                } catch (Exception e) {
//                                      logError(TAG, e, "onSubscribe:  ");
//                                }
                          }

                          @Override
                          public void onNext(@NonNull ShopsResponse shopsResponse) {

                                try {

                                      ShopCardAdapter shopCardAdapter = new ShopCardAdapter(getContext(), shopsResponse.getShop());

                                      binding.get().mallsRecyclerView.setAdapter(shopCardAdapter);

                                      shopCardAdapter.setOnItemClickListener((view, obj, pos) -> {
                                            navigateToStore(obj);
                                      });

                                } catch (Exception e) {
                                      logError(TAG,e, "onNext:  ");
                                }
                          }

                          @Override
                          public void onError(Throwable e) {

//                                if (getActivity() != null)
//                                      reloadSnackBar(binding.get().shopsProgressBar, "Oops, something went wrong!");

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                disposeAPIService(disposable);

                                try {

                                      Utils.RemoveView(binding.get().shopsProgressBar);
                                      Utils.ShowView(binding.get().mallsRecyclerView);

                                }catch (Exception e){
                                      logError(TAG,e, "onComplete:  ");
                                }
                          }
                    });
      }

      private void searchHasOffers(String term, String has_offers){
            final boolean[] empty = {false};

            mAPIService.searchProductsFilter(term, has_offers)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true; }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                              search(term);
                                        });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private void searchLowToHigh(String term){
            final boolean[] empty = {false};

            mAPIService.FilterLowToHigh(term, "true")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true; }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                      search(term);
                                });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private void filterHighToLow(String term){
            final boolean[] empty = {false};

            mAPIService.FilterHighToLow(term, "true")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true; }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                      search(term);
                                });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private void filterCondition(String term, String condition){
            final boolean[] empty = {false};

            mAPIService.FilterCondition(term, condition)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true; }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                      search(term);
                                });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private void filterFreeDelivery(String term){
            final boolean[] empty = {false};

            mAPIService.FilterFreeDelivery(term, "true")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      processProductResults(listingResponse);
                                      if (listingResponse.getInventories().size() == 0){ empty[0] = true; }
                                }catch (Exception e){
                                      Sentry.captureException(e);
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                try{
                                      Utils.RemoveView(binding.get().productsProgressBar);
                                }catch (Exception ex){ Sentry.captureException(e); }

                                Snackbar.make(parentView, "There was a problem loading products!", Snackbar.LENGTH_LONG).setAction("RELOAD!", view -> {
                                      search(term);
                                });

                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                try {
                                      if (empty[0]){
                                            Utils.ShowView(binding.get().noProductsTv);
                                            Utils.RemoveView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(   binding.get().productsProgressBar);

                                      }else {
                                            Utils.RemoveView(binding.get().noProductsTv);
                                            Utils.ShowView(binding.get().productsRecyclerView);
                                            Utils.RemoveView(binding.get().productsProgressBar);
                                      }
                                }catch (Exception e){ Sentry.captureException(e); }
                          }
                    });
      }

      private void processProductResults(@NonNull ListingResponse listingResponse) {
            if (getActivity() != null) {
                  ShopProductGridCardAdapter shopProductGridCardAdapter = new ShopProductGridCardAdapter(getContext(), listingResponse.getInventories());
                  binding.get().productsRecyclerView.setAdapter(shopProductGridCardAdapter);

                  shopProductGridCardAdapter.setOnItemClickListener((view, obj, position) -> {

                        if (view == view.findViewById(R.id.nameTextView)) {
                              navigateToStore(obj);

                        } else if (view == view.findViewById(R.id.addressTextView)) {
                              navigateToLocation(getActivity());

                        } else if (view == view.findViewById(R.id.cardView12)) {
                              navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                        }
                  });
            }
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(SearchEvent event) {
            mTerm = event.getTerm();
            search(event.getTerm());
            searchServices(event.getTerm());
            searchShops(event.getTerm());
      }

      private void reloadSnackBar(ProgressBar servicesProgressBar, String string) {
            Utils.RemoveView(servicesProgressBar);
            Snackbar snackbar = Snackbar.make(parentView, string, Snackbar.LENGTH_INDEFINITE)
                    .setAction("RELOAD!", view -> loadMainData());
            snackbar.show();
      }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

}
