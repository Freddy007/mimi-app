package com.mimi.africa.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.Preference;
import androidx.preference.SwitchPreference;

import com.mimi.africa.R;
import com.mimi.africa.event.DemoEvent;
import com.mimi.africa.event.LogOutEvent;
import com.mimi.africa.ui.aboutus.AboutUsActivity;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.forgotPassword.UserForgotPasswordActivity;
import com.mimi.africa.ui.product.ProductDetailsActivity;
import com.mimi.africa.ui.profile.ProfileFragment;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.PSDialogMsg;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.user.UserViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;


public class AppSettingsFragment extends BaseFragment {

    @Inject
    protected SharedPreferences pref;

    public String userId;

    public AppSettingsFragment() {
        // Required empty public constructor
        if (pref != null) {
            userId =   pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        }
    }

    public static AppSettingsFragment newInstance(String param1, String param2) {
        AppSettingsFragment fragment = new AppSettingsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_settings, container, false);

        return view;
    }

    @Override
    protected void initUIAndActions() {
        userId = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);

    }

    @Override
    protected void initViewModels() {

    }

    @Override
    protected void initAdapters() {

    }

    @Override
    protected void initData() {

    }

    public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

        private static final String TAG = SettingsFragment.class.getSimpleName();
        private UserViewModel userViewModel;


        public EventBus eventBus;
        public boolean registerEventBus = true;
        public SwitchPreference notificationPreference;
        public Preference logoutPreference;
        private PSDialogMsg psDialogMsg;


        public SettingsFragment() {
            notificationPreference = findPreference("notification");
//            logoutPreference = findPreference(getString(R.string.logout));

        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            psDialogMsg = new PSDialogMsg(getActivity(), false);

            if(registerEventBus) {
                eventBus = EventBus.getDefault();
            }

            Preference logOutButton = findPreference(getString(R.string.logout));

            if (logOutButton != null) {
                logOutButton.setOnPreferenceClickListener(preference -> {
                    eventBus.postSticky(new LogOutEvent());
                    return true;
                });
            }

            Preference profileButton = findPreference(getString(R.string.menu__profile));
            if (profileButton != null) {
                if (getActivity() != null) {
                    profileButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            showProfileDialogFragment();
                            return true;
                        }
                    });
                }
            }

            Preference resetButton = findPreference(getString(R.string.menu_reset_password));
            if (resetButton != null) {
                resetButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        if (getActivity() != null )
                            getActivity().startActivity(new Intent(getActivity(), UserForgotPasswordActivity.class));
                        return true;
                    }
                });
            }

            Preference checkUpdateButton = findPreference(getString(R.string.menu_check_update));
            if (checkUpdateButton != null) {
                checkUpdateButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {

                        ((MainHomeActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                        ((MainHomeActivity) getActivity()).progressDialog.setCancelable(false);
                        ((MainHomeActivity) getActivity()).progressDialog.show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                ((MainHomeActivity) getActivity()).progressDialog.hide();

//                                psDialogMsg.showSuccessDialog(getString(R.string.no_updates), getString(R.string.app__ok));
                                psDialogMsg.showInfoDialog(getString(R.string.no_updates), getString(R.string.app__ok));
                                psDialogMsg.show();
                            }
                        },4000);

                        return true;
                    }
                });
            }

            Preference aboutUsButton = findPreference(getString(R.string.menu_about_us));
            if (aboutUsButton != null) {
                aboutUsButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        if (getActivity() != null )
                        getActivity().startActivity(new Intent(getActivity(), AboutUsActivity.class));
                        return true;
                    }
                });
            }


        }

        private void showProfileDialogFragment() {
            ProfileFragment profileFragment = new ProfileFragment();
            showDialog(profileFragment);
        }

        public void showDialog(@NonNull Fragment fragment) {
            if (getActivity() != null ) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.add(android.R.id.content, fragment).addToBackStack(null).commit();
            }
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("notifications")){
                boolean notifications = sharedPreferences.getBoolean(key, true);

                Preference logOutButton = findPreference(getString(R.string.logout));

            }
        }

        @Subscribe
        public void init(DemoEvent loginClickedEvent){}

        @Override
        public void onResume() {
            super.onResume();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean notificationPref = sharedPreferences.getBoolean(Constants.NOTIFICATION_PREF, false);

            Log.i(TAG, "onResume:  Notification pref  " + notificationPref);

            Preference logOutButton = findPreference(getString(R.string.logout));


        }

        @Override
        public void onStart() {
            super.onStart();
            if(registerEventBus)
                eventBus.register(this);
        }
        //
        @Override
        public void onStop() {
            if(registerEventBus) eventBus.unregister(this);

            EventBus.getDefault().removeStickyEvent(DemoEvent.class);
            super.onStop();
        }

        private boolean isUserLoggedIn() {
            return CustomSharedPrefs.getUserStatus(getContext());
        }

        private void logout() {
            userViewModel.deleteUserLogin(userViewModel.user).observe(this, status -> {
                if (status != null) {

//                    navigationController.navigateBackToProfileFragment(this.getActivity());

                    if (getActivity() != null) {
                        if (!(getActivity() instanceof MainHomeActivity)) {
                            getActivity().finish();
                        }
                    }

                }
            });
        }
    }


}
