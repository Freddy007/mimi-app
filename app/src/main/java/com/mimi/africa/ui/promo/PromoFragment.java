package com.mimi.africa.ui.promo;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mimi.africa.R;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.search.AdapterSuggestionSearch;
import com.mimi.africa.ui.search.FragmentSearchTabs;
import com.mimi.africa.ui.search.ProductsFragmentSearchTabs;
import com.mimi.africa.ui.search.ServicesFragmentSearchTabs;
import com.mimi.africa.ui.search.StoresFragmentSearchTabs;
import com.mimi.africa.utils.CustomSharedPrefs;

import java.util.ArrayList;
import java.util.List;


public class PromoFragment extends BaseFragment {

      private static final String TAG = PromoFragment.class.getSimpleName();

      private ViewPager view_pager;
      private TabLayout tab_layout;
      private EditText et_search;
      private ImageButton bt_clear;
      private ImageButton back_button;
      private ProgressBar progress_bar;
      private RecyclerView recyclerSuggestion;
      public AdapterSuggestionSearch mAdapterSuggestion;
      private LinearLayout lyt_suggestion;

      private View view;

      public PromoFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            view =  inflater.inflate(R.layout.fragment_promo, container, false);


            return view;
      }

      @Override
      protected void initUIAndActions() {

            view_pager = view.findViewById(R.id.view_pager);
            FragmentManager fragmentManager = getChildFragmentManager();
            setupViewPager(view_pager, fragmentManager);

            tab_layout = view.findViewById(R.id.tab_layout);
            tab_layout.setupWithViewPager(view_pager);

      }

      @Override
      protected void initViewModels() {

      }

      @Override
      protected void initAdapters() {

      }

      @Override
      protected void initData() {
            CustomSharedPrefs.removeLoadPageStatus(getContext());
            CustomSharedPrefs.setReloadPageStatus(getContext(), true);
      }

      private void setupViewPager(@NonNull ViewPager viewPager, FragmentManager fragmentManager) {
            SectionsPagerAdapter adapter = new SectionsPagerAdapter(fragmentManager);

//        adapter.addFragment(AllFragmentSearchTabs.newInstance(), FragmentSearchTabs.ALL_TAB);
            adapter.addFragment(ProductsFragmentSearchTabs.newInstance(),  FragmentSearchTabs.PRODUCTS_TAB);
            adapter.addFragment(ServicesFragmentSearchTabs.newInstance(),  FragmentSearchTabs.SERVICES_TAB);
            adapter.addFragment(StoresFragmentSearchTabs.newInstance(),  FragmentSearchTabs.STORES_TAB);

            viewPager.setAdapter(adapter);
      }

      private class SectionsPagerAdapter extends FragmentPagerAdapter {

            private final List<Fragment> mFragmentList = new ArrayList<>();
            private final List<String> mFragmentTitleList = new ArrayList<>();

            public SectionsPagerAdapter(FragmentManager manager) {
                  super(manager);
            }

            @Override
            public androidx.fragment.app.Fragment getItem(int position) {
                  return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                  return mFragmentList.size();
            }

            public void addFragment(Fragment fragment, String title) {
                  mFragmentList.add(fragment);
                  mFragmentTitleList.add(title);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                  return mFragmentTitleList.get(position);
            }
      }

}
