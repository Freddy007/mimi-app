package com.mimi.africa.ui.message;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mimi.africa.R;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomMessageOptionsLayoutBinding;
import com.mimi.africa.databinding.FragmentMessageBinding;
import com.mimi.africa.db.MimiDb;
import com.mimi.africa.model.Notification;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.ui.chat.MessageActivity;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.ItemAnimation;
import com.mimi.africa.utils.Utils;
import com.mimi.africa.viewModels.ShopViewModel;
import com.mimi.africa.viewModels.notification.NotificationViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class MessageFragment extends BaseFragment {
      private static final String TAG = MessageFragment.class.getSimpleName();

      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private AutoClearedValue<FragmentMessageBinding> binding;

      private NotificationViewModel notificationViewModel;
      private MessageAdapter mAdapter;
      private List<Notification> items = new ArrayList<>();
      private int animation_type = ItemAnimation.BOTTOM_UP;
      private RecyclerView recyclerView;
      private View view;
      private LinearLayout noNotificationsLinearLayout;
      private LinearLayout progressLinearLayout;

      private AutoClearedValue<BottomMessageOptionsLayoutBinding> bottomMessageOptionsLayoutBinding;
      private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
      private String mShopId;
      private long mId;
      private Notification mNotification;
      private ShopViewModel shopViewModel;

      @Inject
      protected ViewModelProvider.Factory viewModelFactory;


      @Inject
       MimiDb mimiDb;

      public MessageFragment() {}

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Log.d(TAG, "onCreate: MessageFragment");

      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentMessageBinding fragmentMessageBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_message, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentMessageBinding);
            return fragmentMessageBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
//           mimiDb = MimiDb.getInstance(getActivity());
            Context context = getContext();

            recyclerView = binding.get().messagesRecyclerView;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);
            noNotificationsLinearLayout =binding.get().noMessagesLinearLayout;
            progressLinearLayout = binding.get().lytProgress;
            animation_type = ItemAnimation.BOTTOM_UP;

            if (getContext() != null) {

                  mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  bottomMessageOptionsLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_message_options_layout, null, false));
                  mBottomSheetDialog.get().setContentView(bottomMessageOptionsLayoutBinding.get().getRoot());

                  binding.get().allButton.setOnClickListener(v -> {

                        showProgressIndicator();
                        new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                    refresh();
                                    hideProgressBar();

                              }
                        },3000);

                  });

                  binding.get().archiveButton.setOnClickListener(v -> {
                      startActivity(new Intent(MessageFragment.this.getActivity(), ArchivedActivity.class));
                  });

                  binding.get().unreadButton.setOnClickListener(v -> {
                      startActivity(new Intent(MessageFragment.this.getActivity(), UnreadActivity.class));
                  });
            }
      }

      private void hideProgressBar() {
            ((MainHomeActivity) getActivity()).progressDialog.hide();
      }

      private void showProgressIndicator() {
            if (getActivity() != null ) {
                  ((MainHomeActivity) getActivity()).progressDialog.setMessage((Utils.getSpannableString(getContext(), getString(R.string.message__please_wait), Utils.Fonts.MM_FONT)));
                  ((MainHomeActivity) getActivity()).progressDialog.setCancelable(false);
                  ((MainHomeActivity) getActivity()).progressDialog.show();
            }
      }

      @Override
      protected void initViewModels() {
            if (getActivity() != null){
//            notificationViewModel =  ViewModelProviders.of(getActivity()).get(NotificationViewModel.class);
            notificationViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel.class);

            }
      }

      @Override
      protected void initAdapters() {}

      @Override
      protected void initData() {
                  getNotificationsFromDatabase();
      }

      private void getNotificationsFromDatabase() {
            if (getActivity() != null) {
                  notificationViewModel.getMessages().observe(getActivity(), appNotifications -> {
                        items = appNotifications;
                        processView(appNotifications);
                  });
            }
      }

      private void processView(List<Notification> appNotifications) {
            if (appNotifications != null && appNotifications.size() != 0) {
                  setAdapter(appNotifications);
                  noNotificationsLinearLayout.setVisibility(View.GONE);
                  recyclerView.setVisibility(View.VISIBLE);

            } else {
                  noNotificationsLinearLayout.setVisibility(View.VISIBLE);
                  recyclerView.setVisibility(View.GONE);
            }
      }


      private void setAdapter(List<Notification> notificationList) {
            //set data and list adapter
            try{
                  mAdapter = new MessageAdapter(getContext(), notificationList);
                  recyclerView.setAdapter(mAdapter);

                  mAdapter.setOnClickListener(new MessageAdapter.OnClickListener() {
                        @Override
                        public void onItemClick(View view, Notification obj, int pos) {
                              mNotification = obj;
                              appExecutors.getDiskIO().execute(() -> {
                                    obj.setRead(true);
                                    mimiDb.notificationDaoAccess().updateNotification(obj);
                              });

                              mShopId = obj.getSenderId();
                              mId            = obj.getId();
                              Intent intent = new Intent(getContext(), MessageActivity.class);
                              intent.putExtra(Constants.SHOP_ID, Integer.valueOf(obj.getSenderId()));
                              intent.putExtra(Constants.SHOP_NAME, obj.getSender());
                              startActivity(intent);
                        }

                        @Override
                        public void onItemLongClick(View view, Notification obj, int pos) {

                              if (mBottomSheetDialog != null ) {
                                    bottomMessageOptionsLayoutBinding.get().titleTextView.setText(String.format("Conversation with %s", obj.getSender()));

                                    if (obj.isRead()){
                                          bottomMessageOptionsLayoutBinding.get().readStatus.setText("MARK AS UNREAD");
                                    }else {
                                          bottomMessageOptionsLayoutBinding.get().readStatus.setText("MARK AS READ");
                                    }

                                    if (obj.isArchived()){
                                          bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("UNARCHIVE MESSAGE");
                                    }else {
                                          bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("ARCHIVE MESSAGE");
                                    }

                                    mBottomSheetDialog.get().show();

                                    bottomMessageOptionsLayoutBinding.get().archiveMessage.setOnClickListener(v -> {
                                          showProgressIndicator();
                                          archiveMessage(obj);

                                          new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                      hideProgressBar();
                                                      refresh();
                                                }
                                          },3000);


                                          mBottomSheetDialog.get().dismiss();
                                    });

                                    bottomMessageOptionsLayoutBinding.get().readStatus.setOnClickListener(v -> {
                                          setReadStatus(obj);
                                          showProgressIndicator();

                                          new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                      hideProgressBar();
                                                      refresh();
                                                }
                                          },3000);

                                          mBottomSheetDialog.get().dismiss();
                                    });

                              }
                        }
                  });

            }catch (Exception e){
                  if (mAdapter != null ){
//                        mAdapter = new AppNotificationAdapter(getContext(), items, animation_type);
//                        recyclerView.setAdapter(mAdapter);
                  }
            }
      }

      private void setReadStatus(Notification notification) {

            if(notification.isRead()){

                  appExecutors.getDiskIO().execute(() -> {
                        notification.setRead(false);
                        mimiDb.notificationDaoAccess().updateNotification(notification);
                  });

            }else {

                  appExecutors.getDiskIO().execute(() -> {
                        notification.setRead(true);
                        mimiDb.notificationDaoAccess().updateNotification(notification);
                  });
            }

             refresh();
      }

      private void archiveMessage(Notification notification) {

            appExecutors.getDiskIO().execute(() -> {
                  if (notification.isArchived()){
                        bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("UNARCHIVE MESSAGE");
                        notification.setArchived(false);
                  }else {
                        bottomMessageOptionsLayoutBinding.get().archiveMessage.setText("ARCHIVE MESSAGE");
                        notification.setArchived(true);
                  }
                  mimiDb.notificationDaoAccess().updateNotification(notification);
            });
//            refresh();
            Toast.makeText(getContext(), "Archived message", Toast.LENGTH_SHORT).show();
      }

      private void refresh() {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                  ft.setReorderingAllowed(false);
            }
            ft.detach(this).attach(this).commit();
      }

      @Override
      public void onResume() {
            super.onResume();
            Log.d(TAG, "onResume: MessageFragment");
      }

      @Override
      public void onPause() {
            super.onPause();
            Log.d(TAG, "onPause: MessageFragment");
      }

}
