package com.mimi.africa.ui.product;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.binding.FragmentDataBindingComponent;
import com.mimi.africa.databinding.BottomBoxLayoutBinding;
import com.mimi.africa.databinding.BottomCategoriesBoxLayoutBinding;
import com.mimi.africa.databinding.FragmentAllProductListBinding;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.ui.common.BaseFragment;
import com.mimi.africa.ui.mainSearch.MainSearchActivity;
import com.mimi.africa.ui.product.adapter.AllProductVerticalListAdapter;
import com.mimi.africa.ui.product.adapter.CategoryAdapter;
import com.mimi.africa.utils.AutoClearedValue;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Utils;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.sentry.core.Sentry;


public class CategoryProductsListFragment extends BaseFragment {

      private static final String TAG = CategoryProductsListFragment.class.getSimpleName();

      private boolean typeClicked = false;
      private boolean filterClicked = false;
      private final androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
      private View parentView;
      private AutoClearedValue<FragmentAllProductListBinding> binding;
      private AutoClearedValue<BottomBoxLayoutBinding> bottomBoxLayoutBinding;
      private AutoClearedValue<BottomSheetDialog> mBottomSheetDialog;
      private AutoClearedValue<BottomCategoriesBoxLayoutBinding> bottomCategoriesBoxLayoutBinding;
      private AutoClearedValue<BottomSheetDialog> mCategoryBottomSheetDialog;

      private String mCategoryId;
      private String mShopId;

      @Inject
      APIService mAPIService;

      public CategoryProductsListFragment() {
      }

      public static CategoryProductsListFragment newInstance(String category_id, String shop_id){

            CategoryProductsListFragment categoryProductsListFragment = new CategoryProductsListFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.CATEGORY_ID,  category_id);
            bundle.putString(Constants.SHOP_ID,  shop_id);
            categoryProductsListFragment.setArguments(bundle);
            return categoryProductsListFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mCategoryId = getArguments().getString(Constants.CATEGORY_ID);
                  mShopId = getArguments().getString(Constants.SHOP_ID);
            }

      }

      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            FragmentAllProductListBinding fragmentProductListBinding =
                    DataBindingUtil.inflate(inflater, R.layout.fragment_all_product_list, container, false, dataBindingComponent);
            binding = new AutoClearedValue<>(this, fragmentProductListBinding);
            return fragmentProductListBinding.getRoot();
      }

      @Override
      protected void initUIAndActions() {
            parentView = getActivity().findViewById(android.R.id.content);

            if (getContext() != null) {

                  mCategoryBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  mBottomSheetDialog = new AutoClearedValue<>(this, new BottomSheetDialog(getContext()));
                  bottomBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_box_layout, null, false));
                  mBottomSheetDialog.get().setContentView(bottomBoxLayoutBinding.get().getRoot());
                  bottomCategoriesBoxLayoutBinding = new AutoClearedValue<>(this, DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_categories_box_layout, null, false));
                  mCategoryBottomSheetDialog.get().setContentView(bottomCategoriesBoxLayoutBinding.get().getRoot());
            }

            binding.get().typeButton.setOnClickListener(this::ButtonClick);

            binding.get().tuneButton.setOnClickListener(this::ButtonClick);

            binding.get().sortButton.setOnClickListener(this::ButtonClick);

            binding.get().swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.view__primary_line));
            binding.get().swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.global__primary));

            binding.get().swipeRefresh.setOnRefreshListener(() -> {
                  getListings(mShopId, mCategoryId);
            });
      }

      @Override
      protected void initViewModels() {
      }

      @Override
      protected void initAdapters() {
      }

      @Override
      protected void initData() {
            if (mShopId != null && mCategoryId != null){
                  getListings(mShopId, mCategoryId);
            }
      }

      private void getListings(String shop_id, String category_id) {

            getListingResponseObservable(category_id, shop_id)
                    .subscribe(new Observer<ListingResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                                if (getContext() != null) {
                                      binding.get().setLoadingMore(true);
                                }
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {
                                                  navigateToStore(obj);
                                            } else if (view == view.findViewById(R.id.addressTextView)) {
                                                  navigateToLocation(getActivity());
                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {
                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      Log.e(TAG, "onNext: " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Log.e(TAG, "onError: getListings  " + e.getMessage());
                                Sentry.captureException(e);
                          }

                          @Override
                          public void onComplete() {
                                if (getContext() != null) {
                                      binding.get().setLoadingMore(false);
                                      binding.get().swipeRefresh.setRefreshing(false);
                                }
                          }

                    });
      }

      private Observable<ListingResponse> getListingResponseObservable(String category_id,  String shop_id) {
            return mAPIService.getProductsByShopCategory(category_id, shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
      }

      private void tuneButtonClicked(Boolean b) {
            startActivity(new Intent(getContext(), MainSearchActivity.class));
      }

      private void ButtonClick(View view) {

            switch (view.getId()) {
                  case R.id.typeButton:

                        typeButtonClicked(typeClicked);

                        break;

                  case R.id.tuneButton:

                        tuneButtonClicked(filterClicked);

                        break;

                  case R.id.sortButton:

                        mBottomSheetDialog.get().show();
                        ButtonSheetClick();

                        break;

                  default:
                        Utils.psLog("No ID for Buttons");
            }
      }

      private void ButtonSheetClick() {

            bottomBoxLayoutBinding.get().popularButton.setOnClickListener(view -> {

                  getShopListings(mShopId, mCategoryId, "new_arrivals");
                  mBottomSheetDialog.get().dismiss();

            });

            bottomBoxLayoutBinding.get().recentButton.setOnClickListener(view -> {

                  getShopListings(mShopId, mCategoryId, "new_arrivals");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().lowestButton.setOnClickListener(view -> {
                  getShopListings(mShopId, mCategoryId, "low_to_high");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().highestButton.setOnClickListener(view -> {
                  getShopListings(mShopId, mCategoryId, "high_to_low");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().newButton.setOnClickListener(view -> {
                  getListingCondition("condition", "New");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().oldButton.setOnClickListener(view -> {
                  getListingCondition("condition", "Used");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().freeDeliveryButton.setOnClickListener(view -> {
                  getShopListings(mShopId, mCategoryId, "free_shipping");
                  mBottomSheetDialog.get().dismiss();
            });

            bottomBoxLayoutBinding.get().hasOffers.setOnClickListener(view -> {
                  getShopListings(mShopId, mCategoryId, "has_offers");
                  mBottomSheetDialog.get().dismiss();
            });
      }

      private void typeButtonClicked(Boolean b) {
            mCategoryBottomSheetDialog.get().show();
            getAllCategories(mShopId);
      }

      private void getShopListings(String shop_id, String category_id, String filter) {

            mAPIService.getShopCategoryFilter(shop_id, category_id, filter)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) { disposable = d; }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {

                                                  navigateToStore(obj);

                                            } else if (view == view.findViewById(R.id.addressTextView)) {

                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {

                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext: ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                                logError(TAG, e, "onError: getListings  ");
                          }

                          @Override
                          public void onComplete() {
                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private void getListingCondition(String filter, String condition) {
            mAPIService.getCategoryListingCondition(mShopId, mCategoryId, filter, condition)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ListingResponse>() {
                          Disposable disposable;

                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(@NonNull ListingResponse listingResponse) {

                                try {
                                      AllProductVerticalListAdapter allProductVerticalListAdapter = new AllProductVerticalListAdapter(getContext(), listingResponse.getInventories());
                                      binding.get().newsList.setAdapter(allProductVerticalListAdapter);

                                      allProductVerticalListAdapter.setOnItemClickListener((view, obj, position) -> {

                                            if (view == view.findViewById(R.id.news_title_textView)) {

                                                  navigateToStore(obj);

                                            } else if (view == view.findViewById(R.id.addressTextView)) {

                                                  navigateToLocation(getActivity());

                                            } else if (view == view.findViewById(R.id.newsHolderCardView) || view == view.findViewById(R.id.imageView)) {

                                                  navigateToItemDetailActivity(getActivity(), listingResponse.getInventories().get(position));
                                            }
                                      });

                                } catch (Exception e) {
                                      logError(TAG, e, "onNext: ");
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {

                                logError(TAG, e, "onError: getCategoryListingCondition  ");
                          }

                          @Override
                          public void onComplete() {
                                if (!disposable.isDisposed()) {
                                      disposable.dispose();
                                }
                          }
                    });
      }

      private void getAllCategories(String shop_id){

            mAPIService.getShopCategories(shop_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CategoryResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(@NonNull CategoryResponse categoryResponse) {

                                try {

                                      if (categoryResponse.getCategories().size() > 0) {
                                            CategoryAdapter productCategoryAdapter = new CategoryAdapter(getContext(), categoryResponse.getCategories());
                                            bottomCategoriesBoxLayoutBinding.get().productCategoriesRecyclerView
                                                    .setAdapter(productCategoryAdapter);

                                            productCategoryAdapter.setOnItemClickListener(new CategoryAdapter.OnItemClickListener() {
                                                  @Override
                                                  public void onItemClick(View view, ProductCategory obj, int pos) {
                                                        getListings(mShopId, String.valueOf(obj.getId()));
                                                        mCategoryBottomSheetDialog.get().dismiss();
                                                  }
                                            });
                                      }

                                }catch (Exception e){
                                      Log.e(TAG, "onNext:  " + e.getMessage() );
                                }
                          }

                          @Override
                          public void onError(@NonNull Throwable e) {
                          }

                          @Override
                          public void onComplete() {
                          }
                    });
      }

}
