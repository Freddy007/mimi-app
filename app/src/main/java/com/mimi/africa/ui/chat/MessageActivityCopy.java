package com.mimi.africa.ui.chat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.mimi.africa.R;
import com.mimi.africa.api.APIService;
import com.mimi.africa.event.SendMessageEvent;
import com.mimi.africa.model.Chat;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.CustomSharedPrefs;
import com.mimi.africa.utils.TimeAgo;
import com.mimi.africa.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MessageActivityCopy extends BaseActivity implements View.OnClickListener {

      private static final String TAG = MessageActivityCopy.class.getSimpleName();

    @Inject
    APIService mAPIService;

      @Inject
      protected SharedPreferences pref;


      ArrayList<Chat> rowListItem;
    ChatAdapter rcAdapter;

    private TextView storeNameTextView;
    private RecyclerView rView;
    private  EditText mMessageEditText;
    private TextView mTimeView;
    private TextView mLastTime;
    private NestedScrollView nestedScrollView;
    private ImageView addAttachmentImageView;
    private Activity mActivity;
    private ImageView demoImageView;

    private int mShopId;
      private String customer_id ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

//        storeNameTextView = findViewById(R.id.store_name);
        mMessageEditText = findViewById(R.id.sendMessage);
        mTimeView = findViewById(R.id.time);
        nestedScrollView = findViewById(R.id.scroll);
        mLastTime = findViewById(R.id.last_time);
        addAttachmentImageView = findViewById(R.id.add_attachment);
        demoImageView = findViewById(R.id.demo_image_view);
          customer_id = pref.getString(Constant.USER_ID, Constant.EMPTY_STRING);
        mActivity = this;

        String name = "";;

        if (getIntent() != null){
            name = getIntent().getStringExtra(Constants.SHOP_NAME);
            mShopId = getIntent().getIntExtra(Constants.SHOP_ID,0);

            storeNameTextView.setText(name);
        }

        if (CustomSharedPrefs.getLastChatTime(this) != 0){
              String time = TimeAgo.getTimeAgo(CustomSharedPrefs.getLastChatTime(this));
              mTimeView.setText(time);

              mLastTime.setText(Utils.getDateString(CustomSharedPrefs.getLastChatTime(this), "dd/MM/yyyy hh:mm:ss"));
        }

          mMessageEditText.setOnKeyListener((v, keyCode, event) -> {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                      sendMessage();

                      return true;
                }
                return false;
          });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle("");
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rView = findViewById(R.id.recyclerView);
        rView.setHasFixedSize(false);
        rView.setLayoutManager(layoutManager);

        rView.setNestedScrollingEnabled(false);

        getMessages();

          mMessageEditText.setOnClickListener(view -> new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                      layoutManager.scrollToPosition(rView.getAdapter().getItemCount() -1);
                }
          },3000));

          addAttachmentImageView.setOnClickListener(v -> {
                Toast.makeText(this, "Attachment locationImage!", Toast.LENGTH_SHORT).show();
                requestPermission();
          });

    }

      private void sendMessage() {
          if (!mMessageEditText.getText().toString().trim().isEmpty()) {
                sendMessage(mMessageEditText.getText().toString());
                EventBus.getDefault().post(new SendMessageEvent());

                mMessageEditText.setText("");
                hideKeyboard();
          }
      }

      @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_settings:
//                Toast.makeText(this, "action setting clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
          if (view.getId() == R.id.buttonSend) {
                sendMessage();
          }
    }

    private void getMessages() {

          final List<Chat>[] chats = new List[]{new ArrayList<>()};

        mAPIService.getMessages(String.valueOf(mShopId), customer_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Chat>>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull List<Chat> chatList) {
                        rcAdapter = new ChatAdapter(MessageActivityCopy.this, chatList);
                        rView.setAdapter(rcAdapter);
                        chats[0] = chatList;

                        rcAdapter.setOnClickListener(new ChatAdapter.OnClickListener() {
                              @Override
                              public void onItemClick(View view, Chat obj, int pos) {
                                    Toast.makeText(mActivity, "Send Text ", Toast.LENGTH_SHORT).show();
                              }

                              @Override
                              public void onItemLongClick(View view, Chat obj, int pos) {
                                    Toast.makeText(mActivity, "Long Send Text ", Toast.LENGTH_SHORT).show();

                              }
                        });
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {}

                    @Override
                    public void onComplete() {
                          rcAdapter.notifyDataSetChanged();
                          nestedScrollView.post(() -> {
                                nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                          });
                    }
                });
    }

    @Subscribe( threadMode = ThreadMode.MAIN)
    public void onMessageSent(SendMessageEvent event){

          new Handler().postDelayed(() -> {
                getMessages();
                rcAdapter.notifyDataSetChanged();
                nestedScrollView.post(() -> {
                      nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                });
          },2000);

    }

    private void sendMessage(String message){

        String subject = "Message";
        String token = CustomSharedPrefs.getPushyToken(getApplicationContext());

        mAPIService.sendMessage(String.valueOf(mShopId),customer_id,subject,message,token )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JsonElement>() {
                      Disposable disposable;
                      @Override
                    public void onSubscribe(Disposable d) {
                            disposable = d;
                    }

                    @Override
                    public void onNext(JsonElement jsonElement) {}

                    @Override
                    public void onError(@NonNull Throwable e) {}

                    @Override
                    public void onComplete() {
                          disposeAPIService(disposable);
                    }
                });
    }

      private void disposeAPIService(Disposable disposable) {
            if (!disposable.isDisposed()) {
                  disposable.dispose();
            }
      }

      private void hideKeyboard() {
            View view = this.getCurrentFocus();
            if (view != null) {
                  InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                  imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
      }

      private void requestPermission(){
            ActivityCompat.requestPermissions(MessageActivityCopy.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
      }

      private void selectImage(Context context) {
            final CharSequence[] options = {  "Choose from Gallery","Cancel" };
//            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

            try {

                  AlertDialog.Builder builder = new AlertDialog.Builder(context);
                  builder.setTitle("Choose your profile picture");

                  builder.setItems(options, (dialog, item) -> {

                        if (options[item].equals("Take Photo")) {
                              Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                              startActivityForResult(takePicture, 0);

                        } else if (options[item].equals("Choose from Gallery")) {
                              Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                              startActivityForResult(pickPhoto , 1);

                        } else if (options[item].equals("Cancel")) {
                              dialog.dismiss();
                        }
                  });
                  builder.show();

            }catch (Exception e){
                  Log.e(TAG, "selectImage:  " + e.getMessage() );
                  Log.e(TAG, "selectImage Cause  :  " + e.getCause() );
            }
      }

      @Override
      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode != RESULT_CANCELED) {
                  switch (requestCode) {
                        case 0:
                              if (resultCode == RESULT_OK && data != null) {
                                    Bitmap selectedImage = (Bitmap) data.getExtras().get("data");

                                    if (selectedImage != null) {
                                          Log.i(TAG, "onActivityResult:  file " + selectedImage.getWidth());
                                          Log.i(TAG, "onActivityResult:  file " + selectedImage);
                                          Log.i(TAG, "onActivityResult:  file  " + selectedImage.toString());
                                    }
                              }

                              break;
                        case 1:
                              if (resultCode == RESULT_OK && data != null) {
                                    Uri selectedImage = data.getData();
                                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                                    if (selectedImage != null) {
                                          Cursor cursor = getContentResolver().query(selectedImage,
                                                  filePathColumn, null, null, null);
                                          if (cursor != null) {
                                                cursor.moveToFirst();

                                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                                String picturePath = cursor.getString(columnIndex);
                                                Log.i(TAG, "onActivityResult:  file decoded " + (picturePath));
//                                                Log.i(TAG, "onActivityResult:  file decoded " + BitmapFactory.decodeFile(picturePath));
//                                                demoImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//                                                demoImageView.setImageURI((Uri.parse(picturePath)));
                                                sendAttachment(picturePath);
                                                cursor.close();
                                          }
                                    }
                              }
                              break;
                  }
            }
      }

      @Override
      public void onRequestPermissionsResult(int requestCode,
                                             String permissions[], int[] grantResults) {
            switch (requestCode) {
                  case 1: {

                        // If request is cancelled, the result arrays are empty.
                        if (grantResults.length > 0
                                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                              selectImage(mActivity);

                              // permission was granted, yay! Do the
                              // contacts-related task you need to do.
                        } else {

                              // permission denied, boo! Disable the
                              // functionality that depends on this permission.
                              Toast.makeText(MessageActivityCopy.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                        }
                        return;
                  }

                  // other 'case' lines to check for other
                  // permissions this app might request
            }
      }

      private void sendAttachment(String filePath){

          try {

                //Create a file object using file path
                File file = new File(filePath);
                // Create a request body with file and locationImage media type
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("locationImage/*"), file);
                // Create MultipartBody.Part using file request-body,file name and part name
                MultipartBody.Part part = MultipartBody.Part.createFormData("attachments", file.getName(), fileReqBody);
                //Create request body with text description and text media type
                RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "locationImage-type");

                RequestBody customerid = RequestBody.create(MediaType.parse("text/plain"), customer_id);

                RequestBody subject = RequestBody.create(MediaType.parse("text/plain"), "Message");
                RequestBody token = RequestBody.create(MediaType.parse("text/plain"), CustomSharedPrefs.getPushyToken(getApplicationContext()));
                RequestBody shop_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mShopId));
                RequestBody message = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mShopId));

                sendAttachmentAPI(part, description, customerid, shop_id, token, message);

                new Handler().postDelayed(this::getMessages,3000);

          }catch (Exception e){
                Utils.ShowSnackBar(MessageActivityCopy.this, "Failed to send attachment!", Snackbar.LENGTH_LONG);
          }

      }

      private void sendAttachmentAPI(MultipartBody.Part file, RequestBody requestBody,  RequestBody customer_id, RequestBody shop_id, RequestBody token,  RequestBody message){

            mAPIService.sendAttachment(file, requestBody, customer_id, shop_id, token )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          Disposable disposable;
                          @Override
                          public void onSubscribe(Disposable d) {
                                disposable = d;
                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {}

                          @Override
                          public void onError(@NonNull Throwable e) {
                                Toast.makeText(mActivity, "failed sending attachment!", Toast.LENGTH_SHORT).show();
                          }

                          @Override
                          public void onComplete() {
                                Toast.makeText(mActivity, "Successful attachment!", Toast.LENGTH_SHORT).show();
                                disposeAPIService(disposable);
                          }
                    });
      }

}
