package com.mimi.africa.ui.location;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityLocationBinding;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Config;
import com.mimi.africa.utils.Constant;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.MyContextWrapper;
import com.mimi.africa.utils.Tools;

public class LocationActivity extends BaseActivity {


    private Shop mShop;

    //region Override Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityLocationBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_location);

        if (getIntent() != null ){
            mShop = getIntent().getParcelableExtra(Constants.SHOP_OBECT);
        }

        initToolbar("");
        // Init all UI
        initUI(binding);

    }

    @Override
    protected void attachBaseContext(@NonNull Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String CURRENT_LANG_CODE = preferences.getString(Constant.LANGUAGE_CODE, Config.DEFAULT_LANGUAGE);
        String CURRENT_LANG_COUNTRY_CODE = preferences.getString(Constant.LANGUAGE_COUNTRY_CODE, Config.DEFAULT_LANGUAGE_COUNTRY_CODE);

        super.attachBaseContext(MyContextWrapper.wrap(newBase, CURRENT_LANG_CODE, CURRENT_LANG_COUNTRY_CODE, true));
    }

    //endregion


    //region Private Methods

    private void initUI(ActivityLocationBinding binding) {
        if (mShop != null )
        binding.name.setText(mShop.getName());
    }

    private void initToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inventory detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}