package com.mimi.africa.ui.gallery;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivityGalleryBinding;
import com.mimi.africa.model.Shop;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.utils.Constants;

public class GalleryActivity extends BaseActivity {
      private ActivityGalleryBinding activityGalleryBinding;
      private Shop mShop;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_gallery);

           activityGalleryBinding =  DataBindingUtil.setContentView(this, R.layout.activity_gallery);


           if (getIntent() != null ){

                 mShop = getIntent().getParcelableExtra(Constants.SHOP_OBECT);

           }

            initComponents();

      }

      private void initComponents(){

            // Toolbar
            initToolbar(activityGalleryBinding.toolbar, getResources().getString(R.string.gallery__title));

//            Toast.makeText(this, "On new instance " + String.valueOf(id), Toast.LENGTH_SHORT).show();
            GalleryFragment galleryFragment = GalleryFragment.newInstance(mShop);
            setupFragment(galleryFragment);
      }
}
