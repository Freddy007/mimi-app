package com.mimi.africa.ui.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.DialogFragment;

import com.mimi.africa.R;

public class SettingsDialogFragment extends DialogFragment {

            View view;
            AppCompatImageButton closebtn;
            SettingsDialogFragmentListener settingsDialogFragmentListener;


            @Nullable
            @Override
            public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

                  view = inflater.inflate(R.layout.fragment_settings_dialog, container, false);
                  closebtn = view.findViewById(R.id.bt_close);

                  closebtn.setOnClickListener(v -> {
                        if (settingsDialogFragmentListener != null ){
                              dismiss();
                        }
                  });

                  return view;
            }

            @NonNull
            @Override
            public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
                  Dialog dialog = super.onCreateDialog(savedInstanceState);
                  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                  return dialog;
            }

            @Override
            public void onCreate(@Nullable Bundle savedInstanceState) {
                  super.onCreate(savedInstanceState);

                  getChildFragmentManager()
                          .beginTransaction()
                          .replace(R.id.settings_frame, new AppSettingsFragment())
                          .commit();
            }


            @Override
            public void onAttach(@NonNull Context context) {
                  super.onAttach(context);
                  settingsDialogFragmentListener = (SettingsDialogFragmentListener) context;
            }

            public interface SettingsDialogFragmentListener {
                  void onClosed();
            }
}
