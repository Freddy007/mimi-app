package com.mimi.africa.ui.mainhome;

import android.view.View;

public interface EcommerceStyle1ClickListener {
    void itemClicked(View view, int position);
}