package com.mimi.africa.ui.service;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.mimi.africa.R;
import com.mimi.africa.event.AddToCart;
import com.mimi.africa.event.ReviewsClickedEvent;
import com.mimi.africa.model.Service;
import com.mimi.africa.ui.cart.CartActivity;
import com.mimi.africa.ui.common.BaseActivity;
import com.mimi.africa.ui.reviews.ReviewsFragment;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Tools;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ServiceDetailsActivity extends BaseActivity {


      private View parent_view;
      @Nullable
      private Service mService;
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_product_details);
            parent_view = findViewById(R.id.parent_view);


            String title = "";
            if (getIntent() != null){

                  mService = getIntent().getParcelableExtra(Constants.SERVICE_OBECT);
                  if (mService != null) {
                              title = mService.getName();
                  }

            }

            initToolbar(title);
            initComponent();
      }

      private void initToolbar(String title) {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this);
      }

      private void initComponent() {
            Fragment serviceDetailsFragment = ServiceDetailsFragment.newInstance(mService);
            setupFragment(serviceDetailsFragment);
      }



      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
//            getMenuInflater().inflate(R.menu.menu_cart_setting, menu);
            return true;
      }

      @Override
      public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                  finish();
            }else if (item.getItemId() == R.id.action_cart){
                  startActivity(new Intent(getApplicationContext(), CartActivity.class));
            } else {
                  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return super.onOptionsItemSelected(item);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(AddToCart event) {
      }

      @Subscribe( threadMode = ThreadMode.MAIN)
      public void onReviewsClicked(ReviewsClickedEvent event){
            ReviewsFragment reviewsFragment = ReviewsFragment.newInstance(event.getFeedbackList());
            showDialog(reviewsFragment);
      }
}
