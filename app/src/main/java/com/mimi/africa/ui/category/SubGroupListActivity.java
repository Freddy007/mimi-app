package com.mimi.africa.ui.category;

import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.mimi.africa.R;
import com.mimi.africa.databinding.ActivitySubGroupListBinding;
import com.mimi.africa.ui.common.BaseActivity;

public class SubGroupListActivity extends BaseActivity {

      public ProgressDialog progressDialog;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            ActivitySubGroupListBinding activitySubGroupListBinding = DataBindingUtil.setContentView(this, R.layout.activity_sub_group_list);

            initUI(activitySubGroupListBinding);
      }

      private void initUI(ActivitySubGroupListBinding binding) {

            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);

            initToolbar(binding.toolbar, getString(R.string.category__list_title));
            setupFragment(new SubGroupListFragment());
      }

}