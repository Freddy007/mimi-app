package com.mimi.africa.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.mimi.africa.R;
import com.mimi.africa.api.AuthenticationAPIInterface;
import com.mimi.africa.model.Authorization;
import com.mimi.africa.ui.activities.MainHomeActivity;
import com.mimi.africa.utils.AuthenticationUtil;
import com.mimi.africa.utils.Connectivity;
import com.mimi.africa.utils.Constants;
import com.mimi.africa.utils.Session;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Inject
    protected Connectivity connectivity;

    private ProgressBar progress_bar;
    private FloatingActionButton loginFab;
    private View parent_view;
    private TextInputEditText editTextEmail;
    private TextInputEditText editTextPassword;
    private LinearLayout signupForAccount;
    private AuthenticationAPIInterface authenticationAPIInterface;
    private Session session;
    private Button btnSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authenticationAPIInterface = Constants.getRetrofit(Constants.AUTHENTICATION_URL, null).create(AuthenticationAPIInterface.class);
        session = AuthenticationUtil.getSession(this);

        initViews();

        if (session.isLoggedIn()){
            startMainActivity();
        }

        signupForAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(parent_view, "Sign up for an account", Snackbar.LENGTH_SHORT).show();
                Intent registerIntent = new Intent(getApplicationContext(), RegisterUserActivity.class);
                startActivity(registerIntent);
            }
        });

        loginFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAction();
            }
        });

//        loginFab.setOnClickListener(v -> loginAction());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void loginAction() {
        progress_bar.setVisibility(View.VISIBLE);
        loginFab.setAlpha(0f);

            String email    = editTextEmail.getText().toString().trim();
            String password = editTextPassword.getText().toString().trim();
            if (!AuthenticationUtil.isEmpty(email) && !AuthenticationUtil.isEmpty(password)) {
                if (AuthenticationUtil.aBoolean && AuthenticationUtil.aBoolean1 && AuthenticationUtil.aBoolean2 && AuthenticationUtil.emailValidity(email)) {

                    Log.i(TAG, "onResponse:  reached" );

                     loginWithEmail(email, password, getApplicationContext());

                } else {
                      dismissProgressBar();
//                    Toast.makeText(getApplicationContext(), getString(R.string.check_input_field), Toast.LENGTH_LONG).show();
                }

            } else {
                  dismissProgressBar();
//                Toast.makeText(getApplicationContext(), getString(R.string.fiil_field), Toast.LENGTH_LONG).show();
            }
    }

    private void initViews() {
        parent_view      = findViewById(android.R.id.content);
        progress_bar     = findViewById(R.id.progress_bar);
        loginFab = findViewById(R.id.signup_fab);
        editTextEmail    = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        signupForAccount = findViewById(R.id.sign_up_for_account);
        TextView textView =  findViewById(R.id.signup_text);

//        btnSignin = findViewById(R.id.btn_signin);

        SpannableString content = new SpannableString("Sign up  for account");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);

        AuthenticationUtil.checkFieldValidity(editTextEmail, editTextPassword,  null,this);

    }

    public void loginWithEmail(final String email, final String password, final Context context) {
//        if (connectivity.isConnected()) {

            Call<List<Authorization>> call = authenticationAPIInterface.loginAccount(email, password);

            Log.i(TAG, "onResponse:  call reached" );

            call.enqueue(new Callback<List<Authorization>>() {
                @Override
                public void onResponse(Call<List<Authorization>> call, Response<List<Authorization>> response) {

                    Log.i(TAG, "onResponse:  reached" );

                    switch(response.code()){
                        case 200:
                            dismissProgressBar();
                            SuccessfulLoginProcess(response, email, password);
                            break;

                        case 401:
                            dismissProgressBar();
                            Toast.makeText(LoginActivity.this, "Wrong login details!", Toast.LENGTH_LONG).show();
                            break;

                        case 404:

                        case 500:
                            dismissProgressBar();
                            Toast.makeText(LoginActivity.this, "There was problem logging in!", Toast.LENGTH_LONG).show();
                            break;
                    }

                }

                @Override
                public void onFailure(Call<List<Authorization>> call, Throwable t) {
//                    Timber.e("onResponse: Failed: %s", t.getMessage());
                    Log.e("LoginActivity","onResponse: Failed: " +t.getMessage());

                    dismissProgressBar();
                    Toast.makeText(LoginActivity.this, "There was problem logging in!", Toast.LENGTH_LONG).show();

                }
            });
//        } else {
//            Toast.makeText(context, "Please check internet connection!", Toast.LENGTH_SHORT).show();
//        }
    }

    private void SuccessfulLoginProcess(@NonNull Response<List<Authorization>> response,
                                        String email, String password) {

        AuthenticationUtil.processLogin(response, email, password, session, LoginActivity.this);
    }
    private void startMainActivity(){
        Intent mainIntent = new Intent(getApplicationContext(), MainHomeActivity.class);
        startActivity(mainIntent);
    }

    private void dismissProgressBar() {
        progress_bar.setVisibility(View.GONE);
        loginFab.setAlpha(1f);
    }

}
