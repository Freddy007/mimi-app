package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Cart;

import java.util.List;

public class CartResponse {

      @SerializedName("data")
      private List<Cart> carts;

      public List<Cart> getCarts() {
            return carts;
      }

      public void setCarts(List<Cart> carts) {
            this.carts = carts;
      }

}
