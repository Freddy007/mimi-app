package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Group;
import com.mimi.africa.model.SubGroup;

import java.util.List;

public class SubGroupResponse {

      @SerializedName("data")
      private List<SubGroup> subGroups;


      public List<SubGroup> getSubGroups() {
            return subGroups;
      }

      public void setSubGroups(List<SubGroup> subGroups) {
            this.subGroups = subGroups;
      }

}
