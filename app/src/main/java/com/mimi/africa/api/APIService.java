package com.mimi.africa.api;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.gson.JsonElement;
import com.mimi.africa.api.response.CartResponse;
import com.mimi.africa.api.response.CategoryListResponse;
import com.mimi.africa.api.response.CategoryResponse;
import com.mimi.africa.api.response.FeedbackResponse;
import com.mimi.africa.api.response.GroupResponse;
import com.mimi.africa.api.response.ImageResponse;
import com.mimi.africa.api.response.ListingResponse;
import com.mimi.africa.api.response.OrderBatchHistoryResponse;
import com.mimi.africa.api.response.OrderHistoryResponse;
import com.mimi.africa.api.response.ServiceResponse;
import com.mimi.africa.api.response.ShopResponse;
import com.mimi.africa.api.response.ShopsListingResponse;
import com.mimi.africa.api.response.ShopsResponse;
import com.mimi.africa.api.response.SubGroupResponse;
import com.mimi.africa.model.Attribute;
import com.mimi.africa.model.Chat;
import com.mimi.africa.model.Delivery;
import com.mimi.africa.model.Image;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.PaymentStatus;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Rating;
import com.mimi.africa.api.response.SliderResponse;
import com.mimi.africa.model.Shop;
import com.mimi.africa.model.User;
import com.mimi.africa.viewObject.ApiStatus;
import com.mimi.africa.viewObject.CouponDiscount;
import com.mimi.africa.viewObject.PSAppInfo;
import com.mimi.africa.viewObject.ShippingCost;
import com.mimi.africa.viewObject.ShippingCostContainer;
import com.mimi.africa.viewObject.ShippingMethod;
import com.mimi.africa.viewObject.TransactionDetail;
import com.mimi.africa.viewObject.TransactionHeaderUpload;
import com.mimi.africa.viewObject.TransactionObject;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

      @NonNull
      @GET("/api/sliders")
      Observable<SliderResponse> getSliders();

      @NonNull
      @GET("/api/slider-images")
      Observable<List<Image>> getImageSliders();

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getListings();

      @NonNull
      @GET("/api/offers?has_offers=true")
      Observable<ListingResponse> getOffers();

      @NonNull
      @GET("/api/all-shops-listings")
      Observable<ShopsListingResponse> getAllListings();

      @NonNull
      @GET("/api/products-by-shop-category/{category_id}")
      Observable<ListingResponse> getProductsByShopCategory(@Path("category_id") String category_id, @Query("shop_id") String shop_id);

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getShopCategoryFilter(@Query("shop_id") String shop_id, @Query("category_id") String category_id, @Query("filter") String filter);

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getShopsListing(@Query("filter") String filter);

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getShopsListingPage(@Query("filter") String filter,@Query("page") String page);

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getCategoryListingCondition(@Query("shop_id") String shop_id, @Query("category_id") String category_id,@Query("filter") String filter, @Query("condition") String condition);

      @NonNull
      @GET("/api/all-listings")
      Observable<ListingResponse> getListingCondition(@Query("filter") String filter, @Query("condition") String condition);

      @NonNull
      @GET("/api/single-shop-listing/{shop_id}")
      Observable<ListingResponse> getSingleShopListing(@Path("shop_id") String shop_id,@Query("filter") String filter);

      @NonNull
      @GET("/api/shop-trending-products/{shop_id}")
      Observable<ListingResponse> getShopTrendingProducts(@Path("shop_id") String shop_id);

      @NonNull
      @GET("/api/service-listings/{limit}")
      Observable<ServiceResponse> getServiceListings(@Path("limit") String limit);

      @NonNull
      @GET("/api/service-listings/{limit}?offers=true")
      Observable<ServiceResponse> getServiceOffers(@Path("limit") String limit);

      @NonNull
      @GET("/api/all-Image-listings/{image_id}")
      Observable<ImageResponse> getImageListings(@Path("image_id") String imageId);

      @NonNull
      @GET("/api/locationImage-listings/{image_id}")
      Observable<Inventory> getInventoryImages(@Path("image_id") String imageId);

      @NonNull
      @GET("/api/listing-images")
      Observable<ImageResponse> getProductImages(@Query("product_id") String product_id);

      @NonNull
      @GET("/api/shop-locationImage-listings/{image_id}")
      Observable<Inventory> getShopImageListings(@Path("image_id") String imageId);

      @NonNull
      @GET("/api/shop/{slug}")
      Observable<ShopResponse> getShop(@Path("slug") String slug);

      @NonNull
      @GET("/api/shop/{slug}/listings")
      Observable<ListingResponse> getShopProductListings(@Path("slug") String slug);

      @NonNull
      @GET("/api/all-categories")
      Observable<CategoryResponse> getAllCategories();

      @NonNull
      @GET("/api/categories")
      Observable<CategoryResponse> getCategories();

      @NonNull
      @GET("/api/categories?home=home")
      Observable<CategoryResponse> getHomeCategories();

      @NonNull
      @GET("/api/categories")
      Observable<CategoryResponse> getSubGroupCategories(@Query("sub_group_id") String sub_group_id);

      @NonNull
      @GET("/api/category/{slug}")
      Observable<ListingResponse> getCategoryListings(@Path("slug") String slug);

      @NonNull
      @GET("/api/category-products/{shop_id}")
      Observable<List<ProductCategory>> getCategoryShopListings(@Path("shop_id") String shop_id);

      @NonNull
      @GET("/api/category-products/{shop_id}")
      Observable<CategoryResponse> getShopCategoriesWithProducts(@Path("shop_id") String shop_id, @Query("limit") int limit);

      @NonNull
      @GET("/api/shop-category-products/{shop_id}")
      Observable<CategoryResponse> getShopCategoryWithProducts(@Path("shop_id") String shop_id);

      @NonNull
      @GET("/api/shop-categories/{shop_id}")
      Observable<CategoryResponse> getShopCategories(@Path("shop_id") String shop_id);

      @NonNull
      @GET("/api/all-category-products")
      Observable<List<ProductCategory>> getAllCategoryProducts();

      @NonNull
      @GET("/api/shops-by-category/{category_slug}")
      Observable<ShopsResponse> getShopsByCategory(@Path("category_slug") String category_slug);

      @NonNull
      @GET("/api/shops-by-category/{category_slug}")
      Observable<List<Shop>> getShopsByCategoryList(@Path("category_slug") String category_slug);

      @NonNull
      @GET("/api/order-history/{customer_id}")
      Observable<OrderHistoryResponse> getOrderHistory(@Path("customer_id") String customer_id);

      @NonNull
      @GET("/api/check-customer-order")
      Observable<JsonElement> getCheckCustomerOrder(@Query("customer_id") String customer_id, @Query("id") String id);

      @NonNull
      @GET("/api/order-batch-history/{customer_id}")
      Observable<OrderBatchHistoryResponse> getOrderBatchHistory(@Path("customer_id") String customer_id);

//      @NonNull
//      @GET("/mimis/public/api/sliders")
//    Call<Slider> getCategoriesCall(@Query("v") String version,
//                                   @Query("client_id") String clientID, @Query("client_secret") String clientSecret);

//      @NonNull
//      @GET("/v2/venues/{venue_id}/photos")
//      Observable<Slider> getVenuePhotos(@Path("venue_id") String venueId, @Query("v") String version, @Query("client_id") String clientID,
//                                                    @Query("client_secret") String clientSecret , @Query("group") String group, @Query("limit") int limit );


      @NonNull
      @GET("/api/carts")
      Observable<CartResponse> getCarts();

      @NonNull
      @GET("/api/shops")
      Observable<ShopsResponse> getShops();

      @NonNull
      @GET("/api/shops-by-location")
      Observable<ShopsResponse> getShopsByLocation(@Query("location") String location);

      @NonNull
      @GET("/api/shops")
      Observable<ShopsResponse> getPromoShops(@Query("type") String type);

      @GET("/api/shops")
      Observable<ShopsResponse> searchShops(@Query("term") String term);

      @NonNull
      @GET("/api/shops")
      Observable<ShopsResponse> getShopsByAddress(@Query("address") String address);

//      @Headers("X-Requested-With: XMLHttpRequest")
//      @FormUrlEncoded
//      @POST("/api/addToCart/{inventory_slug}")
//      Observable<JsonElement> addToCart(@Field("quantity") int quantity, @Path("inventory_slug" )String inventory_slug);

      @NonNull
      @Headers("X-Requested-With: XMLHttpRequest")
      @FormUrlEncoded
      @POST("/api/addToCart-mobile/{inventory_slug}")
      Observable<JsonElement> addToCart(@Field("quantity") int quantity,
                                        @Field("image_variant") String image_variant,
                                        @Field("size_variant_selected") String size_variant_selected,
                                        @Field("color_variant_selected") String color_variant_selected,
                                        @Path("inventory_slug" )String inventory_slug);

      @NonNull
      @Headers("X-Requested-With: XMLHttpRequest")
      @FormUrlEncoded
      @POST("/api/cart/removeItem")
      Observable<JsonElement> remove(@Field("cart") String cart, @Field("item") String item);

      @NonNull
      @Headers("X-Requested-With: XMLHttpRequest")
      @FormUrlEncoded
      @POST("/api/mobile-checkout/{id}")
      Observable<JsonElement> checkout( @Field("payment_method_id") String payment_method_id, @Field("customer_id") String customerId,
                                        @Field("cart_ids") String cartIds, @Path("id" )String id,
                                        @Field("delivery_charge") String delivery_charge,
                                        @Field("total_with_delivery_charge") String total_with_delivery_charge,
                                        @Field("token") String token);

      @NonNull
      @FormUrlEncoded
      @POST("/api/add-product-feedback")
      Observable<JsonElement> saveProductFeedback( @Field("type") String type, @Field("customer_id") String customer_id, @Field("type_id") String type_id,
                                                   @Field("rating") String rating, @Field("comment" ) String comment, @Field("approved") String approved);

      @NonNull
      @GET("/api/feedbacks")
      Observable<FeedbackResponse> getFeedbacks(@Query("id") String id, @Query("type") String type);

      @NonNull
      @GET("/api/average-feedbacks")
      Observable<Rating> getAverageFeedbackRating(@Query("id") String id, @Query("type") String type);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> searchProducts(@Query("term") String term);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> searchProductsFilter(@Query("term") String term, @Query("has_offers") String hasOffers);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> FilterLowToHigh(@Query("term") String term,
                                                  @Query("low_to_high") String lowToHigh);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> FilterHighToLow(@Query("term") String term,
                                                  @Query("high_to_low") String highToLow);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> FilterCondition(@Query("term") String term,
                                                  @Query("condition") String condition);

      @NonNull
      @GET("/api/search")
      Observable<ListingResponse> FilterFreeDelivery(@Query("term") String term,
                                                  @Query("free_shipping") String freeShipping);

      @NonNull
      @GET("/api/service-listings")
      Observable<ServiceResponse> searchServices(@Query("term") String term);

      @NonNull
      @GET("/api/service-listings/{limit}")
      Observable<ServiceResponse> getShopServices(@Path("limit") String limit, @Query("shop") String shop_id);

      @NonNull
      @GET( "/api/shops-by-type/{type}")
      Observable<ShopsResponse> getShopsByType(@Path("type") String type);

      @NonNull
      @GET("/api/related-products/{id}")
      Observable<List<Inventory>> getRelatedProducts(@Path("id") String id);

      @NonNull
      @GET("/api/product-attributes/{slug}")
      Observable<List<Attribute>> getProductAttributes(@Path("slug") String slug);

      @NonNull
      @GET("/api/related-services/{id}")
      Observable<ServiceResponse> getRelatedServices(@Path("id") String id);

      @NonNull
      @GET("/api/service-categories")
      Observable<List<ProductCategory>> getServiceCategories();

      @NonNull
      @GET("/api/all-shop-messages")
      Observable<List<Chat>> getMessages(@Query("shop_id") String shop_id, @Query("customer_id") String customer_id );

      @NonNull
      @FormUrlEncoded
      @POST("/api/send-message")
      Observable<JsonElement> sendMessage(@Field("shop_id") String shop_id, @Field("customer_id") String customer_id, @Field("subject") String subject,
                                          @Field("message") String message, @Field("token" ) String token);@NonNull

//      @FormUrlEncoded
      @Multipart
      @POST("/api/send-message")
      Observable<JsonElement> sendAttachment(@Part MultipartBody.Part file, @Part("name") RequestBody requestBody,
                                             @Part("customer_id") RequestBody customer_id,  @Part("shop_id") RequestBody shop_id,  @Part("token") RequestBody token );

      @NonNull
//      @FormUrlEncoded
      @POST("/v3/payments")
      Observable<PaymentStatus> sendPaymentRequest(@Header("Authorization")String token, @Body RequestBody requestBody);

      @NonNull
      @FormUrlEncoded
      @POST("/api/remove-from-wish-list")
      Observable<JsonElement> removeFromWishList(  @Field("customer_id") String customer_id,
                                           @Field("product_id") String product_id);

      @NonNull
      @FormUrlEncoded
      @POST("/api/add-to-wish-list")
      Observable<JsonElement> addToWishList(  @Field("customer_id") String customer_id, @Field("inventory_id") String inventory_id,
                                           @Field("product_id") String product_id);

      @NonNull
      @GET("/api/inventory-wishlists/{customer_id}")
      Observable<ListingResponse> getInventoryWishList(@Path("customer_id") String customer_id );

      @NonNull
      @GET("/api/product-wishlists/{customer_id}")
      Observable<ServiceResponse> getProductWishLists(@Path("customer_id") String customer_id );

      @NonNull
      @GET("/api/delivery-charge")
      Observable<Delivery> getDeliveryCharge(@Query("type") String type, @Query("shop_id") String shop_id, @Query("place_id" ) String place_id, @Query("to_coordinates") String coordinates  );

      @NonNull
      @GET("/api/category-grps")
      Observable<GroupResponse> getGroups();

      @NonNull
      @GET("/api/category-subgrps")
      Observable<SubGroupResponse> getSubGroups(@Query("group_id") String group_id);

      @NonNull
      @GET("/api/service-category-subgrps")
      Observable<SubGroupResponse> getServiceSubGroups();


      // New
      //region Check Coupon Discount
      @FormUrlEncoded
      @POST("rest/coupons/check/api_key/{API_KEY}/")
      Call<CouponDiscount> checkCouponDiscount(@Path("API_KEY") String apiKey,
                                               @Field("coupon_code") String couponCode);

      @FormUrlEncoded
      @POST("/api/login")
      LiveData<ApiResponse<User>> postUserLogin(@Field("email") String email, @Field("password") String password);

//      @FormUrlEncoded
//      @POST("/api/register")
//      Call<User> postUser(String apiKey, String checkUserId, String userName, String email, String password, String phone, String deviceToken);
//      Call<User> postUser(String apiKey, String checkUserId, String userName, String email, String password, String phone, String deviceToken);

      @NonNull
      @FormUrlEncoded
      @POST("/api/register")
      Call<User> postUser(@Field("name") String name, @Field("email") String email,
                                                @Field("password") String password, @Field("c_password") String c_password);


      @FormUrlEncoded
      @POST("/api/update")
      LiveData<ApiResponse<User>> putUser(String apiKey, String userId, String userName, String userEmail, String userPhone,
                                          String userAboutMe, String billingFirstName, String billingLastName, String billingCompany, String billingAddress1, String billingAddress2,
                                          String billingCountry, String billingState, String billingCity, String billingPostalCode, String billingEmail, String billingPhone,
                                          String shippingFirstName, String shippingLastName, String shippingCompany, String shippingAddress1, String shippingAddress2,
                                          String shippingCountry, String shippingState, String shippingCity, String shippingPostalCode, String shippingEmail, String shippingPhone, String id);

      @FormUrlEncoded
      @POST("/api/update")
      LiveData<ApiResponse<User>> updateUser( @Field("user_id") String userId,@Field("user_email") String userEmail,
                                          @Field("user_phone")String userPhone, @Field("billing_first_name") String billingFirstName,
                                          @Field("billing_last_name") String billingLastName, @Field("billing_address_1") String billingAddress1,
                                          @Field("billing_address_2")String billingAddress2, @Field("billing_city") String billingCity);

      @GET("/api/shipping-rates/1")
      LiveData<ApiResponse<List<ShippingMethod>>> getShipping();

      @FormUrlEncoded
      @POST("/customer/password/email")
      Observable<JsonElement> forgotPassword(@Field("email") String email);

      LiveData<ApiResponse<ApiStatus>> postForgotPassword(String apiKey, String email);

      LiveData<ApiResponse<ApiStatus>> postPasswordUpdate(String apiKey, String loginUserId, String password);

      LiveData<ApiResponse<User>> doUploadImage(String apiKey, RequestBody useIdRB, RequestBody fullName, MultipartBody.Part body, RequestBody platformRB);

      Call<User> postGoogleLogin(String apiKey, String googleId, String userName, String userEmail, String profilePhotoUrl, String deviceToken);

      Call<User> verifyEmail(String apiKey, String userId, String code);

      Call<ApiStatus> resentCodeAgain(String apiKey, String userEmail);

      Call<User> postPhoneLogin(String apiKey, String phoneId, String userName, String userPhone, String deviceToken);

      Call<User> postFBUser(String apiKey, String fbId, String userName, String email, String imageUrl);

      LiveData<ApiResponse<List<User>>> getUser(String apiKey, String userId);

      LiveData<ApiResponse<List<TransactionDetail>>> getTransactionOrderList(String apiKey, String transactionHeaderId, String valueOf, String offset);

      LiveData<ApiResponse<List<TransactionObject>>> getTransList(String apiKey, String checkUserId, String valueOf, String offset);

      Call<TransactionObject> uploadTransactionHeader(TransactionHeaderUpload transactionHeaderUpload, String apiKey);

      LiveData<ApiResponse<TransactionObject>> getTransactionDetail(String apiKey, String userId, String id);

      Call<ShippingCost> postShippingByCountryAndCity(String apiKey, ShippingCostContainer shippingCostContainer);

      LiveData<ApiResponse< com.mimi.africa.viewObject.Shop>> getShopById(String api_key);

      Call<ApiStatus> getPaypalToken(String apiKey);

      @NonNull
      @GET("/api/single-category-products/{category}")
      Observable<CategoryListResponse> getSingleCategoryListings(@Path("category") String category);

      @NonNull
      @GET("/api/single-category-products/{category}")
      Call<PSAppInfo> getDeletedHistory(@Path("category") String category,@Query("start") String startDate, @Query("date") String endDate);
}

