package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Inventory;

import java.util.List;

public class CategoryListResponse {

      @SerializedName("listings")
      private List<Inventory> inventories;

      public List<Inventory> getInventories() {
            return inventories;
      }

      public void setInventories(List<Inventory> inventories) {
            this.inventories = inventories;
      }
}
