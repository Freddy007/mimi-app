package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Image;

import java.util.List;

public class ImageResponse {

      @SerializedName("data")
      private List<Image> images;

      public List<Image> getImages() {
            return images;
      }

      public void setImages(List<Image> images) {
            this.images = images;
      }
}
