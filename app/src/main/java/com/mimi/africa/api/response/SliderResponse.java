package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Slider;

import java.util.List;

public class SliderResponse {

      @SerializedName("data")
      private List<Slider> slider;

      public List<Slider> getSlider() {
            return slider;
      }

      public void setSlider(List<Slider> slider) {
            this.slider = slider;
      }
}
