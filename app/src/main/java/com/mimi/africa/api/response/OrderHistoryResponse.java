package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Order;

import java.util.List;

public class OrderHistoryResponse {

      @SerializedName("data")
      private List<Order> orders;

      public List<Order> getOrders() {
            return orders;
      }

      public void setOrders(List<Order> orders) {
            this.orders = orders;
      }

}
