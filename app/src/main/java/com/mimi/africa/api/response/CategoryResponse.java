package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.ProductCategory;

import java.util.List;

public class CategoryResponse {

      @SerializedName("data")
      private List<ProductCategory> categories;

      public List<ProductCategory> getCategories() {
            return categories;
      }

      public void setCategories(List<ProductCategory> categories) {
            this.categories = categories;
      }
}
