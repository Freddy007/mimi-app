package com.mimi.africa.api;


import androidx.annotation.NonNull;

import com.mimi.africa.model.Authorization;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AuthenticationAPIInterface {

    @NonNull
    @FormUrlEncoded
    @POST("/api/login")
    Call<List<Authorization>> loginAccount(@Field("email") String email, @Field("password") String password);

    @NonNull
    @FormUrlEncoded
    @POST("/api/register")
    Call<List<Authorization>> registerAccount(@Field("name") String name, @Field("email") String email,
                                              @Field("password") String password, @Field("c_password") String c_password);

    @NonNull
    @POST("/api/details")
    Call<Authorization> accountDetails(@Header("Authorization") String token);

    @NonNull
    @POST("/api/set-first-time-login-status/{status}")
    Call<List<Authorization>> setBoardingStatus(@Path("status") int status);

    @NonNull
    @FormUrlEncoded
    @POST("/api/save-user-interests")
    Call<List<Authorization>> saveCategoryInterests(@Field("interests[]") List<String> interests);

}

