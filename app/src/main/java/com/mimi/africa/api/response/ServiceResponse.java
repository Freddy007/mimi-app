package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Service;

import java.util.List;

public class ServiceResponse {

      @SerializedName("data")
      private List<Service> services;

      public List<Service> getServices() {
            return services;
      }

      public void setServices(List<Service> services) {
            this.services = services;
      }
}
