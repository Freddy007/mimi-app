package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Shop;

import java.util.List;

public class ShopsListingResponse {

      @SerializedName("data")
      private List<Shop> shops;

      public List<Shop> getShops() {
            return shops;
      }

      public void setShops(List<Shop> shops) {
            this.shops = shops;
      }
}
