package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.OrderBatch;

import java.util.List;

public class OrderBatchHistoryResponse {

      @SerializedName("data")
      private List<OrderBatch> orders;

      public List<OrderBatch> getOrders() {
            return orders;
      }

      public void setOrders(List<OrderBatch> orders) {
            this.orders = orders;
      }
}
