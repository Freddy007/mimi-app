package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Feedback;

import java.util.List;

public class FeedbackResponse {

      @SerializedName("feedbacks")
      private List<Feedback> feedbacks;

      public List<Feedback> getFeedbacks() {
            return feedbacks;
      }

      public void setFeedbacks(List<Feedback> feedbacks) {
            this.feedbacks = feedbacks;
      }
}
