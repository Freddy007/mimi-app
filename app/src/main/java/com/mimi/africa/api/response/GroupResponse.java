package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Group;
import com.mimi.africa.model.SubGroup;

import java.util.List;

public class GroupResponse {

      @SerializedName("data")
      private List<Group> groups;

      public List<Group> getGroups() {
            return groups;
      }

      public void setGroups(List<Group> groups) {
            this.groups = groups;
      }

}
