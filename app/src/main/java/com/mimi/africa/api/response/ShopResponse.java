package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Shop;

public class ShopResponse {

      @SerializedName("data")
      private Shop shop;

      public Shop getShop() {
            return shop;
      }

      public void setShop(Shop shop) {
            this.shop = shop;
      }
}
