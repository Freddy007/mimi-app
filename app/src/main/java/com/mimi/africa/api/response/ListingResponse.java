package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.PaginateLinks;

import java.util.List;

public class ListingResponse {

      @SerializedName("data")
      private List<Inventory> inventories;

      @SerializedName("links")
      private PaginateLinks links;

      public List<Inventory> getInventories() {
            return inventories;
      }

      public void setInventories(List<Inventory> inventories) {
            this.inventories = inventories;
      }


      public PaginateLinks getLinks() {
            return links;
      }

      public void setLinks(PaginateLinks links) {
            this.links = links;
      }
}
