package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Shop;

import java.util.List;

public class ShopsResponse {

      @SerializedName("data")
      private List<Shop> shops;

      public List<Shop> getShop() {
            return shops;
      }

      public void setShop(List<Shop> shops) {
            this.shops = shops;
      }
}
