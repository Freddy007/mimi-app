package com.mimi.africa.api.response;

import com.mimi.africa.model.ProductCategory;

import java.util.List;

public class CategoryProductsResponse {

      private List<ProductCategory> categories;

      public List<ProductCategory> getCategories() {
            return categories;
      }

      public void setCategories(List<ProductCategory> categories) {
            this.categories = categories;
      }
}
