package com.mimi.africa.api.response;

import com.google.gson.annotations.SerializedName;
import com.mimi.africa.model.Inventory;

public class InventoryResponse {

      @SerializedName("data")
      private Inventory inventory;

      public Inventory getInventory() {
            return inventory;
      }

      public void setInventory(Inventory inventory) {
            this.inventory = inventory;
      }
}
