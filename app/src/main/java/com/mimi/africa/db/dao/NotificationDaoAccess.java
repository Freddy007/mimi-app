package com.mimi.africa.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.mimi.africa.model.Notification;

import java.util.List;

@Dao
public interface NotificationDaoAccess {

    @Insert
    long insert(Notification notification);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOnlySingleNotification(Notification notification);

    @Insert
    void insertMultipleNotifications(List<Notification> notifications);

    @Query("SELECT * FROM notifications WHERE id = :id")
    Notification fetchNotificationById(int id);

    @Query("SELECT * FROM notifications WHERE sender_id = :shop_id")
    Notification fetchNotificationByShopId(String shop_id);

    @Query("SELECT * FROM notifications WHERE name = :name")
    Notification fetchNotificationByName(String name);

    @Query("SELECT * FROM notifications ORDER BY RANDOM() LIMIT 1")
    Notification fetchOneRandomNotification();

    @Query("SELECT * FROM notifications ORDER BY date_created DESC")
    LiveData<List<Notification>> fetchAllNotifications();

    @Query("SELECT * FROM notifications WHERE owner = 'chats' AND archived = 0  ORDER BY date_created DESC ")
    LiveData<List<Notification>> fetchChatNotifications();

    @Query("SELECT * FROM notifications WHERE owner = 'chats' AND archived = 1  ORDER BY date_created DESC ")
    LiveData<List<Notification>> fetchArchivedChatNotifications();

    @Query("SELECT * FROM notifications WHERE owner = 'chats' AND archived = 1  ORDER BY date_created DESC ")
    List<Notification> fetchArchivedChats();

    @Query("SELECT * FROM notifications WHERE owner = 'chats'  ORDER BY date_created DESC ")
    List<Notification> fetchMessages();

    @Query("SELECT * FROM notifications WHERE owner = 'chats' AND read = 0  ORDER BY date_created DESC ")
    LiveData<List<Notification>> fetchUnreadChatNotifications();

    @Query("SELECT * FROM notifications WHERE owner = 'posts' ORDER BY date_created DESC")
    LiveData<List<Notification>> fetchNotifications();

    @Query("UPDATE  notifications SET read = 1 WHERE id = :id")
    void setRead(long id);

    @Query("UPDATE  notifications SET archived = 1 WHERE sender_id = :shop_id")
    void setArchived(String shop_id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateNotification(Notification notification);

    @Delete
    void deleteNotification(Notification notification);

    @Query("DELETE FROM notifications")
    void deleteAllNotifications();

    @Query("DELETE FROM notifications WHERE id = :id")
    int deleteNotificationById(long id);

}
