package com.mimi.africa.db.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.mimi.africa.model.BasketCount;

import java.util.List;

@Dao
public abstract class BasketCountDao {
    //region basket

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(BasketCount basketCount);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateBasketCount(BasketCount basketCount);

    @Query("SELECT * FROM basket_counts")
    public abstract LiveData<List<BasketCount>> getAllBasketCount();

    @Query("SELECT *  FROM basket_counts WHERE id = :id LIMIT 1")
    public abstract BasketCount getCount(long id);

    @Query("SELECT COUNT(*) FROM basket_counts")
    public abstract int getBasketCount();

    @Query("UPDATE basket_counts SET count =:count WHERE id = :id")
    public abstract void updateBasketCount(long id,int count);

    @Query("DELETE FROM basket_counts")
    public abstract void deleteAllBasketCount();

    //endregion
}
