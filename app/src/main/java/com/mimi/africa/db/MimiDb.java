package com.mimi.africa.db;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mimi.africa.db.dao.BasketCountDao;
import com.mimi.africa.db.dao.BasketDao;
import com.mimi.africa.db.dao.DeletedObjectDao;
import com.mimi.africa.db.dao.NotificationDaoAccess;
import com.mimi.africa.db.dao.PSAppInfoDao;
import com.mimi.africa.db.dao.PSAppVersionDao;
import com.mimi.africa.db.dao.ProductCategoryDaoAccess;
import com.mimi.africa.db.dao.ProductColorDao;
import com.mimi.africa.db.dao.ShippingMethodDao;
import com.mimi.africa.db.dao.ShopDaoAccess;
import com.mimi.africa.db.dao.TransactionDao;
import com.mimi.africa.db.dao.TransactionOrderDao;
import com.mimi.africa.db.dao.UserDao;
import com.mimi.africa.model.BasketCount;
import com.mimi.africa.model.Inventory;
import com.mimi.africa.model.Notification;
import com.mimi.africa.model.ProductCategory;
import com.mimi.africa.model.Shop;
import com.mimi.africa.model.User;
import com.mimi.africa.ui.basket.Basket;
import com.mimi.africa.viewModels.ProductColor;
import com.mimi.africa.viewObject.DeletedObject;
import com.mimi.africa.viewObject.PSAppInfo;
import com.mimi.africa.viewObject.PSAppVersion;
import com.mimi.africa.viewObject.ShippingMethod;
import com.mimi.africa.viewObject.TransactionDetail;
import com.mimi.africa.viewObject.TransactionObject;
import com.mimi.africa.viewObject.UserLogin;

import io.sentry.core.Sentry;

@Database(entities = {Shop.class, ProductCategory.class, Notification.class, Basket.class, Inventory.class,
         TransactionDetail.class, TransactionObject.class, ShippingMethod.class, User.class,  UserLogin.class,   PSAppInfo.class,
        PSAppVersion.class,
        DeletedObject.class, ProductColor.class, BasketCount.class}, version = 32, exportSchema = false)

public abstract class MimiDb extends RoomDatabase {

    private static final String TAG = MimiDb.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "mimi_db";
    private static  MimiDb sInstance;

//    public static MimiDb getInstance(Context context){
//        try {
//            if (sInstance == null) {
//                synchronized (LOCK) {
//                    Log.d(TAG, "Creating a new database instance");
//                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
//                            MimiDb.class, MimiDb.DATABASE_NAME)
////                        .allowMainThreadQueries()
//                            .fallbackToDestructiveMigration()
//                            .build();
//                }
//            }
//        }catch(Exception e){
//            Sentry.captureException(e);
//        }
////
//        Log.d(TAG, "Getting the Mimi database instance");
////
//        return sInstance;
//    }

    /**
     * Switches the internal implementation with an empty in-memory database.
     *
     * @param
     */
//    @VisibleForTesting
//    public static void switchToInMemory(Context context) {
//        sInstance = Room.inMemoryDatabaseBuilder(context.getApplicationContext(),
//                LocationDatabase.class).build();
//    }

    public abstract BasketDao basketDao();

    public abstract ShopDaoAccess shopDaoAccess();

    public abstract ProductCategoryDaoAccess productCategoryDaoAccess();

    public abstract NotificationDaoAccess notificationDaoAccess();

    public abstract TransactionOrderDao transactionOrderDao();

    public abstract TransactionDao transactionDao();

    public abstract ShippingMethodDao shippingMethodDao();

//    public abstract ShopDao shopDao();

    public abstract UserDao userDao();

    abstract public PSAppInfoDao psAppInfoDao();

    abstract public PSAppVersionDao psAppVersionDao();

    abstract public DeletedObjectDao deletedObjectDao();

    abstract public ProductColorDao productColorDao();

    abstract public BasketCountDao basketCountDao();

}
