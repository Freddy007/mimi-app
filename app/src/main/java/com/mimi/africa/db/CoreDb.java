package com.mimi.africa.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.mimi.africa.db.common.Converters;
import com.mimi.africa.model.User;


@Database(entities = {
//        Image.class,
        User.class,
//        Banner.class
}, version = 1, exportSchema = false)
// app version 1.1 = db version 1
// app version 1.0 = db version 1


@TypeConverters({Converters.class})

public abstract class CoreDb extends RoomDatabase {

//    abstract public UserDao userDao();

//    abstract public HomeBannerDao homeBannerDao();

//    /**
//     * Migrate from:
//     * version 1 - using Room
//     * to
//     * version 2 - using Room where the {@link } has an extra field: addedDateStr
//     */
//    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE news "
//                    + " ADD COLUMN addedDateStr INTEGER NOT NULL DEFAULT 0");
//        }
//    };

    /* More migration write here */
}