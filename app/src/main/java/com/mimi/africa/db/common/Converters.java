package com.mimi.africa.db.common;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.util.Date;


public class Converters {

    @Nullable
    @TypeConverter
    public static Date fromTimestamp(@Nullable Long value) {
        return value == null ? null : new Date(value);
    }

    @Nullable
    @TypeConverter
    public static Long dateToTimestamp(@Nullable Date date) {
        return date == null ? null : date.getTime();
    }
}
