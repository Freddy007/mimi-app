package com.mimi.africa.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.mimi.africa.model.ProductCategory;

import java.util.List;

@Dao
public interface ProductCategoryDaoAccess {

    @Insert
    long insert(ProductCategory productCategory);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOnlySingleProductCategory(ProductCategory productCategory);

    @Insert
    void insertMultipleProductCategories(List<ProductCategory> productCategoryList);

    @Query("SELECT * FROM product_categories WHERE id = :productCategoryId")
    ProductCategory fetchProductCategoryById(int productCategoryId);

    @Query("SELECT * FROM product_categories WHERE name = :name")
    ProductCategory  fetchProductCategoryByName(String name);

    @Query("SELECT * FROM product_categories ORDER BY RANDOM() LIMIT 1")
    ProductCategory fetchRandomCategory();


    @Query("SELECT * FROM product_categories ORDER BY RANDOM()")
    LiveData<List<ProductCategory>> fetchAll();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateProductCategory(ProductCategory productCategory);

    @Delete
    void deleteProductCategory(ProductCategory productCategory);

    @Query("DELETE FROM product_categories")
    void deleteAllProductCategories();

    @Query("DELETE FROM product_categories WHERE id = :productCategoryId")
    int deleteProductCategoryById(long productCategoryId);

}
