package com.mimi.africa.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.mimi.africa.model.Shop;

import java.util.List;

@Dao
public interface ShopDaoAccess {

    @Insert
    long insert(Shop shop);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOnlySingleShop(Shop shop);

    @Insert
    void insertMultipleShops (List<Shop> shopList);

    @Query("SELECT * FROM shops WHERE id = :shopId")
    Shop fetchShopById(int shopId);

    @Query("SELECT * FROM shops WHERE name = :name")
    Shop fetchShopByName (String name);

    @Query("SELECT * FROM shops ORDER BY RANDOM() LIMIT 1")
    Shop fetchOneRandomShop();

    @Query("SELECT * FROM shops ORDER BY RANDOM()")
    LiveData<List<Shop>> fetchAllShops();

//    @Query("SELECT * FROM shops WHERE favourite = 1 " )
//    LiveData<List<Location>> fetchFavouriteShops();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateShop(Shop shop);

    @Delete
    void deleteShop(Shop shop);

    @Query("DELETE FROM shops")
    void deleteAllShops();

    @Query("DELETE FROM shops WHERE id = :shopId")
    int deleteShopById(long shopId);

}
